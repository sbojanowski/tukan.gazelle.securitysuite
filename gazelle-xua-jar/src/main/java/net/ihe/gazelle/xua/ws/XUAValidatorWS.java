package net.ihe.gazelle.xua.ws;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.xua.util.DetailedResultTransformer;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS",
        serviceName = "ModelBasedValidationWSService",
        portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class XUAValidatorWS extends AbstractModelBasedValidation implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate CDA documents using model based validation.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    private String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                DetailedResultTransformer.save(baos, dr);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            String res = baos.toString();
            res = deleteUnicodeZero(res);
            return res;
        }
        return null;
    }

    private static String deleteUnicodeZero(String s) {
        return s.replaceAll("[\\u0000]", "");
    }


    private List<String> getListOfValidators() throws SOAPException {
        List<String> res = new ArrayList<String>();
        Validators[] dd = Validators.values();
        for (Validators validators : dd) {
            res.add(validators.value);
        }
        return res;
    }

    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateDocument(
            @WebParam(name = "document") String document,
            @WebParam(name = "validator") String validator)
            throws SOAPException {
        Lifecycle.beginCall();
        Validators val = Validators.getValidatorFromValue(validator);
        if (val == null) {
            throw new SOAPException("The validator shall be from this list : " + this.getListOfValidators());
        }
        if ((null == document) || (document.isEmpty())){
            throw new SOAPException("A document must be provided");
        }
        DetailedResult dr = null;
        switch (val) {
            case IHE_XUA_BASIC:
                dr = XUADocumentValidation.validateIHE_BASIC_XUA(document);
                break;
            case IHE_XUA_SUBJECT_ROLE:
                dr = XUADocumentValidation.validateIHE_XUA_SUBJECT_ROLE(document);
                break;
            case IHE_XUA_Authz_CONSENT:
                dr = XUADocumentValidation.validateIHE_XUA_Authz_CONSENT(document);
                break;
            case IHE_XUA_PURPOSE_OF_USE:
                dr = XUADocumentValidation.validateIHE_XUA_PURPOSE_OF_USE(document);
                break;
            case KSA_XUA:
                dr = XUADocumentValidation.validateKSA_XUA(document);
                break;
            case SER_ITI79_REQUEST:
                dr = XUADocumentValidation.validateSER_ITI79_REQUEST(document);
                break;
            case SER_ITI79_RESPONSE:
                dr = XUADocumentValidation.validateSER_ITI79_RESPONSE(document);
                break;
            case CH_XUA:
                dr = XUADocumentValidation.validateCH_XUA(document);
                break;
            default:
                throw new SOAPException("The validator shall be from this list : " + this.getListOfValidators());
        }
        String res = this.getDetailedResultAsString(dr);
        if (res.contains("?>")) {
            res = res.substring(res.indexOf("?>") + 2);
        }
        Lifecycle.endCall();
        return res;
    }

    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
            throws SOAPException {
        List<String> res = new ArrayList<String>();
        if (null == descriminator){
            descriminator = "";
        }
        for (Validators val : Validators.values()) {
            if (val.getValue().toLowerCase().contains(descriminator.toLowerCase())) {
                res.add(val.getValue());
            }
        }
        return res;
    }

    @Override
    protected String executeValidation(String document, ValidatorDescription validator) throws Exception {
        //TODO half-stub to implement !
        return validateDocument(document, validator.getName()) ;
    }

    @Override
    protected ValidatorDescription getValidatorByOidOrName(String value) {
        //TODO stub to implement
        return null;
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator)  {
        //TODO half-stub to implement !
        try {
            List<String> ValidatorNames = getListOfValidators(descriminator);
            List<ValidatorDescription> validatorDescs = new ArrayList<ValidatorDescription>();
            for (final String name : ValidatorNames) {
                validatorDescs.add(new ValidatorDescription() {
                    @Override
                    public String getOid() {
                        return null;
                    }

                    @Override
                    public String getName() {
                        return name;
                    }

                    @Override
                    public String getDescriminator() {
                        return null;
                    }
                });
            }
            return validatorDescs ;
        } catch (SOAPException e) {
            return null ;
        }
    }

}


