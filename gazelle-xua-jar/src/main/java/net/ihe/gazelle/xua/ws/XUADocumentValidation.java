package net.ihe.gazelle.xua.ws;

import net.ihe.gazelle.adapters.DoubleAdapter;
import net.ihe.gazelle.chxua.validator.chxua.CHXUAPackValidator;
import net.ihe.gazelle.datatypes.datatypes.DATATYPESPackValidator;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.ksaml.ksaml.KSAMLPackValidator;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.saml.assertion.AssertionType;
import net.ihe.gazelle.samlihe.samlihe.SAMLIHEPackValidator;
import net.ihe.gazelle.samlihe.samliheauthz.SAMLIHEAUTHZPackValidator;
import net.ihe.gazelle.samlihe.samliheoptionspec.SAMLIHEOPTIONSPECPackValidator;
import net.ihe.gazelle.samlihe.samlihepurp.SAMLIHEPURPPackValidator;
import net.ihe.gazelle.samlihe.samlihesubrole.SAMLIHESUBROLEPackValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xua.util.POMVersion;
import net.ihe.gazelle.xua.util.XMLValidation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class XUADocumentValidation {

    enum ValidatorType {XUA, SER_REQ, SER_RESP}

    private static Logger log = LoggerFactory.getLogger(XUADocumentValidation.class);

    static {
        log.info("Initiating the CDA validator of the application");
        try {
            log.info("11svs = " + PreferenceService.getString("svs_simulator_restful_url"));
            CommonOperations.setValueSetProvider(new SVSConsumer() {
                @Override
                protected String getSVSRepositoryUrl() {
                    log.info("svs = " + PreferenceService.getString("svs_simulator_restful_url"));
                    return PreferenceService.getString("svs_simulator_restful_url");
                }
            });
        } catch (Exception e) {
            log.error("Fail to initialize SVS repository URL",e);
        }
    }

    private XUADocumentValidation() {
    }

    public static DetailedResult validateWithBasicValidator(String req, Validators val, List<ConstraintValidatorModule> listConstraintValidatorModule,
                                                            ValidatorType valtype) {
        DetailedResult res = new DetailedResult();
        AssertionType ass = null;
        List<Notification> ln = new ArrayList<Notification>();
        if (valtype != null && valtype == ValidatorType.XUA) {
            try {
                ass = loadAssertionType(new ByteArrayInputStream(req.getBytes("UTF8")), valtype);
                if (ass == null) {
                    throw new Exception();
                }
                for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
                    AssertionType.validateByModule(ass, "/Assertion", constraintValidatorModule, ln);
                }
            } catch (ValidatorException e) {
                errorWhenExtracting(val, req, e, ln);
            } catch (Exception e) {
                errorWhenExtracting(val, req, ln);
            }
        }
        validateToSchema(res, req, valtype);
        return updateValidationResult(req, val, res, ln);
    }

    private static DetailedResult updateValidationResult(String req, Validators val, DetailedResult res, List<Notification> ln) {
        if (res == null) {
            return null;
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
        updateMDAValidation(res.getMDAValidation(), ln);
        summarizeDetailedResult(res, val);

        return res;
    }


    public static DetailedResult validateIHE_BASIC_XUA(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SAMLIHEOPTIONSPECPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.IHE_XUA_BASIC, listConstraintValidatorModule, ValidatorType.XUA);
    }

    public static DetailedResult validateIHE_XUA_SUBJECT_ROLE(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SAMLIHESUBROLEPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEOPTIONSPECPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.IHE_XUA_SUBJECT_ROLE, listConstraintValidatorModule, ValidatorType.XUA);
    }

    public static DetailedResult validateIHE_XUA_Authz_CONSENT(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SAMLIHEAUTHZPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEOPTIONSPECPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.IHE_XUA_Authz_CONSENT, listConstraintValidatorModule, ValidatorType.XUA);
    }

    public static DetailedResult validateIHE_XUA_PURPOSE_OF_USE(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SAMLIHEPURPPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEOPTIONSPECPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.IHE_XUA_PURPOSE_OF_USE, listConstraintValidatorModule, ValidatorType.XUA);
    }

    public static DetailedResult validateKSA_XUA(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new KSAMLPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPURPPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEOPTIONSPECPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.KSA_XUA, listConstraintValidatorModule, ValidatorType.XUA);
    }

    public static DetailedResult validateSER_ITI79_REQUEST(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.SER_ITI79_REQUEST, listConstraintValidatorModule, ValidatorType.SER_REQ);
    }

    public static DetailedResult validateSER_ITI79_RESPONSE(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.SER_ITI79_RESPONSE, listConstraintValidatorModule, ValidatorType.SER_RESP);
    }

    public static DetailedResult validateCH_XUA(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new CHXUAPackValidator());
        listConstraintValidatorModule.add(new SAMLIHEPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return XUADocumentValidation.validateWithBasicValidator(req, Validators.CH_XUA, listConstraintValidatorModule, ValidatorType.XUA);
    }

    private static void updateMDAValidation(MDAValidation mda, List<Notification> ln) {
        mda.setResult("PASSED");
        for (Notification notification : ln) {
            if (notification instanceof net.ihe.gazelle.validation.Error) {
                mda.setResult("FAILED");
            }
        }
    }

    static void errorWhenExtracting(Validators val, String string, ValidatorException vexp, List<Notification> ln) {
        if (ln == null) return;
        if (vexp != null) {
            if (vexp.getDiagnostic() != null) {
                for (Notification notification : vexp.getDiagnostic()) {
                    ln.add(notification);
                }
            }
        }
    }


    static void errorWhenExtracting(Validators val, String string, List<Notification> ln) {
        if (ln == null) return;
        Error err = new Error();
        err.setTest("structure");
        err.setLocation("All the document");
        err.setDescription("The tool is not able to find urn:hl7-org:v3:ClinicalDocument element as the root of the validated document.");
        ln.add(err);
    }

    static void summarizeDetailedResult(DetailedResult dr, Validators val) {
        if (dr != null) {
            Date dd = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
            DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
            dr.setValidationResultsOverview(new ValidationResultsOverview());
            dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
            dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
            dr.getValidationResultsOverview().setValidationServiceName("Gazelle CDA Validation : " + val.value);
            dr.getValidationResultsOverview().setValidationTestResult("PASSED");
            if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null) && (dr.getDocumentValidXSD().getResult().equals("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() != null) && (dr.getDocumentWellFormed().getResult().equals("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null) && (dr.getMDAValidation().getResult().equals("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            dr.getValidationResultsOverview().setValidationServiceVersion(POMVersion.getVersion());
        }
    }

    private static void validateToSchema(DetailedResult res, String sub, ValidatorType val) {
        DocumentWellFormed dd = XMLValidation.isXMLWellFormed(sub);
        res.setDocumentWellFormed(dd);
        try {
            switch (val) {
                case XUA:
                    res.setDocumentValidXSD(XMLValidation.isXSDValidXUA(sub));
                    break;
                case SER_REQ:
                    res.setDocumentValidXSD(XMLValidation.isXSDValidSERReq(sub));
                    break;
                case SER_RESP:
                    res.setDocumentValidXSD(XMLValidation.isXSDValidSERResp(sub));
                    break;
            }
        } catch (Exception e) {
            log.info("error when validating with the schema", e);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            e.printStackTrace(ps);
            DocumentValidXSD docv = new DocumentValidXSD();
            docv.setResult("FAILED");
            docv.getXSDMessage().add(new XSDMessage());
            docv.getXSDMessage().get(0).setSeverity("error");
            docv.getXSDMessage().get(0).setMessage("error when validating with the schema. The error is : " + StringUtils.substring(baos.toString(), 0, 300));
            res.setDocumentValidXSD(docv);
        }
    }

    private static AssertionType loadAssertionType(InputStream is, ValidatorType valtype)
            throws ValidatorException {
        if (valtype == null || valtype != ValidatorType.XUA) {
            return null;
        }
        final ValidatorException vexp = new ValidatorException("Errors when trying to parse the document", new ArrayList<Notification>());
        try {
            JAXBContext jc = JAXBContext.newInstance(AssertionType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(new ValidationEventHandler() {

                @Override
                public boolean handleEvent(ValidationEvent event) {
                    return XUADocumentValidation.handleEvent(event, vexp);
                }
            });
            u.setAdapter(new DoubleAdapter());
            AssertionType mimi = (AssertionType) u.unmarshal(is);
            return mimi;
        } catch (JAXBException e) {
        	Notification not = new Error();
            not.setDescription("JAXBException : " + (e.getMessage()!=null?","+e.getMessage():"") +
            		(e.getLinkedException().getMessage()!=null?","+e.getLinkedException().getMessage():""));
            not.setTest("message_parsing");
            not.setLocation("All the document");
            vexp.getDiagnostic().add(not);
            throw vexp;
        }
    }

    protected static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
        if (event.getSeverity() != ValidationEvent.WARNING) {
            ValidationEventLocator vel = event.getLocator();
            Notification not = new Error();
            not.setDescription("Line:Col[" + vel.getLineNumber() +
                    ":" + vel.getColumnNumber() +
                    "]:" + event.getMessage());
            not.setTest("message_parsing");
            not.setLocation("All the document");
            vexp.getDiagnostic().add(not);
        }
        return true;
    }

}
