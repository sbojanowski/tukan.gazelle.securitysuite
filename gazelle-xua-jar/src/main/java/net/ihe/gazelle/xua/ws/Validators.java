package net.ihe.gazelle.xua.ws;

public enum Validators {

    IHE_XUA_BASIC("IHE - ITI - XUA"),
    IHE_XUA_SUBJECT_ROLE("IHE - ITI - XUA - Subject-Role Option"),
    IHE_XUA_Authz_CONSENT("IHE - ITI - XUA - Authz-Consent Option"),
    IHE_XUA_PURPOSE_OF_USE("IHE - ITI - XUA - PurposeOfUse Option"),
    KSA_XUA("KSA - XUA"),
    SER_ITI79_REQUEST("IHE - ITI - SeR - ITI-79 - Authorization Decisions Query Request"),
    SER_ITI79_RESPONSE("IHE - ITI - SeR - ITI-79 - Authorization Decisions Query Response"),
    CH_XUA("CH - IHE - ITI - XUA");

    Validators(String val) {
        this.value = val;
    }

    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Validators getValidatorFromValue(String value) {
        for (Validators val : Validators.values()) {
            if (val.value.equals(value)) return val;
        }
        return null;
    }

}
