package net.ihe.gazelle.xua.util;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class XMLValidation {

    private static final String UNEXPECTED_ERROR = "Unexpected error";
    private static final String SEVERITY_ERROR = "error";
    private static final String SEVERITY_WARNING = "warning";
    private static final String STATUS_PASSED = "PASSED";
    private static final String STATUS_FAILED = "FAILED";

    private XMLValidation() {
    }

    private static Logger log = LoggerFactory.getLogger(XMLValidation.class);

    private static SAXParserFactory factoryBASIC;

    private static SAXParserFactory factoryXUA;

    private static SAXParserFactory factoryXACMLProtocol;

    private static SAXParserFactory factoryXACMLAssertion;

    static {
        try {
            factoryBASIC = SAXParserFactory.newInstance();
            factoryBASIC.setValidating(false);
            factoryBASIC.setNamespaceAware(true);
        } catch (Exception e) {
        }

        try {
            factoryXUA = SAXParserFactory.newInstance();
            factoryXUA.setNamespaceAware(true);
            SchemaFactory sfactory =
                    SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = sfactory.newSchema(new URL(PreferenceService.getString("xua_xsd")));
            factoryXUA.setSchema(schema);
        } catch (Exception e) {
            log.error("Fail to build XUA SAX Parser factory", e);
        }

        try {
            factoryXACMLProtocol = SAXParserFactory.newInstance();
            factoryXACMLProtocol.setNamespaceAware(true);
            SchemaFactory sfactory =
                    SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = sfactory.newSchema(new URL(PreferenceService.getString("ser_req_xsd")));
            factoryXACMLProtocol.setSchema(schema);
        } catch (Exception e) {
            log.error("Fail to build XACML Protocol SAX Parser factory", e);
        }

        try {
            factoryXACMLAssertion = SAXParserFactory.newInstance();
            factoryXACMLAssertion.setNamespaceAware(true);
            SchemaFactory sfactory =
                    SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = sfactory.newSchema(new URL(PreferenceService.getString("ser_resp_xsd")));
            factoryXACMLAssertion.setSchema(schema);
        } catch (Exception e) {
            log.error("Fail to build XACML Assertion SAX Parser factory", e);
        }

    }

    /**
     * Parses the file using SAX to check that it is a well-formed XML file
     *
     * @param string document to verify
     * @return a {@link DocumentWellFormed} or null
     */
    public static DocumentWellFormed isXMLWellFormed(String string) {
        return validateIfDocumentWellFormedXML(string, "", factoryBASIC);
    }


    /**
     * Checks that the XDW document is valid (uses XDW.xsd file)
     *
     * @param document document to verify
     * @return a {@link DocumentValidXSD} or null
     */
    public static DocumentValidXSD isXSDValidXUA(String document) {
        if (document == null) {
            return null;
        } else {
            return validXMLUsingXSDXUA(document);
        }
    }

    public static DocumentValidXSD isXSDValidSERReq(String document) {
        if (document == null) {
            return null;
        } else {
            return validXMLUsingXSD(document, factoryXACMLProtocol);
        }
    }

    public static DocumentValidXSD isXSDValidSERResp(String document) {
        if (document == null) {
            return null;
        } else {
            return validXMLUsingXSD(document, factoryXACMLAssertion);
        }
    }

    private static DocumentWellFormed validateIfDocumentWellFormedXML(String cdaDocument, String xsdpath, SAXParserFactory factory) {
        DocumentWellFormed dv = new DocumentWellFormed();
        return validXMLUsingXSD(cdaDocument, factory, dv);
    }


    /**
     * Validate a document against the XUA XSD schema
     *
     * @param document document to verify
     * @return a {@link DocumentValidXSD} or null
     */
    private static DocumentValidXSD validXMLUsingXSDXUA(String document) {
        return validXMLUsingXSD(document, factoryXUA);
    }

    private static DocumentValidXSD validXMLUsingXSD(String cdaDocument, SAXParserFactory factory) {
        DocumentValidXSD dv = new DocumentValidXSD();
        return validXMLUsingXSD(cdaDocument, factory, dv);
    }

    private static <T extends DocumentValidXSD> T validXMLUsingXSD(String cdaDocument, SAXParserFactory factory, T dv) {
        List<ValidationException> exceptions = new ArrayList<ValidationException>();
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(cdaDocument.getBytes("UTF8"));
            exceptions = XSDValidator.validateUsingFactoryAndSchema(bais, null, factory);
        } catch (Exception e) {
            exceptions.add(handleException(e));
        }
        return extractValidationResult(exceptions, dv);
    }

    private static ValidationException handleException(Exception e) {

        ValidationException validationException = new ValidationException();
        validationException.setLineNumber("0");
        validationException.setColumnNumber("0");
        if (e != null && e.getMessage() != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ": " + e.getMessage());
        } else if (e != null && e.getCause() != null && e.getCause().getMessage() != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ": " + e.getCause().getMessage());
        } else if (e != null) {
            validationException.setMessage(UNEXPECTED_ERROR + ". The exception is of kind: " + e.getClass().getSimpleName());
        } else {
            validationException.setMessage(UNEXPECTED_ERROR);
        }
        validationException.setSeverity(SEVERITY_ERROR);
        return validationException;
    }

    private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, T validationReport) {

        validationReport.setResult(STATUS_PASSED);

        if (exceptions == null || exceptions.size() == 0) {
            validationReport.setResult(STATUS_PASSED);
            return validationReport;
        } else {
            Integer nbOfErrors = 0;
            Integer nbOfWarnings = 0;
            for (ValidationException ve : exceptions) {
                if (ve.getSeverity() == null) {
                    ve.setSeverity(SEVERITY_ERROR);
                }
                if ((ve.getSeverity() != null) && (ve.getSeverity().equals(SEVERITY_WARNING))) {
                    nbOfWarnings++;
                } else {
                    nbOfErrors++;
                }
                XSDMessage xsd = new XSDMessage();
                xsd.setMessage(ve.getMessage());
                xsd.setSeverity(ve.getSeverity());

                if (StringUtils.isNumeric(ve.getLineNumber()) && StringUtils.isNumeric(ve.getColumnNumber())) {
                    xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
                    xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
                }
                validationReport.getXSDMessage().add(xsd);
            }
            validationReport.setNbOfErrors(nbOfErrors.toString());
            validationReport.setNbOfWarnings(nbOfWarnings.toString());
            if (nbOfErrors > 0) {
                validationReport.setResult(STATUS_FAILED);
            }
            return validationReport;
        }

    }

}
