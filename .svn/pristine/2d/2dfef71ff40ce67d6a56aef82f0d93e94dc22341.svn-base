package net.ihe.gazelle.pki.validator;

import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.pki.validator.epsos.v1.*;
import net.ihe.gazelle.pki.validator.epsos.v3.EpsosNCP;
import net.ihe.gazelle.pki.validator.epsos.v3.EpsosTLS;
import net.ihe.gazelle.pki.validator.epsos.v3.EpsosVPN;
import net.ihe.gazelle.pki.validator.epsos.v4.EpsosSeal;
import net.ihe.gazelle.pki.validator.tls.CertificateAuthorityValidator;
import net.ihe.gazelle.pki.validator.tls.TlsClientValidator;
import net.ihe.gazelle.pki.validator.tls.TlsServerValidator;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public enum CertificateValidatorType implements
        CertificateValidator, Serializable {

    EPSOS_VPN_CLIENT(ContextType.EPSOS, EpsosVPNClientValidator.class, "epSOS VPN client v1"),

    EPSOS_VPN_SERVER(ContextType.EPSOS, EpsosVPNServerValidator.class, "epSOS VPN server v1"),

    EPSOS_SERVICE_CONSUMER(ContextType.EPSOS, EpsosServiceConsumerValidator.class, "epSOS service consumer v1"),

    EPSOS_SERVICE_PROVIDER(ContextType.EPSOS, EpsosServiceProviderValidator.class, "epSOS service provider v1"),

    EPSOS_NCP_SIGNATURE(ContextType.EPSOS, EpsosNCPValidator.class, "epSOS NCP signature v1"),

    EPSOS_OCSP_RESPONDER(ContextType.EPSOS, EpsosOCSPResponderValidator.class, "epSOS OCSP responder v1"),

    TLS_SERVER(ContextType.IHE, TlsServerValidator.class, "TLS server"),

    TLS_CLIENT(ContextType.IHE, TlsClientValidator.class, "TLS client"),

    CA(ContextType.IHE, CertificateAuthorityValidator.class, "CA"),

//    EPSOS_PPT_VPN(ContextType.EPSOS, EpsosPPTVPNValidator.class, "epSOS VPN v2"),
//
//    EPSOS_PPT_TLS(ContextType.EPSOS, EpsosPPTTLSValidator.class, "epSOS TLS v2"),
//
//    EPSOS_PPT_OBJECT_SIGNING(ContextType.EPSOS, EpsosPPTObjectSigningValidator.class, "epSOS Object Signing v2"),

    EPSOS_VPN_V3(ContextType.EPSOS, EpsosVPN.class, "epSOS VPN v3"),

    EPSOS_TLS_V3(ContextType.EPSOS, EpsosTLS.class, "epSOS TLS v3"),

    EPSOS_NCP_V3(ContextType.EPSOS, EpsosNCP.class, "epSOS Object Signing v3"),

    EPSOS_SEAL_V4(ContextType.EPSOS, EpsosSeal.class, "epSOS Seal v4"),

    EPSOS_TLS_V4(ContextType.EPSOS, net.ihe.gazelle.pki.validator.epsos.v4.EpsosTLS.class, "epSOS TLS v4");

    private String friendlyName;

    private CertificateValidator instance;

    private ContextType context;

    private CertificateValidatorType(ContextType context,
                                     Class<? extends CertificateValidator> certificateValidatorClass, String friendlyName) {
        try {
            instance = certificateValidatorClass.newInstance();
        } catch (InstantiationException e) {
            instance = null;
            CertificateValidator.log.error("Failed to init " + this, e);
        } catch (IllegalAccessException e) {
            instance = null;
            CertificateValidator.log.error("Failed to init " + this, e);
        }
        this.friendlyName = friendlyName;
        this.context = context;
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, Collection<X509Certificate> trusted,
                                               boolean revocation) {
        return instance.validate(chain, trusted, revocation);
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, EntityManager em, boolean revocation) {
        return instance.validate(chain, em, revocation);
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, boolean revocation) {
        return instance.validate(chain, revocation);
    }

    public ContextType getContext() {
        return context;
    }

    public CertificateValidator getCertificateValidator() {
        return instance;
    }

    public String toString() {
        return friendlyName;
    }

    /**
     * Get the enumeration values formated as Items for jsf or rich components
     * @return the enumeration values formated as Items for jsf or rich components
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = CertificateValidatorType.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

    /**
     * Get the enumeration values filtered by context formated as Items for jsf or rich components
     * @param contextFilter the context filter
     * @return the enumeration values filtered by context formated as Items for jsf or rich components
     */
    public static SelectItem[] getSelectItems(ContextType contextFilter) {

        if (contextFilter != null) {
            //Retrieve all validators.
            CertificateValidatorType[] validators = CertificateValidatorType.values();

            //Must use a temp list to determine which elements match with the context and get the size of SelectItem[] array.
            List<SelectItem> tempList = new ArrayList<SelectItem>();
            for (int i = 0; i < validators.length; i++) {
                if (validators[i].context == contextFilter) {
                    tempList.add(new SelectItem(validators[i], validators[i].toString()));
                }
            }
            //Finally convert the list into SelectItem array
            return tempList.toArray(new SelectItem[tempList.size()]);

        } else {
            return getSelectItems();
        }
    }

    public static CertificateValidatorType getValidatorTypeByFriendlyName(String friendlyName) {
        for (CertificateValidatorType validator : CertificateValidatorType.values()) {
            if (validator.toString().equalsIgnoreCase(friendlyName)) {
                return validator;
            }
        }
        return null;
    }
}
