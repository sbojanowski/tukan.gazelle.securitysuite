package net.ihe.gazelle.pki.validator.epsos.v1;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Name;

import java.security.cert.CertificateException;
import java.util.List;
import java.util.Vector;

public abstract class EpsosServerValidator extends EpsosValidator {

    protected abstract String getCertificateType();

    @Override
    public void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings) {
        super.validateSubject(name, exceptions, warnings);
        X509Name x509name;
        try {
            x509name = new X509Name(name);
        } catch (IllegalArgumentException e) {
            exceptions.add(new CertificateNormException(e, "epSOS WP 3.4.2 - Chapter 5.4.4/5.4.6 - "
                    + getCertificateType()));
            return;
        }
        Vector<String> cnVector = getSubjectVector(x509name, X509Name.CN);
        if (cnVector.size() > 0) {

            warnings.add(new CertificateNormException(
                    "The CN (Common Name) MUST include the DNS server name; if it doesn’t, "
                            + "the client’s default setting identifies the server certificate as untrustworthy.",
                    "epSOS WP 3.4.2 - Chapter 5.4.4/5.4.6 - " + getCertificateType()));

            // FIXME 5.4.4 The CN (Common Name) MUST include the DNS server
            // name; if it doesn’t, the client’s default setting identifies the
            // server certificate as untrustworthy.
        }
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        validateExtendedKeyUsagesOneValue(extendedKeyUsage, "ServerAuth (OID 1.3.6.1.5.5.7.3.1)",
                KeyPurposeId.id_kp_serverAuth, exceptions, "epSOS WP 3.4.2 - Chapter 5.4.4/5.4.6 - "
                        + getCertificateType());
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        validateKeyUsageValue(bits, KeyUsage.keyEncipherment, "For " + getCertificateType()
                        + " certificates, only the keyEncipherment usage type MUST be specified.", exceptions,
                "epSOS WP 3.4.2 - Chapter 5.4.4/5.4.6 - " + getCertificateType());
    }

}
