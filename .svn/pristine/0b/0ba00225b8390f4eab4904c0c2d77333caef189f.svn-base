--
-- Rename TlsActor to TlsSimulator
--

ALTER TABLE tls_test_actor RENAME TO tls_simulator ;
ALTER SEQUENCE tls_test_actor_id_seq RENAME TO tls_simulator_id_seq ;
ALTER INDEX tls_test_actor_is_enabled_idx RENAME TO tls_simulator_is_enabled_idx ;
ALTER INDEX tls_test_actor_pkey RENAME TO tls_simulator_pkey ;

ALTER TABLE tls_test_actor_tls_test_ciphersuite RENAME COLUMN tls_test_actor_id TO simulator_id ;
ALTER TABLE tls_test_actor_tls_test_ciphersuite RENAME TO tls_simulator_ciphersuites ;

ALTER TABLE tls_test_actor_tls_test_protocol RENAME COLUMN tls_test_actor_id TO simulator_id ;
ALTER TABLE tls_test_actor_tls_test_protocol RENAME TO tls_simulator_protocols ;

ALTER TABLE tls_test_actor_trusted_issuers RENAME COLUMN tls_test_actor_id TO simulator_id ;
ALTER TABLE tls_test_actor_trusted_issuers RENAME TO tls_simulator_trusted_issuers ;
ALTER INDEX tls_test_actor_tls_certificate_pkey RENAME TO tls_simulator_trusted_issuers_pkey ;

-- Extend renaming to other related tables

ALTER TABLE tls_test_case RENAME COLUMN actor_id TO simulator_id ;

--
-- Rename tls_test_connection to tls_connection
--

ALTER TABLE tls_test_connection RENAME TO tls_connection ;
ALTER TABLE tls_connection RENAME COLUMN tls_actor_id TO simulator_id ;
ALTER SEQUENCE tls_test_connection_id_seq RENAME TO tls_connection_id_seq ;
ALTER INDEX tls_test_connection_pkey RENAME TO tls_connection_pkey ;
ALTER INDEX tls_test_connection_tls_actor_id_idx RENAME TO tls_connection_simulator_idx ;

ALTER TABLE tls_test_connection_data_item RENAME TO tls_connection_data_item ;
ALTER TABLE tls_connection_data_item RENAME COLUMN tls_test_connection_data_item_type TO tls_connection_data_item_type ;
ALTER SEQUENCE tls_test_connection_data_item_id_seq RENAME TO tls_connection_data_item_id_seq ;
ALTER INDEX tls_test_connection_data_item_pkey RENAME TO tls_connection_data_item_pkey ;

ALTER TABLE tls_test_connection_data_item_aliases_item RENAME TO tls_connection_data_item_aliases_item ;
ALTER SEQUENCE tls_test_connection_data_item_aliases_item_id_seq RENAME TO tls_connection_data_item_aliases_item_id_seq ;
ALTER INDEX tls_test_connection_data_item_aliases_item_pkey RENAME TO tls_connection_data_item_aliases_item_pkey ;

ALTER TABLE tls_test_connection_tls_test_connection_data_item RENAME TO tls_connection_tls_connection_data_item ;
ALTER TABLE tls_connection_tls_connection_data_item RENAME COLUMN tls_test_connection_id TO connection_id ;
ALTER TABLE tls_connection_tls_connection_data_item RENAME COLUMN items_id TO data_item_id ;
ALTER TABLE tls_connection_tls_connection_data_item DROP CONSTRAINT tls_test_connection_tls_test_connection_data_item_items_id_key ;
ALTER TABLE tls_connection_tls_connection_data_item DROP CONSTRAINT uk_f84fbjybf7s9hwvfmo23qe03e ;
--
-- Change database 'tls' prefix to 'pki' for PKI tables
--

ALTER TABLE tls_certificate RENAME TO pki_certificate ;
ALTER SEQUENCE tls_certificate_id_seq RENAME TO pki_certificate_id_seq ;
ALTER INDEX tls_certificate_pkey RENAME TO pki_certificate_pkey ;
ALTER INDEX tls_certificate_certificateauthority_id_idx RENAME TO pki_certificate_certificateauthority_idx ;

ALTER TABLE tls_certificate_key RENAME TO pki_certificate_key ;
ALTER SEQUENCE tls_certificate_key_id_seq RENAME TO pki_certificate_key_id_seq ;
ALTER INDEX tls_certificate_key_pkey RENAME TO pki_certificate_key_pkey ;

ALTER TABLE tls_certificate_request RENAME TO pki_certificate_request ;
ALTER SEQUENCE tls_certificate_request_id_seq RENAME TO pki_certificate_request_id_seq ;
ALTER INDEX tls_certificate_request_pkey RENAME TO pki_certificate_request_pkey ;

ALTER TABLE tls_certificate_x509 RENAME TO pki_certificate_x509 ;
ALTER SEQUENCE tls_certificate_x509_id_seq RENAME TO pki_certificate_x509_id_seq ;
ALTER INDEX tls_certificate_x509_pkey RENAME TO pki_certificate_x509_pkey ;

--
-- Rename join tables regarding tls_test_data_set
--

ALTER TABLE tls_test_data_set_tls_test_ciphersuite RENAME TO tls_test_data_set_ciphersuites ;
ALTER TABLE tls_test_data_set_tls_test_protocol RENAME TO tls_test_data_set_protocols ;

--
-- Indexes on pki_certificate_request table
--

CREATE INDEX pki_request_subject_idx ON pki_certificate_request(subject) ;
CREATE INDEX pki_request_certificateauthority_idx ON pki_certificate_request(certificateauthority_id) ;
CREATE INDEX pki_request_privatekey_idx ON pki_certificate_request(privatekey_id) ;
CREATE INDEX pki_request_publickey_idx ON pki_certificate_request(publickey_id) ;
CREATE INDEX pki_request_requesterusername_idx ON pki_certificate_request(requesterusername) ;

--
-- Indexes on pki_certificate table
--

CREATE INDEX pki_certificate_subject_idx ON pki_certificate(subject) ;
CREATE INDEX pki_certificate_privatekey_idx ON pki_certificate(privatekey_id) ;
CREATE INDEX pki_certificate_publickey_idx ON pki_certificate(publickey_id) ;
CREATE INDEX pki_certificate_request_idx ON pki_certificate(request_id) ;
CREATE INDEX pki_certificate_certificatex509_idx ON pki_certificate(certificatex509_id) ;

--
-- Indexes on tls_simulator table
--

CREATE INDEX tls_simulator_certificate_id_idx ON tls_simulator(certificate_id) ;
CREATE INDEX tls_simulator_channel_type_idx ON tls_simulator(channel_type) ;

--
-- Indexes on tls_simulator ciphersuites and protocols tables
--

ALTER TABLE ONLY tls_simulator_ciphersuites
    ADD CONSTRAINT tls_simulator_ciphersuites_pkey PRIMARY KEY (simulator_id, ciphersuite);

ALTER TABLE ONLY tls_simulator_protocols
    ADD CONSTRAINT tls_simulator_protocols_pkey PRIMARY KEY (simulator_id, protocol);

--
-- Indexes on tls_connection
--

CREATE INDEX tls_connection_remote_host_idx ON tls_connection(remote_host) ;
CREATE INDEX tls_connection_is_success_idx ON tls_connection(is_success) ;

--
-- Indexes on tls_connection_data_item_aliases_item
--

CREATE INDEX tls_connection_data_item_aliases_item_key_type_idx ON tls_connection_data_item_aliases_item(join_key_type) ;
CREATE INDEX tls_connection_data_item_aliases_item_issuer_idx ON tls_connection_data_item_aliases_item(join_issuer) ;

--
-- Indexes on tls_connection_tls_connection_data_item
--

ALTER TABLE ONLY tls_connection_tls_connection_data_item
    ADD CONSTRAINT tls_connection_tls_connection_data_item_pkey PRIMARY KEY (connection_id, data_item_id);

--
-- Indexes on tls_test_case
--

CREATE INDEX tls_test_case_simulator_idx ON tls_test_case(simulator_id) ;
CREATE INDEX tls_test_case_dataset_idx ON tls_test_case(dataset_id) ;

--
-- Indexes on tls_test_case_expectedalertdescriptions
--

ALTER TABLE ONLY tls_test_case_expectedalertdescriptions
    ADD CONSTRAINT tls_test_case_expectedalertdescriptions_pkey PRIMARY KEY (tls_test_case_id, element);

--
-- Indexes on tls_test_data_set
--

CREATE INDEX tls_test_data_set_certificate_idx ON tls_test_data_set(certificate_id) ;


--
-- Indexes on tls_test_data_set join tables
--

ALTER INDEX tls_test_data_set_tls_certificate_pkey RENAME TO tls_test_data_set_trusted_issuers_pkey ;

ALTER TABLE ONLY tls_test_data_set_ciphersuites
    ADD CONSTRAINT tls_test_data_set_ciphersuites_pkey PRIMARY KEY (tls_test_data_set_id, ciphersuite);

ALTER TABLE ONLY tls_test_data_set_protocols
    ADD CONSTRAINT tls_test_data_set_protocols_pkey PRIMARY KEY (tls_test_data_set_id, protocol);


--
-- Indexes on tls_test_instance
--

CREATE INDEX tls_test_instance_test_case_idx ON tls_test_instance(test_case_id) ;
CREATE INDEX tls_test_instance_connection_idx ON tls_test_instance(connection_id) ;
CREATE INDEX tls_test_instance_sut_host_idx ON tls_test_instance(sut_host) ;
CREATE INDEX tls_test_instance_last_modifier_idx ON tls_test_instance(last_modifier_id) ;
CREATE INDEX tls_test_instance_verdict_idx ON tls_test_instance(test_verdict) ;

--
-- Syslog migration
--

DROP TABLE syslog_schematron_validation ;
DROP SEQUENCE syslog_schematron_validation_id_seq ;

--
-- Syslog: Common columns migration
--

ALTER TABLE syslog_message ALTER COLUMN id DROP DEFAULT ;

ALTER TABLE syslog_message
  ADD COLUMN transport_type integer;

ALTER TABLE syslog_message
   ALTER COLUMN raw_message DROP NOT NULL,
   ALTER COLUMN raw_message DROP DEFAULT;

ALTER TABLE syslog_message
   ALTER COLUMN error_message TYPE text,
   ALTER COLUMN error_message DROP NOT NULL;

ALTER TABLE syslog_message RENAME COLUMN xml_message TO message_content ;
ALTER TABLE syslog_message
  ALTER COLUMN message_content DROP NOT NULL,
  ALTER COLUMN message_content DROP DEFAULT;

ALTER TABLE syslog_message DROP COLUMN message_name ;

--
-- Syslog: RFC 3164 column migration
--

ALTER TABLE syslog_message ADD COLUMN rfc3164_parse_succeed boolean;
UPDATE syslog_message SET rfc3164_parse_succeed = true WHERE rfc3641_parse = 1 ;
UPDATE syslog_message SET rfc3164_parse_succeed = false WHERE rfc3641_parse = 2 ;
ALTER TABLE syslog_message DROP COLUMN rfc3641_parse ;

ALTER TABLE syslog_message RENAME COLUMN rfc3641_error_message TO rfc3164_error_message;
ALTER TABLE syslog_message ALTER COLUMN rfc3164_error_message DROP NOT NULL;

ALTER TABLE syslog_message RENAME COLUMN rfc3641_error_substring TO rfc3164_error_substring;
ALTER TABLE syslog_message
  ALTER COLUMN rfc3164_error_substring TYPE CHARACTER VARYING(255),
  ALTER COLUMN rfc3164_error_substring DROP NOT NULL;

ALTER TABLE syslog_message RENAME COLUMN rfc3641_error_location TO rfc3164_error_location;
ALTER TABLE syslog_message
  ALTER COLUMN rfc3164_error_location DROP NOT NULL,
  ALTER COLUMN rfc3164_error_location DROP DEFAULT;

--
-- Syslog: RFC 5424 column migration
--

ALTER TABLE syslog_message ADD COLUMN rfc5424_parse_succeed boolean;
UPDATE syslog_message SET rfc5424_parse_succeed = true WHERE rfc5424_parse = 1 ;
UPDATE syslog_message SET rfc5424_parse_succeed = false WHERE rfc5424_parse = 2 ;
ALTER TABLE syslog_message DROP COLUMN rfc5424_parse ;

ALTER TABLE syslog_message ALTER COLUMN rfc5424_error_message DROP NOT NULL;

ALTER TABLE syslog_message
  ALTER COLUMN rfc5424_error_substring TYPE CHARACTER VARYING(255),
  ALTER COLUMN rfc5424_error_substring DROP NOT NULL;

ALTER TABLE syslog_message
  ALTER COLUMN rfc5424_error_location DROP NOT NULL,
  ALTER COLUMN rfc5424_error_location DROP DEFAULT;

--
-- Syslog: Drop deprecated RFC 3881 columns
--

ALTER TABLE syslog_message DROP COLUMN rfc3881_parse ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_parse_error_message ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_parse_line ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_parse_column ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_validate ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_validate_error_message ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_validate_line ;
ALTER TABLE syslog_message DROP COLUMN rfc3881_validate_column ;

--
-- Syslog: Drop event related columns (IHE Audit-message dependent meaning)
--

ALTER TABLE syslog_message DROP COLUMN event_type ;
ALTER TABLE syslog_message DROP COLUMN event_id ;

--
-- Indexes on syslog_message
--

CREATE INDEX syslog_message_sender_ip_idx ON syslog_message(sender_ip) ;
CREATE INDEX syslog_message_transport_type_idx ON syslog_message(transport_type) ;

--
-- Update mbv-documentation to 1.1.6
--

ALTER TABLE mbv_constraint ADD COLUMN kind CHARACTER VARYING(255);
