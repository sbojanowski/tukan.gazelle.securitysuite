\c postgres
ALTER DATABASE tls RENAME TO gss ;
\c gss

UPDATE tls_certificate SET subject = REPLACE(subject, ',SN=', ',SERIALNUMBER=') ;
ALTER TABLE tls_certificate RENAME COLUMN serialnumber TO serialnumber_counter ;

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'syslog_collector_enabled', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'syslog_directory', '/opt/syslog');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'syslog_automatic_start_enabled', 'false');

ALTER TABLE am_audit_message_specification_active_prticipants RENAME TO am_audit_message_specification_active_participants ;
ALTER TABLE tls_test_actor_tls_certificate RENAME TO tls_test_actor_trusted_issuers ;
ALTER TABLE tls_test_actor_trusted_issuers RENAME issuers_id TO certificate_id ;
ALTER TABLE tls_test_data_set_tls_certificate RENAME TO tls_test_data_set_trusted_issuers ;
ALTER TABLE tls_test_data_set_trusted_issuers RENAME trustedissuers_id TO certificate_id ;

--
-- Name: syslog_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE syslog_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.syslog_message_id_seq OWNER TO gazelle;

--
-- Name: syslog_message; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE syslog_message (
    id integer DEFAULT nextval('syslog_message_id_seq'::regclass) NOT NULL,
    sender_ip character varying(15) NOT NULL,
    collector_ip character varying(15) NOT NULL,
    collector_port integer NOT NULL,
    arrival_time timestamp with time zone NOT NULL,
    error_message character varying(256) NOT NULL,
    rfc3641_parse smallint DEFAULT 0 NOT NULL,
    rfc3641_error_message text NOT NULL,
    rfc3641_error_substring character varying(256) NOT NULL,
    rfc3641_error_location integer DEFAULT (-1) NOT NULL,
    rfc5424_parse smallint DEFAULT 0 NOT NULL,
    rfc5424_error_message text NOT NULL,
    rfc5424_error_substring character varying(256) NOT NULL,
    rfc5424_error_location integer DEFAULT (-1) NOT NULL,
    rfc3881_parse smallint DEFAULT 0 NOT NULL,
    rfc3881_parse_error_message text NOT NULL,
    rfc3881_parse_line smallint DEFAULT 0 NOT NULL,
    rfc3881_parse_column smallint DEFAULT 0 NOT NULL,
    rfc3881_validate smallint DEFAULT 0 NOT NULL,
    rfc3881_validate_error_message text NOT NULL,
    rfc3881_validate_line smallint DEFAULT 0 NOT NULL,
    rfc3881_validate_column smallint DEFAULT 0 NOT NULL,
    event_type character varying(12) NOT NULL,
    event_id character varying(12) NOT NULL,
    message_name character varying(256) NOT NULL,
    raw_message text DEFAULT ''::text NOT NULL,
    xml_message text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.syslog_message OWNER TO gazelle;

--
-- Name: syslog_schematron_validation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE syslog_schematron_validation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.syslog_schematron_validation_id_seq OWNER TO gazelle;

--
-- Name: syslog_schematron_validation; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE syslog_schematron_validation (
    id integer DEFAULT nextval('syslog_schematron_validation_id_seq'::regclass) NOT NULL,
    syslog_message_id integer,
    audit_message_name character varying(256) NOT NULL,
    schematron_validate smallint DEFAULT 0 NOT NULL,
    schematron_validate_error_message text NOT NULL,
    schematron_validate_line smallint DEFAULT 0 NOT NULL,
    schematron_validate_column smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.syslog_schematron_validation OWNER TO gazelle;


--
-- Name: syslog_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY syslog_message
    ADD CONSTRAINT syslog_message_pkey PRIMARY KEY (id);


--
-- Name: syslog_schematron_validation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY syslog_schematron_validation
    ADD CONSTRAINT syslog_schematron_validation_pkey PRIMARY KEY (id);

--
-- Name: syslog_schematron_validation_syslog_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY syslog_schematron_validation
    ADD CONSTRAINT syslog_schematron_validation_syslog_message_id_fkey FOREIGN KEY (syslog_message_id) REFERENCES syslog_message(id) ON DELETE CASCADE;

--
-- Remove duplicated foreign keys
--

ALTER TABLE tls_certificate DROP CONSTRAINT IF EXISTS fk7524c0531a6215f3 ;
ALTER TABLE tls_certificate DROP CONSTRAINT IF EXISTS fk7524c0533b792b8d ;
ALTER TABLE tls_certificate DROP CONSTRAINT IF EXISTS fk7524c05346407e55 ;

ALTER TABLE tls_certificate_request DROP CONSTRAINT IF EXISTS fk447356a31a6215f3 ;
ALTER TABLE tls_certificate_request DROP CONSTRAINT IF EXISTS fk447356a346407e55 ;

ALTER TABLE tls_test_actor_trusted_issuers DROP CONSTRAINT IF EXISTS fkc72dd3e027a6047e ;

ALTER TABLE tls_test_connection_tls_test_connection_data_item DROP CONSTRAINT IF EXISTS fk4e62e8d8873cf29a ;
