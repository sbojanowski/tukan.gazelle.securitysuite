package net.ihe.gazelle.simulators.tls.common;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManager;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import net.ihe.gazelle.simulators.tls.model.TlsClient;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jboss.netty.channel.ChannelHandler;

public abstract class TlsProxyConnectionConfig implements ConnectionConfig {

    private static final String FAILED_TO_INITIALIZE_TLS_CONTEXT = "Failed to initialize TLS context";

    private TlsListener listener;
    private SSLContext sslContext;

    private int proxyProviderPort;

    private String proxyConsumerHost;

    private int proxyConsumerPort;

    private ChannelType channelType;

    private boolean useClientMode;

    private SSLParameters parameters;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public TlsProxyConnectionConfig(TlsSimulator simulator, List<TlsConnectionListener> connectionListeners,
                                    int proxyProviderPort, String proxyConsumerHost, int proxyConsumerPort, ChannelType channelType)
            throws TlsSimulatorException {
        this.proxyProviderPort = proxyProviderPort;
        this.proxyConsumerHost = proxyConsumerHost;
        this.proxyConsumerPort = proxyConsumerPort;
        this.channelType = channelType;

        this.listener = new TlsListener(simulator, connectionListeners);

        CertificateBasedKeyManager certificateBasedKeyManager = new CertificateBasedKeyManager(simulator.getCertificate(),
                listener);
        try {
            this.sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            throw new TlsSimulatorException(FAILED_TO_INITIALIZE_TLS_CONTEXT, e);
        }
        KeyManager[] keyManagers = new KeyManager[]{certificateBasedKeyManager};
        try {
            TestTrustManager trustManager = new TestTrustManager(simulator, listener);

            TrustManager[] trustManagers = new TrustManager[]{trustManager};
            sslContext.init(keyManagers, trustManagers, null);
        } catch (KeyManagementException e) {
            throw new TlsSimulatorException(FAILED_TO_INITIALIZE_TLS_CONTEXT, e);
        }

        useClientMode = simulator instanceof TlsClient;
        parameters = simulator.toSSLParameters();
    }

    @Override
    public int getProxyProviderPort() {
        return proxyProviderPort;
    }

    @Override
    public String getProxyConsumerHost() {
        return proxyConsumerHost;
    }

    @Override
    public int getProxyConsumerPort() {
        return proxyConsumerPort;
    }

    @Override
    public ChannelType getChannelType() {
        return channelType;
    }

    public List<ChannelHandler> getHandlers() {
        List<ChannelHandler> result = new ArrayList<ChannelHandler>();

        SSLEngine engine = sslContext.createSSLEngine();
        engine.setUseClientMode(useClientMode);

        if (parameters != null) {
            engine.setSSLParameters(parameters);
        }

        TlsHandlerListenHandshake sslHandler = new TlsHandlerListenHandshake(engine, listener);
        sslHandler.setEnableRenegotiation(true);

        result.add(sslHandler);
        return result;
    }

}
