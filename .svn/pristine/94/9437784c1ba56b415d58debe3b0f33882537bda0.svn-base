package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.qualified.QCStatement;

import java.security.cert.CertificateException;
import java.util.List;

public class EpsosTLS extends EpsosValidator {

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits[] = {KeyUsage.digitalSignature | KeyUsage.keyEncipherment, KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage
                .dataEncipherment};
        validateKeyUsageValue(bits, keyUsageBits,
                "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other KeyUSage bits that MUST be " +
                        "set to false, excepted for the dataEncipherment bit that MAY be set to true.", exceptions,
                EHEALTH_X509_3_1);
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsages, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsages, exceptions);
        if (extendedKeyUsages != null && extendedKeyUsages.size() > 0) {
            if (!(extendedKeyUsages.contains(KeyPurposeId.id_kp_clientAuth.getId())
                    && extendedKeyUsages.contains(KeyPurposeId.id_kp_serverAuth.getId()))) {
                exceptions.add(new CertificateNormException(
                        "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.", EHEALTH_X509_3_1));
            }
        } else {
            exceptions.add(new CertificateNormException(
                    "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.", EHEALTH_X509_3_1));
        }
    }

    @Override
    protected void validateQCStatementUsages(QCStatement qcStatement, List<CertificateException> exceptions) {
        if (qcStatement.getStatementId() != new DERObjectIdentifier(QCStatement.id_etsi_qcs + ".6.3")) {
            exceptions.add(new CertificateNormException(
                    "The esi4-qcStatement-6 id-etsi-qcs-QcType 3 statement SHOULD be included on its own as specified in [ETSI EN 319 412-5] to " +
                            "indicate that it is used for the purposes of electronic website authentication.", EHEALTH_X509_3_1));
        }
    }
}
