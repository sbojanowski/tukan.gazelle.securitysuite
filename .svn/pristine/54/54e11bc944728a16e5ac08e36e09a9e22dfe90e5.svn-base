package net.ihe.gazelle.simulators.tls.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.proxy.netty.basictls.TlsCredentials;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.test.TlsPilot;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.net.ssl.SSLParameters;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

@Entity
@Table(name = "tls_simulator", schema = "public")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("type")
@SequenceGenerator(name = "tls_simulator_sequence", sequenceName = "tls_simulator_id_seq", allocationSize = 1)
public abstract class TlsSimulator implements Serializable, TlsPilot {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "tls_simulator_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    @NotNull
    private String keyword;

    @ElementCollection
    @CollectionTable(name = "tls_simulator_ciphersuites", joinColumns = @JoinColumn(name = "simulator_id"))
    @Column(name = "ciphersuite")
    private Set<CipherSuiteType> cipherSuites;

    @ElementCollection
    @CollectionTable(name = "tls_simulator_protocols", joinColumns = @JoinColumn(name = "simulator_id"))
    @Column(name = "protocol")
    private Set<ProtocolType> protocols;

    @Column(name = "validator")
    private CertificateValidatorType validator;

    @ManyToOne
    @JoinColumn(name = "certificate_id")
    private Certificate certificate;

    @Column(name = "is_use_ca_certs")
    private boolean useCACerts;

    @Column(name = "is_check_revocation")
    protected boolean checkRevocation;

    @Column(name = "is_enabled")
    protected boolean enabled;

    @ManyToMany
    @JoinTable(name = "tls_simulator_trusted_issuers",
            joinColumns = @JoinColumn(name = "simulator_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "certificate_id", referencedColumnName = "id"))
    private Set<Certificate> issuers = new HashSet<Certificate>();

    @OneToMany(mappedBy = "simulator", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<TlsConnection> connections;

    public TlsSimulator() {
        super();
        this.useCACerts = false;
        this.checkRevocation = false;
        this.enabled = true;
    }

    protected abstract void updateParameters(SSLParameters sslParameters);

    public CertificateValidatorType getValidator() {
        return validator;
    }

    public void setValidator(CertificateValidatorType validator) {
        this.validator = validator;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Set<Certificate> getIssuers() {
        return issuers;
    }

    public void setIssuers(Set<Certificate> issuers) {
        this.issuers = issuers;
    }

    public List<TlsConnection> getConnections() {
        if (connections == null) {
            connections = new ArrayList<TlsConnection>();
        }
        return connections;
    }

    public void setConnections(List<TlsConnection> connections) {
        this.connections = connections;
    }

    public abstract SimulatorType getType();

    public Set<CipherSuiteType> getCipherSuites() {
        if (cipherSuites == null) {
            cipherSuites = new HashSet<CipherSuiteType>();
        }
        return cipherSuites;
    }

    public void setCipherSuites(Set<CipherSuiteType> cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

    public Set<ProtocolType> getProtocols() {
        if (protocols == null) {
            protocols = new HashSet<ProtocolType>();
        }
        return protocols;
    }

    public void setProtocols(Set<ProtocolType> protocols) {
        this.protocols = protocols;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public SSLParameters toSSLParameters() {
        SSLParameters sslParameters = new SSLParameters();

        updateParameters(sslParameters);

        String[] cipherSuitesArray = new String[cipherSuites.size()];
        int i = 0;
        for (CipherSuiteType cipherSuite : cipherSuites) {
            cipherSuitesArray[i] = cipherSuite.name();
            i++;
        }
        sslParameters.setCipherSuites(cipherSuitesArray);

        String[] protocolsArray = new String[protocols.size()];
        i = 0;
        for (ProtocolType protocol : protocols) {
            protocolsArray[i] = protocol.getName();
            i++;
        }
        sslParameters.setProtocols(protocolsArray);

        return sslParameters;
    }

    public X509Certificate[] getIssuersArray() throws CertificateException {
        X509Certificate[] result = new X509Certificate[issuers.size()];
        int i = 0;
        for (Certificate certificate : issuers) {
            result[i] = certificate.getCertificateX509().getX509Certificate();
            i++;
        }
        return result;
    }

    public void loadCertificates() {
        if (certificate != null) {
            try {
                certificate.getChainAsArray();
            } catch (CertificateException e) {
            }
        }
        for (Certificate certificate : issuers) {
            try {
                certificate.getChainAsArray();
            } catch (CertificateException e) {
            }
        }
    }


    @Override
    public String toString() {
        return "TlsSimulator [id=" + id + "]";
    }

    public void setUseCACerts(boolean useCACerts) {
        this.useCACerts = useCACerts;
    }

    public boolean isUseCACerts() {
        return useCACerts;
    }

    public boolean isCheckRevocation() {
        return checkRevocation;
    }

    public void setCheckRevocation(boolean checkRevocation) {
        this.checkRevocation = checkRevocation;
    }

    protected TlsCredentials getCredentials() {
        TlsCredentials credentials;
        try {
            credentials = new TlsCredentials(getCertificate().getChainAsArray(), getCertificate().getPrivateKey()
                    .getKey());
        } catch (CertificateException e) {
            return null;
            // log.error(e);
        }
        return credentials;
    }

    /**
     * The minimum number of server port number.
     */
    public static final int MIN_PORT_NUMBER = 1;

    /**
     * The maximum number of server port number.
     */
    public static final int MAX_PORT_NUMBER = 49151;

    /**
     * Gets the next available port starting at a port.
     *
     * @param fromPort the port to scan for availability
     * @return the next available port on the machine.
     * @throws NoSuchElementException if there are no ports available
     */
    public static int getNextAvailable(int fromPort) {
        if (fromPort < MIN_PORT_NUMBER || fromPort > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + fromPort);
        }

        for (int i = fromPort; i <= MAX_PORT_NUMBER; i++) {
            if (available(i)) {
                return i;
            }
        }

        throw new NoSuchElementException("Could not find an available port " + "above " + fromPort);
    }

    /**
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     * @return true if the port is available, false otherwise
     */
    public static boolean available(int port) {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
            // Do nothing
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TlsSimulator other = (TlsSimulator) obj;
        if (keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!keyword.equals(other.keyword)) {
            return false;
        }
        return true;
    }

    public Integer getId() {
        return id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public TlsSimulator clone() {
        TlsSimulator copy = newInstance();
        copy.setKeyword(this.getKeyword() + "_COPY");
        copy.setCipherSuites(new HashSet<>(this.getCipherSuites()));
        copy.setProtocols(new HashSet<ProtocolType>(this.getProtocols()));
        copy.setValidator(this.getValidator());
        copy.setCertificate(this.getCertificate());
        copy.setUseCACerts(this.isUseCACerts());
        copy.setCheckRevocation(this.isCheckRevocation());
        copy.setEnabled(this.isEnabled());
        copy.setIssuers(new HashSet<Certificate>(this.getIssuers()));
        return copy;
    }

    protected abstract <T extends TlsSimulator> T newInstance();

}
