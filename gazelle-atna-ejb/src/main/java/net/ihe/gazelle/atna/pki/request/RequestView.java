package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.util.Map;

/**
 * Created by cel on 17/02/16.
 */
@Name("requestView")
@Scope(ScopeType.PAGE)
public class RequestView extends AbstractRequestView implements Serializable {

    private static final long serialVersionUID = 5514267367380791575L;
    private static final String NO_SUCH_RECORD = "No such certificate request recorded in database";
    private static final String INVALID_ID = "' is not a valid certificate request id";
    private static final String MISSING_PARAM = "Missing parameter 'id', unable to retrieve certificate request.";

    private static Logger log = LoggerFactory.getLogger(RequestView.class);
    private static String certificateViewBaseLink = PreferenceService.getString("application_url") +
            Pages.PKI_VIEW_CERTIFICATE.getMenuLink();
    private static String requestListLink = PreferenceService.getString("application_url") +
            Pages.PKI_LIST_REQUESTS.getMenuLink();

    private int requestId = 0;
    private boolean ready = false;
    private transient CertificateRequest request;

    @Create
    public void init() throws IllegalArgumentException {
        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("id") != null) {
            try {
                requestId = Integer.decode(urlParameters.get("id"));
                request = EntityManagerService.provideEntityManager().find(CertificateRequest.class,
                        requestId);
                if (request != null) {
                    ready = true;
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, NO_SUCH_RECORD);
                    log.warn(NO_SUCH_RECORD);
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("id") + INVALID_ID);
                log.warn("'" + urlParameters.get("id") + INVALID_ID, e);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, MISSING_PARAM);
            log.warn(MISSING_PARAM);
        }
    }

    public boolean isReady() {
        return ready;
    }

    public CertificateRequest getRequest() {
        if (request == null && requestId != 0) {
            //if bean is serialized
            request = EntityManagerService.provideEntityManager().find(CertificateRequest.class, requestId);
        }
        return request;
    }

    public String getPublicKey() {
        return request.getCertificatePublicKey().toString();
    }

    public String getPrivateKey() {
        return request.getCertificatePrivateKey().toString();
    }

    public boolean isPrivateKeyAvailable() {
        if (isUserAllowedToSeePrivateKey() && isPrivateKeyLoadable()) {
            return true;
        }
        return false;
    }

    public static String getCertificateViewLink(int certificateId) {
        return certificateViewBaseLink + "?id=" + certificateId;
    }

    public static String getRequestListLink() {
        return requestListLink;
    }

    private boolean isUserAllowedToSeePrivateKey() {
        Identity identity = Identity.instance();
        if (identity != null) {
            if (identity.hasRole("admin_role")) {
                return true;
            }
            if (identity.getPrincipal() != null) {
                String requester = request.getRequesterUsername();
                String requesterCasKey = request.getRequesterCasKey();
                String currentUsername = identity.getPrincipal().getName();
                String currentUserCasKey = UserAttributes.instance().getCasKeyword();
                if (requester != null && requester.equals(currentUsername)
                        && requesterCasKey != null && requesterCasKey.equals(currentUserCasKey)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isPrivateKeyLoadable() {
        try {
            if (request.getCertificatePrivateKey() != null &&
                    request.getCertificatePrivateKey().getKey() != null) {
                return true;
            }
        } catch (CertificateException e) {
            log.error("Failed to load private key", e);
        }
        return false;
    }
}
