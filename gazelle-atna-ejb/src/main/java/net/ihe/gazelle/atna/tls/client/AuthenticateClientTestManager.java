package net.ihe.gazelle.atna.tls.client;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.action.converters.ClientTypeConverter;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.AbstractAuthenticateTestManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.ClientType;
import net.ihe.gazelle.simulators.tls.TlsConnectionRecorder;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Name("authenticateClientTestManager")
@Scope(ScopeType.PAGE)
public class AuthenticateClientTestManager extends AbstractAuthenticateTestManager<TlsClient> {

    private static final long serialVersionUID = -1701303139327626198L;
    private static Logger log = LoggerFactory.getLogger(AuthenticateClientTestManager.class);

    private ClientType clientType = null;
    private Client client = null;
    private String host;
    private Integer port;

    @Override
    public Class<TlsClient> getTlsSimulatorClass() {
        return TlsClient.class;
    }

    @Override
    @Create
    public void initialize() {
        super.initialize();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageIP = params.get("ip");
        String messagePort = params.get("port");
        String messageType = params.get("type");
        if (messageIP != null && messageIP.trim().length() > 0) {
            setHost(messageIP);
        }
        if (messagePort != null && messagePort.trim().length() > 0) {
            try {
                Integer port = Integer.valueOf(messagePort);
                setPort(port);
            } catch (Exception e) {
                log.error("Unable to retrieve port.", e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to retrieve port.");
            }
        }
        if (messageType != null && messageType.trim().length() > 0) {
            ClientTypeConverter converter = new ClientTypeConverter();
            ClientType appType = (ClientType) converter.getAsObject(null, null, messageType.trim());
            if (appType != null) {
                setClientType(appType);
            } else {
                setClientType(ClientType.RAW);
            }
        } else {
            setClientType(ClientType.RAW); // RAW is the default message type value for the ATNA Authenticate Client Test.
        }
    }

    @Override
    public String linkToSimulatorList() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_LIST_CLIENTS.getMenuLink();
    }

    public List<ClientType> getClientTypes() {
        return Arrays.asList(ClientType.values());
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        if (this.clientType != clientType) {
            this.clientType = clientType;
            client = clientType.newClient();
        }
    }

    public void startClient() {
        setSelectedSimulator(EntityManagerService.provideEntityManager().find(TlsClient.class, getSelecteSimulatorId()));
        getSelectedSimulator().loadCertificates();
        TlsConnectionRecorder connectionListener = new TlsConnectionRecorder();
        try {
            getSelectedSimulator().ping(connectionListener, host, port, client);
        } catch (TlsSimulatorException e) {
            log.error("Failed to ping", e);
            StatusMessages.instance().addToControl("buttonStartClient", "Failed to ping (" + e.getMessage() + ")");
        }
        if (connectionListener.getConnectionCount() == 0) {
            StatusMessages.instance().addToControl("buttonStartClient",
                    "No connection recorded, is " + host + ":" + port + " running?", host, port);
        } else {
            StatusMessages.instance().addToControl("buttonStartClient",
                    "Recorded " + connectionListener.getConnectionCount() + " connections.");
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

}
