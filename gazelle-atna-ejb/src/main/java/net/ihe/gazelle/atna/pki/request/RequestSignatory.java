package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.CertificateRequest;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.cert.CertificateException;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
public final class RequestSignatory {

    private static Logger log = LoggerFactory.getLogger(RequestSignatory.class);

    private RequestSignatory() {
        //Used to hide constructor of utility class.
    }

    public static String sign(CertificateRequest request) {

        request.setNotAfter(CertificateUtil.getNotAfter());
        request.setNotBefore(CertificateUtil.getNotBefore());
        request.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);

        try {
            CertificateManager.createCertificate(request, EntityManagerService.provideEntityManager());
        } catch (CertificateException e) {
            String msg = "Failed to create certificate (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
            return null;
        }

        RequestNotification.signed(request);
        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Certificate request validated (subject :" + request.getSubject() + ")");
        return Pages.PKI_LIST_REQUESTS.getMenuLink();
    }

}
