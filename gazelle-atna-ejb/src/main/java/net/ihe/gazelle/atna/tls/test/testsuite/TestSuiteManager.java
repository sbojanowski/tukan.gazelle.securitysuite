package net.ihe.gazelle.atna.tls.test.testsuite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCaseQuery;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestSuite;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("testSuiteManager")
@Scope(ScopeType.PAGE)
public class TestSuiteManager implements Serializable {

    private static final long serialVersionUID = -1054917871336064675L;

    private static Logger log = LoggerFactory.getLogger(TestSuiteManager.class);

    private TlsTestSuite testSuite = new TlsTestSuite();
    private List<TlsTestCase> availableTestCases;
    private List<TlsTestCase> selectedTestCases;

    @Create
    public void initialize() {

        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("testSuite") != null) {
            try {
                testSuite = EntityManagerService.provideEntityManager().find(TlsTestSuite.class,
                        Integer.decode(urlParameters.get("testSuite")));
                if (testSuite != null) {

                    // load object parameters
                    this.selectedTestCases = testSuite.getTestCases();
                    this.availableTestCases = getUnselectedTestCases();

                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "No such test suite recorded in database");
                    log.error("No such test suite recorded in database");
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("testSuite") + "' is not a valid test suite id");
                log.error("'" + urlParameters.get("testSuite") + "' is not a valid test suite id", e);
            }
        } else {
            // TODO default parameters
            this.availableTestCases = getFilteredTlsTestCases();
            this.selectedTestCases = new ArrayList<TlsTestCase>();
        }
    }

    public TlsTestSuite getTestSuite() {
        return testSuite;
    }

    public List<TlsTestCase> getAvailableTestCases() {
        return availableTestCases;
    }

    public void setAvailableTestCases(List<TlsTestCase> availableTestCases) {
        this.availableTestCases = availableTestCases;
    }

    public List<TlsTestCase> getSelectedTestCases() {
        return selectedTestCases;
    }

    public void setSelectedTestCases(List<TlsTestCase> selectedTestCases) {
        this.selectedTestCases = selectedTestCases;
    }

    public SelectItem[] getContextTypeItems() {
        return ContextType.getSelectItems();
    }

    public String saveTestSuite() {

        // load parameters selection into testCase Object.
        testSuite.setTestCases(selectedTestCases);

        try {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(testSuite);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test suite correctly saved");
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Failed to save test suite.");
            log.error("Failed to save test suite.", e);
        }
        return backToList();
    }

    public String backToList() {
        this.testSuite = null;
        return Pages.TLS_LIST_TEST_SUITE.getMenuLink();
    }

    public String resetTlsTestCases() {
        availableTestCases = getFilteredTlsTestCases();
        selectedTestCases = null;
        return null;
    }

    private List<TlsTestCase> getUnselectedTestCases() {
        List<TlsTestCase> unselectedTestCases = getFilteredTlsTestCases();
        unselectedTestCases.removeAll(selectedTestCases);
        return unselectedTestCases;
    }

    private List<TlsTestCase> getFilteredTlsTestCases() {
        TlsTestCaseQuery testCaseQuery = new TlsTestCaseQuery();
        if (testSuite.getContext() != null) {
            testCaseQuery.context().eq(testSuite.getContext());
        }
        List<TlsTestCase> list = testCaseQuery.getList();
        Collections.sort(list, new Comparator<TlsTestCase>() {
            public int compare(TlsTestCase o1, TlsTestCase o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }

}
