package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.pki.request.AbstractRequestView;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestQuery;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.lang.ArrayUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.TransactionRequiredException;
import java.io.IOException;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;

@Name("certificatesListBean")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 2000)
public class CertificatesListBean extends AbstractRequestView
        implements Serializable, QueryModifier<CertificateRequest> {

    private static final long serialVersionUID = -2311259192832083933L;
    private String subjectCertificateAuthority = "CN=myCA,O=Company,OU=Unit,C=FR";

    private static Logger log = LoggerFactory.getLogger(CertificatesListBean.class);

    private transient CertificateDataModel dataModel = null;
    private transient FilterDataModel<CertificateRequest> dataModelRequests;
    private transient Filter<CertificateRequest> filter;

    private boolean displayOnlyNotProcessed = false;

    private Integer certificateToDeleteId;

    private boolean pki;

    public Filter<CertificateRequest> getFilter() {
        if (this.filter == null) {
            this.filter = new Filter<CertificateRequest>(getHqlCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return this.filter;
    }

    private HQLCriterionsForFilter<CertificateRequest> getHqlCriterionsForFilter() {
        CertificateRequestQuery query = new CertificateRequestQuery();
        HQLCriterionsForFilter<CertificateRequest> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("requester", query.requesterUsername());
        criterions.addPath("type", query.certificateExtension());
        criterions.addQueryModifier(this);
        return criterions;
    }

    private enum CertificateFormat {
        DER, PEM;
    }

    public void setPKI(boolean pki) {
        this.pki = pki;
    }

    public void downloadCertDER(int certificateId) {
        downloadCert(certificateId, CertificateFormat.DER);
    }

    public void downloadCertPEM(int certificateId) {
        downloadCert(certificateId, CertificateFormat.PEM);
    }

    public void downloadCert(int certificateId, CertificateFormat certificateFormat) {
        try {
            Certificate certificate = CertificateDAO
                    .getByID(certificateId, EntityManagerService.provideEntityManager());

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            byte[] bytes = null;
            String certificateExtension = null;
            if (certificateFormat == CertificateFormat.PEM) {
                String certificateStr = certificate.getPEM();
                bytes = ArrayUtils.clone(certificateStr.getBytes(CertificateConstants.UTF_8));
                certificateExtension = "pem";
            } else {
                bytes = certificate.getCertificateX509().getX509Certificate().getEncoded();
                certificateExtension = "der";
            }
            ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setContentLength(bytes.length);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + certificateId + "."
                    + certificateExtension + "\"");
            servletOutputStream.write(bytes);
            servletOutputStream.flush();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (CertificateException e) {
            String msg = "Failed to retrieve certificate from database";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        } catch (IOException e) {
            String msg = "Failed to download file";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void setCertificateToDelete(Integer certificateId) {
        certificateToDeleteId = certificateId;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public String deleteCertificateReal() {
        if (certificateToDeleteId != null) {
            try {
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                CertificateDAO.delete(certificateToDeleteId, entityManager);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Certificate deleted");
                certificateToDeleteId = null;
            } catch (IllegalArgumentException e) {
                String message = "Certificate with Id '" + certificateToDeleteId + "' does not exist in database.";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
                log.warn(message, e);
            } catch (PersistenceException e) {
                String message = "Unable to delete certificate #" + certificateToDeleteId + " (referenced by another entity).";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
                log.warn(message, e);
            } catch (TransactionRequiredException e) {
                String message = "Unexpected error, unable to delete certificate. Please contact an administrator";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
                log.error(message, e);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No certificate selected for deletion");
        }
        return Pages.PKI_LIST_CERTIFICATES.getMenuLink();
    }


    public String getSubjectCertificateAuthority() {
        return subjectCertificateAuthority;
    }

    public void setSubjectCertificateAuthority(String subjectCertificateAuthority) {
        this.subjectCertificateAuthority = subjectCertificateAuthority;
    }

    public HibernateDataModel<Certificate> getDataModel() {
        if (dataModel == null || dataModel.getPKI() != pki) {
            dataModel = new CertificateDataModel(pki);
        }
        return dataModel;
    }

    public FilterDataModel<CertificateRequest> getDataModelRequests() {
        if (dataModelRequests == null) {
            dataModelRequests = new FilterDataModel<CertificateRequest>(getFilter()) {
                @Override
                protected Object getId(CertificateRequest t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return dataModelRequests;
    }

    public void resetCertificateRequestFilter() {
        setDisplayOnlyNotProcessed(false);
        this.getFilter().clear();
        this.getDataModelRequests().resetCache();
    }

    public List<CertificateRequest> getRequests() {
        return CertificateDAO.getRequests(EntityManagerService.provideEntityManager());
    }

    public List<Certificate> getCertificates() {
        return CertificateDAO.getCertificates(EntityManagerService.provideEntityManager());
    }

    public String getRequestViewLink(int requestId) {
        return PreferenceService.getString("application_url") +
                Pages.PKI_VIEW_REQUEST.getMenuLink() + "?id=" + requestId;
    }

    public String printCert(Certificate certificate) {
        return CertificateUtil.rnTobr(certificate.toString());
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<CertificateRequest> queryBuilder, Map<String, Object> filterValuesApplied) {
        if (displayOnlyNotProcessed) {
            queryBuilder.addRestriction(HQLRestrictions.isEmpty("certificate"));
        }
    }

    public boolean isDisplayOnlyNotProcessed() {
        return displayOnlyNotProcessed;
    }

    public void setDisplayOnlyNotProcessed(boolean displayOnlyNotProcessed) {
        this.displayOnlyNotProcessed = displayOnlyNotProcessed;
        this.getFilter().modified();
    }

}
