package net.ihe.gazelle.atna;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;

import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;


@MetaInfServices(PreferenceProvider.class)
public class GazelleAtnaPreferenceProvider implements PreferenceProvider {

    private static final String DATE_PATTERN = "YYYYMMDDHHmmss";

    private static Logger log = LoggerFactory.getLogger(GazelleAtnaPreferenceProvider.class);

    /*
     * Note: this method has a natural ordering that is inconsistent with equals.
     */
    @Override
    public int compareTo(PreferenceProvider o) {
        return getWeight().compareTo(o.getWeight());
    }

    @Override
    public Boolean getBoolean(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            return Boolean.valueOf(prefAsString);
        } else {
            return null;
        }
    }

    @Override
    public Date getDate(String arg0) {
        String prefAsString = getString(arg0);
        if (prefAsString != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
                return sdf.parse(prefAsString);
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Integer getInteger(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            try {
                return Integer.decode(prefAsString);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object getObject(Object arg0) {
        return null;
    }

    @Override
    public String getString(String key) {
        return ApplicationConfiguration.getValueOfVariable(key);
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return -100;
        } else {
            return 100;
        }
    }

    @Override
    public void setBoolean(String arg0, Boolean arg1) {
        setString(arg0, arg1.toString());
    }

    @Override
    public void setDate(String arg0, Date arg1) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
            setString(arg0, sdf.format(arg1));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void setInteger(String arg0, Integer arg1) {
        if (arg1 != null) {
            setString(arg0, arg1.toString());
        } else {
            setString(arg0, null);
        }
    }

    @Override
    public void setObject(Object arg0, Object arg1) {

    }

    @Override
    public void setString(String arg0, String arg1) {
        ApplicationConfiguration appConf = ApplicationConfiguration.getApplicationConfigurationByVariable(arg0);
        if (appConf == null) {
            appConf = new ApplicationConfiguration(arg0, arg1);
        } else {
            appConf.setValue(arg1);
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(appConf);
        entityManager.flush();
    }

}
