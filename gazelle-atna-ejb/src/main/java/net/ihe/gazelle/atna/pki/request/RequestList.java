package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.CertificateRequest;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.security.cert.CertificateException;

@Name("requestList")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = RequestList.SYNCHRONIZED_TIMEOUT)
public class RequestList implements Serializable {

    static final long SYNCHRONIZED_TIMEOUT = 2000 ;

    private static final long serialVersionUID = 741213608357348192L;

    private int requestToDeleteId;
    private int requestToRegenerateId;

    private static Logger log = LoggerFactory.getLogger(RequestList.class);

    public RequestList() {
        super();
    }

    public void setRequestToDelete(Integer requestId) {
        requestToDeleteId = requestId;
    }

    public void setRequestToRegenerate(Integer requestId) {
        requestToRegenerateId = requestId;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void deleteSelectedRequest() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        CertificateRequest request = CertificateDAO.getRequestByID(requestToDeleteId, entityManager);
        String subject = request.getSubject();

        entityManager.remove(request);
        entityManager.flush();

        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Certificate request delete (subject :" + subject + ")");
    }

    public void regenerateSelectedRequest() {
        regenerate(requestToRegenerateId);
    }

    public void regenerate(Integer requestId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        CertificateRequest certificateRequest = CertificateDAO.getRequestByID(requestId, entityManager);
        try {
            certificateRequest.setNotAfter(CertificateUtil.getNotAfter());
            certificateRequest.setNotBefore(CertificateUtil.getNotBefore());
            certificateRequest.initRequestDate();
            if (certificateRequest.getCertificateExtension() == CertificateType.CA_KEY_USAGE_ALL) {
                CertificateManager.updateCertificateAuthority(certificateRequest.getCertificate(), entityManager);
            } else {
                CertificateManager.updateCertificate(certificateRequest, entityManager);
            }
        } catch (CertificateException e) {
            String msg = "Failed to update certificate (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
            return;
        }
        RequestNotification.regenerated(certificateRequest);
        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Certificate request (and children) updated (" + certificateRequest.getSubject() + ")");
    }

    public void validate(Integer requestId) {
        CertificateRequest request = EntityManagerService.provideEntityManager().find(
                CertificateRequest.class, requestId);
        RequestSignatory.sign(request);
    }

}
