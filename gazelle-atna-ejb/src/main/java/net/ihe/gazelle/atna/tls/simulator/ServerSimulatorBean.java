package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.server.ServerLauncher;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by cel on 22/09/15.
 */
@Name("serverSimulatorBean")
@Scope(ScopeType.PAGE)
public class ServerSimulatorBean extends AbstractSimulatorBean<TlsServer> {

    private static final long serialVersionUID = 2105789416864306130L;
    private static Logger log = LoggerFactory.getLogger(ServerSimulatorBean.class);

    @Override
    @Create
    public void init() {
        super.init();
        // If it's a simulator creation
        if (!isEditMode()) {
            setCertificateValidator(CertificateValidatorType.TLS_CLIENT);
            setClientAuthenticationRequired(true);
            setChannelType(ChannelType.RAW);
        }
    }

    public boolean isClientAuthenticationRequired() {
        return simulator.isNeedClientAuth();
    }

    public void setClientAuthenticationRequired(boolean clientAuthenticationRequired) {
        simulator.setNeedClientAuth(clientAuthenticationRequired);
    }

    public ChannelType getChannelType() {
        return simulator.getChannelType();
    }

    public void setChannelType(ChannelType channelType) {
        simulator.setChannelType(channelType);
    }

    public Integer getLocalPort() {
        return simulator.getLocalPort();
    }

    public void setLocalPort(Integer localPort) {
        simulator.setLocalPort(localPort);
    }

    public String getRemoteHost() {
        return simulator.getRemoteHost();
    }

    public void setRemoteHost(String remoteHost) {
        simulator.setRemoteHost(remoteHost);
    }

    public Integer getRemotePort() {
        return simulator.getRemotePort();
    }

    public void setRemotePort(Integer remotePort) {
        simulator.setRemotePort(remotePort);
    }

    public List<ChannelType> getChannelTypes() {
        return Arrays.asList(ChannelType.values());
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public String saveAndStart() {
        if (saveSimulator() != null) {
            ServerLauncher.startServer(simulator);
            return navigateToSimulatorView(simulator.getId());
        } else {
            return null;
        }
    }
    @Override
    public Class<TlsServer> getSimulatorClass() {
        return TlsServer.class;
    }

    @Override
    public String getSimulatorDescription() {
        return "TLS Server";
    }

    @Override
    public String linkToSimulatorList() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_LIST_SERVER.getMenuLink();
    }

    @Override
    public String navigateToSimulatorView(Integer id) {
        return Pages.TLS_TEST_SERVER.getMenuLink() + "?simulator=" + id;
    }

    @Override
    public boolean isPeerAuthenticationRequired() {
        return isClientAuthenticationRequired();
    }

    @Override
    public TlsServer newSimulatorInstance() {
        return new TlsServer();
    }

    @Override
    protected void preSave() {
        super.preSave();
        if (isEditMode()) {
            ServerLauncher.stopServer(simulator);
        } else {
            simulator.setRunning(false);
        }
    }


}
