package net.ihe.gazelle.atna.tls.test.testcase;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.test.TlsTestDataSetDisplayer;
import net.ihe.gazelle.atna.tls.test.testinstance.TestInstanceBrowser;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.ClientType;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSet;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstance;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstanceQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Name("testRunManager")
@Scope(ScopeType.PAGE)
public class TestRunManager extends TestInstanceBrowser implements Serializable, TlsTestDataSetDisplayer {

    private static final long serialVersionUID = 5260410226982521923L;

    private static Logger log = LoggerFactory.getLogger(TestRunManager.class);

    private int testCaseId;
    private transient TlsTestCase testCase = new TlsTestCase();
    private int tlsDataSetId;
    private transient TlsTestDataSet tlsDataSet;

    private ClientType clientType = null;
    private Client client = null;
    private String host;
    private Integer port;

    @Create
    public void initialize() {

        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("testCase") != null) {
            try {
                testCaseId = Integer.decode(urlParameters.get("testCase"));
                testCase = EntityManagerService.provideEntityManager().find(TlsTestCase.class, testCaseId);
                if (testCase != null) {

                    setTlsDataSet(testCase.getTlsDataSet());
                    setClientType(ClientType.RAW);

                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "No such test case recorded in database");
                    log.error("No such test case recorded in database");
                    redirectToTestCaseList();
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("testCase") + "' is not a valid test case id");
                log.error("'" + urlParameters.get("testCase") + "' is not a valid test case id", e);
                redirectToTestCaseList();
            }
        } else {
            redirectToTestCaseList();
        }
    }

    public TlsTestCase getTestCase() {
        if (testCase == null) {
            testCase = EntityManagerService.provideEntityManager().find(TlsTestCase.class, testCaseId);
        }
        return testCase;
    }

    private void setTestCase(TlsTestCase testCase) {
        this.testCaseId = testCase.getId();
        this.testCase = testCase;
    }

    public TlsTestDataSet getTlsDataSet() {
        if (tlsDataSet == null) {
            tlsDataSet = EntityManagerService.provideEntityManager().find(TlsTestDataSet.class, tlsDataSetId);
        }
        return tlsDataSet;
    }

    private void setTlsDataSet(TlsTestDataSet tlsDataSet) {
        this.tlsDataSetId = tlsDataSet.getId();
        this.tlsDataSet = tlsDataSet;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        if (this.clientType != clientType) {
            this.clientType = clientType;
            client = clientType.newClient();
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String backToList() {
        return Pages.TLS_LIST_TEST_CASE.getMenuLink();
    }

    public String linkToList() {
        return ApplicationManagerBean.getUrl() + backToList();
    }

    public String navigateRunTestCase() {
        return Pages.TLS_RUN_TEST_CASE.getMenuLink() + "?testCase=" + getTestCase().getId();
    }

    private void redirectToTestCaseList() {
        Redirect redirect = Redirect.instance();
        redirect.setViewId(Pages.TLS_LIST_TEST_CASE.getMenuLink());
        redirect.execute();
    }

    @Override
    public String linkToCertificate() {
        if (getTlsDataSet() != null && getTlsDataSet().getCertificate() != null) {
            return ApplicationManagerBean.getUrl() + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() +
                    "?id=" + getTlsDataSet().getCertificate().getId();
        } else {
            return null;
        }
    }

    public List<ClientType> getClientTypes() {
        return Arrays.asList(ClientType.values());
    }

    public boolean isClientSimulator() {
        if (getTestCase() != null) {
            //if the SUT is a SERVER, then the simulator is a CLIENT.
            if (testCase.getSutType() == SimulatorType.SERVER) {
                return true;
            }
        }
        return false;
    }

    public String runTestCase() {

        if (getTestCase().getSimulator() == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "ERROR, this automated test case does not have any allocated simulator. Please contact an administrator.");
        } else {
            TlsTestInstance testInstance = new TlsTestInstance(getTestCase(), host, port);
            try {
                testInstance.execute(testCase.getSimulator(), client);
            } catch (TlsSimulatorException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to execute test" + e.getCause());
                log.error("Failed to execute test", e);
            }
            save(testInstance, "test instance");
        }

        return null;
    }

    @Override
    public HQLCriterionsForFilter<TlsTestInstance> getHQLCriterionsForFilter() {
        TlsTestInstanceQuery query = new TlsTestInstanceQuery();
        HQLCriterionsForFilter<TlsTestInstance> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("tlsTestCase", query.testCase(), getTestCase(), getTestCase());
        criteria.addPath("runBy", query.lastModifierId());
        criteria.addPath("timestamp", query.lastChanged());
        criteria.addPath("sutHost", query.sutHost());
        criteria.addPath("verdict", query.testVerdict());
        return criteria;
    }

    private void save(Object instance, String instanceType) {
        try {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(instance);
            entityManager.flush();
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to save " + instanceType);
            log.error("Failed to save " + instanceType, e);
        }
    }
}
