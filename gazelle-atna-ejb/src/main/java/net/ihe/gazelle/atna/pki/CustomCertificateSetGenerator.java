package net.ihe.gazelle.atna.pki;

import java.io.Serializable;
import java.security.cert.CertificateException;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateGenerators;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.preferences.PreferenceService;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("customCertificateGenerator")
@Scope(ScopeType.PAGE)
public class CustomCertificateSetGenerator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2938264766180850476L;

    private static Logger log = LoggerFactory.getLogger(CustomCertificateSetGenerator.class);

    @In
    private LocaleSelector localeSelector = new LocaleSelector();

    @In
    private transient EntityManager entityManager;

    private String commonName;
    private String organization;
    private String country;
    private String userName;
    private String userCasKey;
    private Integer cACertificateId = null;
    private boolean validCertificateRequired;
    private boolean selfSignedCertificateRequired;
    private boolean expiredCertificateRequired;
    private boolean unknownCertificateRequired;
    private boolean revokedCertificateRequired;
    private boolean corruptedCertificateRequired;
    private boolean wrongKeyCertificateRequired;

    @Create
    public void initialize() {
        userName = Identity.instance().getCredentials().getUsername();
        userCasKey = UserAttributes.instance().getCasKeyword();
        cACertificateId = PreferenceService.getInteger("certificate_authority_Id");

        if (cACertificateId == null) {
            log.error("Unable to load system CA");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "No system CA define ! Fix it in application preferences or ask an administrator !");
        }

        validCertificateRequired = true;
        selfSignedCertificateRequired = true;
        expiredCertificateRequired = true;
        unknownCertificateRequired = true;
        corruptedCertificateRequired = true;
        revokedCertificateRequired = true;
        wrongKeyCertificateRequired = true;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSystemCASubject() {
        String subject = "None" ;
        if (cACertificateId != null) {
            Certificate cACertificate = CertificateDAO.getByID(cACertificateId, entityManager);
            if(cACertificate != null) {
                subject = cACertificate.getSubject();
            }
        }
        return subject ;
    }

    public boolean isValidCertificateRequired() {
        return validCertificateRequired;
    }

    public void setValidCertificateRequired(boolean validCertificateRequired) {
        this.validCertificateRequired = validCertificateRequired;
    }

    public boolean isSelfSignedCertificateRequired() {
        return selfSignedCertificateRequired;
    }

    public void setSelfSignedCertificateRequired(boolean selfSignedCertificateRequired) {
        this.selfSignedCertificateRequired = selfSignedCertificateRequired;
    }

    public boolean isExpiredCertificateRequired() {
        return expiredCertificateRequired;
    }

    public void setExpiredCertificateRequired(boolean expiredCertificateRequired) {
        this.expiredCertificateRequired = expiredCertificateRequired;
    }

    public boolean isUnknownCertificateRequired() {
        return unknownCertificateRequired;

    }

    public void setUnknownCertificateRequired(boolean unknownCertificateRequired) {
        this.unknownCertificateRequired = unknownCertificateRequired;
    }

    public boolean isRevokedCertificateRequired() {
        return revokedCertificateRequired;
    }

    public void setRevokedCertificateRequired(boolean revokedCertificateRequired) {
        this.revokedCertificateRequired = revokedCertificateRequired;
    }

    public boolean isCorruptedCertificateRequired() {
        return corruptedCertificateRequired;
    }

    public void setCorruptedCertificateRequired(boolean corruptedCertificateRequired) {
        this.corruptedCertificateRequired = corruptedCertificateRequired;
    }

    public boolean isWrongKeyCertificateRequired() {
        return wrongKeyCertificateRequired;
    }

    public void setWrongKeyCertificateRequired(boolean wrongKeyCertificateRequired) {
        this.wrongKeyCertificateRequired = wrongKeyCertificateRequired;
    }

    public SelectItem[] getCountryItems() {
        return CertificateUtil.getCountries(localeSelector.getLocale());
    }

    public String generate() {

        if (cACertificateId != null) {
            boolean isSucceed = true;

            isSucceed = isSucceed
                    && generationBox(isValidCertificateRequired(), new CertificateGenerators.ValidCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isSelfSignedCertificateRequired(),
                    new CertificateGenerators.SelfSignedCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isExpiredCertificateRequired(),
                    new CertificateGenerators.ExpiredCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isRevokedCertificateRequired(),
                    new CertificateGenerators.RevokedCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isUnknownCertificateRequired(),
                    new CertificateGenerators.UnknownCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isCorruptedCertificateRequired(),
                    new CertificateGenerators.CorruptedCertificateGenerator());
            isSucceed = isSucceed
                    && generationBox(isWrongKeyCertificateRequired(),
                    new CertificateGenerators.WrongKeyCertificateGenerator());

            if (isSucceed) {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Certificates generated.");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Generation disable. Define a Certificate Authority to the system first.");
        }

        return null;
    }

    private boolean generationBox(boolean isRequired, CertificateGenerators.CertificateGenerator generator) {

        String msg;
        boolean isSucceed = true;
        Certificate cACertificate ;

        if (isRequired) {
            try {
                cACertificate = CertificateDAO.getByID(cACertificateId, entityManager);
                generator.generate(commonName, organization, country, userName, userCasKey, cACertificate, entityManager);
            } catch (CertificateException e) {
                msg = "Unable to generate a " + generator.getSubject() + "certificate for " + commonName + ", "
                        + organization + ", " + country + " with CA " + getSystemCASubject();
                log.error(msg, e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
                isSucceed = false;
            }
        }
        return isSucceed;
    }

}
