package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.pki.validator.CertificateValidatorErrorTrace;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cedric Eoche-Duval, Kereval 2015
 */
@Name("certificateValidationBean")
@Scope(ScopeType.PAGE)
public class CertificateValidationBean implements Serializable {

    private static final long serialVersionUID = -6309190285450761261L;
    private static Logger log = LoggerFactory.getLogger(CertificateValidationBean.class);

    private byte[] validatePem;
    private ContextType context;
    private CertificateValidatorType certificateValidatorType;
    private boolean checkRevocation;
    private ArrayList<String> warningResults;
    private ArrayList<String> errorResults;
    private boolean displayResults;

    @Create
    public void init() {
        validatePem = null;
        context = null;
        certificateValidatorType = null;
        checkRevocation = false;
        initResults();
    }

    public byte[] getValidatePem() {
        return validatePem.clone();
    }

    public void setValidatePem(byte[] validatePem) {
        this.validatePem = validatePem.clone();
    }

    public void uploadListener(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        setValidatePem(item.getData());
    }

    public ContextType getContext() {
        return context;
    }

    public void setContext(ContextType context) {
        this.context = context;
    }

    public CertificateValidatorType getCertificateValidatorType() {
        return certificateValidatorType;
    }

    public void setCertificateValidatorType(CertificateValidatorType certificateType) {
        this.certificateValidatorType = certificateType;
    }

    public boolean isCheckRevocation() {
        return checkRevocation;
    }

    public void setCheckRevocation(boolean checkRevocation) {
        this.checkRevocation = checkRevocation;
    }

    public ArrayList<String> getWarningResults() {
        return warningResults;
    }

    public ArrayList<String> getErrorResults() {
        return errorResults;
    }

    public boolean isDisplayResults() {
        return displayResults;
    }

    public SelectItem[] getContextTypeItems() {
        return ContextType.getSelectItems();
    }

    public SelectItem[] getCertificateValidatorTypeItems() {
        return CertificateValidatorType.getSelectItems(context);
    }

    public void resetCertificateValidatorType() {
        certificateValidatorType = null;
    }

    public String validate() {
        initResults();
        if (certificateValidatorType != null) {
            List<X509Certificate> certificates = uploadCertificates(validatePem);

            if (certificates != null) {
                CertificateValidatorResult result = certificateValidatorType.validate(certificates,
                        EntityManagerService.provideEntityManager(), checkRevocation);

                if (result.getWarnings().size() > 0) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN,
                            "Warnings during certificates validation.");
                    for (CertificateValidatorErrorTrace warn : result.getWarnings()) {
                        warningResults.add(warn.getCleanMessage());
                    }
                }

                if (result.getResult().equals(CertificateValidatorResultEnum.FAILED)) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Certificate validation failed");
                    for (CertificateValidatorErrorTrace error : result.getErrors()) {
                        errorResults.add(error.getCleanMessage());
                    }
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "Certificate validation passed");
                }
                displayResults = true;

            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to load PEM file content");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please select a validator");
        }
        return null;
    }

    private List<X509Certificate> uploadCertificates(byte[] pemFile) {
        try {
            List<X509Certificate> certificates = CertificateUtil.loadCertificates(new String(pemFile));
            return certificates;
        } catch (CertificateException e) {
            log.warn("Unable to load user pem file");
            return null;
        }
    }

    private void initResults() {
        warningResults = new ArrayList<String>();
        errorResults = new ArrayList<String>();
        displayResults = false;
    }

}
