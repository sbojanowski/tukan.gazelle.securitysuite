package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorKeywordGenerator;
import net.ihe.gazelle.simulators.tls.enums.CertificateScope;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by cel on 18/09/15.
 */
public abstract class AbstractSimulatorBean<T extends TlsSimulator> implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractSimulatorBean.class);

    @RequestParameter("id")
    private Integer id;
    @In
    EntityManager entityManager;

    protected T simulator;
    private CertificateScope certificateScope;

    public void init() {
        if (getId() != null && getId() != 0) {
            simulator = entityManager.find(getSimulatorClass(), getId());
            // Lazy loading
            simulator.getIssuers().size();
        }
        if (simulator == null) {
            setCertificateScope(CertificateScope.PKI);
            simulator = newSimulatorInstance();
            simulator.setKeyword(TlsSimulatorKeywordGenerator.getValidRandomKeyword(getSimulatorClass()));
            simulator.setProtocols(new HashSet<ProtocolType>(Arrays.asList(ProtocolType.TLSv12)));
            simulator.setCipherSuites(new HashSet<CipherSuiteType>(Arrays.asList(CipherSuiteType.TLS_RSA_WITH_AES_128_CBC_SHA)));
            simulator.setEnabled(true);
        } else {
            setCertificateScope(simulator.getCertificate().isCustom() ? CertificateScope.CUSTOM : CertificateScope.PKI);
        }
    }

    protected Integer getId() {
        return id;
    }

    public String getKeyword() {
        return simulator.getKeyword();
    }

    public void setKeyword(String keyword) {
        simulator.setKeyword(keyword);
    }

    public CertificateScope getCertificateScope() {
        return certificateScope;
    }

    public void setCertificateScope(CertificateScope certificateScope) {
        this.certificateScope = certificateScope;
    }

    public Certificate getCertificate() {
        return simulator.getCertificate();
    }

    public void setCertificate(Certificate certificate) {
        simulator.setCertificate(certificate);
    }

    public boolean isUseCACerts() {
        return simulator.isUseCACerts();
    }

    public void setUseCACerts(boolean useCACerts) {
        simulator.setUseCACerts(useCACerts);
    }

    public List<Certificate> getIssuers() {
        return new ArrayList<>(simulator.getIssuers());
    }

    public void setIssuers(List<Certificate> issuers) {
        simulator.setIssuers(new HashSet<Certificate>(issuers));
    }

    public CertificateValidatorType getCertificateValidator() {
        return simulator.getValidator();
    }

    public void setCertificateValidator(CertificateValidatorType certificateValidator) {
        simulator.setValidator(certificateValidator);
    }

    public boolean isCheckRevocation() {
        return simulator.isCheckRevocation();
    }

    public void setCheckRevocation(boolean checkRevocation) {
        simulator.setCheckRevocation(checkRevocation);
    }

    public List<ProtocolType> getProtocols() {
        return new ArrayList<>(simulator.getProtocols());
    }

    public void setProtocols(List<ProtocolType> protocols) {
        simulator.setProtocols(new HashSet<ProtocolType>(protocols));
    }

    public List<CipherSuiteType> getCipherSuites() {
        return new ArrayList<>(simulator.getCipherSuites());
    }

    public void setCipherSuites(List<CipherSuiteType> cipherSuites) {
        simulator.setCipherSuites(new HashSet<CipherSuiteType>(cipherSuites));
    }

    public boolean isEditMode() {
        return simulator.getId() != null && simulator.getId() != 0;
    }

    public abstract Class<T> getSimulatorClass();

    public abstract String getSimulatorDescription();

    public abstract T newSimulatorInstance();

    public abstract boolean isPeerAuthenticationRequired();

    @Restrict("#{s:hasRole('admin_role')}")
    public String saveSimulator() {
        preSave();
        try {
            simulator = entityManager.merge(simulator);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, getSimulatorDescription() + " correctly saved");
            return navigateToSimulatorView(simulator.getId());
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to create " + getSimulatorDescription() + " (" + e.getMessage() + ")");
            LOG.error("Failed to create " + getSimulatorDescription(), e);
            return null;
        }
    }

    public abstract String linkToSimulatorList();

    public abstract String navigateToSimulatorView(Integer id);

    public SelectItem[] getCertificateScopes() {
        return getSelectItems(CertificateScope.values());
    }

    public SelectItem[] getAvailableCertificates() {
        List<Certificate> availableCertificates;
        availableCertificates = CertificateDAO.getCertificatesForSigningBySource(
                EntityManagerService.provideEntityManager(),
                null, getCertificateScope() == CertificateScope.PKI);
        subjectCertificateSort(availableCertificates);
        return convertCertificateListToSelectItemArray(availableCertificates);
    }

    public SelectItem[] getAvailableIssuers() {
        List<Certificate> availableIssuers;
        availableIssuers = CertificateDAO.getCertificateAuthorities(EntityManagerService.provideEntityManager(), false);
        subjectCertificateSort(availableIssuers);
        return convertCertificateListToSelectItemArray(availableIssuers);

    }

    public SelectItem[] getCertificateValidators() {
        return getSelectItems(new Object[]{CertificateValidatorType.TLS_SERVER, CertificateValidatorType.TLS_CLIENT});
    }

    public SelectItem[] getAvailableCipherSuites() {
        return getSelectItems(CipherSuiteType.values());
    }

    public SelectItem[] getAvailableProtocols() {
        return getSelectItems(ProtocolType.values());
    }

    protected void preSave() {
        if (simulator.isUseCACerts()) {
            simulator.setIssuers(null);
        }
    }

    protected static SelectItem[] getSelectItems(Object[] items) {
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

    protected static void subjectCertificateSort(List<Certificate> certificateList) {
        Collections.sort(certificateList, new Comparator<Certificate>() {
            public int compare(Certificate o1, Certificate o2) {
                return o1.getSubject().compareTo(o2.getSubject());
            }
        });
    }

    protected static SelectItem[] convertCertificateListToSelectItemArray(List<Certificate> certificateList) {
        List<SelectItem> result = new ArrayList<SelectItem>();
        for (Certificate certificate : certificateList) {
            result.add(new SelectItem(certificate, certificate.getSubject() + " (#" + certificate.getId() + ")"));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public boolean hasConnections() {
        return simulator.getConnections().size() > 0;
    }

}
