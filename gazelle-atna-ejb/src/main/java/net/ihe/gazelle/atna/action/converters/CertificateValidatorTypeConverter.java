package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.pki.validator.CertificateValidatorType;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("certificateValidatorTypeConverter")
@BypassInterceptors
@Converter(forClass = CertificateValidatorType.class)
public class CertificateValidatorTypeConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CertificateValidatorType[] values = CertificateValidatorType.values();
        for (CertificateValidatorType certificateValidatorType : values) {
            if (certificateValidatorType.toString().equals(value)) {
                return certificateValidatorType;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
