package net.ihe.gazelle.atna.tls.server;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.AbstractAuthenticateTestManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

@Name("authenticateServerTestManager")
@Scope(ScopeType.PAGE)
public class AuthenticateServerTestManager extends AbstractAuthenticateTestManager<TlsServer> {

    private static final long serialVersionUID = 2054156132026744409L;

    private boolean editMode = false;

    @Override
    public Class<TlsServer> getTlsSimulatorClass() {
        return TlsServer.class;
    }

    @Override
    public String linkToSimulatorList() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_LIST_SERVER.getMenuLink();
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isForwardReachable() {
        if (getSelectedSimulator().isEnabled()) {
            return SimulatorForward.isReachable(getSelectedSimulator());
        } else {
            return false;
        }
    }

    public void clearForwardCache() {
        SimulatorForward.reset();
    }

    public void startServer() {
        ServerLauncher.startServer(getSelectedSimulator());
        retrieveSimulator(getSelectedSimulator().getId()); //reload simulator
    }

    public void stopServer() {
        ServerLauncher.stopServer(getSelectedSimulator());
        retrieveSimulator(getSelectedSimulator().getId()); //reload simulator
    }

    public void saveServer() {
        saveSimulator();
        SimulatorForward.reset();
        setEditMode(false);
    }

    public void saveAndRestartServer() {
        ServerLauncher.stopServer(getSelectedSimulator());
        saveSimulator();
        ServerLauncher.startServer(getSelectedSimulator());
        retrieveSimulator(getSelectedSimulator().getId());
        SimulatorForward.reset();
        setEditMode(false);
    }

    public void cancelEdit() {
        retrieveSimulator(getSelectedSimulator().getId());
        setEditMode(false);
    }

    private void saveSimulator() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(getSelectedSimulator());
        entityManager.flush();
    }
}
