package net.ihe.gazelle.atna.pki.wsval;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.validator.CertificateValidatorErrorTrace;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("CertificateValidator")
@WebService(name = "CertificateValidator", serviceName = "CertificateValidatorService", portName = "CertificateValidatorPort", targetNamespace = "http://ws.pki.action.atna.gazelle.ihe.net")
@GenerateInterface("CertificateValidatorLocal")
public class CertificateValidator implements CertificateValidatorLocal {

    // @WebMethod
    // public List<CertificateValidatorTypeDescription> getTypes() {
    // System.out.println(CertificateValidatorTypeDescription.class.getName());
    // return CertificateValidatorTypeDescription.getTypes();
    // }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. Its aim is to validate PEM certificates.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    @Override
    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
            throws SOAPException {
        List<String> res = new ArrayList<String>();
        if (null == descriminator){
            descriminator = "";
        }
        for (CertificateValidatorType val : CertificateValidatorType.values()) {
            if (val.getContext().toString().toUpperCase().contains(descriminator.toUpperCase())) {
                res.add(val.toString());
            }
        }
        return res;
    }

    @Override
    @WebMethod
    @WebResult(name = "DetailedResult")
    public CertificateValidatorResult validate(
            @WebParam(name = "certificatesInPEMFormat") String certificatesInPEMFormat,
            @WebParam(name = "type") String type, @WebParam(name = "checkRevocation") boolean revocation) {
        List<X509Certificate> certificates;
        CertificateValidatorType realType = null;
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        try {
            certificates = CertificateUtil.loadCertificates(certificatesInPEMFormat);
            try {
                realType = CertificateValidatorType.getValidatorTypeByFriendlyName(type);
                if(null == realType){
                    throw new CertificateException("Invalid type");
                }
            } catch (IllegalArgumentException e) {
                throw new CertificateException("Invalid type " + type);
            } catch (NullPointerException e) {
                throw new CertificateException("Null type");
            }
        } catch (CertificateException e) {
            CertificateValidatorResult certificateValidationResult = new CertificateValidatorResult();
            certificateValidationResult.getErrors().add(new CertificateValidatorErrorTrace(e));
            certificateValidationResult.setResult(CertificateValidatorResultEnum.FAILED);
            return certificateValidationResult;
        }
        return realType.validate(certificates, entityManager, revocation);
    }

}
