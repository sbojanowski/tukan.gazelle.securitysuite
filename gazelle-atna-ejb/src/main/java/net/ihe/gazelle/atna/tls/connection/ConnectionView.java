package net.ihe.gazelle.atna.tls.connection;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.action.datamodels.MessagesDataModel;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.StringWrapper;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.SimulatorDAO;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("connectionView")
@Scope(ScopeType.PAGE)
public class ConnectionView implements Serializable {

    private static final long serialVersionUID = 1073586335512430899L;
    private static Logger log = LoggerFactory.getLogger(ConnectionView.class);

    private static final ThreadLocal<Integer> CONNECTION_ID = new ThreadLocal<Integer>();

    private static final int WRAPPER_SCREEN_WIDTH = 55;
    private static final int WRAPPER_FIRST_INDENT = 0;
    private static final int WRAPPER_INDENT = 0;
    private static final String WRAPPER_SPACE = " ";
    private static final String WRAPPER_SECOND_NEW_LINE = "<br />";
    private static final String FAILED_TO_DISPLAY_CERTIFICATE = "Failed to display certificate";

    @In
    private transient EntityManager entityManager;

    private TlsConnection connection;
    private List<X509Certificate> peerCertificates;
    private List<Certificate> certificates;
    private MessagesDataModel connectionMessages;

    public ConnectionView() {
        super();
    }

    @Create
    public void retrieveConnection() throws IllegalArgumentException {
        Integer id = CONNECTION_ID.get();
        String message;

        try {
            if (id == null) {
                Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
                        .getRequestParameterMap();
                String messageId = params.get("id");
                if (!(messageId == null || messageId.trim().length() < 1)) {
                    id = Integer.valueOf(messageId);
                } else {
                    message = "Failed to load connection, no id provided";
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, message);
                    throw new IllegalArgumentException(message);
                }
            }
            connection = SimulatorDAO.getConnectionByID(id, entityManager);
        } catch (Exception e) {
            connection = null;
            message = "Failed to load connection (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
            throw new IllegalArgumentException(message, e.getCause());
        }

        if (connection != null) {
            loadConnectionMessages(connection);
            loadConnectionCertificates(connection);
        } else {
            message = "No connection found for id #'" + id + "'";
            FacesMessages.instance().add(StatusMessage.Severity.WARN, message);
            throw new EntityNotFoundException(message);
        }
    }

    public static String getPermanentLink(TlsConnection tlsConnection) {
        String link = "";
        if (tlsConnection != null) {
            link = ApplicationManagerBean.getUrl() + Pages.TLS_VIEW_CONNECTION.getMenuLink() +
                    "?id=" + tlsConnection.getId();
        }
        return link;
    }

    public String getExceptionMessageLabel() {
        if (getConnection().isSuccess()) {
            return "Warning message";
        } else {
            return ResourceBundle.instance().getString("net.ihe.gazelle.atna.ErrorMessage");
        }
    }

    public String getExceptionMessageStyleClass() {
        if (getConnection().isSuccess()) {
            return "gzl-alert gzl-alert-orange";
        } else {
            return "gzl-alert gzl-alert-red";
        }
    }

    private void loadConnectionMessages(TlsConnection connection) {
        String message;
        try {
            connectionMessages = new MessagesDataModel(connection.getProxyConnectionUuid());
            return;
        } catch (Exception e) {
            connectionMessages = null;
            message = "Failed to load messages for connection #" + connection.getId() + " (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.WARN, message);
            log.warn(message, e);
            return;
        }
    }

    private void loadConnectionCertificates(TlsConnection connection) {
        String message;
        try {
            peerCertificates = CertificateUtil.loadCertificates(connection.getPeerCertificateChainPEM());
            certificates = parseCertificates(peerCertificates);
            return;
        } catch (CertificateException e) {
            peerCertificates = null;
            certificates = null;
            message = "Unable to retrieve peer certificates for connection #" + connection
                    .getId() + " from the application database.";
            if (!e.getMessage().contains("No certificate found")) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, message);
                log.warn(message, e);
            }
            return;
        }
    }

    public MessagesDataModel getConnectionMessages() {
        return connectionMessages;
    }

    public boolean isThereConnectionMessages() {
        return connectionMessages.size() > 0;
    }

    private List<Certificate> parseCertificates(List<X509Certificate> x509Certificates) throws CertificateException {
        List<Certificate> result = new ArrayList<Certificate>(x509Certificates.size());
        for (X509Certificate peerCertificate : x509Certificates) {
            Certificate certificate = CertificateDAO.getUniqueBySubject(peerCertificate.getSubjectDN().toString(),
                    entityManager);
            if (certificate != null) {
                PublicKey dbPublicKey = certificate.getPublicKey().getKey();
                PublicKey publicKey = peerCertificate.getPublicKey();
                if (!publicKey.equals(dbPublicKey)) {
                    certificate = null;
                }
            }
            if (certificate == null) {
                certificate = new Certificate();
                certificate.setId(null);
                certificate.setSubject(peerCertificate.getSubjectDN().getName());
                certificate.getCertificateX509().setX509Certificate(peerCertificate);
            }
            result.add(certificate);
        }
        return result;
    }

    public TlsConnection getConnection() {
        return connection;
    }

    public void setConnection(TlsConnection connection) {
        this.connection = connection;
    }

    public String redirectToCertificate(int id) {
        return "/details/cert.seam?id=" + id;
    }

    public String redirectToCertificate() {
        if (connection.getSimulator() != null && connection.getSimulator().getCertificate() != null) {
            return redirectToCertificate(connection.getSimulator().getCertificate().getId());
        }
        return null;
    }

    public String escape(String toEscape) {
        return CertificateUtil.rnTobr(toEscape);
    }

    public String escapePdf(String toEscape) {
        String result = toEscape.replaceAll(",", ", ");
        StringBuffer sb = new StringBuffer(result);

        result = StringWrapper.wrap(sb, WRAPPER_SCREEN_WIDTH, WRAPPER_FIRST_INDENT, WRAPPER_INDENT,
                CertificateConstants.NEW_LINE, false, WRAPPER_SPACE).toString();

        result = result.replaceAll(CertificateConstants.NEW_LINE, WRAPPER_SECOND_NEW_LINE)
                .replaceAll("\r", WRAPPER_SECOND_NEW_LINE)
                .replaceAll("\n", WRAPPER_SECOND_NEW_LINE);
        result = "<span>" + result.replaceAll(WRAPPER_SECOND_NEW_LINE, "</span><span>") + "</span>";

        return result;
    }

    public String getError() {
        if (connection != null && connection.getExceptionStackTrace() != null) {
            return escape(connection.getExceptionStackTrace());
        } else {
            return "";
        }
    }

    public String getErrorPdf() {
        if (connection != null && connection.getExceptionStackTrace() != null) {
            return escapePdf(connection.getExceptionStackTrace());
        } else {
            return "";
        }
    }

    public List<X509Certificate> getPeerCertificates() {
        return peerCertificates;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public boolean isPEM(String value) {
        try {
            CertificateUtil.loadCertificates(value);
            return true;
        } catch (CertificateException e) {
            return false;
        }
    }

    public List<Certificate> parseCertificates(String value) {
        try {
            List<X509Certificate> x509certificates = CertificateUtil.loadCertificates(value);
            return parseCertificates(x509certificates);
        } catch (CertificateException e) {
            log.error("Failed to parse certificates", e);
        }
        return null;
    }

    public String getAsString(Certificate cert) {
        try {
            return escape(cert.getCertificateX509().getX509Certificate().toString());
        } catch (CertificateException e) {
            log.error(FAILED_TO_DISPLAY_CERTIFICATE, e);
            return FAILED_TO_DISPLAY_CERTIFICATE;
        }
    }

    public String getAsStringPdf(Certificate cert) {
        try {
            return escapePdf(cert.getCertificateX509().getX509Certificate().toString());
        } catch (CertificateException e) {
            log.error(FAILED_TO_DISPLAY_CERTIFICATE, e);
            return FAILED_TO_DISPLAY_CERTIFICATE;
        }
    }

    public String getChain() {
        if (connection.getPeerCertificateChainPEM() != null) {
            return CertificateUtil.rnTobr(connection.getPeerCertificateChainPEM());
        }
        return "";
    }

    public String redirectToSimulator() {
        if (connection.getSimulator() instanceof TlsClient) {
            return "/client/test.seam?simulator=" + connection.getSimulator().getId();
        } else {
            return "/server/test.seam?simulator=" + connection.getSimulator().getId();
        }
    }

}
