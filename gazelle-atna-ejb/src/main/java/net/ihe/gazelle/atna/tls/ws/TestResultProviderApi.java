package net.ihe.gazelle.atna.tls.ws;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Local
@Path("/testResult")
public interface TestResultProviderApi {

    @GET
    @Path("/connection/{id}")
    @Produces("text/plain")
    public Response getConnectionTestResult(@PathParam("id") String id);

    @GET
    @Path("/instance/{id}")
    @Produces("text/plain")
    public Response getTestInstanceResult(@PathParam("id") String id);

}
