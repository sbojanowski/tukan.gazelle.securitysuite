package net.ihe.gazelle.atna.pki.wsval;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.pki.validator.CertificateValidatorType;

public class CertificateValidatorTypeDescription {

    private static final CertificateValidatorType[] CERTIFICATE_VALIDATOR_TYPES = new CertificateValidatorType[]{
            CertificateValidatorType.EPSOS_VPN_V3, CertificateValidatorType.EPSOS_TLS_V3,
            CertificateValidatorType.EPSOS_NCP_V3, 
//            CertificateValidatorType.EPSOS_PPT_VPN,
//            CertificateValidatorType.EPSOS_PPT_TLS, CertificateValidatorType.EPSOS_PPT_OBJECT_SIGNING,
            CertificateValidatorType.EPSOS_NCP_SIGNATURE, CertificateValidatorType.EPSOS_OCSP_RESPONDER,
            CertificateValidatorType.EPSOS_SERVICE_CONSUMER, CertificateValidatorType.EPSOS_SERVICE_PROVIDER,
            CertificateValidatorType.EPSOS_VPN_CLIENT, CertificateValidatorType.EPSOS_VPN_SERVER
            // , CertificateValidatorType.TLS_CLIENT,CertificateValidatorType.TLS_SERVER
    };

    private String type;

    private String description;

    public static List<CertificateValidatorTypeDescription> getTypes() {
        List<CertificateValidatorTypeDescription> result = new ArrayList<CertificateValidatorTypeDescription>(
                CERTIFICATE_VALIDATOR_TYPES.length);

        for (CertificateValidatorType certificateValidatorType : CERTIFICATE_VALIDATOR_TYPES) {
            result.add(new CertificateValidatorTypeDescription(certificateValidatorType.name(),
                    certificateValidatorType.toString()));
        }
        return result;
    }

    public CertificateValidatorTypeDescription() {
        super();
    }

    public CertificateValidatorTypeDescription(String type, String description) {
        super();
        this.type = type;
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
