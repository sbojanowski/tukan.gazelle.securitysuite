package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Name("certificateBean")
@Scope(ScopeType.PAGE)
public class CertificateBean implements Serializable {

    private static final long serialVersionUID = 949028971509571953L;
    private static Logger log = LoggerFactory.getLogger(CertificateBean.class);

    private byte[] pkdata;
    private String pkpassword;
    private String generateAlias = "tomcat";
    private String generatePassword = "password";

    private int certId;
    private transient Certificate cert;

    @In
    private Identity identity;

    public CertificateBean() {
        super();
    }

    public void initialize() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageId = params.get("id");
        if (messageId == null || messageId.trim().length() < 1) {
            log.error("Unable to load certificate, no id defined");
            return;
        }
        try {
            loadCertificate(Integer.valueOf(messageId));
        } catch (NumberFormatException e) {
            String msg = "Failed to load certificate, '" + messageId + "' is not an Id.";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public int getCertId() {
        return certId;
    }

    public void setCertId(int certId) {
        this.certId = certId;
        loadCertificate(certId);
    }

    public Certificate getCert() throws EntityNotFoundException {
        if (cert == null) {
            loadCertificate(certId);
        }
        return cert;
    }

    public void setCert(Certificate cert) {
        this.cert = cert;
        this.certId = cert.getId();
    }

    /**
     * Get url to the certificate page for an "action" attribute in a commandButton or a commandLink element.
     *
     * @param certificateId the certificate id
     * @return url to the certificate page for an "action" attribute in a commandButton or a commandLink element.
     */
    public String redirectToCert(Integer certificateId) {
        return Pages.PKI_VIEW_CERTIFICATE.getMenuLink() + "?id=" + certificateId;
    }

    /**
     * Get url to the certificate page as a permanent link or for the "value" attribute in an outputLink element.
     *
     * @param certificateId the certificate id
     * @return url to the certificate page as a permanent link or for the "value" attribute in an outputLink element.
     */
    public String permanentLinkToCert(int certificateId) {
        return ApplicationManagerBean.getUrl() + redirectToCert(certificateId);
    }

    public boolean isPrivateKeyAvailable() {
        boolean nullPrivateKey = true;
        try {
            if (getCert().getPrivateKey() != null) {
                if (getCert().getPrivateKey().getKey() != null) {
                    nullPrivateKey = false;
                }
            }
        } catch (CertificateException e) {
            nullPrivateKey = true;
            log.error("Failed to load private key", e);
        }

        if (nullPrivateKey) {
            return false;
        }

        if (identity != null) {
            if (identity.hasRole("admin_role")) {
                return true;
            }
            CertificateRequest request = getCert().getRequest();
            if (request != null) {
                if (identity.getPrincipal() != null) {
                    String requester = request.getRequesterUsername();
                    String requesterCasKey = request.getRequesterCasKey();
                    String currentUsername = identity.getPrincipal().getName();
                    String currentUserCasKey = UserAttributes.instance().getCasKeyword();
                    if (requester != null && requester.equals(currentUsername)
                            && requesterCasKey != null && requesterCasKey.equals(currentUserCasKey)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String getHtmlPublicKey() {
        return CertificateUtil.rnTobr(getCert().getPublicKey().toString());
    }

    public String getHtmlPrivateKey() {
        return CertificateUtil.rnTobr(getCert().getPrivateKey().toString());
    }

    public String getPublicKey() {
        return cert.getPublicKey().toString();
    }

    public String getPrivateKey() {
        return cert.getPrivateKey().toString();
    }

    public String getChain() {
        String result = "";
        List<X509Certificate> chain;
        try {
            chain = getCert().getChain();
            result = CertificateUtil.getChain(chain);
        } catch (CertificateException e) {
            result = "Failed to load chain";
        }
        return result;
    }

    public String getHtmlChain() {
        return CertificateUtil.rnTobr(getChain());
    }

    public byte[] getPkdata() {
        return pkdata.clone();
    }

    public void setPkdata(byte[] pkdata) {
        this.pkdata = pkdata.clone();
    }

    public String getPkpassword() {
        return pkpassword;
    }

    public void setPkpassword(String pkpassword) {
        this.pkpassword = pkpassword;
    }

    public String getGeneratePassword() {
        return generatePassword;
    }

    public void setGeneratePassword(String generatePassword) {
        this.generatePassword = generatePassword;
    }

    public String getGenerateAlias() {
        return generateAlias;
    }

    public void setGenerateAlias(String generateAlias) {
        this.generateAlias = generateAlias;
    }

    public void downloadP12() {
        try {
            CertificateDownloader.downloadP12(getCert().getId(), generatePassword,
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to download keystore for certificate id " + getCert().getId() + ".";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public void downloadJKS() {
        try {
            CertificateDownloader.downloadJKS(getCert().getId(), generatePassword, generateAlias,
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to download keystore for certificate id " + getCert().getId() + ".";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public void downloadDER() {
        try {
            CertificateDownloader.downloadDER(getCert().getId(),
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to download certificate (id " + getCert().getId() + ").";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public void downloadPEM() {
        try {
            CertificateDownloader.downloadPEM(getCert().getId(),
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to download certificate (id " + getCert().getId() + ").";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public void downloadKEY() {
        try {
            CertificateDownloader.downloadKEY(getCert().getId(),
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to download private key for certificate id " + getCert().getId() + ".";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public void generateP12() {
        PrivateKey privateKey = loadPrivateKeyFromFile();
        if (privateKey != null) {
            try {
                CertificateDownloader.generateP12(getCert().getId(), privateKey, generatePassword,
                        EntityManagerService.provideEntityManager(), FacesContext.getCurrentInstance());
            } catch (Exception e) {
                String msg = "Failed to download keystore for certificate id " + getCert().getId() + ".";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
                log.error(msg, e);
            }
        }
    }

    public void generateJKS() {
        PrivateKey privateKey = loadPrivateKeyFromFile();
        if (privateKey != null) {
            try {
                CertificateDownloader.generateJKS(getCert().getId(), privateKey, generatePassword, generateAlias,
                        EntityManagerService.provideEntityManager(), FacesContext.getCurrentInstance());
            } catch (Exception e) {
                String msg = "Failed to download keystore for certificate id " + getCert().getId() + ".";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
                log.error(msg, e);
            }
        }
    }

    public void downloadCACert() {
        initialize();
        try {
            CertificateDownloader.installCACertInBrowser(getCert().getId(),
                    EntityManagerService.provideEntityManager(),
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to install certificate authority #" + getCert().getId() + ".";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

    public SelectItem[] getCertificateExtensions() {
        List<CertificateType> list = Arrays.asList(CertificateType.values());
        List<SelectItem> result = new ArrayList<SelectItem>();
        for (CertificateType certificateType : list) {
            if (displayType(certificateType)) {
                result.add(new SelectItem(certificateType, certificateType.getFriendlyName()));
            }
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    private void loadCertificate(int id) throws EntityNotFoundException {
        certId = id;
        cert = CertificateDAO.getByID(certId, EntityManagerService.provideEntityManager());
        try {
            cert.getPrivateKey().getKey();
            cert.getPublicKey().getKey();
            cert.getChainAsArray();
        } catch (NullPointerException e) {
            String msg = "Unable to load certificate for Id '" + certId + "'.";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            throw new EntityNotFoundException(msg);
        } catch (CertificateException e) {
            String msg = "An error has occured during certificate loading (Id '" + certId + "')";
            FacesMessages.instance().add(StatusMessage.Severity.WARN, msg, e);
            log.warn(msg, e);
        }
    }

    private boolean displayType(CertificateType certificateType) {
        if (identity != null) {
            if (identity.hasRole("admin_role")) {
                return true;
            }
        }
        if (certificateType.isAlwaysVisible()) {
            return true;
        }
        return false;
    }

    private KeyPair retrieveKeyPEM() throws IOException {
        if (pkdata != null) {

            Reader reader = null;
            PEMReader pemReader = null;
            KeyPair key = null;
            IOException ioe = null;

            if (pkpassword == null) {
                pkpassword = "";
            }

            try {
                reader = new InputStreamReader(new ByteArrayInputStream(pkdata), StandardCharsets.UTF_8);
                pemReader = new PEMReader(reader, new PasswordFinder() {
                    @Override
                    public char[] getPassword() {
                        return pkpassword.toCharArray();
                    }
                });
                key = (KeyPair) pemReader.readObject();
                pemReader.close();
                reader.close();
            } finally {
                IOUtils.closeQuietly(pemReader);
                IOUtils.closeQuietly(reader);
            }

            return key;

        } else {
            throw new IOException("No key file provided");
        }
    }

    private PrivateKey loadPrivateKeyFromFile() {
        PrivateKey result = null;
        try {
            KeyPair keyPair = retrieveKeyPEM();
            if (!keyPair.getPublic().equals(cert.getPublicKey().getKey())) {
                String msg = "Invalid private key, not matching with public key from certificate!";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            } else {
                result = keyPair.getPrivate();
            }
        } catch (IOException e) {
            String msg = "Unable to load private key from file! " + e.getMessage();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            log.info(msg, e);
        } catch (CertificateException e) {
            String msg = "Unable to compare the private key with the public key.";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.info(msg, e);
        }
        return result;
    }

    public void uploadListener(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        setPkdata(item.getData());
    }
}
