package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.model.Certificate;
import org.apache.commons.lang.ArrayUtils;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;

/**
 * Created by cel on 23/07/15.
 */
public class CertificateDownloader {
    /**
     * Download certificate in PEM format
     *
     * @param certificateId the certificate id
     * @param entityManager an active entity manager
     * @param facesContext  the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the PEM file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void downloadPEM(int certificateId, EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".pem";
            String pemStr = certificate.getPEM();
            byte[] bytes = ArrayUtils.clone(pemStr.getBytes(CertificateConstants.UTF_8));
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Download certificate in DER format
     *
     * @param certificateId the certificate id
     * @param entityManager an active entity manager
     * @param facesContext  the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the DER file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void downloadDER(int certificateId, EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".der";
            byte[] bytes = certificate.getCertificateX509().getX509Certificate().getEncoded();
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /***
     * Download private key in PEM format
     *
     * @param certificateId the certificate id
     * @param entityManager an active entity manager
     * @param facesContext  the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the KEY file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void downloadKEY(int certificateId, EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".key";
            String pemStr = certificate.getKEY();
            byte[] bytes = ArrayUtils.clone(pemStr.getBytes(CertificateConstants.UTF_8));
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Download certificate keystore in JKS format
     *
     * @param certificateId    the certificate id
     * @param keyStorePassword the keystore password
     * @param keyStoreAlias    the certificate alias in the keystore
     * @param entityManager    an active entity manager
     * @param facesContext     the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the JKS file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void downloadJKS(int certificateId, String keyStorePassword, String keyStoreAlias,
                                   EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".jks";
            byte[] bytes = certificate.getJKS(keyStorePassword, keyStoreAlias);
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Download certificate keystore in PKCS12 format
     *
     * @param certificateId    the certificate id
     * @param keyStorePassword the keystore password
     * @param entityManager    an active entity manager
     * @param facesContext     the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the P12 file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void downloadP12(int certificateId, String keyStorePassword, EntityManager entityManager,
                                   FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".p12";
            byte[] bytes = certificate.getP12(keyStorePassword);
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Similar to downloadJKS, except that the private key is given instead of retrieved from database.
     *
     * @param certificateId    the certificate id
     * @param privateKey       the private key
     * @param keyStorePassword the keystore password
     * @param keyStoreAlias    the certificate alias in the keystore
     * @param entityManager    an active entity manager
     * @param facesContext     the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the JKS file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void generateJKS(int certificateId, PrivateKey privateKey, String keyStorePassword,
                                   String keyStoreAlias,
                                   EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".jks";
            certificate.getPrivateKey().setKey(privateKey);
            byte[] bytes = certificate.getJKS(keyStorePassword, keyStoreAlias);
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Similar to downloadP12, except that the private key is given instead of retrieved from database.
     *
     * @param certificateId    the certificate id
     * @param privateKey       the private key
     * @param keyStorePassword the keystore password
     * @param entityManager    an active entity manager
     * @param facesContext     the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the P12 file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void generateP12(int certificateId, PrivateKey privateKey, String keyStorePassword,
                                   EntityManager entityManager,
                                   FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        try {
            Certificate certificate = CertificateDAO.getByID(certificateId, entityManager);
            String filename = certificate.getId() + ".p12";
            certificate.getPrivateKey().setKey(privateKey);
            byte[] bytes = certificate.getP12(keyStorePassword);
            downloadBytes(bytes, filename, "application/octet-stream", facesContext);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + certificateId + "'", e);
        }
    }

    /**
     * Use to directly install CA certificate in the browser. (tested with Firefox)
     *
     * @param cAId          id of the certificate authority
     * @param entityManager an active entity manager
     * @param facesContext  the faces context
     * @throws IllegalArgumentException if no certificate can be found for certificateId.
     * @throws CertificateException     if the P12 file cannot be built.
     * @throws IOException              if the bytes stream cannot be build in the HTTP response to transfert the file.
     */
    static public void installCACertInBrowser(int cAId, EntityManager entityManager, FacesContext facesContext)
            throws IllegalArgumentException, CertificateException, IOException {
        Certificate certificate = CertificateDAO.getByID(cAId, entityManager);
        byte[] bytes;
        try {
            bytes = certificate.getCertificateX509().getX509Certificate().getEncoded();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("No certificate for the id '" + cAId + "'", e);
        }
        downloadBytes(bytes, null, "application/x-x509-ca-cert", facesContext);
    }

    static public void downloadBytes(byte[] bytes, String fileName, String contentType, FacesContext facesContext)
            throws IOException {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                .getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType(contentType);
        response.setContentLength(bytes.length);
        if (fileName != null) {
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
        }
        servletOutputStream.write(bytes);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

}
