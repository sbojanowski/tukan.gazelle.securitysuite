package net.ihe.gazelle.atna.tls.server;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.SimulatorDAO;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.TlsConnectionRecorder;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import org.jboss.netty.channel.ChannelException;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public class ServerLauncher {

    private static Logger log = LoggerFactory.getLogger(ServerLauncher.class);

    public static void startServer(final TlsServer tlsServer) {
        String msg;
        TlsServer server = (TlsServer) (SimulatorDAO.getByID(tlsServer.getId(), EntityManagerService.provideEntityManager()));
        server.loadCertificates();
        TlsConnectionListener connectionListener = new TlsConnectionRecorder();
        try {

            // start
            ServerManagerLocal serverManager = (ServerManagerLocal) Component.getInstance("serverManager");
            serverManager.startServer(server, connectionListener);
            server.setRunning(true);

            // save
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(server);
            entityManager.flush();

            log.info("Server '" + server.getKeyword() + "' started");

        } catch (Exception e) {

            if (e instanceof ChannelException) {
                msg = "Failed to start server " + server.getKeyword() + ". The port " + server.getLocalPort() +
                        " seems already alocated. Change the port or restart the application server.";
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            } else {
                msg = "Failed to start server " + server.getKeyword();
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg + " (" + e.getMessage() + ")");
            }

            log.error(msg, e);

        }
    }

    public static void stopServer(TlsServer server) {
        if (server.isRunning()) {
            try {
                // stop
                ServerManagerLocal serverManager = (ServerManagerLocal) Component.getInstance("serverManager");
                serverManager.stopServer(server);
                log.info("Server '" + server.getKeyword() + "' stopped");

            } catch (IllegalStateException e) {
                log.error("Failed to stop server, not started.", e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Failed to stop  server " + server.getKeyword() + " (" + e.getMessage() + ")");
            }
            server.setRunning(false);

            // save
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(server);
            entityManager.flush();
        }
    }

}
