package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.exceptions.PreferenceException;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.security.cert.CertificateException;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
@Name("certificateRequestCreator")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = CertificateRequestCreator.SYNCHRONIZED_TIMEOUT)
public class CertificateRequestCreator extends RequestWithSubjectCreator implements Serializable {

    static final long SYNCHRONIZED_TIMEOUT = 2000 ;

    private static final long serialVersionUID = -7874231516577918775L;
    private static Logger log = LoggerFactory.getLogger(CertificateRequestCreator.class);

    private CertificateType certificateExtension;

    @Override
    public void init() throws CertificateException {
        super.init();
        certificateExtension = CertificateType.CLIENT_AND_SERVER;
    }

    @Override
    public CertificateType getCertificateExtension() {
        return certificateExtension;
    }

    @Override
    public void setCertificateExtension(CertificateType certificateExtension) {
        this.certificateExtension = certificateExtension;
    }

    public String submitWithoutCSR() throws CertificateException {

        setCertificateRequest(new CertificateRequestWithGeneratedKeys(DEFAULT_KEY_ALGORITHM, getKeySize().getSize()));

        try {
            getCertificateRequest().setCertificateAuthority(getAppCertificateAuthority());
        } catch (PreferenceException pe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, pe.getMessage());
            log.error(pe.getMessage());
            getCertificateRequest().setCertificateAuthority(null);
        }

        if (validateWithoutCSR()) {

            getCertificateRequest().setCertificateExtension(certificateExtension);
            getCertificateRequest().setRequester(Identity.instance().getCredentials().getUsername(),
                    UserAttributes.instance().getCasKeyword());

            getCertificateRequest().setSubjectUsingAttributes(getCountry(), getOrganization(), getCommonName(), getTitle(),
                    getGivenName(), getSurname(), getOrganizationalUnit(), geteMail());

            if (isAutomaticSigningEnabled()) {
                return RequestSignatory.sign(getCertificateRequest());
            } else {
                EntityManagerService.provideEntityManager().merge(getCertificateRequest());
                FacesMessages.instance().add(StatusMessage.Severity.INFO,
                        "Certificate request added (subject :" + getCertificateRequest().getSubject() + ")");
                RequestNotification.requested(getCertificateRequest());
                return Pages.PKI_LIST_REQUESTS.getMenuLink();
            }
        } else {
            return null;
        }
    }

    private boolean validateWithoutCSR() {
        if (getCertificateRequest().getCertificateAuthority() == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "No certificate authority defined for the application. Contact an administrator");
            return false ;
        }
        if (StringUtils.trimToNull(getOrganization()) == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Organization MUST be provided");
            return false;
        }
        if (StringUtils.trimToNull(getCommonName()) == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Common name MUST be provided");
            return false;
        }
        return true;
    }

}
