package net.ihe.gazelle.atna.tls.test.testcase;

import java.io.Serializable;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCaseQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

@Name("testCaseListManager")
@Scope(ScopeType.PAGE)
public class TestCaseListManager extends TestCaseBrowser implements Serializable {

    private static final long serialVersionUID = -4140849161526951481L;

    public void deleteTestCase() {
        //TODO implement deletion of pending test case.
    }

    @Override
    public HQLCriterionsForFilter<TlsTestCase> getHQLCriterionsForFilter() {
        TlsTestCaseQuery query = new TlsTestCaseQuery();
        HQLCriterionsForFilter<TlsTestCase> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("testCaseName", query.name());
        criteria.addPath("context", query.context());
        criteria.addPath("sut", query.sutType());
        return criteria;
    }


    public String navigateViewTestCase(int testCaseId) {
        return Pages.TLS_VIEW_TEST_CASE.getMenuLink() + "?testCase=" + testCaseId;
    }

    public String navigateRunTestCase(int testCaseId) {
        return Pages.TLS_RUN_TEST_CASE.getMenuLink() + "?testCase=" + testCaseId;
    }

    public String navigateEditTestCase(int testCaseId) {
        return Pages.TLS_CREATE_TEST_CASE.getMenuLink() + "?testCase=" + testCaseId;
    }

    public String navigateNewTestCase() {
        return Pages.TLS_CREATE_TEST_CASE.getMenuLink();
    }

    public String linkToViewTestCase(int testCaseId) {
        return ApplicationManagerBean.getUrl() + navigateViewTestCase(testCaseId);
    }

    public String linkToRunTestCase(int testCaseId) {
        return ApplicationManagerBean.getUrl() + navigateRunTestCase(testCaseId);
    }

    public String linkToEditTestCase(int testCaseId) {
        return ApplicationManagerBean.getUrl() + navigateEditTestCase(testCaseId);
    }

    public String linkToNewTestCase() {
        return ApplicationManagerBean.getUrl() + navigateNewTestCase();
    }

    public String linkToAllocatedSimulator(int testCaseId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TlsTestCase tc = entityManager.find(TlsTestCase.class, testCaseId);
        if (tc.getSimulator() != null) {
            if(tc.getSimulator().getClass().equals(TlsServer.class)) {
                return ApplicationManagerBean.getUrl() + Pages.TLS_TEST_SERVER.getMenuLink() +
                        "?simulator=" + tc.getSimulator().getId();
            } else {
                return ApplicationManagerBean.getUrl() + Pages.TLS_TEST_CLIENT.getMenuLink() +
                        "?simulator=" + tc.getSimulator().getId();
            }
        } else {
            return null;
        }
    }

}
