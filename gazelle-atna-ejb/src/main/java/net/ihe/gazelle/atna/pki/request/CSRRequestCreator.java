package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.exceptions.PreferenceException;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.CertificateRequestCSR;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
@Name("csrRequestCreator")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = CSRRequestCreator.SYNCHRONIZED_TIMEOUT)
public class CSRRequestCreator extends AbstractRequestCreator implements Serializable {

    static final long SYNCHRONIZED_TIMEOUT = 2000;

    private static final long serialVersionUID = -8269044208941856784L;
    private static Logger log = LoggerFactory.getLogger(CSRRequestCreator.class);

    private byte[] csr;
    private byte[] pkdata;
    private String pkpassword;

    public byte[] getCsr() {
        return csr.clone();
    }

    public void setCsr(byte[] csr) {
        this.csr = csr.clone();
    }

    public byte[] getPkdata() {
        return pkdata.clone();
    }

    public void setPkdata(byte[] pkdata) {
        this.pkdata = pkdata.clone();
    }

    public String getPkpassword() {
        return pkpassword;
    }

    public void setPkpassword(String pkpassword) {
        this.pkpassword = pkpassword;
    }

    public void init() {
        setCertificateRequest(new CertificateRequestCSR());

        try {
            getCertificateRequest().setCertificateAuthority(getAppCertificateAuthority());
        } catch (PreferenceException pe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, pe.getMessage());
            log.error(pe.getMessage());
            getCertificateRequest().setCertificateAuthority(null);
        }
        getCertificateRequest().setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        getCertificateRequest().setRequester(Identity.instance().getCredentials().getUsername(),
                UserAttributes.instance().getCasKeyword());
    }

    public String submitWithCSR() {
        if (validateCSR()) {
            if (isAutomaticSigningEnabled()) {
                return RequestSignatory.sign(getCertificateRequest());
            } else {
                EntityManagerService.provideEntityManager().merge(getCertificateRequest());
                FacesMessages.instance().add(StatusMessage.Severity.INFO,
                        "Certificate request added (subject :" + getCertificateRequest().getSubject() + ")");

                RequestNotification.requested(getCertificateRequest());
                return Pages.PKI_LIST_REQUESTS.getMenuLink();
            }
        } else {
            return "";
        }
    }

    private boolean validateCSR() {
        CertificateRequestCSR certificateRequestCSR = (CertificateRequestCSR) getCertificateRequest();
        try {
            certificateRequestCSR.loadFromCsr(new String(csr, StandardCharsets.UTF_8));
        } catch (Exception e) {
            String msg = "Failed to import CSR (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
            return false;
        }

        if (pkdata != null && pkdata.length > 0) {

            Reader reader = null;
            PEMReader pemReader = null;

            try {
                reader = new InputStreamReader(new ByteArrayInputStream(pkdata), StandardCharsets.UTF_8);

                if (pkpassword == null) {
                    pkpassword = "";
                }
                pemReader = new PEMReader(reader, new PasswordFinder() {
                    @Override
                    public char[] getPassword() {
                        return pkpassword.toCharArray();
                    }
                });
                KeyPair key = (KeyPair) pemReader.readObject();
                if (key == null) {
                    throw new IOException("Does not match supported key formats");
                } else {
                    certificateRequestCSR.setPrivateKey(key.getPrivate());
                }
                pemReader.close();
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Failed to import private key (" + e.getMessage() + ")", e);
                return false;
            } finally {
                IOUtils.closeQuietly(reader);
                IOUtils.closeQuietly(pemReader);
            }
        }

        return true;
    }


    public void uploadListener(FileUploadEvent event) throws IOException {
        UploadedFile item = event.getUploadedFile();
        setCsr(item.getData());
    }

    public void uploadpkDataListener(FileUploadEvent event) throws IOException {
        UploadedFile item = event.getUploadedFile();
        setPkdata(item.getData());
    }
}
