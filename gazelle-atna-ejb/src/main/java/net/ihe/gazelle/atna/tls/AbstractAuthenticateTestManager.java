package net.ihe.gazelle.atna.tls;

import net.ihe.gazelle.atna.tls.connection.ConnectionBrowser;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.StringWrapper;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionQuery;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class AbstractAuthenticateTestManager<T extends TlsSimulator> extends ConnectionBrowser implements
        Serializable {

    private static final long serialVersionUID = 6344213395873093205L;
    private static Logger log = LoggerFactory.getLogger(AbstractAuthenticateTestManager.class);

    private transient T selectedSimulator;
    private int selecteSimulatorId;

    public abstract Class<T> getTlsSimulatorClass();

    @Create
    public void initialize() {

        Integer id = 0;
        selectedSimulator = null;
        selecteSimulatorId = 0;

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String messageId = params.get("simulator");
        if (messageId != null && messageId.trim().length() >= 1) {
            try {
                id = Integer.valueOf(messageId);
                retrieveSimulator(id);
            } catch (Exception e) {
                log.error("'" + messageId + "' is not a valid simulator id", e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Error, '" + messageId + "' is not a valid simulator id");
            }
        }
    }

    public T getSelectedSimulator() {
        if (selectedSimulator == null) {
            retrieveSimulator(selecteSimulatorId);
        }
        return selectedSimulator;
    }

    public void setSelectedSimulator(T selectedSimulator) {
        if (selectedSimulator == null) {
            this.selecteSimulatorId = 0;
        } else {
            this.selecteSimulatorId = selectedSimulator.getId();
        }
        this.selectedSimulator = selectedSimulator;
    }

    public int getSelecteSimulatorId() {
        return selecteSimulatorId;
    }

    public List<ChannelType> getMessageTypes() {
        return Arrays.asList(ChannelType.values());
    }

    public static String rnTobr(String result) {
        String splitted = StringWrapper.wrap(result, 80);
        return splitted.replaceAll(" ", "&nbsp;").replaceAll(CertificateConstants.NEW_LINE, "<br \\>")
                .replaceAll("\r", "<br \\>").replaceAll("\n", "<br \\>");
    }

    public boolean showIssuers() {
        if (isServer()) {
            TlsServer server = (TlsServer) getSelectedSimulator();
            return server.isNeedClientAuth() && !server.isUseCACerts();
        } else {
            return !getSelectedSimulator().isUseCACerts();
        }
    }

    public boolean isServer() {
        return getSelectedSimulator().getType().equals(SimulatorType.SERVER);
    }

    public boolean isClientAuthentication() {
        if (isServer()) {
            TlsServer server = (TlsServer) getSelectedSimulator();
            return server.isNeedClientAuth();
        } else {
            return false;
        }
    }

    public List<Certificate> getSimulatorTrustedIssuers() {
        if (getSelectedSimulator().getIssuers() != null) {
            return new ArrayList<Certificate>(getSelectedSimulator().getIssuers());
        } else {
            return new ArrayList<Certificate>();
        }
    }

    public List<ProtocolType> getSimulatorProtocols() {
        if (getSelectedSimulator().getProtocols() != null) {
            return new ArrayList<ProtocolType>(getSelectedSimulator().getProtocols());
        } else {
            return new ArrayList<ProtocolType>();
        }
    }

    public List<CipherSuiteType> getSimulatorCipherSuites() {
        if (getSelectedSimulator().getCipherSuites() != null) {
            return new ArrayList<CipherSuiteType>(getSelectedSimulator().getCipherSuites());
        } else {
            return new ArrayList<CipherSuiteType>();
        }
    }

    @Override
    public HQLCriterionsForFilter<TlsConnection> getHQLCriterionsForFilter() {
        TlsConnectionQuery query = new TlsConnectionQuery();
        HQLCriterionsForFilter<TlsConnection> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("simulator", query.simulator(), getSelectedSimulator(), getSelectedSimulator());
        criteria.addPath("timestamp", query.date());
        criteria.addPath("remoteHost", query.remoteHost());
        criteria.addPath("success", query.success());
        return criteria;
    }

    protected void retrieveSimulator(int id) {
        try {
            setSelectedSimulator(EntityManagerService.provideEntityManager().find(getTlsSimulatorClass(), id));
        } catch (Exception e) {
            selectedSimulator = null;
            selecteSimulatorId = 0;
            log.error("Failed to load simulator with id '" + id + "'", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to load simulator with id '" + id + "'");
        }
    }

    public abstract String linkToSimulatorList();

}
