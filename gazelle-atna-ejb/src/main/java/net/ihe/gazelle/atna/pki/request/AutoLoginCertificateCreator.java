package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.exceptions.PreferenceException;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.pki.CertificateDownloader;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestCSR;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.netscape.NetscapeCertRequest;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.Map;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
@Name("autoLoginCertificateCreator")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = AutoLoginCertificateCreator.SYNCHRONIZED_TIMEOUT)
public class AutoLoginCertificateCreator extends AbstractRequestCreator implements Serializable {

    static final long SYNCHRONIZED_TIMEOUT = 2000;

    private static final long serialVersionUID = -1202645010328704574L;
    private static Logger log = LoggerFactory.getLogger(AutoLoginCertificateCreator.class);

    public void computeAutoLoginCertificate() throws CertificateException {
        setCertificateRequest(new CertificateRequestCSR());

        try {
            getCertificateRequest().setCertificateAuthority(getAppCertificateAuthority());
        } catch (PreferenceException pe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, pe.getMessage());
            log.error(pe.getMessage());
            return;
        }
        getCertificateRequest().setCertificateExtension(CertificateType.BASIC);
        getCertificateRequest().setRequester(Identity.instance().getCredentials().getUsername(),
                UserAttributes.instance().getCasKeyword());

        String time = Long.toHexString(new Date().getTime());
        getCertificateRequest()
                .setSubjectUsingAttributes("FR", "IHE", Identity.instance().getPrincipal().getName(), "CAS",
                        time, null, null, null);

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String keygen = params.get("keygen");
        if (keygen != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (char c : keygen.toCharArray()) {
                if (!Character.isWhitespace(c)) {
                    bos.write(c);
                }
            }
            byte[] charArray = bos.toByteArray();
            byte[] bts = new Base64().decode(charArray);

            try {
                NetscapeCertRequest netscapeCertRequest = new NetscapeCertRequest(bts);
                getCertificateRequest().setPublicKey(netscapeCertRequest.getPublicKey());
                getCertificateRequest().setNotAfter(CertificateUtil.getNotAfter());
                getCertificateRequest().setNotBefore(CertificateUtil.getNotBefore());
                getCertificateRequest().setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
                EntityManagerService.provideEntityManager().persist(getCertificateRequest());
                CertificateManager
                        .createCertificate(getCertificateRequest(), EntityManagerService.provideEntityManager());

                installAutoLoginCertificate(getCertificateRequest().getCertificate());

            } catch (IOException e) {
                log.error("Failed to parse keygen...", e);
                throw new CertificateException(e);
            } catch (CertificateException e) {
                log.error("Failed to generate certificate...", e);
                throw e;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to generate certificate...");
        }
    }

    public String getCACertificateLink() {
        try {
            return Pages.PKI_DOWNLOAD_CA.getMenuLink() + "?id=" + getAppCertificateAuthorityId();
        } catch (PreferenceException pe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, pe.getMessage());
            log.error(pe.getMessage());
            return null;
        }
    }

    private void installAutoLoginCertificate(Certificate certificate) {
        try {
            byte[] bytes = certificate.getCertificateX509().getX509Certificate().getEncoded();
            CertificateDownloader.downloadBytes(bytes, null, "application/x-x509-user-cert",
                    FacesContext.getCurrentInstance());
        } catch (Exception e) {
            String msg = "Failed to install personnal certificate";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
        }
    }

}
