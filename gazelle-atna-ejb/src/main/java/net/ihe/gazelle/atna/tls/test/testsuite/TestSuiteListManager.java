package net.ihe.gazelle.atna.tls.test.testsuite;

import java.io.Serializable;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestSuite;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestSuiteQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("testSuiteListManager")
@Scope(ScopeType.PAGE)
public class TestSuiteListManager extends TestSuiteBrowser implements Serializable {

    private static final long serialVersionUID = -2839626849352839196L;

    @Override
    public HQLCriterionsForFilter<TlsTestSuite> getHQLCriterionsForFilter() {
        TlsTestSuiteQuery query = new TlsTestSuiteQuery();
        HQLCriterionsForFilter<TlsTestSuite> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("name", query.name());
        criteria.addPath("context", query.context());
        return criteria;
    }

    public String navigateRunTestSuite(TlsTestSuite testSuite) {
        return Pages.TLS_RUN_TEST_SUITE.getMenuLink() + "?testSuite=" + testSuite.getId();
    }

    public String navigateEditTestSuite(TlsTestSuite testSuite) {
        return Pages.TLS_CREATE_TEST_SUITE.getMenuLink() + "?testSuite=" + testSuite.getId();
    }

    public String navigateNewTestSuite() {
        return Pages.TLS_CREATE_TEST_SUITE.getMenuLink();
    }

}
