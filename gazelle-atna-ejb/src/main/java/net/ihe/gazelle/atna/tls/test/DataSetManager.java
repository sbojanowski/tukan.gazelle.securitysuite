package net.ihe.gazelle.atna.tls.test;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.enums.CertificateScope;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSet;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Name("dataSetManager")
@Scope(ScopeType.PAGE)
public class DataSetManager implements Serializable {

    private static final long serialVersionUID = -6295918155980911130L;

    private static Logger log = LoggerFactory.getLogger(DataSetManager.class);

    private int selectedDataSetId;

    private String name;
    private ContextType context;
    private Date lastChanged;
    private String lastModifierId;
    private boolean useSystemCACerts;
    private boolean checkRevocation;
    private boolean needClientAuth;
    private Certificate certificate;
    private List<ProtocolType> protocols;
    private List<CipherSuiteType> cipherSuites;
    private List<Certificate> trustedIssuers;
    private CertificateScope certificateScope;

    @Create
    public void initialize() {

        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();
        TlsTestDataSet currentDataSet;

        if (urlParameters.get("dataSet") != null) {
            try {
                selectedDataSetId = Integer.decode(urlParameters.get("dataSet"));
                currentDataSet = EntityManagerService.provideEntityManager().find(TlsTestDataSet.class,
                        selectedDataSetId);
                if (currentDataSet != null) {
                    loadDataSetParameters(currentDataSet);
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No such data set recorded in database");
                }
            } catch (NumberFormatException e) {
                selectedDataSetId = 0;
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "'" + urlParameters.get("dataSet") + "' is not a valid data set id");
            }
        } else {
            setDefaultParameters();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContextType getContext() {
        return context;
    }

    public void setContext(ContextType context) {
        this.context = context;
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public String getLastModifierId() {
        return lastModifierId;
    }

    public boolean isUseSystemCACerts() {
        return useSystemCACerts;
    }

    public void setUseSystemCACerts(boolean useSystemCACerts) {
        this.useSystemCACerts = useSystemCACerts;
    }

    public boolean isCheckRevocation() {
        return checkRevocation;
    }

    public void setCheckRevocation(boolean checkRevocation) {
        this.checkRevocation = checkRevocation;
    }

    public boolean isNeedClientAuth() {
        return needClientAuth;
    }

    public void setNeedClientAuth(boolean needClientAuth) {
        this.needClientAuth = needClientAuth;
    }

    public List<ProtocolType> getProtocols() {
        return protocols;
    }

    public void setProtocols(List<ProtocolType> protocols) {
        this.protocols = protocols;
    }

    public List<CipherSuiteType> getCipherSuites() {
        return cipherSuites;
    }

    public void setCipherSuites(List<CipherSuiteType> cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

    public List<Certificate> getTrustedIssuers() {
        return trustedIssuers;
    }

    public void setTrustedIssuers(List<Certificate> trustedIssuers) {
        this.trustedIssuers = trustedIssuers;
    }

    public CertificateScope getCertificateScope() {
        return certificateScope;
    }

    public void setCertificateScope(CertificateScope selectedCertificateAuhtorityType) {
        this.certificateScope = selectedCertificateAuhtorityType;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Certificate getCertificate() {
        return this.certificate;
    }

    public String saveDataSet() {
        TlsTestDataSet dataSet;
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        if (selectedDataSetId != 0) {
            dataSet = entityManager.find(TlsTestDataSet.class, selectedDataSetId);
        } else {
            dataSet = new TlsTestDataSet();
        }

        dataSet.setName(this.name);
        dataSet.setContext(this.context);
        dataSet.setUseSystemCACerts(this.useSystemCACerts);
        dataSet.setCheckRevocation(this.checkRevocation);
        dataSet.setNeedClientAuth(this.needClientAuth);
        dataSet.setCertificate(this.certificate);
        dataSet.setProtocols(new HashSet<ProtocolType>(this.protocols));
        dataSet.setCipherSuites(new HashSet<CipherSuiteType>(this.cipherSuites));
        dataSet.setTrustedIssuers(new HashSet<Certificate>(this.trustedIssuers));

        try {
            entityManager.merge(dataSet);
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test data set correctly saved");
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to save data set.");
            log.error("Failed to save data set.", e);
        }
        return backToList();
    }

    public String backToList() {
        return Pages.TLS_LIST_DATA_SET.getMenuLink();
    }

    public String linkToList() {
        return ApplicationManagerBean.getUrl() + backToList();
    }

    public SelectItem[] getContextTypeItems() {
        return ContextType.getSelectItems();
    }

    public SelectItem[] getProtocolTypeItems() {
        return ProtocolType.getSelectItems();
    }

    public SelectItem[] getCipherSuiteTypeItems() {
        return CipherSuiteType.getSelectItems();
    }

    public SelectItem[] getCertificateScopeItems() {
        return CertificateScope.getSelectItems();
    }

    public SelectItem[] getAvailableCertificates() {
        List<Certificate> availableCertificates;
        availableCertificates = CertificateDAO.getCertificatesForSigningBySource(
                EntityManagerService.provideEntityManager(),
                null, getCertificateScope() == CertificateScope.PKI);
        subjectCertificateSort(availableCertificates);
        return convertCertificateListToSelectItemArray(availableCertificates);
    }

    public SelectItem[] getAvailableIssuers() {
        List<Certificate> availableIssuers;
        availableIssuers = CertificateDAO.getCertificateAuthorities(EntityManagerService.provideEntityManager(), false);
        subjectCertificateSort(availableIssuers);
        return convertCertificateListToSelectItemArray(availableIssuers);

    }

    public boolean isEditForm(){
        return selectedDataSetId != 0;
    }

    private void setDefaultParameters() {
        selectedDataSetId = 0;
        this.context = ContextType.IHE;
        this.lastChanged = null ;
        this.lastModifierId = null ;
        this.useSystemCACerts = false;
        this.checkRevocation = false;
        this.needClientAuth = true;
        this.certificate = null;
        this.protocols = new ArrayList<ProtocolType>();
        this.protocols.add(ProtocolType.TLSv12);
        this.cipherSuites = new ArrayList<CipherSuiteType>();
        this.cipherSuites.add(CipherSuiteType.TLS_RSA_WITH_AES_128_CBC_SHA);
        this.trustedIssuers = new ArrayList<Certificate>();
        this.certificateScope = CertificateScope.PKI;
    }

    private void loadDataSetParameters(TlsTestDataSet currentDataSet) {
        selectedDataSetId = currentDataSet.getId();
        this.name = currentDataSet.getName();
        this.context = currentDataSet.getContext();
        this.lastChanged = currentDataSet.getLastChanged();
        this.lastModifierId = currentDataSet.getLastModifierId();
        this.useSystemCACerts = currentDataSet.isUseSystemCACerts();
        this.checkRevocation = currentDataSet.isCheckRevocation();
        this.needClientAuth = currentDataSet.isNeedClientAuth();
        this.certificate = currentDataSet.getCertificate();
        this.protocols = new ArrayList<ProtocolType>(currentDataSet.getProtocols());
        this.cipherSuites = new ArrayList<CipherSuiteType>(currentDataSet.getCipherSuites());
        this.trustedIssuers = new ArrayList<Certificate>(currentDataSet.getTrustedIssuers());
        if (currentDataSet.getCertificate()!= null && currentDataSet.getCertificate().isCustom()) {
            this.certificateScope = CertificateScope.CUSTOM;
        } else {
            this.certificateScope = CertificateScope.PKI;
        }
    }

    private static void subjectCertificateSort(List<Certificate> certificateList) {
        Collections.sort(certificateList, new Comparator<Certificate>() {
            public int compare(Certificate o1, Certificate o2) {
                return o1.getSubject().compareTo(o2.getSubject());
            }
        });
    }

    private static SelectItem[] convertCertificateListToSelectItemArray(List<Certificate> certificateList) {
        List<SelectItem> result = new ArrayList<SelectItem>();
        for (Certificate certificate : certificateList) {
            result.add(new SelectItem(certificate, certificate.getSubject() + " (#" + certificate.getId() + ")"));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

}
