package net.ihe.gazelle.atna.tls.server;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.TlsConnectionRecorder;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.model.TlsServerQuery;
import org.jboss.netty.channel.ChannelException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This bean Manages proxy channels associated with server simulators. It is used to start/stop servers and keep proxies in memory.
 */
@Name("serverManager")
@Scope(ScopeType.APPLICATION)
@Startup(depends = {"entityManager"})
@GenerateInterface("ServerManagerLocal")
public class ServerManager implements ServerManagerLocal {

    private static final Logger LOG = LoggerFactory.getLogger(ServerManager.class);

    private Map<Integer, Proxy<?, ?>> proxies;
    private String ips;

    @Create
    @Transactional
    public void startServerManager() {

        // variable initialisation
        proxies = new HashMap<Integer, Proxy<?, ?>>();
        ips = retrieveIps();

        // Because we have no idea of how the server/the application has been shut down previously,
        // all servers must be stopped to properly reset their status.
        stopAllServers();
        startAllServers();

    }

    public void startAllServers() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        TlsServerQuery query = new TlsServerQuery();
        query.enabled().eq(true);
        List<TlsServer> servers = query.getList();

        for (TlsServer server : servers) {
            if (!isServerStarted(server.getId())) {
                try {
                    startServer(server, new TlsConnectionRecorder());
                    server.setRunning(true);
                    LOG.info("Server '" + server.getKeyword() + "' started");
                } catch (Exception e) {
                    LOG.error("Unable to start server " + server.getKeyword(), e);
                    server.setRunning(false);
                }
                entityManager.merge(server);
            }
        }
        entityManager.flush();
    }

    public void stopAllServers() {
        TlsServerQuery query = new TlsServerQuery();
        List<TlsServer> servers = query.getList();

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        for (TlsServer server : servers) {
            try {
                stopServer(server);
                LOG.info("Server '" + server.getKeyword() + "' stopped");
            } catch (IllegalStateException e) {
                LOG.error("Unable to stop server " + server.getKeyword() + ". Not started", e);
            }
            server.setRunning(false);
            entityManager.merge(server);
        }
        entityManager.flush();
    }

    /**
     * In case of undeployment, proxies must be stopped. If not, the channels are not freed. It is not possible at this stage to save server status in database because contexts are already destroyed.
     * However at restarting, status will be properly updated. Finally, In case of Application server killing, the @Destroy method will not be called, but the proxy will be entirely closed... so no
     * more problems !
     */
    @Destroy
    public void killProxies() {
        for (Proxy<?, ?> proxy : proxies.values()) {
            // proxy must be tested against null, because if some servers are stopped or disabled
            // at runtime, their proxy value is set to null (see stopServer method).
            if (proxy != null) {
                proxy.stop();
            }
        }
        LOG.info("proxy channels killed");
    }

    public String getIps() {
        return ips;
    }

    /**
     * i : the primary key of the TlsServer in the database
     */
    public boolean isServerStarted(int i) {
        Integer id = Integer.valueOf(i);
        Proxy<?, ?> proxy = proxies.get(id);
        if (proxy != null && proxy.isOpen()) {
            return true;
        }
        return false;
    }

    public int getServerPort(int i) {
        Integer id = Integer.valueOf(i);
        Proxy<?, ?> proxy = proxies.get(id);
        if (proxy != null && proxy.isOpen()) {
            return proxy.getProxyProviderPort();
        } else {
            return -1;
        }
    }

    @Override
    public void startServer(TlsServer server, TlsConnectionListener connectionListener)
            throws TlsSimulatorException, ChannelException {
        if (server == null) {
            throw new TlsSimulatorException("server parameter is null");
        } else if (server.getLocalPort() == null) {
            throw new TlsSimulatorException("No local port provided");
        } else {
            Proxy<?, ?> proxy = proxies.get(server.getId());
            if (proxy != null) {
                throw new TlsSimulatorException("Already started");
            }
            proxy = server.start(connectionListener);
            if (proxy != null) {
                proxies.put(server.getId(), proxy);
            } else {
                throw new ChannelException("Unable to start server channel on port " + server.getLocalPort());
            }
        }
    }

    @Override
    public void stopServer(TlsServer server) throws IllegalStateException {
        Proxy<?, ?> proxy = proxies.get(server.getId());
        if (proxy != null) {
            proxies.put(server.getId(), null);
            proxy.stop();
        }
    }

    @Override
    public int getServerForwardPort(int i) {
        Integer id = Integer.valueOf(i);
        Proxy<?, ?> proxy = proxies.get(id);
        if (proxy != null && proxy.isOpen()) {
            return proxy.getProxyConsumerPort();
        } else {
            return -1;
        }
    }

    @Override
    public String getServerForwardHost(int i) {
        Integer id = Integer.valueOf(i);
        Proxy<?, ?> proxy = proxies.get(id);
        if (proxy != null && proxy.isOpen()) {
            return proxy.getProxyConsumerHost();
        } else {
            return "";
        }
    }

    @Override
    public ChannelType getServerType(int i) {
        Integer id = Integer.valueOf(i);
        Proxy<?, ?> proxy = proxies.get(id);
        if (proxy != null && proxy.isOpen()) {
            return proxy.getChannelType();
        } else {
            return null;
        }
    }

    private String retrieveIps() {

        if (PreferenceService.getBoolean("tls_automatic_simulator_address_enabled")) {
            StringBuilder sb = new StringBuilder("");

            try {
                boolean first = true;
                Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();

                while (e.hasMoreElements()) {
                    NetworkInterface ni = (NetworkInterface) e.nextElement();
                    Enumeration<InetAddress> e2 = ni.getInetAddresses();
                    while (e2.hasMoreElements()) {
                        InetAddress ip = (InetAddress) e2.nextElement();
                        if (ip instanceof Inet4Address) {
                            String hostName = ip.getHostName();
                            if (!hostName.startsWith("localhost")) {
                                if (!first) {
                                    sb.append(", ");
                                }
                                sb.append(ip.getHostAddress());
                                sb.append(" (");
                                sb.append(hostName);
                                sb.append(")");
                                first = false;
                            }
                        }
                    }
                }

            } catch (Exception e) {
                LOG.error("Failed to get server IP", e);
            }

            return sb.toString();
        } else {
            return PreferenceService.getString("tls_simulator_address");
        }
    }

}
