package net.ihe.gazelle.atna.pki;

import java.io.Serializable;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.pki.request.RequestSubject;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.KeySize;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.simulators.tls.enums.CertificateScope;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("customCertificateManager")
@Scope(ScopeType.PAGE)
public class CustomCertificateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1183684393851993120L;

    private static Logger log = LoggerFactory.getLogger(CustomCertificateManager.class);

    @In
    private transient EntityManager entityManager;

    @In
    private LocaleSelector localeSelector;

    private Certificate certificateAuthority;

    private CertificateVersion certificateVersion;

    private CertificateType certificateExtension;

    private KeyAlgorithm keyAlgorithm;

    private KeySize keySize;

    private SignatureAlgorithm signatureAlgorithm;

    private Date validFrom;

    private Date validTo;

    private RequestSubject requestSubject;

    private CertificateScope certificateScope = CertificateScope.PKI;

    private boolean isCA;

    public CustomCertificateManager() {
        super();
    }

    public void initCA() {
        init();
        isCA = true;
    }

    public void init() {
        isCA = false;
        requestSubject = new RequestSubject();
        certificateVersion = CertificateVersion.V3;
        certificateExtension = CertificateType.HTTPS_SERVER;
        keyAlgorithm = KeyAlgorithm.RSA;
        keySize = KeySize._1024;
        signatureAlgorithm = SignatureAlgorithm.SHA512WITHRSAENCRYPTION;
        validFrom = CertificateUtil.getNotBefore();
        validTo = CertificateUtil.getNotAfter();
    }

    private SelectItem[] getSelectItems(Object[] items) {
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

    public SelectItem[] getCertificateVersions() {
        return getSelectItems(CertificateVersion.values());
    }

    public SelectItem[] getKeySizes() {
        return getSelectItems(KeySize.values());
    }

    public SelectItem[] getSignatureAlgorithms() {
        return getSelectItems(SignatureAlgorithm.values());
    }

    public SelectItem[] getCertificateScopes() {
        return getSelectItems(CertificateScope.values());
    }

    public SelectItem[] getCountries() {
        return CertificateUtil.getCountries(localeSelector.getLocale());
    }

    public SelectItem[] getCertificateExtensions() {
        List<CertificateType> list = Arrays.asList(CertificateType.values());
        List<SelectItem> result = new ArrayList<SelectItem>();
        for (CertificateType certificateType : list) {
            if (certificateType != CertificateType.CA_KEY_USAGE_ALL) {
                result.add(new SelectItem(certificateType, certificateType.getFriendlyName()));
            }
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public SelectItem[] getCertificateAuthorities(boolean fromPKI) {
        List<Certificate> list = CertificateDAO.getCertificateAuthoritiesBySource(entityManager, fromPKI);
        List<SelectItem> result = new ArrayList<SelectItem>();
        if (isCA) {
            result.add(new SelectItem(null, "None"));
        }
        for (Certificate certificate : list) {
            result.add(new SelectItem(certificate, certificate.getSubject()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }

    public SelectItem[] getKeyAlgorithms() {
        return getSelectItems(KeyAlgorithm.values());
    }

    public void createCertificateAuthority() {
        try {
            CertificateRequestAuthority cra = new CertificateRequestAuthority(keyAlgorithm, keySize.getSize());

            cra.setRequester(Identity.instance().getCredentials().getUsername(),
                    UserAttributes.instance().getCasKeyword());

            cra.setSignatureAlgorithm(signatureAlgorithm);
            cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
            cra.setCertificateVersion(certificateVersion);

            cra.setSubjectUsingAttributes(requestSubject.getCountry(), requestSubject.getOrganization(),
                    requestSubject.getCommonName(), requestSubject.getTitle(),
                    requestSubject.getGivenName(), requestSubject.getSurname(),
                    requestSubject.getOrganizationalUnit(), requestSubject.geteMail());
            cra.setIssuer(cra.getSubject());

            cra.setNotAfter(validTo);
            cra.setNotBefore(validFrom);

            Certificate certificate = CertificateManager.createCertificateAuthority(cra, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);

            redirectToCertificate(certificate);

        } catch (CertificateException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to create certificate.");
            log.error("Failed to create certificate.", e);
        }
    }

    public void createCertificate() {
        try {
            CertificateRequestWithGeneratedKeys crc = new CertificateRequestWithGeneratedKeys(keyAlgorithm, keySize.getSize());

            crc.setRequester(Identity.instance().getCredentials().getUsername(),
                    UserAttributes.instance().getCasKeyword());

            crc.setSubjectUsingAttributes(requestSubject.getCountry(), requestSubject.getOrganization(),
                    requestSubject.getCommonName(), requestSubject.getTitle(),
                    requestSubject.getGivenName(), requestSubject.getSurname(),
                    requestSubject.getOrganizationalUnit(), requestSubject.geteMail());
            crc.setCertificateAuthority(certificateAuthority);
            crc.setSignatureAlgorithm(signatureAlgorithm);
            crc.setCertificateExtension(certificateExtension);

            crc.setNotAfter(validTo);
            crc.setNotBefore(validFrom);

            Certificate certificate = CertificateManager.createCertificate(crc, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);

            redirectToCertificate(certificate);

        } catch (CertificateException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to create certificate.");
            log.error("Failed to create certificate.", e);
        }
    }

    private void redirectToCertificate(Certificate certificate) {
        Redirect redirect = Redirect.instance();
        redirect.setParameter("id", certificate.getId());
        redirect.setViewId("/details/cert.xhtml");
        redirect.execute();
    }

    public boolean isPKISelectedScope() {
        return certificateScope == CertificateScope.PKI;
    }

    public String redirectToCertificate() {
        if (certificateAuthority != null) {
            return "/details/cert.seam?id=" + certificateAuthority.getId();
        }
        return null;
    }

    public String getCountry() {
        return requestSubject.getCountry();
    }

    public void setCountry(String country) {
        requestSubject.setCountry(country);
    }

    public String getOrganization() {
        return requestSubject.getOrganization();
    }

    public void setOrganization(String organization) {
        requestSubject.setOrganization(organization);
    }

    public String getCommonName() {
        return requestSubject.getCommonName();
    }

    public void setCommonName(String commonName) {
        requestSubject.setCommonName(commonName);
    }

    public String getTitle() {
        return requestSubject.getTitle();
    }

    public void setTitle(String title) {
        requestSubject.setTitle(title);
    }

    public String getGivenName() {
        return requestSubject.getGivenName();
    }

    public void setGivenName(String givenName) {
        requestSubject.setGivenName(givenName);
    }

    public String getSurname() {
        return requestSubject.getSurname();
    }

    public void setSurname(String surname) {
        requestSubject.setSurname(surname);
    }

    public String getOrganizationalUnit() {
        return requestSubject.getOrganizationalUnit();
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        requestSubject.setOrganizationalUnit(organizationalUnit);
    }

    public String geteMail() {
        return requestSubject.geteMail();
    }

    public void seteMail(String eMail) {
        requestSubject.seteMail(eMail);
    }

    public Certificate getCertificateAuthority() {
        return certificateAuthority;
    }

    public void setCertificateAuthority(Certificate certificateAuthority) {
        this.certificateAuthority = certificateAuthority;
    }

    public CertificateVersion getCertificateVersion() {
        return certificateVersion;
    }

    public void setCertificateVersion(CertificateVersion certificateVersion) {
        this.certificateVersion = certificateVersion;
    }

    public CertificateType getCertificateExtension() {
        return certificateExtension;
    }

    public void setCertificateExtension(CertificateType certificateExtension) {
        this.certificateExtension = certificateExtension;
    }

    public KeyAlgorithm getKeyAlgorithm() {
        return keyAlgorithm;
    }

    public void setKeyAlgorithm(KeyAlgorithm keyAlgorithm) {
        this.keyAlgorithm = keyAlgorithm;
    }

    public KeySize getKeySize() {
        return keySize;
    }

    public void setKeySize(KeySize keySize) {
        this.keySize = keySize;
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public CertificateScope getCertificateScope() {
        return certificateScope;
    }

    public void setCertificateScope(CertificateScope certificateScope) {
        this.certificateScope = certificateScope;
    }

}
