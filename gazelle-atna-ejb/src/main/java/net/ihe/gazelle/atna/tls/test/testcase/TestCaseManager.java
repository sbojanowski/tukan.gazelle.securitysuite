package net.ihe.gazelle.atna.tls.test.testcase;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.test.TlsTestDataSetDisplayer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.enums.TlsAlertDescription;
import net.ihe.gazelle.simulators.tls.enums.TlsAlertLevel;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import net.ihe.gazelle.simulators.tls.test.AutomatedSimulatorFactory;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSet;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSetQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;

@Name("testCaseManager")
@Scope(ScopeType.PAGE)
public class TestCaseManager implements Serializable, TlsTestDataSetDisplayer {

    private static final long serialVersionUID = -1054917871336064675L;

    private static Logger log = LoggerFactory.getLogger(TestCaseManager.class);

    private int currentTestCaseId = 0;
    private transient TlsTestCase currentTestCase = null;
    private int currentDataSetId = 0;
    private transient TlsTestDataSet currentDataSet = null;
    private List<TlsAlertDescription> alertDescriptions = new ArrayList<TlsAlertDescription>();

    @Create
    public void initialize() {

        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("testCase") != null) {
            try {
                currentTestCaseId = Integer.decode(urlParameters.get("testCase"));
                currentTestCase = EntityManagerService.provideEntityManager().find(TlsTestCase.class,
                        currentTestCaseId);
                if (currentTestCase != null) {
                    // load object parameters
                    setCurrentDataSet(currentTestCase.getTlsDataSet());
                    this.alertDescriptions = new ArrayList<TlsAlertDescription>(
                            currentTestCase.getExpectedAlertDescriptions());

                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No such test case recorded in database");
                    log.warn("No such test case recorded in database");
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("testCase") + "' is not a valid test case id");
                log.warn("'" + urlParameters.get("testCase") + "' is not a valid test case id", e);
            }
        } else {
            // default parameters
            this.currentTestCase = new TlsTestCase();
            this.currentTestCase.setContext(ContextType.IHE);
            this.currentTestCase.setSutType(SimulatorType.SERVER);
            this.currentTestCase.setExpectedHandshakeSuccess(true);
            this.currentTestCase.setMandatoryHandshakeSuccess(true);
        }
    }

    public TlsTestCase getCurrentTestCase() {
        if (currentTestCase == null) {
            currentTestCase = EntityManagerService.provideEntityManager().find(TlsTestCase.class, currentTestCaseId);
        }
        return currentTestCase;
    }

    private void setCurrentTestCase(TlsTestCase testCase){
        this.currentTestCaseId = testCase.getId();
        this.currentTestCase = testCase;
    }

    public TlsTestDataSet getCurrentDataSet() {
        if (currentDataSet == null) {
            currentDataSet = EntityManagerService.provideEntityManager().find(TlsTestDataSet.class, currentDataSetId);
        }
        return currentDataSet;
    }

    public void setCurrentDataSet(TlsTestDataSet currentDataSet) {
        if(currentDataSet == null) {
            this.currentDataSetId = 0;
        } else {
            this.currentDataSetId = currentDataSet.getId();
        }
        this.currentDataSet = currentDataSet;
    }

    public List<TlsAlertDescription> getAlertDescriptions() {
        return alertDescriptions;
    }

    public void setAlertDescriptions(List<TlsAlertDescription> alertDescriptions) {
        this.alertDescriptions = alertDescriptions;
    }

    public String resetTlsDataSet() {
        setCurrentDataSet(null);
        return null;
    }

    public boolean isTlsDataSetSelected() {
        return getCurrentDataSet() != null;
    }

    public SelectItem[] getContextTypeItems() {
        return ContextType.getSelectItems();
    }

    public SelectItem[] getSimulatorTypeItems() {
        return SimulatorType.getSelectItems();
    }

    public boolean isEditForm() {
        return this.currentTestCaseId != 0;
    }

    public String saveTestCase() {

        currentTestCase = getCurrentTestCase();
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        // load parameters selection into testCase Object.
        currentTestCase.setTlsDataSet(getCurrentDataSet());
        currentTestCase.setExpectedAlertDescriptions(new HashSet<TlsAlertDescription>(alertDescriptions));
        TlsSimulator newSimu = AutomatedSimulatorFactory.newSimulator(currentTestCase);
        currentTestCase.setSimulator(newSimu);

        try {
            setCurrentTestCase(entityManager.merge(currentTestCase));
            entityManager.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO,
                    "Test case \"" + currentTestCase.getName() + "\" saved.");
            return backToList();
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Failed to save test case (" + e.getMessage() + ").");
            log.error("Failed to save test case.", e);
            return null;
        }
    }

    public String backToList() {
        return Pages.TLS_LIST_TEST_CASE.getMenuLink();
    }

    public String linkToTestCaseList() {
        return ApplicationManagerBean.getUrl() + backToList();
    }

    @Override
    public String linkToCertificate() {
        if (currentDataSet != null) {
            return ApplicationManagerBean.getUrl() + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() +
                    "?id=" + currentDataSet.getCertificate().getId();
        } else {
            return null;
        }
    }

    public SelectItem[] getTlsDataSetItems() {
        List<SelectItem> result = new ArrayList<SelectItem>();

        for (TlsTestDataSet dataset : getTlsDataSets()) {
            result.add(new SelectItem(dataset, dataset.getName() + " (" + dataset.getId() + ")"));
        }
        return result.toArray(new SelectItem[result.size()]);
    }


    public SelectItem[] getAlertLevelItems() {
        return TlsAlertLevel.getSelectItems();
    }

    public SelectItem[] getAlertDescriptionItems() {
        return TlsAlertDescription.getSelectItems();
    }

    private List<TlsTestDataSet> getTlsDataSets() {
        TlsTestDataSetQuery dataSetQuery = new TlsTestDataSetQuery();
        dataSetQuery.context().eq(getCurrentTestCase().getContext());
        List<TlsTestDataSet> list = dataSetQuery.getList();
        Collections.sort(list, new Comparator<TlsTestDataSet>() {
            public int compare(TlsTestDataSet o1, TlsTestDataSet o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }


}
