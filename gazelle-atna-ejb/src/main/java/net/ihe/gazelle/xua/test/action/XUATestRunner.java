package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xua.dao.AncillaryTransactionDAO;
import net.ihe.gazelle.xua.dao.ServiceProviderTestCaseDAO;
import net.ihe.gazelle.xua.executor.XUATestExecutor;
import net.ihe.gazelle.xua.model.AncillaryTransaction;
import net.ihe.gazelle.xua.model.ServiceProviderTestCase;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

/**
 * Created by aberge on 02/06/17.
 */

@Name("xuaTestRunner")
@Scope(ScopeType.PAGE)
public class XUATestRunner {

    private ServiceProviderTestInstance testInstance;

    private ServiceProviderTestCase testCase;
    private AncillaryTransaction selectedTransaction;
    private List<AncillaryTransaction> availableTransactions = null;
    private boolean displayResult;

    @Create
    public void initialize(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String testIdAsString = urlParams.get("testcaseid");
        if (testIdAsString == null){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You shall provide a test case identifier");
        } else {
            Integer testCaseId = Integer.parseInt(testIdAsString);
            this.testCase = ServiceProviderTestCaseDAO.getTestCaseById(testCaseId);
            if (this.testCase == null){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, testIdAsString + " does not match a known test case identifier");
            } else {
                initializeTestInstanceForTest(testCase);
            }
        }
        displayResult = false;
    }

    private void initializeTestInstanceForTest(ServiceProviderTestCase testCase) {
        this.testInstance = new ServiceProviderTestInstance(testCase);
        if (Identity.instance().isLoggedIn()){
            testInstance.setUsername(Identity.instance().getCredentials().getUsername());
        }
    }

    public List<AncillaryTransaction> getAvailableTransactions(){
        if (this.availableTransactions == null){
            availableTransactions = AncillaryTransactionDAO.getAllTransactions();
        }
        return this.availableTransactions;
    }

    public ServiceProviderTestInstance getTestInstance() {
        return testInstance;
    }

    public void setTestInstance(ServiceProviderTestInstance testInstance) {
        this.testInstance = testInstance;
    }

    public ServiceProviderTestCase getTestCase() {
        return testCase;
    }

    public void execute(){
        String xuaFileLocation = PreferenceService.getString("xua_soap_body_directory");
        testInstance.setAncillaryTransaction(selectedTransaction);
        if (xuaFileLocation == null){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No directory defined to read soap body files, create preference xua_soap_body_directory");
        } else {
            XUATestExecutor executor = new XUATestExecutor(testInstance, xuaFileLocation);
            testInstance = executor.execute();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test case completed, see details below");
            displayResult = true;
        }
    }

    public AncillaryTransaction getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(AncillaryTransaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public String getSentMessage(){
        if (testInstance != null){
            return testInstance.getRequestAsFormattedXML();
        } else {
            return null;
        }
    }


    public String getReceivedMessage(){
        if (testInstance != null){
            return testInstance.getResponseAsFormattedXML();
        } else {
            return "No message has been recorded";
        }
    }

    public boolean isDisplayResult() {
        return displayResult;
    }

    public void resetTestInstance(){
        initializeTestInstanceForTest(this.testCase);
        this.displayResult = false;
    }

    public String backToListOfTestInstances(){
        return Pages.XUA_TEST_INSTANCES.getMenuLink();
    }
}
