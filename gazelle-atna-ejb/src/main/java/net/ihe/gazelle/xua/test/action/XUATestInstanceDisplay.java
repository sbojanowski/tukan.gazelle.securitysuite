package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.xua.dao.ServiceProviderTestInstanceDAO;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import org.dom4j.DocumentException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by aberge on 07/06/17.
 */
@Name("xuaTestInstanceDisplay")
@Scope(ScopeType.PAGE)
public class XUATestInstanceDisplay implements Serializable {

    public static final String UTF_8 = "UTF-8";

    public ServiceProviderTestInstance getSelectedTestInstance() {
        return selectedTestInstance;
    }

    private ServiceProviderTestInstance selectedTestInstance;

    @Create
    public void initialize(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idAsString = urlParams.get("id");
        if (idAsString == null){
            selectedTestInstance = null;
        } else {
            try {
                Integer id = Integer.parseInt(idAsString);
                selectedTestInstance = ServiceProviderTestInstanceDAO.getTestInstanceById(id);
            }catch (NumberFormatException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, idAsString + " is not a valid test instance id");
            }
        }
    }



    public String getSentMessage(){
        if (selectedTestInstance != null){
            return selectedTestInstance.getRequestAsFormattedXML();
        } else {
            return null;
        }
    }


    public String getReceivedMessage(){
        if (selectedTestInstance != null){
            return selectedTestInstance.getResponseAsFormattedXML();
        } else {
            return "No message has been recorded";
        }
    }

    public String backToListOfbackToListOfTestCasesTestInstances(){
        return Pages.XUA_TEST_CASES.getMenuLink();
    }
}
