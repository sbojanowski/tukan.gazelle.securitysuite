package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.xua.dao.ServiceProviderTestCaseDAO;
import net.ihe.gazelle.xua.model.ServiceProviderTestCase;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by aberge on 08/06/17.
 */
@Name("xuaTestCaseDisplay")
@Scope(ScopeType.PAGE)
public class XUATestCaseDisplay implements Serializable {

    private ServiceProviderTestCase selectedTest;

    @Create
    public void initialize(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idAsString = urlParams.get("id");
        if (idAsString == null){
            selectedTest = null;
        } else {
            try {
                Integer id = Integer.parseInt(idAsString);
                selectedTest = ServiceProviderTestCaseDAO.getTestCaseById(id);
            }catch (NumberFormatException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, idAsString + " is not a valid test case id");
            }
        }
    }

    public ServiceProviderTestCase getSelectedTest() {
        return selectedTest;
    }

    public String executeTest(){
        return "/xuatesting/execute.seam?testcaseid=" + selectedTest.getId();
    }

    public String backToTestList(){
        return Pages.XUA_TEST_CASES.getMenuLink();
    }
}
