package net.ihe.gazelle.simulators.tls.test.common;

import net.ihe.gazelle.pki.env.CrlServer;
import net.ihe.gazelle.pki.env.TestParam;
import net.ihe.gazelle.preferences.PreferenceService;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.security.Security;

/**
 * Created by cel on 22/07/15.
 */
@PowerMockIgnore({"javax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class })
@RunWith(PowerMockRunner.class)
public abstract class TLSTestEnvironment {

    private static CrlServer crlServer;
    private static String BCproviderName;

    @Before
    public void mockPreferences(){
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getString("crl_url")).thenReturn(TestParam.CRL_URL);
        Mockito.when(PreferenceService.getString("java_cacerts_truststore_pwd")).thenReturn(TestParam.CACERTS_PASSWORD);
    }

    /*******
     * SETUP / TEARDOWN
     ******/

    @BeforeClass
    public static void classSetUp() throws Exception {
        System.out.println("setUp PKI Test dependencies");
        setUpBouncyCastle();
        setUpCrlServer();
    }

    @AfterClass
    public static void classTearDown() throws Exception {
        System.out.println("tearDown PKI Test dependencies");
        tearDownCrlServer();
        tearDownBouncyCastle();
    }

    /******
     * BOUNCYCASTLE
     ******/

    protected static void setUpBouncyCastle() {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        BCproviderName = provider.getName();
        Security.addProvider(provider);
    }

    protected static void tearDownBouncyCastle() {
        Security.removeProvider(BCproviderName);
    }

    /******
     * CRL SERVER
     *****/

    protected static void setUpCrlServer() {
        crlServer = new CrlServer(TestParam.CRL_PORT, false);
        crlServer.start();
    }

    protected static void tearDownCrlServer() {
        crlServer.stop();
    }

}
