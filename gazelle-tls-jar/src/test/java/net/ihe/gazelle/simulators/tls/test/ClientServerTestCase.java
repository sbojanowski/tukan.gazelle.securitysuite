package net.ihe.gazelle.simulators.tls.test;

import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.client.ClientHTTPSimple;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.test.common.SslConnectionListenerStream;
import net.ihe.gazelle.simulators.tls.test.common.TLSTestEnvironment;
import org.junit.Before;
import org.junit.Test;

import java.security.cert.CertificateException;

import static org.junit.Assert.*;

public class ClientServerTestCase extends TLSTestEnvironment {

    private static final KeyAlgorithm KEY_ALGORITHM = KeyAlgorithm.RSA;
    private static final int KEY_LENGTH = 1024;

    private Certificate[] certificateAuthorities = new Certificate[2];
    private Certificate[] certificateClients = new Certificate[2];
    private Certificate[] certificateServers = new Certificate[2];

    @Before
    public void setUp() throws Exception {
        for (int i = 0; i < 2; i++) {
            certificateAuthorities[i] = createCertificateAuthority("CN=JUnit" + i + ", OU=JUnit" + i + ", O=JUnit" + i
                    + ", C=FR");

            certificateClients[i] = generateCertificate("JUnit client " + i, CertificateType.HTTPS_CLIENT_AUTH,
                    certificateAuthorities[i]);
            certificateServers[i] = generateCertificate("JUnit server " + i, CertificateType.HTTPS_SERVER,
                    certificateAuthorities[i]);

            CrlUtil.addTestCertificate(certificateAuthorities[i]);
            CrlUtil.addTestCertificate(certificateClients[i]);
            CrlUtil.addTestCertificate(certificateServers[i]);
        }
    }

    private Certificate generateCertificate(String commonName, CertificateType type, Certificate ca)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys crwgk = new CertificateRequestWithGeneratedKeys(KEY_ALGORITHM, KEY_LENGTH);
        crwgk.setSubjectUsingAttributes("FR", "JUnit", commonName, null, null, null, null, null);
        crwgk.setCertificateAuthority(ca);
        crwgk.setCertificateExtension(type);
        crwgk.setNotAfter(CertificateUtil.getNotAfter());
        crwgk.setNotBefore(CertificateUtil.getNotBefore());
        crwgk.setRequester("JUnit", "TEST");

        Certificate certificate = CertificateManager.createCertificate(crwgk, null);
        return certificate;
    }

    private Certificate createCertificateAuthority(String subject) throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);

        cra.setSubject(subject);
        cra.setIssuer(subject);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        return CertificateManager.createCertificateAuthority(cra, null);
    }


    @Test
    public void testClientServerCross1111() throws Exception {
        clientServer(0, 0, 0, 0, false, 15000);
    }

    @Test
    public void testClientServerCross1121() throws Exception {
        clientServer(0, 0, 1, 0, true, 15001);
    }

    @Test
    public void testClientServerCross1112() throws Exception {
        clientServer(0, 0, 0, 1, true, 15002);
    }

    @Test
    public void testClientServerCross1122() throws Exception {
        clientServer(0, 0, 1, 1, true, 15003);
    }

    @Test
    public void testClientServerCross2111() throws Exception {
        clientServer(1, 0, 0, 0, true, 15004);
    }

    @Test
    public void testClientServerCross2121() throws Exception {
        clientServer(1, 0, 1, 0, true, 15005);
    }

    @Test
    public void testClientServerCross2112() throws Exception {
        clientServer(1, 0, 0, 1, false, 15006);
    }

    @Test
    public void testClientServerCross2122() throws Exception {
        clientServer(1, 0, 1, 1, true, 15007);

    }

    @Test
    public void testClientServerCross1211() throws Exception {
        clientServer(0, 1, 0, 0, true, 15008);
    }

    @Test
    public void testClientServerCross1221() throws Exception {
        clientServer(0, 1, 1, 0, false, 15009);
    }

    @Test
    public void testClientServerCross1212() throws Exception {
        clientServer(0, 1, 0, 1, true, 15010);
    }

    @Test
    public void testClientServerCross1222() throws Exception {
        clientServer(0, 1, 1, 1, true, 15011);

    }

    @Test
    public void testClientServerCross2211() throws Exception {
        clientServer(1, 1, 0, 0, true, 15012);
    }

    @Test
    public void testClientServerCross2221() throws Exception {
        clientServer(1, 1, 1, 0, true, 15013);
    }

    @Test
    public void testClientServerCross2212() throws Exception {
        clientServer(1, 1, 0, 1, true, 15014);
    }

    @Test
    public void testClientServerCross2222() throws Exception {
        clientServer(1, 1, 1, 1, false, 15015);
    }

    private void clientServer(int clientCert, int serverCert, int trustedClient, int trustedServer, boolean shouldFail,
                              int port) throws Exception {
        clientServer(clientCert + "-" + serverCert + "-" + trustedClient + "-" + trustedServer,
                certificateClients[clientCert], certificateServers[serverCert], certificateAuthorities[trustedClient],
                certificateAuthorities[trustedServer], shouldFail, 15015);
    }

    private void clientServer(String code, Certificate clientCert, Certificate serverCert, Certificate trustedClient,
                              Certificate trustedServer, boolean shouldFail, int port) throws Exception {
        System.out.println(code + " " + shouldFail);

        TlsServer server = getServer(serverCert, trustedServer);
        TlsClient client = getClient(clientCert, trustedClient);

        SslConnectionListenerStream listenerServer = new SslConnectionListenerStream("<<< ", System.out);
        SslConnectionListenerStream listenerClient = new SslConnectionListenerStream(">>> ", System.out);

        pingClientServer(listenerServer, listenerClient, server, client, port);

        if (shouldFail) {
            TlsConnection lastConnection = listenerServer.getLastConnection();
            assertFalse(lastConnection.isSuccess());

            lastConnection = listenerClient.getLastConnection();
            assertFalse(lastConnection.isSuccess());
        } else {
            TlsConnection lastConnection = listenerServer.getLastConnection();
            assertTrue(lastConnection.isSuccess());

            lastConnection = listenerClient.getLastConnection();
            assertTrue(lastConnection.isSuccess());
        }

    }

    private void pingClientServer(TlsConnectionListener listenerServer, TlsConnectionListener listenerClient,
                                  TlsServer server, TlsClient client, int port) {
        Proxy<?, ?> proxy = null;
        try {
            server.setLocalPort(port);
            proxy = server.start(listenerServer);
        } catch (TlsSimulatorException e) {
            fail(e.toString());
        }

        try {
            ClientHTTPSimple clientHttp = new ClientHTTPSimple();
            clientHttp.getUrl().setValueAsString("/");
            client.ping(listenerClient, "127.0.0.1", port, clientHttp);
        } catch (TlsSimulatorException e1) {
            fail("Unable to ping");
            proxy.stop();
        }

        proxy.stop();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private TlsServer getServer(Certificate certificate, Certificate... issuers) {
        TlsServer server = new TlsServer();
        setParametersServer(server);
        server.setCertificate(certificate);
        for (Certificate issuer : issuers) {
            server.getIssuers().add(issuer);
        }
        server.setValidator(CertificateValidatorType.TLS_SERVER);
        return server;
    }

    private TlsClient getClient(Certificate certificate, Certificate... issuers) {
        TlsClient client = new TlsClient();
        setParametersClient(client);
        client.setCertificate(certificate);
        for (Certificate issuer : issuers) {
            client.getIssuers().add(issuer);
        }
        client.setValidator(CertificateValidatorType.TLS_CLIENT);
        return client;
    }

    private void setParametersClient(TlsClient simulator) {
        simulator.getCipherSuites().add(CipherSuiteType.TLS_RSA_WITH_AES_128_CBC_SHA);
        simulator.getProtocols().add(ProtocolType.TLSv1);
        simulator.setCheckRevocation(true);
    }

    private void setParametersServer(TlsServer simulator) {
        simulator.getCipherSuites().add(CipherSuiteType.TLS_RSA_WITH_AES_128_CBC_SHA);
        simulator.getCipherSuites().add(CipherSuiteType.TLS_DHE_RSA_WITH_AES_128_CBC_SHA);
        simulator.getProtocols().add(ProtocolType.TLSv1);
        simulator.setNeedClientAuth(true);
        simulator.setCheckRevocation(true);
        simulator.setRemoteHost("www.google.fr");
        simulator.setRemotePort(80);
        simulator.setChannelType(ChannelType.HTTP);
    }
}
