package net.ihe.gazelle.simulators.tls.client;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;

import java.util.List;

public interface Client {

    /**
     * @param proxyToUse proxy to start
     * @return Should TlsClient close the proxy on return?
     */
    boolean start(Proxy<?, ?> proxyToUse);

    List<ClientParameter> getParameters();

    ChannelType getChannelType();

}
