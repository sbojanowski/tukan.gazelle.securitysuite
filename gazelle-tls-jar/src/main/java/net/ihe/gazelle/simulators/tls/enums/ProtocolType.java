package net.ihe.gazelle.simulators.tls.enums;

import javax.faces.model.SelectItem;

public enum ProtocolType {

    SSLv2Hello("SSLv2Hello"),
    SSLv3("SSLv3"),
    TLSv1("TLSv1"),
    TLSv11("TLSv1.1"),
    TLSv12("TLSv1.2");

    private String name;

    ProtocolType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return getName();
    }

    public static ProtocolType getProtocolTypeFromName(String name) {
        if (name != null && !name.isEmpty()) {
            for (ProtocolType value : ProtocolType.values()) {
                if (value.getName().equals(name)) {
                    return value;
                }
            }
            throw new IllegalArgumentException("No enum constant " + ProtocolType.class.getCanonicalName() + "." + name);
        } else {
            throw new IllegalArgumentException("Name cannot be null or empty to find " + ProtocolType.class.getCanonicalName() + " enum value");
        }
    }

    /**
     * Get the enumeration values formated as Items for a h:SelectMenu
     * @return the enumeration values formated as Items for a h:SelectMenu
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = ProtocolType.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

}
