package net.ihe.gazelle.simulators.tls.test.model;

import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.TestVerdictType;
import net.ihe.gazelle.simulators.tls.enums.TlsAlertDescription;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.test.TlsPilot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

@Entity
@Table(name = "tls_test_instance", schema = "public")
@SequenceGenerator(name = "tls_test_instance_sequence", sequenceName = "tls_test_instance_id_seq", allocationSize = 1)
public class TlsTestInstance extends AuditedObject implements TlsConnectionListener {

    private static Logger log = LoggerFactory.getLogger(TlsTestInstance.class);

    // can be define in an abstract TestInstance Class
    @Id
    @GeneratedValue(generator = "tls_test_instance_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    // A link to an abstract TestCase class can be define in the abstract TestInstance class
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "test_case_id")
    private TlsTestCase testCase;

    // specific to TLS
    @Column(name = "sut_host")
    private String sutHost;

    // specific to TLS
    @Column(name = "sut_port")
    private int sutPort;

    // specific to TLS
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private TlsConnection connection;

    // can be define in an abstract TestInstance Class
    @Column(name = "test_verdict")
    private TestVerdictType testVerdict;

    // specific to TLS
    @Column(name = "handshake_verdict")
    private TestVerdictType handshakeVerdict;

    // specific to TLS
    @Column(name = "alert_level_verdict")
    private TestVerdictType alertLevelVerdict;

    // specific to TLS
    @Column(name = "alert_description_verdict")
    private TestVerdictType alertDescriptionVerdict;

    // specific to TLS
    public TlsTestInstance() {
        this.testVerdict = TestVerdictType.NOT_RUN;
        this.handshakeVerdict = TestVerdictType.NOT_RUN;
        this.alertLevelVerdict = TestVerdictType.NOT_RUN;
        this.alertDescriptionVerdict = TestVerdictType.NOT_RUN;
    }

    /**
     * TlsTestInstance constructor initialized a new instance.
     *
     * @param tlsTestCase     The test case of this instance.
     * @param sutHost         The host of the SUT
     * @param sutPort         The port of the SUT
     */
    public TlsTestInstance(TlsTestCase tlsTestCase, String sutHost, int sutPort) {
        // can be set by super() method of the abstract TestInstance class.
        this.testVerdict = TestVerdictType.NOT_RUN;

        // specific to TLS
        this.handshakeVerdict = TestVerdictType.NOT_RUN;
        this.alertLevelVerdict = TestVerdictType.NOT_RUN;
        this.alertDescriptionVerdict = TestVerdictType.NOT_RUN;
        this.testCase = tlsTestCase;
        this.sutHost = sutHost;
        this.sutPort = sutPort;
    }

    public int getId() {
        return id;
    }

    public TlsTestCase getTestCase() {
        return testCase;
    }

    public int getTestCaseId() {
        return testCase.getId();
    }

    public TlsTestDataSet getTestDataSet() {
        return testCase.getTlsDataSet();
    }

    public String getSutHost() {
        return sutHost;
    }

    public int getSutPort() {
        return sutPort;
    }

    public TlsConnection getConnection() {
        return connection;
    }

    public void setConnection(TlsConnection connection) {
        this.connection = connection;
        Analyze();
    }

    public TestVerdictType getTestVerdict() {
        return testVerdict;
    }

    public TestVerdictType getHandshakeVerdict() {
        return handshakeVerdict;
    }

    public TestVerdictType getAlertLevelVerdict() {
        return alertLevelVerdict;
    }

    public TestVerdictType getAlertDescriptionVerdict() {
        return alertDescriptionVerdict;
    }

    /**
     * This method to run the test instance. It should be called only once per testInstance. The given pilot will be initialized with data set values, So it's better to give a new object without
     * existing connections. The proxy will then callback the TlsConnectionListenner (here the testInstance) to indicate the test is terminated.
     *
     * @param pilot          simulator that run the test.
     * @param applicationMsg client message (HL7, RAW, etc) to transmit. Can be null if simulator is a server.
     * @throws TlsSimulatorException if unable to set up TLS context.
     */
    public void execute(TlsPilot pilot, Client applicationMsg) throws TlsSimulatorException {
        pilot.executeTest(this, this.getTestCaseId(), sutHost, sutPort, applicationMsg);
    }

    @Override
    public void logConnection(final TlsConnection connection, int channelId) {
        this.setConnection(connection);
    }

    private void Analyze() {
        if (connection != null) {

            analyzeHandshake();
            analyzeAlertLevel();
            analyzeAlertDescription();

            testVerdict = TestVerdictType.PASSED;
            // as soon one of the criteria is failed and mandatory, the test is failed.
            if (handshakeVerdict == TestVerdictType.FAILED && testCase.isMandatoryHandshakeSuccess()
                    || alertLevelVerdict == TestVerdictType.FAILED && testCase.isMandatoryAlertLevel()
                    || alertDescriptionVerdict == TestVerdictType.FAILED && testCase.isMandatoryAlertDescription()) {
                testVerdict = TestVerdictType.FAILED;
            }
            // and if all criteria are failed and NOT mandatory, the test is also failed.
            if (handshakeVerdict == TestVerdictType.FAILED && alertLevelVerdict == TestVerdictType.FAILED
                    && alertDescriptionVerdict == TestVerdictType.FAILED) {
                testVerdict = TestVerdictType.FAILED;
            }

        } else {
            testVerdict = TestVerdictType.FAILED;
            log.error("connection could not be retrieve.");
        }
    }

    private void analyzeHandshake() {
        if (testCase.isExpectedHandshakeSuccess() == true) {
            if (connection.isSuccess()) {
                handshakeVerdict = TestVerdictType.PASSED;
            } else {
                handshakeVerdict = TestVerdictType.FAILED;
            }
        } else {
            if (connection.isSuccess() == false && connection.getExceptionMessage() != null && connection
                    .getExceptionMessage().contains("Received")) {
                handshakeVerdict = TestVerdictType.PASSED;
            } else {
                handshakeVerdict = TestVerdictType.FAILED;
            }
        }
    }

    private void analyzeAlertLevel() {
        if (connection.getExceptionMessage() == null && testCase.getExpectedAlertLevel() == null) {
            alertLevelVerdict = TestVerdictType.PASSED;
        } else if (connection.getExceptionMessage() != null && testCase.getExpectedAlertLevel() != null) {
            if (connection.getExceptionMessage().contains(testCase.getExpectedAlertLevel().toString())) {
                alertLevelVerdict = TestVerdictType.PASSED;
            } else {
                alertLevelVerdict = TestVerdictType.FAILED;
            }
        } else {
            alertLevelVerdict = TestVerdictType.FAILED;
        }
    }

    private void analyzeAlertDescription() {
        if (connection.getExceptionMessage() == null && testCase.getExpectedAlertDescriptions().isEmpty()) {
            alertDescriptionVerdict = TestVerdictType.PASSED;
        } else if (connection.getExceptionMessage() != null && !testCase.getExpectedAlertDescriptions().isEmpty()) {

            //initialize description verdict before loop
            alertDescriptionVerdict = TestVerdictType.FAILED;
            //loop trough every accepted alert description and see if the connection error message contains the alert.
            for (TlsAlertDescription alert : testCase.getExpectedAlertDescriptions()) {
                if (connection.getExceptionMessage().contains(alert.toString())) {
                    alertDescriptionVerdict = TestVerdictType.PASSED;
                }
            }

        } else {
            alertDescriptionVerdict = TestVerdictType.FAILED;
        }
    }

}
