package net.ihe.gazelle.simulators.tls.common;

import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;
import net.ihe.gazelle.simulators.tls.model.*;
import org.bouncycastle.jce.X509Principal;

import java.util.ArrayList;
import java.util.List;

public class TlsConnectionListenerCheckClientAuth implements TlsConnectionListener {

    private TlsClient tlsClient;

    public TlsConnectionListenerCheckClientAuth(TlsClient tlsClient) {
        super();
        this.tlsClient = tlsClient;
    }

    @Override
    public void logConnection(TlsConnection connection, int channelId) {
        if (connection != null && connection.isSuccess()) {
            CheckClientAuthResult result = CheckClientAuthResult.NO_AUTHENTICATION;
            if (connection.getData() != null && connection.getData().getItems() != null) {
                List<TlsConnectionDataItem> items = connection.getData().getItems();
                for (TlsConnectionDataItem tlsConnectionDataItem : items) {
                    if (tlsConnectionDataItem.getType().equals(TlsConnectionDataItemType.CHOOSE_ALIAS_CLIENT)) {
                        result = checkValidAuthentication(connection,
                                (TlsConnectionDataItemAliases) tlsConnectionDataItem);
                    }
                }
            }
            if (result != CheckClientAuthResult.OK) {
                String exceptionMessage = connection.getExceptionMessage();
                if (exceptionMessage == null) {
                    exceptionMessage = result.getMessage();
                } else {
                    exceptionMessage = result.getMessage() + " AND " + exceptionMessage;
                }
                connection.setExceptionMessage(exceptionMessage);
            }
            connection.setSuccess(result.isValidRegardingATNA());
        }
    }

    private CheckClientAuthResult checkValidAuthentication(TlsConnection connection,
                                                           TlsConnectionDataItemAliases tlsConnectionDataItem) {
        List<X509Principal> validIssuers = new ArrayList<X509Principal>();
        addIssuers(tlsClient.getCertificate(), validIssuers);

        List<TlsConnectionDataItemAliasesItem> issuers = tlsConnectionDataItem.getIssuers();
        if (issuers.isEmpty()){
            return CheckClientAuthResult.CA_LIST_EMPTY;
        } else {
            for (TlsConnectionDataItemAliasesItem issuer : issuers) {
                X509Principal x509Name = new X509Principal(issuer.getValue());
                if (validIssuers.contains(x509Name)) {
                    return CheckClientAuthResult.OK;
                }
            }
            return CheckClientAuthResult.CA_NOT_FOUND;
        }
    }

    private void addIssuers(Certificate certificate, List<X509Principal> validIssuers) {
        validIssuers.add(new X509Principal(certificate.getSubject()));
        Certificate certificateAuthority = certificate.getCertificateAuthority();
        if (certificateAuthority != null) {
            addIssuers(certificateAuthority, validIssuers);
        }
    }
}
