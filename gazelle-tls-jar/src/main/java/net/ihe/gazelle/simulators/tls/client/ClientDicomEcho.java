package net.ihe.gazelle.simulators.tls.client;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterString;

import org.apache.log4j.Logger;
import org.dcm4che2.tool.dcmecho.DcmEcho;

public class ClientDicomEcho implements Client {

    private static final Logger log = Logger.getLogger(ClientDicomEcho.class);

    private ClientParameterString aet = new ClientParameterString("Application entity title (AET)", "OF_MAWF");

    @Override
    public boolean start(Proxy<?, ?> proxyToUse) {
        DcmEcho dcmecho = new DcmEcho("DCMECHO");
        dcmecho.setCalledAET(aet.getValueAsString(), false);
        dcmecho.setRemoteHost("127.0.0.1");
        dcmecho.setRemotePort(proxyToUse.getProxyProviderPort());

        try {
            dcmecho.open();
            dcmecho.echo();
            dcmecho.close();
        } catch (Exception e) {
            log.error(e);
        }

        return true;
    }

    public ClientParameterString getAet() {
        return aet;
    }

    @Override
    public List<ClientParameter> getParameters() {
        ArrayList<ClientParameter> list = new ArrayList<ClientParameter>();
        list.add(aet);
        return list;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.DICOM;
    }
}
