package net.ihe.gazelle.simulators.tls.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterString;

public class ClientHTTPSimple extends ClientSocket {

    private ClientParameterString url;

    private List<ClientParameter> parameters;

    public ClientHTTPSimple() {
        super();
        url = new ClientParameterString("Relative URI", "/");

        parameters = new ArrayList<ClientParameter>();
        parameters.add(url);
    }

    public ClientParameterString getUrl() {
        return url;
    }

    @Override
    public void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        println(writer, "GET " + url.getValueAsString() + " HTTP/1.1");
        println(writer, "User-Agent: TLS IHE testing tools");
        println(writer, "Accept: */*");
        println(writer, "Host: " + proxyToUse.getProxyConsumerHost() + ":" + proxyToUse.getProxyConsumerPort());
        println(writer, "Connection: close");
        println(writer, "");
        println(writer, "");
        writer.flush();
    }

    @Override
    public List<ClientParameter> getParameters() {
        return parameters;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.HTTP;
    }

}
