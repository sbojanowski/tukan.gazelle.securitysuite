package net.ihe.gazelle.simulators.tls.enums;

import javax.faces.model.SelectItem;
import java.io.Serializable;

public enum TlsAlertLevel implements Serializable {

    WARNING("warning", 1),
    FATAL("fatal", 2);

    private String label;
    private int value;

    TlsAlertLevel(String label, int value) {
        this.label = label;
        this.value = value;
    }

    public String toString() {
        return label;
    }

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    /**
     * Get the enumeration values formated as Items for a h:SelectMenu
     * @return the enumeration values formated as Items for a h:SelectMenu
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = TlsAlertLevel.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }
}
