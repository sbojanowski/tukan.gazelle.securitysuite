package net.ihe.gazelle.simulators.tls.enums;

import javax.faces.model.SelectItem;

public enum TlsAlertDescription {

    CLOSE_NOTIFY("close_notify", 0),
    UNEXPECTED_MESSAGE("unexpected_message", 10),
    BAD_RECORD_MAC("bad_record_mac", 20),
    DECRYPTION_FAILED("decryption_failed", 21),
    RECORD_OVERFLOW("record_overflow", 22),
    DECOMPRESSION_FAILURE("decompression_failure", 30),
    HANDSHAKE_FAILURE("handshake_failure", 40),
    BAD_CERTIFICATE("bad_certificate", 42),
    UNSUPPORTED_CERTIFICATE("unsupported_certificate", 43),
    CERTIFICATE_REVOKED("certificate_revoked", 44),
    CERTIFICATE_EXPIRED("certificate_expired", 45),
    CERTIFICATE_UNKNOWN("certificate_unknown", 46),
    ILLEGAL_PARAMETER("illegal_parameter", 47),
    UNKNOWN_CA("unknown_ca", 48),
    ACCESS_DENIED("access_denied", 49),
    DECODE_ERROR("decode_error", 50),
    DECRYPT_ERROR("decrypt_error", 51),
    EXPORT_RESTRICTION("export_restriction", 60),
    PROTOCOL_VERSION("protocol_version", 70),
    INSUFFICIENT_SECURITY("insufficient_security", 71),
    INTERNAL_ERROR("internal_error", 80),
    USER_CANCELED("user_canceled", 90),
    NO_RENEGOTIATION("no_renegotiation", 100);

    private String label;
    private int value;

    TlsAlertDescription(String label, int value) {
        this.label = label;
        this.value = value;
    }

    public String toString() {
        return label;
    }

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    /**
     * Get the enumeration values formated as Items for a h:SelectMenu
     * @return the enumeration values formated as Items for a h:SelectMenu
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = TlsAlertDescription.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }
}
