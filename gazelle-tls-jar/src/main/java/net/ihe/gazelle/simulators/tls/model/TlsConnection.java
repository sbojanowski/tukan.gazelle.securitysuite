package net.ihe.gazelle.simulators.tls.model;

import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLSession;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.security.cert.X509Certificate;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;

@Entity
@Table(name = "tls_connection", schema = "public")
@SequenceGenerator(name = "tls_connection_sequence", sequenceName = "tls_connection_id_seq", allocationSize = 1)
public class TlsConnection implements Serializable {

    private static final long serialVersionUID = -5615576821955233258L;
    static final Logger log = LoggerFactory.getLogger(TlsConnection.class);

    @Id
    @GeneratedValue(generator = "tls_connection_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", columnDefinition = "timestamp with time zone")
    private Date date;

    @Column(name = "local_host")
    private String localHost;

    @Column(name = "local_port")
    private int localPort;

    @Column(name = "remote_host")
    private String remoteHost;

    @Column(name = "remote_port")
    private int remotePort;

    @Column(name = "is_success")
    private boolean success;

    @Column(name = "renegociation_counter")
    private int renegociationCounter;

    @Column(name = "proxy_connection_uuid")
    private String proxyConnectionUuid;

    @Column(name = "exception_message")
    @Lob
    @Type(type = "text")
    private String exceptionMessage;

    @Column(name = "exception_stacktrace")
    @Lob
    @Type(type = "text")
    private String exceptionStackTrace;

    @Column(name = "peer_certificate_chain_pem")
    @Lob
    @Type(type = "text")
    private String peerCertificateChainPEM;

    @Embedded
    private TlsConnectionData data;

    @Column(name = "cipher_suite")
    private CipherSuiteType cipherSuite;

    @Column(name = "protocol")
    private ProtocolType protocol;

    @ManyToOne
    @JoinColumn(name = "simulator_id")
    private TlsSimulator simulator;

    public TlsConnection() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getRenegociationCounter() {
        return renegociationCounter;
    }

    public void setRenegociationCounter(int renegociationCounter) {
        this.renegociationCounter = renegociationCounter;
    }

    public TlsSimulator getSimulator() {
        return simulator;
    }

    public void setSimulator(TlsSimulator simulator) {
        this.simulator = simulator;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionStackTrace() {
        return exceptionStackTrace;
    }

    public void setExceptionStackTrace(String exceptionStackTrace) {
        this.exceptionStackTrace = exceptionStackTrace;
    }

    public CipherSuiteType getCipherSuite() {
        return cipherSuite;
    }

    public void setCipherSuite(CipherSuiteType cipherSuite) {
        this.cipherSuite = cipherSuite;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolType protocol) {
        this.protocol = protocol;
    }

    public String getLocalHost() {
        return localHost;
    }

    public void setLocalHost(String localHost) {
        this.localHost = localHost;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public String getPeerCertificateChainPEM() {
        return peerCertificateChainPEM;
    }

    public void setPeerCertificateChainPEM(String peerCertificateChainPEM) {
        this.peerCertificateChainPEM = peerCertificateChainPEM;
    }

    public TlsConnectionData getData() {
        return data;
    }

    public void setData(TlsConnectionData data) {
        this.data = data;
    }

    public String getProxyConnectionUuid() {
        return proxyConnectionUuid;
    }

    public void setProxyConnectionUuid(String proxyConnectionUuid) {
        this.proxyConnectionUuid = proxyConnectionUuid;
    }

    public void setSession(SSLSession session) {
        try {
            X509Certificate[] peerCertificateChain = session.getPeerCertificateChain();
            peerCertificateChainPEM = CertificateUtil.getPEM(peerCertificateChain);
        } catch (Exception e) {
            peerCertificateChainPEM = null;
        }
        if (session == null) {
            cipherSuite = null;
            protocol = null;
        } else {
            try {
                cipherSuite = CipherSuiteType.valueOf(session.getCipherSuite());
            } catch (IllegalArgumentException e) {
                cipherSuite = null;
            }
            try {
                protocol = ProtocolType.getProtocolTypeFromName(session.getProtocol());
            } catch (IllegalArgumentException e) {
                protocol = null;
            }
        }
    }

    public void setException(Throwable cause) {
        if (cause == null) {
            setExceptionMessage(null);
            setExceptionStackTrace(null);
        } else {
            // FIXME ExceptionUtil
            StringWriter writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            cause.printStackTrace(printWriter);
            setExceptionStackTrace(writer.toString());

            Throwable rootCause = getRootCause(cause);
            setExceptionMessage(rootCause.getMessage());
        }
    }

    private Throwable getRootCause(Throwable cause) {
        Throwable parentCause = cause.getCause();
        if (parentCause == null || parentCause == cause) {
            return cause;
        } else {
            return getRootCause(parentCause);
        }
    }

}
