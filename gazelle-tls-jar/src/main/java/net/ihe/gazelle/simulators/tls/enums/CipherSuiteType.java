package net.ihe.gazelle.simulators.tls.enums;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

public enum CipherSuiteType {

    SSL_RSA_WITH_RC4_128_MD5(true),

    SSL_RSA_WITH_RC4_128_SHA(true),

    TLS_RSA_WITH_AES_128_CBC_SHA(true),

    TLS_RSA_WITH_AES_256_CBC_SHA(false),

    TLS_DHE_RSA_WITH_AES_128_CBC_SHA(true),

    TLS_DHE_RSA_WITH_AES_256_CBC_SHA(false),

    TLS_DHE_DSS_WITH_AES_128_CBC_SHA(true),

    TLS_DHE_DSS_WITH_AES_256_CBC_SHA(false),

    SSL_RSA_WITH_3DES_EDE_CBC_SHA(true),

    SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA(true),

    SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA(true),

    SSL_RSA_WITH_DES_CBC_SHA(true),

    SSL_DHE_RSA_WITH_DES_CBC_SHA(true),

    SSL_DHE_DSS_WITH_DES_CBC_SHA(true),

    SSL_RSA_EXPORT_WITH_RC4_40_MD5(true),

    SSL_RSA_EXPORT_WITH_DES40_CBC_SHA(true),

    SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA(true),

    SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA(true),

    TLS_EMPTY_RENEGOTIATION_INFO_SCSV(true),

    SSL_RSA_WITH_NULL_MD5(true),

    SSL_RSA_WITH_NULL_SHA(true),

    SSL_DH_anon_WITH_RC4_128_MD5(false),

    TLS_DH_anon_WITH_AES_128_CBC_SHA(false),

    TLS_DH_anon_WITH_AES_256_CBC_SHA(false),

    SSL_DH_anon_WITH_3DES_EDE_CBC_SHA(false),

    SSL_DH_anon_WITH_DES_CBC_SHA(false),

    SSL_DH_anon_EXPORT_WITH_RC4_40_MD5(false),

    SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA(false),

    TLS_KRB5_WITH_RC4_128_SHA(true),

    TLS_KRB5_WITH_RC4_128_MD5(true),

    TLS_KRB5_WITH_3DES_EDE_CBC_SHA(true),

    TLS_KRB5_WITH_3DES_EDE_CBC_MD5(true),

    TLS_KRB5_WITH_DES_CBC_SHA(true),

    TLS_KRB5_WITH_DES_CBC_MD5(true),

    TLS_KRB5_EXPORT_WITH_RC4_40_SHA(true),

    TLS_KRB5_EXPORT_WITH_RC4_40_MD5(true),

    TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA(true),

    TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5(true);

    boolean safe;

    CipherSuiteType(boolean safe) {
        this.safe = safe;
    }

    public static List<CipherSuiteType> safeValues() {
        List<CipherSuiteType> result = new ArrayList<CipherSuiteType>();
        for (CipherSuiteType value : values()) {
            if (value.safe) {
                result.add(value);
            }
        }
        return result;
    }

    public static SelectItem[] getSelectItems() {
        Object[] items = CipherSuiteType.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

}
