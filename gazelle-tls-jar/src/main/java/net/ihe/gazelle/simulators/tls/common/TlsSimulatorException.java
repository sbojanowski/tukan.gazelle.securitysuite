package net.ihe.gazelle.simulators.tls.common;

public class TlsSimulatorException extends Exception {

    private static final long serialVersionUID = -3744517132350619514L;

    public TlsSimulatorException() {
        super();
    }

    public TlsSimulatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public TlsSimulatorException(String message) {
        super(message);
    }

    public TlsSimulatorException(Throwable cause) {
        super(cause);
    }

}
