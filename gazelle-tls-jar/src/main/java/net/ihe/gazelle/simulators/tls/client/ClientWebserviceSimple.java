package net.ihe.gazelle.simulators.tls.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterString;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterText;

public class ClientWebserviceSimple extends ClientSocket {

    private static final String LINE_FEED = "\r\n";
    private static final String DEFAULT_QUERY = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
            + LINE_FEED
            + "   <soapenv:Header/>"
            + LINE_FEED
            + "   <soapenv:Body>"
            + LINE_FEED
            + "   </soapenv:Body>"
            + LINE_FEED + "</soapenv:Envelope>";

    private ClientParameterString url;
    private ClientParameterText text;

    private List<ClientParameter> parameters;

    public ClientWebserviceSimple() {
        super();
        url = new ClientParameterString("Relative URI", "/");
        text = new ClientParameterText("XML Query", DEFAULT_QUERY);

        parameters = new ArrayList<ClientParameter>();
        parameters.add(url);
        parameters.add(text);
    }

    public ClientParameterString getUrl() {
        return url;
    }

    @Override
    public void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        byte[] bytes = text.getValueAsString().getBytes("UTF-8");
        println(writer, "POST " + url.getValueAsString() + " HTTP/1.1");
        println(writer, "User-Agent: TLS IHE testing tools");
        println(writer, "Content-Type: application/soap+xml; charset=utf-8");
        println(writer, "Accept: */*");
        println(writer, "Host: " + proxyToUse.getProxyConsumerHost() + ":" + proxyToUse.getProxyConsumerPort());
        println(writer, "Connection: close");
        println(writer, "Content-Length: " + bytes.length);
        println(writer, "");
        writer.flush();
        outputStream.write(bytes);
    }

    @Override
    public List<ClientParameter> getParameters() {
        return parameters;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.HTTP;
    }

}
