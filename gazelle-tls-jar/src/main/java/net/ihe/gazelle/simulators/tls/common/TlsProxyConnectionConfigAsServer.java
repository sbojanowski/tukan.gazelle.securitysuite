package net.ihe.gazelle.simulators.tls.common;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;

import org.jboss.netty.channel.ChannelHandler;

public class TlsProxyConnectionConfigAsServer extends TlsProxyConnectionConfig {

    public TlsProxyConnectionConfigAsServer(TlsSimulator simulator, List<TlsConnectionListener> connectionListeners,
                                            int proxyProviderPort, String proxyConsumerHost, int proxyConsumerPort, ChannelType channelType)
            throws TlsSimulatorException {
        super(simulator, connectionListeners, proxyProviderPort, proxyConsumerHost, proxyConsumerPort, channelType);
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyProvider() {
        return getHandlers();
    }

    @Override
    public List<ChannelHandler> getHandlersForProxyConsumer() {
        return new ArrayList<ChannelHandler>();
    }

}
