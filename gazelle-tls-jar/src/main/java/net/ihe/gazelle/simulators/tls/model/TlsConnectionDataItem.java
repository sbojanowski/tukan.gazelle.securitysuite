package net.ihe.gazelle.simulators.tls.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.ihe.gazelle.proxy.netty.channel.TimestampTool;
import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tls_connection_data_item_type")
@Table(name = "tls_connection_data_item", schema = "public")
@SequenceGenerator(name = "tls_connection_data_item_sequence", sequenceName = "tls_connection_data_item_id_seq", allocationSize = 1)
public abstract class TlsConnectionDataItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "tls_connection_data_item_sequence", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "type")
    protected TlsConnectionDataItemType type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp with time zone")
    protected Date date;

    public TlsConnectionDataItem() {
        super();
    }

    protected void init(TlsConnectionDataItemType tlsConnectionDataItemType) {
        this.date = TimestampTool.getNanoPrecisionTimestamp();
        this.type = tlsConnectionDataItemType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TlsConnectionDataItemType getType() {
        return type;
    }

    public void setType(TlsConnectionDataItemType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLabel() {
        return type.toString();
    }

    public abstract boolean isAliases();
}
