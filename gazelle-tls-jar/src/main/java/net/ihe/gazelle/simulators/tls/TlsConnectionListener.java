package net.ihe.gazelle.simulators.tls;

import net.ihe.gazelle.simulators.tls.model.TlsConnection;

public interface TlsConnectionListener {

    /**
     * SSL event when a connection has been created (or not).<br>
     * Use getSession() on engine to get informations on connection (cipher suite, protocol, ...)
     *
     * @param connection the connection that occured
     * @param channelId Proxy channel id
     */
    void logConnection(TlsConnection connection, int channelId);

}
