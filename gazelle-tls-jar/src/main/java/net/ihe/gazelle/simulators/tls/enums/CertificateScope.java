package net.ihe.gazelle.simulators.tls.enums;

import javax.faces.model.SelectItem;
import java.io.Serializable;

public enum CertificateScope implements Serializable {

    PKI,
    CUSTOM;

    /**
     * Get the enumeration values formated as Items for a h:SelectMenu
     * @return the enumeration values formated as Items for a h:SelectMenu
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = CertificateScope.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

}
