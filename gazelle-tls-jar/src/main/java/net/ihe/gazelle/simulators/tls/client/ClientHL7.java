package net.ihe.gazelle.simulators.tls.client;

import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.llp.MinLLPWriter;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterString;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterText;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ClientHL7 extends ClientSocket {

    private static final String SEGMENT_SEPARATOR = "\r";
    private static final String DEFAULT_QUERY = "MSH|^~\\&|TLS_TOOL|IL_LAB_BRK|MODULAB_LIP|MODULAB|20110414121938||QBP^SLI^QBP_Q11|627498221219696500|D|2.5|||||ITA||EN"
            + SEGMENT_SEPARATOR
            + "QPD|SLI^Specimen Labeling Instructions^IHE_LABTF|QRY68751825846181340|||PG2^OP^OID^ISO||||"
            + SEGMENT_SEPARATOR + "RCP|I||R";

    private ClientParameterString message = new ClientParameterText("HL7 message", DEFAULT_QUERY);

    @Override
    public void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException {
        MinLLPWriter mllpWriter = new MinLLPWriter(outputStream);
        try {
            mllpWriter.writeMessage(message.getValueAsString());
        } catch (LLPException e) {
            throw new IOException(e);
        }
    }

    @Override
    public List<ClientParameter> getParameters() {
        List<ClientParameter> list = new ArrayList<>();
        list.add(message);
        return list;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.HL7;
    }

}
