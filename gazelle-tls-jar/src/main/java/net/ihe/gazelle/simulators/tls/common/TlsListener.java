package net.ihe.gazelle.simulators.tls.common;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.SSLEngine;

import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionData;

public final class TlsListener {

    private TlsSimulator simulator;

    private List<TlsConnectionListener> connectionListeners;

    private ThreadLocal<TlsConnectionData> connectionData = new ThreadLocal<TlsConnectionData>() {
        @Override
        protected TlsConnectionData initialValue() {
            return new TlsConnectionData();
        }
    };

    public TlsListener(TlsSimulator simulator, List<TlsConnectionListener> connectionListeners) {
        super();
        this.simulator = simulator;
        this.connectionListeners = connectionListeners;
    }

    public final void event(boolean success, Throwable cause, int renegociationCounter, SSLEngine engine,
                            SocketAddress localAddress, SocketAddress remoteAddress, int channelId) {
        TlsConnection connection = new TlsConnection();

        connection.setSimulator(simulator);
        connection.setDate(Calendar.getInstance().getTime());
        connection.setRenegociationCounter(renegociationCounter);

        if (localAddress != null && localAddress instanceof InetSocketAddress) {
            InetSocketAddress localInetAddress = (InetSocketAddress) localAddress;
            connection.setLocalHost(localInetAddress.getHostName());
            connection.setLocalPort(localInetAddress.getPort());
        } else {
            connection.setLocalHost(null);
            connection.setLocalPort(-1);
        }

        if (remoteAddress != null && remoteAddress instanceof InetSocketAddress) {
            InetSocketAddress remoteInetAddress = (InetSocketAddress) remoteAddress;
            connection.setRemoteHost(remoteInetAddress.getHostName());
            connection.setRemotePort(remoteInetAddress.getPort());
        } else {
            connection.setRemoteHost(null);
            connection.setRemotePort(-1);
        }

        if (engine == null) {
            connection.setSession(null);
        } else {
            connection.setSession(engine.getSession());
        }
        connection.setException(cause);
        connection.setSuccess(success);

        connection.setData(connectionData.get());

        if (connectionListeners != null) {
            for (TlsConnectionListener connectionListener : connectionListeners) {
                connectionListener.logConnection(connection, channelId);
            }
        }

        connectionData.remove();
    }

    public void chooseEngineClientAlias(String[] keyType, Principal[] issuers, SSLEngine paramSSLEngine) {
        /*
		Choose an alias to authenticate the client side of an SSLEngine connection given the public key type and the list of certificate issuer authorities recognized by the peer (if any).
		The default implementation returns null.

		Parameters:
		keyType - the key algorithm type name(s), ordered with the most-preferred key type first.
		issuers - the list of acceptable CA issuer subject names or null if it does not matter which issuers are used.
		engine - the SSLEngine to be used for this connection. This parameter can be null, which indicates that implementations of this interface are free to select an alias applicable to any engine.
		 */
        connectionData.get().chooseAliasClient(keyType, issuers);
    }

    public void chooseEngineServerAlias(String keyType, Principal[] issuers, SSLEngine paramSSLEngine) {
		/*
		Choose an alias to authenticate the server side of an SSLEngine connection given the public key type and the list of certificate issuer authorities recognized by the peer (if any).
		The default implementation returns null.

		Parameters:
		keyType - the key algorithm type name.
		issuers - the list of acceptable CA issuer subject names or null if it does not matter which issuers are used.
		engine - the SSLEngine to be used for this connection. This parameter can be null, which indicates that implementations of this interface are free to select an alias applicable to any engine.
		 */
        connectionData.get().chooseAliasServer(keyType, issuers);
    }

    public void chooseClientAlias(String[] keyType, Principal[] issuers, Socket paramSocket) {
		/*
		Choose an alias to authenticate the client side of a secure socket given the public key type and the list of certificate issuer authorities recognized by the peer (if any).
		Parameters:
		keyType - the key algorithm type name(s), ordered with the most-preferred key type first.
		issuers - the list of acceptable CA issuer subject names or null if it does not matter which issuers are used.
		socket - the socket to be used for this connection. This parameter can be null, which indicates that implementations are free to select an alias applicable to any socket.
		Returns:
		the alias name for the desired key, or null if there are no matches.
		 */
        connectionData.get().chooseAliasClient(keyType, issuers);
    }

    public void chooseServerAlias(String keyType, Principal[] issuers, Socket paramSocket) {
		/*
		 Choose an alias to authenticate the server side of a secure socket given the public key type and the list of certificate issuer authorities recognized by the peer (if any).
		Parameters:
		keyType - the key algorithm type name.
		issuers - the list of acceptable CA issuer subject names or null if it does not matter which issuers are used.
		socket - the socket to be used for this connection. This parameter can be null, which indicates that implementations are free to select an alias applicable to any socket.
		Returns:
		the alias name for the desired key, or null if there are no matches.
		*/
        connectionData.get().chooseAliasServer(keyType, issuers);
    }

    public void getClientAliases(String keyType, Principal[] issuers) {
		/*
		 Get the matching aliases for authenticating the client side of a
		 secure socket given the public key type and the list of certificate
		 issuer authorities recognized by the peer (if any).
		Parameters:
		keyType - the key algorithm type name
		issuers - the list of acceptable CA issuer subject names, or null if it does not matter which issuers are used.
		Returns:
		an array of the matching alias names, or null if there were no matches.
		*/
        connectionData.get().getAliasesClient(keyType, issuers);
    }

    public void getServerAliases(String keyType, Principal[] issuers) {
		/*
		Get the matching aliases for authenticating the server side of a secure socket given the public key type and the list of certificate issuer authorities recognized by the peer (if any).
		Parameters:
		keyType - the key algorithm type name
		issuers - the list of acceptable CA issuer subject names or null if it does not matter which issuers are used.
		Returns:
		an array of the matching alias names, or null if there were no matches.
		*/
        connectionData.get().getAliasesServer(keyType, issuers);
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType) {
		/*
		Given the partial or complete certificate chain provided by the peer, build a certificate path to a trusted root and return if it can be validated and is trusted for client SSL authentication based on the authentication type. The authentication type is determined by the actual certificate used. For instance, if RSAPublicKey is used, the authType should be "RSA". Checking is case-sensitive.
		Parameters:
		chain - the peer certificate chain
		authType - the authentication type based on the client certificate
		*/
        connectionData.get().checkTrustedClient(chain, authType);
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType) {
		/*
		Given the partial or complete certificate chain provided by the peer, build a certificate path to a trusted root and return if it can be validated and is trusted for server SSL authentication based on the authentication type. The authentication type is the key exchange algorithm portion of the cipher suites represented as a String, such as "RSA", "DHE_DSS".
		Note: for some exportable cipher suites, the key exchange algorithm is determined at run time during the handshake. For instance, for TLS_RSA_EXPORT_WITH_RC4_40_MD5, the authType should be RSA_EXPORT when an ephemeral RSA key is used for the key exchange, and RSA when the key from the server certificate is used. Checking is case-sensitive.
		Parameters:
		chain - the peer certificate chain
		authType - the key exchange algorithm used
		*/
        connectionData.get().checkTrustedServer(chain, authType);
    }

}
