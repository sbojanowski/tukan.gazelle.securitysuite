package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsServer;

import javax.faces.model.SelectItem;
import java.io.Serializable;

public enum SimulatorType implements Serializable {

    CLIENT(TlsClient.class, "Client"),
    SERVER(TlsServer.class, "Server");

    private String friendlyName;
    private Class simulatorClass;

    SimulatorType(Class simulatorClass, String friendlyName) {
        this.simulatorClass = simulatorClass;
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public Class getSimulatorClass() {
        return simulatorClass;
    }

    public String toString() {
        return getFriendlyName();
    }

    /**
     * Get the enumeration values formated as Items for a h:SelectMenu
     * @return the enumeration values formated as Items for a h:SelectMenu
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = SimulatorType.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }
}
