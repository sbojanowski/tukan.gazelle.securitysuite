package net.ihe.gazelle.simulators.tls.model;

import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsProxyConnectionConfigAsServer;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;

import javax.net.ssl.SSLParameters;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collections;

@Entity
@DiscriminatorValue("Server")
public class TlsServer extends TlsSimulator {

    private static final long serialVersionUID = 4866424928812641378L;

    @Column(name = "is_need_client_auth")
    private boolean needClientAuth;

    @Column(name = "remote_host")
    private String remoteHost;

    @Column(name = "remote_port")
    private Integer remotePort;

    @Column(name = "local_port")
    private Integer localPort;

    @Column(name = "channel_type")
    private ChannelType channelType;

    @Column(name = "is_running")
    private boolean running;

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String previousHost) {
        this.remoteHost = previousHost;
    }

    public Integer getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(Integer previousPort) {
        this.remotePort = previousPort;
    }

    public boolean isNeedClientAuth() {
        return needClientAuth;
    }

    public void setNeedClientAuth(boolean needClientAuth) {
        this.needClientAuth = needClientAuth;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType previousType) {
        this.channelType = previousType;
    }

    public TlsServer() {
        super();
        this.channelType = ChannelType.RAW;
    }

    @Override
    public SimulatorType getType() {
        return SimulatorType.SERVER;
    }

    @Override
    public TlsServer clone() {
        TlsServer copy = (TlsServer) super.clone();
        copy.setNeedClientAuth(this.isNeedClientAuth());
        copy.setRemoteHost(this.getRemoteHost());
        copy.setRemotePort(this.getRemotePort());
        copy.setLocalPort(this.getLocalPort());
        copy.setChannelType(this.getChannelType());
        copy.setRunning(false);
        return copy;
    }

    @Override
    protected TlsServer newInstance() {
        return new TlsServer();
    }

    @Override
    protected void updateParameters(SSLParameters sslParameters) {
        sslParameters.setNeedClientAuth(needClientAuth);
    }

    public Proxy<?, ?> start(TlsConnectionListener connectionListener) throws TlsSimulatorException {
        ConnectionConfig connectionConfig = new TlsProxyConnectionConfigAsServer(this,
                Collections.singletonList(connectionListener), getLocalPort(), getRemoteHost(), getRemotePort(),
                getChannelType());
        Proxy<?, ?> proxy = channelType.getProxy(connectionConfig);
        proxy.start();
        if (!proxy.iscException()) {
            return proxy;
        } else {
            return null;
        }

    }

    public Integer getLocalPort() {
        return localPort;
    }

    public void setLocalPort(Integer localPort) {
        this.localPort = localPort;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    public static String getSimulatorKeywordPrefix() {
        return "server_";
    }

    @Override
    public void executeTest(TlsConnectionListener tlsTestInstanceCallbacker, int testCaseId, String sutHost,
                            int sutPort, Client applicationMsg) throws TlsSimulatorException {
        // TODO Auto-generated method stub

    }
}
