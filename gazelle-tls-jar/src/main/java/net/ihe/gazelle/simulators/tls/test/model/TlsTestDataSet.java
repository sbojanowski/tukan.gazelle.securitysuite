package net.ihe.gazelle.simulators.tls.test.model;

import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * TlsTestDataSet is a container for a test data set that will be injected in a TLS client/server simulator.
 *
 * @author cel
 */
@Entity
@Table(name = "tls_test_data_set", schema = "public")
@SequenceGenerator(name = "tls_test_data_set_sequence", sequenceName = "tls_test_data_set_id_seq", allocationSize = 1)
public class TlsTestDataSet extends AuditedObject implements Serializable {

    private static final long serialVersionUID = -3513187561417197357L;

    @Id
    @GeneratedValue(generator = "tls_test_data_set_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private int id;

    @Column(name = "name", unique = true, nullable = false)
    @NotNull
    private String name;

    @Column(name = "context")
    private ContextType context;

    @ElementCollection
    @CollectionTable(name = "tls_test_data_set_protocols", joinColumns = @JoinColumn(name = "tls_test_data_set_id"))
    @Column(name = "protocol")
    private Set<ProtocolType> protocols;

    @ElementCollection
    @CollectionTable(name = "tls_test_data_set_ciphersuites", joinColumns = @JoinColumn(name = "tls_test_data_set_id"))
    @Column(name = "ciphersuite")
    private Set<CipherSuiteType> cipherSuites;

    @ManyToOne
    @JoinColumn(name = "certificate_id")
    private Certificate certificate;

    @Column(name = "is_use_sys_ca_certs")
    private boolean useSystemCACerts;

    @ManyToMany
    @JoinTable(name = "tls_test_data_set_trusted_issuers",
            joinColumns = @JoinColumn(name = "tls_test_data_set_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "certificate_id", referencedColumnName = "id"))
    private Set<Certificate> trustedIssuers = new HashSet<Certificate>();

    @Column(name = "is_check_revocation")
    private boolean checkRevocation;

    @Column(name = "is_need_client_auth")
    private boolean needClientAuth;

    public TlsTestDataSet() {
        super();
        this.checkRevocation = false;
        this.needClientAuth = false;
        this.useSystemCACerts = false;
    }

    /**
     * @return the id of the TLS data set
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name of the TLS data set
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the TLS data set
     */
    public void setName(String name) {
        this.name = name;
    }

    public ContextType getContext() {
        return context;
    }

    public void setContext(ContextType context) {
        this.context = context;
    }

    /**
     * @return the supported Protocols
     */
    public Set<ProtocolType> getProtocols() {
        return protocols;
    }

    /**
     * Define the supported protocols.
     *
     * @param protocols the protocols supported.
     */
    public void setProtocols(Set<ProtocolType> protocols) {
        this.protocols = protocols;
    }

    /**
     * @return the supported cipherSuites
     */
    public Set<CipherSuiteType> getCipherSuites() {
        return cipherSuites;
    }

    /**
     * Define the supported cipher suites.
     *
     * @param cipherSuites the supported cipherSuites to set
     */
    public void setCipherSuites(Set<CipherSuiteType> cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

    /**
     * @return true if the certificate revocation must be checked, false otherwise.
     */
    public boolean isCheckRevocation() {
        return checkRevocation;
    }

    /**
     * define if the certificate revocation must be checked before allowing a connection.
     *
     * @param checkRevocation true if the certificate revocation must be checked, false otherwise.
     */
    public void setCheckRevocation(boolean checkRevocation) {
        this.checkRevocation = checkRevocation;
    }

    /**
     * @return true if the client must be authenticated, false otherwise.
     */
    public boolean isNeedClientAuth() {
        return needClientAuth;
    }

    /**
     * Define if the client must be authenticated to allow connection.
     *
     * @param needClientAuth true if the client must be authenticated, false otherwise.
     */
    public void setNeedClientAuth(boolean needClientAuth) {
        this.needClientAuth = needClientAuth;
    }

    /**
     * @return the certificate to set to the client/server.
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * Define the certificate.
     *
     * @param certificate the certificate to set
     */
    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * @return the useSystemCACerts
     */
    public boolean isUseSystemCACerts() {
        return useSystemCACerts;
    }

    /**
     * @param useSystemCACerts the useSystemCACerts to set
     */
    public void setUseSystemCACerts(boolean useSystemCACerts) {
        this.useSystemCACerts = useSystemCACerts;
    }

    /**
     * @return the trustedIssuers
     */
    public Set<Certificate> getTrustedIssuers() {
        return trustedIssuers;
    }

    /**
     * @param trustedIssuers the trustedIssuers to set
     */
    public void setTrustedIssuers(Set<Certificate> trustedIssuers) {
        this.trustedIssuers = trustedIssuers;
    }

    public String getTrustedIssuersToString() {
        StringBuilder stringBuilder = new StringBuilder("");
        for (Certificate certificate : this.trustedIssuers) {
            stringBuilder.append(certificate.getSubject());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public String toString() {
        return this.name;
    }


}
