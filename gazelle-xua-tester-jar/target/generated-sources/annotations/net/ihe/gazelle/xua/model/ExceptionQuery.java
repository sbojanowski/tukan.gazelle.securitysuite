package net.ihe.gazelle.xua.model;

import javax.persistence.EntityManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ExceptionQuery extends ExceptionEntity<Exception> implements HQLQueryBuilderInterface<Exception> {

	public ExceptionQuery() {
		super("this", new HQLQueryBuilder(Exception.class));
	}

	public ExceptionQuery(HQLQueryBuilder<?> queryBuilder) {
		super("this", queryBuilder);
	}
	
	public ExceptionQuery(EntityManager entityManager) {
		super("this", new HQLQueryBuilder(entityManager,Exception.class));
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return queryBuilder.getCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Exception> getList() {
		return (List<Exception>) queryBuilder.getList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Exception> getListNullIfEmpty() {
		return (List<Exception>) queryBuilder.getListNullIfEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Exception getUniqueResult() {
		return (Exception) queryBuilder.getUniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HQLStatistic<Exception>> getListWithStatistics(List<HQLStatisticItem> items) {
		List<?> result = queryBuilder.getListWithStatistics(items);
		return (List<HQLStatistic<Exception>>) result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> getListWithStatisticsItems(List<HQLStatisticItem> items, HQLStatistic<Exception> item,
			int statisticItemIndex) {
		HQLStatistic safeItem = item;
		List<Object> result = queryBuilder.getListWithStatisticsItems(items, safeItem, statisticItemIndex);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFirstResult(int firstResult) {
		queryBuilder.setFirstResult(firstResult);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxResults(int maxResults) {
		queryBuilder.setMaxResults(maxResults);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCachable() {
		return queryBuilder.isCachable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCachable(boolean cachable) {
		queryBuilder.setCachable(cachable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addRestriction(HQLRestriction restriction) {
		queryBuilder.addRestriction(restriction);
	}

}

