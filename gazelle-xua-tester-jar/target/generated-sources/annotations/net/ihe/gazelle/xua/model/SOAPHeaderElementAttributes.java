package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class SOAPHeaderElementAttributes<T extends SOAPHeaderElement> extends net.ihe.gazelle.xua.model.SOAPHeaderPartAttributes<T> {

	public SOAPHeaderElementAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return SOAPHeaderElement.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to localName of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> localName() {
		return new HQLSafePathBasicString<String>(this, path + ".localName", queryBuilder, String.class);
	}

	/**
	 * @return Path to value of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> value() {
		return new HQLSafePathBasicString<String>(this, path + ".value", queryBuilder, String.class);
	}

	/**
	 * @return Path to namespaceUri of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> namespaceUri() {
		return new HQLSafePathBasicString<String>(this, path + ".namespaceUri", queryBuilder, String.class);
	}

	/**
	 * @return Path to mustUnderstand of type Boolean
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<Boolean> mustUnderstand() {
		return new HQLSafePathBasic<Boolean>(this, path + ".mustUnderstand", queryBuilder, Boolean.class);
	}



}

