package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ExceptionAttributes<T extends Exception> extends HQLSafePathEntity<T> {

	public ExceptionAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Exception.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to newValue of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> newValue() {
		return new HQLSafePathBasicString<String>(this, path + ".newValue", queryBuilder, String.class);
	}

	/**
	 * @return Path to exceptionType of type net.ihe.gazelle.xua.model.ExceptionType
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<ExceptionType> exceptionType() {
		return new HQLSafePathBasic<ExceptionType>(this, path + ".exceptionType", queryBuilder, ExceptionType.class);
	}

	/**
	 * @return Path to part of type net.ihe.gazelle.xua.model.SOAPHeaderPart
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public SOAPHeaderPartEntity<SOAPHeaderPart> part() {
		return new SOAPHeaderPartEntity<SOAPHeaderPart>(path + ".part", queryBuilder);
	}

	/**
	 * @return Path to xpath of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> xpath() {
		return new HQLSafePathBasicString<String>(this, path + ".xpath", queryBuilder, String.class);
	}



}

