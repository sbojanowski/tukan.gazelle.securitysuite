package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class TimestampAttributes<T extends Timestamp> extends net.ihe.gazelle.xua.model.SOAPHeaderPartAttributes<T> {

	public TimestampAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Timestamp.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to duration of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> duration() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".duration", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to createdOffset of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicNumber<Integer> createdOffset() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".createdOffset", queryBuilder, Integer.class);
	}



}

