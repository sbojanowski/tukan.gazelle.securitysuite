package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class SOAPHeaderPartAttributes<T extends SOAPHeaderPart> extends HQLSafePathEntity<T> {

	public SOAPHeaderPartAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return SOAPHeaderPart.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to exceptions of type net.ihe.gazelle.xua.model.Exception
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public ExceptionAttributes<Exception> exceptions() {
		return new ExceptionAttributes<Exception>(path + ".exceptions", queryBuilder);
	}

	/**
	 * @return Path to testCase of type net.ihe.gazelle.xua.model.ServiceProviderTestCase
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public ServiceProviderTestCaseEntity<ServiceProviderTestCase> testCase() {
		return new ServiceProviderTestCaseEntity<ServiceProviderTestCase>(path + ".testCase", queryBuilder);
	}



}

