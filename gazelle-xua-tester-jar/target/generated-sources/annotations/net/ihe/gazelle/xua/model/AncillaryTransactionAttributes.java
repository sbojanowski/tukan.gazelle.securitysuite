package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class AncillaryTransactionAttributes<T extends AncillaryTransaction> extends HQLSafePathEntity<T> {

	public AncillaryTransactionAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return AncillaryTransaction.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to description of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> description() {
		return new HQLSafePathBasicString<String>(this, path + ".description", queryBuilder, String.class);
	}

	/**
	 * @return Path to name of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> name() {
		return new HQLSafePathBasicString<String>(this, path + ".name", queryBuilder, String.class);
	}

	/**
	 * @return Path to keyword of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> keyword() {
		return new HQLSafePathBasicString<String>(this, path + ".keyword", queryBuilder, String.class);
	}

	/**
	 * @return Path to soapAction of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> soapAction() {
		return new HQLSafePathBasicString<String>(this, path + ".soapAction", queryBuilder, String.class);
	}

	/**
	 * @return Path to pathToSoapBody of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> pathToSoapBody() {
		return new HQLSafePathBasicString<String>(this, path + ".pathToSoapBody", queryBuilder, String.class);
	}

	/**
	 * @return Path to variables of type net.ihe.gazelle.xua.model.MessageVariable
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public MessageVariableAttributes<MessageVariable> variables() {
		return new MessageVariableAttributes<MessageVariable>(path + ".variables", queryBuilder);
	}



}

