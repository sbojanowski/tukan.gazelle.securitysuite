package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ServiceProviderTestInstanceEntity<T extends ServiceProviderTestInstance> extends ServiceProviderTestInstanceAttributes<T> {

	public ServiceProviderTestInstanceEntity(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public boolean isSingle() {
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<T> getListDistinct() {
		return (List<T>) queryBuilder.getListDistinct(path);
	}

	public List<T> getListDistinctOrdered() {
        return (List<T>) queryBuilder.getListDistinctOrdered(path);
    }

	public int getCountOnPath() {
		return queryBuilder.getCountOnPath(path);
	}

	public int getCountDistinctOnPath() {
		return queryBuilder.getCountDistinctOnPath(path);
	}

	public void eq(T value) {
		queryBuilder.addRestriction(eqRestriction(value));
	}

	public void eqIfValueNotNull(T value) {
		if (value != null) {
			eq(value);
		}
	}

	public void neq(T value) {
		queryBuilder.addRestriction(neqRestriction(value));
	}

	public void in(Collection<? extends T> elements) {
		queryBuilder.addRestriction(inRestriction(elements));
	}

	public void nin(Collection<? extends T> elements) {
		queryBuilder.addRestriction(ninRestriction(elements));
	}

	public void isNotNull() {
		queryBuilder.addRestriction(isNotNullRestriction());
	}

	public void isNull() {
		queryBuilder.addRestriction(isNullRestriction());
	}

	public HQLRestriction eqRestriction(T value) {
		return HQLRestrictions.eq(path, value);
	}

	public HQLRestriction neqRestriction(T value) {
		return HQLRestrictions.neq(path, value);
	}

	public HQLRestriction inRestriction(Collection<? extends T> elements) {
		return HQLRestrictions.in(path, elements);
	}

	public HQLRestriction ninRestriction(Collection<? extends T> elements) {
		return HQLRestrictions.nin(path, elements);
	}

	public HQLRestriction isNotNullRestriction() {
		return HQLRestrictions.isNotNull(path);
	}

	public HQLRestriction isNullRestriction() {
		return HQLRestrictions.isNull(path);
	}

}

