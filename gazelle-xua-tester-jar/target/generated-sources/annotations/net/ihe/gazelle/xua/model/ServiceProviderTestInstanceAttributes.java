package net.ihe.gazelle.xua.model;

import java.util.Date;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ServiceProviderTestInstanceAttributes<T extends ServiceProviderTestInstance> extends HQLSafePathEntity<T> {

	public ServiceProviderTestInstanceAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return ServiceProviderTestInstance.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to timestamp of type java.util.Date
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicDate<Date> timestamp() {
		return new HQLSafePathBasicDate<Date>(this, path + ".timestamp", queryBuilder, Date.class);
	}

	/**
	 * @return Path to response of type byte[]
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<byte[]> response() {
		return new HQLSafePathBasic<byte[]>(this, path + ".response", queryBuilder, byte[].class);
	}

	/**
	 * @return Path to ancillaryTransaction of type net.ihe.gazelle.xua.model.AncillaryTransaction
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public AncillaryTransactionEntity<AncillaryTransaction> ancillaryTransaction() {
		return new AncillaryTransactionEntity<AncillaryTransaction>(path + ".ancillaryTransaction", queryBuilder);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to username of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> username() {
		return new HQLSafePathBasicString<String>(this, path + ".username", queryBuilder, String.class);
	}

	/**
	 * @return Path to testedEndpoint of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> testedEndpoint() {
		return new HQLSafePathBasicString<String>(this, path + ".testedEndpoint", queryBuilder, String.class);
	}

	/**
	 * @return Path to testStatus of type net.ihe.gazelle.xua.model.TestStatus
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<TestStatus> testStatus() {
		return new HQLSafePathBasic<TestStatus>(this, path + ".testStatus", queryBuilder, TestStatus.class);
	}

	/**
	 * @return Path to request of type byte[]
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<byte[]> request() {
		return new HQLSafePathBasic<byte[]>(this, path + ".request", queryBuilder, byte[].class);
	}

	/**
	 * @return Path to testCase of type net.ihe.gazelle.xua.model.ServiceProviderTestCase
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public ServiceProviderTestCaseEntity<ServiceProviderTestCase> testCase() {
		return new ServiceProviderTestCaseEntity<ServiceProviderTestCase>(path + ".testCase", queryBuilder);
	}

	/**
	 * @return Path to reasonForFailure of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> reasonForFailure() {
		return new HQLSafePathBasicString<String>(this, path + ".reasonForFailure", queryBuilder, String.class);
	}



}

