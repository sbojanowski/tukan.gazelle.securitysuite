package net.ihe.gazelle.xua.model;

import java.util.Date;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class ServiceProviderTestCaseAttributes<T extends ServiceProviderTestCase> extends HQLSafePathEntity<T> {

	public ServiceProviderTestCaseAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return ServiceProviderTestCase.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to expectedResultDetails of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> expectedResultDetails() {
		return new HQLSafePathBasicString<String>(this, path + ".expectedResultDetails", queryBuilder, String.class);
	}

	/**
	 * @return Path to lastChanged of type java.util.Date
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicDate<Date> lastChanged() {
		return new HQLSafePathBasicDate<Date>(this, path + ".lastChanged", queryBuilder, Date.class);
	}

	/**
	 * @return Path to soapHeaderParts of type net.ihe.gazelle.xua.model.SOAPHeaderPart
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = false, mappedBy = true, notNull = false)
	public SOAPHeaderPartAttributes<SOAPHeaderPart> soapHeaderParts() {
		return new SOAPHeaderPartAttributes<SOAPHeaderPart>(path + ".soapHeaderParts", queryBuilder);
	}

	/**
	 * @return Path to expectedResult of type net.ihe.gazelle.xua.model.ExpectedResult
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<ExpectedResult> expectedResult() {
		return new HQLSafePathBasic<ExpectedResult>(this, path + ".expectedResult", queryBuilder, ExpectedResult.class);
	}

	/**
	 * @return Path to description of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> description() {
		return new HQLSafePathBasicString<String>(this, path + ".description", queryBuilder, String.class);
	}

	/**
	 * @return Path to keyword of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> keyword() {
		return new HQLSafePathBasicString<String>(this, path + ".keyword", queryBuilder, String.class);
	}

	/**
	 * @return Path to lastModifier of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> lastModifier() {
		return new HQLSafePathBasicString<String>(this, path + ".lastModifier", queryBuilder, String.class);
	}

	/**
	 * @return Path to available of type Boolean
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasic<Boolean> available() {
		return new HQLSafePathBasic<Boolean>(this, path + ".available", queryBuilder, Boolean.class);
	}



}

