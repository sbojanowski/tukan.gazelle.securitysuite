package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class SignatureAttributes<T extends Signature> extends net.ihe.gazelle.xua.model.SOAPHeaderPartAttributes<T> {

	public SignatureAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return Signature.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to alias of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> alias() {
		return new HQLSafePathBasicString<String>(this, path + ".alias", queryBuilder, String.class);
	}

	/**
	 * @return Path to keystore of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> keystore() {
		return new HQLSafePathBasicString<String>(this, path + ".keystore", queryBuilder, String.class);
	}

	/**
	 * @return Path to signedElementNamespaceUri of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> signedElementNamespaceUri() {
		return new HQLSafePathBasicString<String>(this, path + ".signedElementNamespaceUri", queryBuilder, String.class);
	}

	/**
	 * @return Path to keystorePassword of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> keystorePassword() {
		return new HQLSafePathBasicString<String>(this, path + ".keystorePassword", queryBuilder, String.class);
	}

	/**
	 * @return Path to privateKeyPassword of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> privateKeyPassword() {
		return new HQLSafePathBasicString<String>(this, path + ".privateKeyPassword", queryBuilder, String.class);
	}

	/**
	 * @return Path to signedElementLocalName of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> signedElementLocalName() {
		return new HQLSafePathBasicString<String>(this, path + ".signedElementLocalName", queryBuilder, String.class);
	}



}

