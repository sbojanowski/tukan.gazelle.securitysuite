package net.ihe.gazelle.xua.model;

import javax.persistence.EntityManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;


import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class AncillaryTransactionQuery extends AncillaryTransactionEntity<AncillaryTransaction> implements HQLQueryBuilderInterface<AncillaryTransaction> {

	public AncillaryTransactionQuery() {
		super("this", new HQLQueryBuilder(AncillaryTransaction.class));
	}

	public AncillaryTransactionQuery(HQLQueryBuilder<?> queryBuilder) {
		super("this", queryBuilder);
	}
	
	public AncillaryTransactionQuery(EntityManager entityManager) {
		super("this", new HQLQueryBuilder(entityManager,AncillaryTransaction.class));
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return queryBuilder.getCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AncillaryTransaction> getList() {
		return (List<AncillaryTransaction>) queryBuilder.getList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AncillaryTransaction> getListNullIfEmpty() {
		return (List<AncillaryTransaction>) queryBuilder.getListNullIfEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AncillaryTransaction getUniqueResult() {
		return (AncillaryTransaction) queryBuilder.getUniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HQLStatistic<AncillaryTransaction>> getListWithStatistics(List<HQLStatisticItem> items) {
		List<?> result = queryBuilder.getListWithStatistics(items);
		return (List<HQLStatistic<AncillaryTransaction>>) result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> getListWithStatisticsItems(List<HQLStatisticItem> items, HQLStatistic<AncillaryTransaction> item,
			int statisticItemIndex) {
		HQLStatistic safeItem = item;
		List<Object> result = queryBuilder.getListWithStatisticsItems(items, safeItem, statisticItemIndex);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFirstResult(int firstResult) {
		queryBuilder.setFirstResult(firstResult);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxResults(int maxResults) {
		queryBuilder.setMaxResults(maxResults);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCachable() {
		return queryBuilder.isCachable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCachable(boolean cachable) {
		queryBuilder.setCachable(cachable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addRestriction(HQLRestriction restriction) {
		queryBuilder.addRestriction(restriction);
	}

}

