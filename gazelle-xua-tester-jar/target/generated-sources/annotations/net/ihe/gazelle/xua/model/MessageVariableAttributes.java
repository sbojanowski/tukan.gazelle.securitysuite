package net.ihe.gazelle.xua.model;



import java.util.*;
import net.ihe.gazelle.hql.*;
import net.ihe.gazelle.hql.beans.*;
import net.ihe.gazelle.hql.paths.*;
import net.ihe.gazelle.hql.restrictions.*;

@SuppressWarnings("all")
public class MessageVariableAttributes<T extends MessageVariable> extends HQLSafePathEntity<T> {

	public MessageVariableAttributes(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public Class<?> getEntityClass() {
		return MessageVariable.class;
	}

	public boolean isSingle() {
		return false;
	}

	public void isNotEmpty() {
		queryBuilder.addRestriction(isNotEmptyRestriction());
	}

	public void isEmpty() {
		queryBuilder.addRestriction(isEmptyRestriction());
	}

	public HQLRestriction isNotEmptyRestriction() {
		return HQLRestrictions.isNotEmpty(path);
	}

	public HQLRestriction isEmptyRestriction() {
		return HQLRestrictions.isEmpty(path);
	}

	/**
	 * @return Path to id of type java.lang.Integer
	 */
	@net.ihe.gazelle.hql.paths.Path(id = true, unique = true, uniqueSet = 0, single = true, mappedBy = false, notNull = true)
	public HQLSafePathBasicNumber<Integer> id() {
		return new HQLSafePathBasicNumber<Integer>(this, path + ".id", queryBuilder, Integer.class);
	}

	/**
	 * @return Path to transaction of type net.ihe.gazelle.xua.model.AncillaryTransaction
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public AncillaryTransactionEntity<AncillaryTransaction> transaction() {
		return new AncillaryTransactionEntity<AncillaryTransaction>(path + ".transaction", queryBuilder);
	}

	/**
	 * @return Path to token of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> token() {
		return new HQLSafePathBasicString<String>(this, path + ".token", queryBuilder, String.class);
	}

	/**
	 * @return Path to defaultValue of type java.lang.String
	 */
	@net.ihe.gazelle.hql.paths.Path(id = false, unique = false, uniqueSet = 0, single = true, mappedBy = false, notNull = false)
	public HQLSafePathBasicString<String> defaultValue() {
		return new HQLSafePathBasicString<String>(this, path + ".defaultValue", queryBuilder, String.class);
	}



}

