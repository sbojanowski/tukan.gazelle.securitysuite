package net.ihe.gazelle.xua.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by aberge on 31/05/17.
 */
@Entity
@Name("soapHeaderElement")
@DiscriminatorValue("soapHeaderElement")
public class SOAPHeaderElement extends SOAPHeaderPart {

    @Column(name = "namespace_uri")
    private String namespaceUri;

    @Column(name = "local_name")
    private String localName;

    @Column(name = "value")
    private String value;

    @Column(name = "must_understand")
    private boolean mustUnderstand;

    public SOAPHeaderElement(){
        this.mustUnderstand = false;
    }

    public String getNamespaceUri() {
        return namespaceUri;
    }

    public void setNamespaceUri(String namespaceUri) {
        this.namespaceUri = namespaceUri;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isMustUnderstand() {
        return mustUnderstand;
    }

    public void setMustUnderstand(boolean mustUnderstand) {
        this.mustUnderstand = mustUnderstand;
    }

    @Override
    public String getType() {
        return "SOAPHeaderElement";
    }
}
