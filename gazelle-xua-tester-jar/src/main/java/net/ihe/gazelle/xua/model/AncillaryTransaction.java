package net.ihe.gazelle.xua.model;

import net.ihe.gazelle.hql.FilterLabel;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aberge on 23/05/17.
 */

@Entity
@Name("ancillaryTransaction")
@Table(name = "xua_ancillary_transaction", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "xua_ancillary_transaction_sequence", sequenceName = "xua_ancillary_transaction_id_seq", allocationSize = 1)
public class AncillaryTransaction implements Serializable{

    @Id
    @GeneratedValue(generator = "xua_ancillary_transaction_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "path_to_soap_body")
    private String pathToSoapBody;

    @Column(name = "soap_action")
    private String soapAction;

    @OneToMany(targetEntity = MessageVariable.class, mappedBy = "transaction")
    private List<MessageVariable> variables;

    public AncillaryTransaction(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathToSoapBody() {
        return pathToSoapBody;
    }

    public void setPathToSoapBody(String pathToSoapBody) {
        this.pathToSoapBody = pathToSoapBody;
    }

    public String getSoapAction() {
        return soapAction;
    }

    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

    public List<MessageVariable> getVariables() {
        return variables;
    }

    public void setVariables(List<MessageVariable> variables) {
        this.variables = variables;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AncillaryTransaction)) {
            return false;
        }

        AncillaryTransaction that = (AncillaryTransaction) o;

        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (pathToSoapBody != null ? !pathToSoapBody.equals(that.pathToSoapBody) : that.pathToSoapBody != null) {
            return false;
        }
        return soapAction != null ? soapAction.equals(that.soapAction) : that.soapAction == null;
    }

    @Override
    public int hashCode() {
        int result = keyword != null ? keyword.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (pathToSoapBody != null ? pathToSoapBody.hashCode() : 0);
        result = 31 * result + (soapAction != null ? soapAction.hashCode() : 0);
        return result;
    }
}
