package net.ihe.gazelle.xua.model;

/**
 * <p>ExpectedResult enum.</p>
 *
 * @author aberge
 * @version 1.0: 24/10/17
 */
public enum ExpectedResult {

    SOAP_FAULT,
    NO_SOAP_FAULT,
    UNSPECIFIED
}
