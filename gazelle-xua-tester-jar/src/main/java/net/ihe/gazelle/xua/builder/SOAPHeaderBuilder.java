package net.ihe.gazelle.xua.builder;

import net.ihe.gazelle.security.builders.AssertionBuilder;
import net.ihe.gazelle.security.builders.TimestampBuilder;
import net.ihe.gazelle.sequoia.builders.SequoiaSignatureBuilder;
import net.ihe.gazelle.simulator.sts.client.WSSEConstants;
import net.ihe.gazelle.wstrust.utility.TimestampType;
import net.ihe.gazelle.xua.dao.SOAPHeaderPartDAO;
import net.ihe.gazelle.xua.model.Assertion;
import net.ihe.gazelle.xua.model.Exception;
import net.ihe.gazelle.xua.model.MessageVariable;
import net.ihe.gazelle.xua.model.SOAPHeaderElement;
import net.ihe.gazelle.xua.model.SOAPHeaderPart;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import net.ihe.gazelle.xua.model.Signature;
import net.ihe.gazelle.xua.model.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by aberge on 31/05/17.
 */
public class SOAPHeaderBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(SOAPHeaderBuilder.class);
    private static final String SOAP_NS = "http://www.w3.org/2003/05/soap-envelope";
    private static final String SOAP_PREFIX = "env";
    private static final String MUST_UNDERSTAND_LOCAL_NAME = SOAP_PREFIX + ":mustUnderstand";
    private static final QName SECURITY_NAME = new QName(WSSEConstants.WSSE_NS, WSSEConstants.WSSE_LOCAL);
    public static final String PATTERN = "$$";
    public static final String ACTION_PATTERN = "$$ACTION$$";
    public static final String ENDPOINT_PATTERN = "$$ENDPOINT$$";
    public static final String ANONYMOUS_PATTERN = "$$ANONYMOUS$$";
    public static final String RANDOM_PATTERN = "$$RANDOM$$";
    public static final String ANONYMOUS_URL = "http://www.w3.org/2005/08/addressing/anonymous";
    public static final String TIMESTAMP_LOCAL = "Timestamp";
    public static final String ADDRESSING_NS = "http://www.w3.org/2005/08/addressing";
    public static final String ADDRESS_LOCAL = "Address";

    private ServiceProviderTestInstance testInstance;

    public SOAPHeaderBuilder(ServiceProviderTestInstance inTestInstance) {
        this.testInstance = inTestInstance;
    }

    public boolean buildSoapHeader(SOAPMessage soapMessage) {
        try {
            SOAPHeader soapHeader = soapMessage.getSOAPHeader();
            soapMessage.getSOAPPart().getEnvelope().setPrefix(SOAP_PREFIX);
            SOAPFactory factory = SOAPFactory.newInstance();
            List<SOAPHeaderPart> headerParts = SOAPHeaderPartDAO.getPartsforTestCase(testInstance.getTestCase());
            if (headerParts != null) {
                Signature signature = null;
                Timestamp timestamp = null;
                String timestampId = null;
                for (SOAPHeaderPart part : headerParts) {
                    if (part instanceof Signature) {
                        // signature might reference other pieces of the soapHeader: keep it for processing at the very latest
                        signature = (Signature) part;
                    } else if (part instanceof Assertion) {
                        Assertion assertion = (Assertion) part;
                        try {
                            // assertion is to be placed in wsse:Security, create it if it does not exists
                            Element assertionNode = AssertionBuilder.getAssertionFromSTS(assertion.getUsername(), assertion.getPassword(), testInstance.getTestedEndpoint(), assertion.getStsEndpoint());
                            SOAPElement assertionElement = factory.createElement(assertionNode);
                            addToSoapSecurity(soapHeader, factory, assertionElement);
                        } catch (java.lang.Exception e) {
                            LOG.error(e.getMessage());
                        }
                    } else if (part instanceof Timestamp) {
                        timestamp = (Timestamp) part;
                        TimestampType timestampElement = TimestampBuilder.buildWsuTimestamp(timestamp.getCreatedOffset(), timestamp.getDuration());
                        timestampId = timestampElement.getId();
                        SOAPElement soapTimestamp = factory.createElement((Element) timestampElement.get_xmlNodePresentation());
                        if (soapTimestamp != null) {
                            // timestamp is to be placed in the wsse:Security element, create it if missing
                            addToSoapSecurity(soapHeader, factory, soapTimestamp);
                        }
                    } else if (part instanceof SOAPHeaderElement) {
                        SOAPHeaderElement elementToAdd = (SOAPHeaderElement) part;
                        SOAPElement soapElement = factory.createElement(new QName(elementToAdd.getNamespaceUri(), elementToAdd.getLocalName()));
                        if (elementToAdd.isMustUnderstand()) {
                            soapElement.setAttributeNS(SOAP_NS, MUST_UNDERSTAND_LOCAL_NAME, "true");
                        }
                        String nodeValue = elementToAdd.getValue();
                        if (nodeValue != null && nodeValue.contains(ACTION_PATTERN)) {
                            nodeValue = nodeValue.replace(ACTION_PATTERN, testInstance.getAncillaryTransaction().getSoapAction());
                        } else if (nodeValue != null && nodeValue.contains(ENDPOINT_PATTERN)) {
                            nodeValue = nodeValue.replace(ENDPOINT_PATTERN, testInstance.getTestedEndpoint());
                        } else if (nodeValue != null && nodeValue.contains(RANDOM_PATTERN)) {
                            nodeValue = nodeValue.replace(RANDOM_PATTERN, UUID.randomUUID().toString());
                        } else if (nodeValue != null && nodeValue.contains(ANONYMOUS_PATTERN)) {
                            // replyTo
                            SOAPElement address = factory.createElement(new QName(ADDRESSING_NS, ADDRESS_LOCAL));
                            address.addTextNode(ANONYMOUS_URL);
                            soapElement.addChildElement(address);
                            nodeValue = null; // set back nodeValue to null so that no value is appended as text node for this soap element
                        } else if (nodeValue != null && nodeValue.startsWith(PATTERN) && nodeValue.endsWith(PATTERN)) {
                            String variableName = nodeValue.replace(PATTERN, "");
                            nodeValue = MessageVariable.getValueForVariable(variableName, testInstance.getAncillaryTransaction().getVariables());
                        }
                        if (nodeValue != null) {
                            soapElement.addTextNode(nodeValue);
                        }
                        Node importedSoapHeaderElement = soapHeader.getOwnerDocument().importNode(soapElement, true);
                        soapHeader.appendChild(importedSoapHeaderElement);
                    }
                }
                if (signature != null && TIMESTAMP_LOCAL.equals(signature.getSignedElementLocalName())){
                    SequoiaSignatureBuilder signatureBuilder = new SequoiaSignatureBuilder(signature.getKeystore(), signature.getAlias(),
                            signature.getKeystorePassword(), signature.getPrivateKeyPassword());
                    QName idName = new QName(WSSEConstants.WSU_NS, WSSEConstants.ID);
                    QName timestampName = new QName(WSSEConstants.WSU_NS, signature.getSignedElementLocalName());
                    try {
                        signatureBuilder.signElement(soapMessage.getSOAPPart(), timestampId, timestampName, idName);
                        Node signatureElement = getTimestampSignature(soapHeader);
                        if (signature.getExceptions() != null) {
                            SoapMessageModifier modifier = new SoapMessageModifier((Element) signatureElement);
                            for (Exception exception : signature.getExceptions()) {
                                modifier.modify(exception);
                            }
                        }
                    } catch (java.lang.Exception e){
                        LOG.error(e.getMessage());
                        return false;
                    }
                }
                // if we want to modify the timestamp, we do so after it has been signed
                if (timestamp != null && timestamp.getExceptions() != null){
                    Node timestampFromHeader = getTimestamp(soapHeader);
                    SoapMessageModifier modifier = new SoapMessageModifier((Element) timestampFromHeader);
                    for (Exception exception: timestamp.getExceptions()){
                        modifier.modify(exception);
                    }
                }
            }
            return true;
        } catch (SOAPException e) {
            LOG.error(e.getMessage());
            return false;
        }
    }

    private void addToSoapSecurity(SOAPHeader soapHeader, SOAPFactory factory, SOAPElement securityChild) throws SOAPException {
        SOAPElement securityNode = getWsseSecurityElement(soapHeader);
        if (securityNode != null) {
            securityNode.addChildElement(securityChild);
        } else {
            securityNode = factory.createElement(SECURITY_NAME);
            securityNode.addChildElement(securityChild);
            Node importedSecurityNode = soapHeader.getOwnerDocument().importNode(securityNode, true);
            soapHeader.appendChild(importedSecurityNode);
        }
    }

    private SOAPElement getWsseSecurityElement(SOAPHeader soapHeader) {
        Iterator<SOAPElement> elements = soapHeader.getChildElements(SECURITY_NAME);
        if (elements != null && elements.hasNext()) {
            return elements.next();
        } else {
            return null;
        }
    }

    private SOAPElement getTimestampSignature(SOAPHeader soapHeader) {
        SOAPElement securityElement = getWsseSecurityElement(soapHeader);
        if (securityElement != null) {
            Iterator<SOAPElement> elements = securityElement.getChildElements(new QName(WSSEConstants.XMLDSIGN_NS, WSSEConstants.SIGNATURE_LOCAL));
            if (elements != null && elements.hasNext()) {
                return elements.next();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private SOAPElement getTimestamp(SOAPHeader soapHeader){
        SOAPElement securityElement = getWsseSecurityElement(soapHeader);
        if (securityElement != null){
            Iterator<SOAPElement> elements = securityElement.getChildElements(new QName(WSSEConstants.WSU_NS, TIMESTAMP_LOCAL));
            if (elements != null && elements.hasNext()){
                return elements.next();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
