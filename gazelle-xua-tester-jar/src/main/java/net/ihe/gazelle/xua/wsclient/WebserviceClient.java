package net.ihe.gazelle.xua.wsclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.net.URL;

/**
 * Created by aberge on 24/05/17.
 */
public class WebserviceClient {

    private static final Logger LOG = LoggerFactory.getLogger(WebserviceClient.class);
    private String sutUrl;

    public WebserviceClient(String sutUrl) {
        this.sutUrl = sutUrl;
    }

    public SOAPMessage sendMessage(SOAPMessage soapRequest, String soapAction) {
        try {
            URL endpoint = new URL(sutUrl);
            StringBuilder contentTypeHeader = new StringBuilder("application/soap+xml;charset=UTF-8;action=\"");
            contentTypeHeader.append(soapAction);
            contentTypeHeader.append('\"');
            soapRequest.getMimeHeaders().addHeader("Content-Type", contentTypeHeader.toString());
            SOAPConnectionFactory factory = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = factory.createConnection();
            return connection.call(soapRequest, endpoint);

        } catch (SOAPException | IOException e) {
            LOG.error("Error while sending SOAP message: " + e.getMessage());
        }

        return null;
    }
}
