package net.ihe.gazelle.xua.model;

import net.ihe.gazelle.common.util.XmlFormatter;
import org.dom4j.DocumentException;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * Created by aberge on 23/05/17.
 */

@Entity
@Name("serviceProviderTestInstance")
@Table(name = "xua_service_provider_test_instance", schema = "public")
@SequenceGenerator(name = "xua_service_provider_test_instance_sequence", sequenceName = "xua_service_provider_test_instance_id_seq", allocationSize = 1)
public class ServiceProviderTestInstance implements Serializable {

    private static final String UTF_8 = "UTF-8";
    @Id
    @GeneratedValue(generator = "xua_service_provider_test_instance_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(targetEntity = ServiceProviderTestCase.class)
    @JoinColumn(name = "test_case_id")
    private ServiceProviderTestCase testCase;

    @ManyToOne(targetEntity = AncillaryTransaction.class)
    @JoinColumn(name = "transaction_id")
    private AncillaryTransaction ancillaryTransaction;

    @Column(name = "request")
    private byte[] request;

    @Column(name = "response")
    private byte[] response;

    @Column(name = "tested_endpoint")
    private String testedEndpoint;

    @Column(name = "test_status")
    @Enumerated(EnumType.STRING)
    private TestStatus testStatus;

    @Column(name = "reason_for_failure")
    @Lob
    @Type(type = "text")
    private String reasonForFailure;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "username")
    private String username;

    public ServiceProviderTestInstance(){

    }

    public ServiceProviderTestInstance(ServiceProviderTestCase testCase) {
        this.testCase = testCase;
        this.timestamp = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ServiceProviderTestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(ServiceProviderTestCase testCase) {
        this.testCase = testCase;
    }

    public AncillaryTransaction getAncillaryTransaction() {
        return ancillaryTransaction;
    }

    public void setAncillaryTransaction(AncillaryTransaction ancillaryTransaction) {
        this.ancillaryTransaction = ancillaryTransaction;
    }

    public byte[] getRequest() {
        return request;
    }

    public void setRequest(byte[] request) {
        if (request == null){
            this.request = null;
        } else {
            this.request = request.clone();
        }
    }

    public byte[] getResponse() {
        return response;
    }

    public void setResponse(byte[] response) {
        if (response == null){
            this.response = null;
        } else {
            this.response = response.clone();
        }
    }

    public String getTestedEndpoint() {
        return testedEndpoint;
    }

    public void setTestedEndpoint(String testedEndpoint) {
        this.testedEndpoint = testedEndpoint;
    }

    public TestStatus getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(TestStatus testStatus) {
        this.testStatus = testStatus;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceProviderTestInstance)) {
            return false;
        }

        ServiceProviderTestInstance that = (ServiceProviderTestInstance) o;

        if (testCase != null ? !testCase.equals(that.testCase) : that.testCase != null) {
            return false;
        }
        if (ancillaryTransaction != null ? !ancillaryTransaction.equals(that.ancillaryTransaction) : that.ancillaryTransaction != null) {
            return false;
        }
        if (testedEndpoint != null ? !testedEndpoint.equals(that.testedEndpoint) : that.testedEndpoint != null) {
            return false;
        }
        if (testStatus != null ? !testStatus.equals(that.testStatus) : that.testStatus != null) {
            return false;
        }
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) {
            return false;
        }
        return username != null ? username.equals(that.username) : that.username == null;
    }

    @Override
    public int hashCode() {
        int result = testCase != null ? testCase.hashCode() : 0;
        result = 31 * result + (ancillaryTransaction != null ? ancillaryTransaction.hashCode() : 0);
        result = 31 * result + (testedEndpoint != null ? testedEndpoint.hashCode() : 0);
        result = 31 * result + (testStatus != null ? testStatus.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }

    public String getRequestAsFormattedXML(){
        if (this.request != null){
            return getFormattedXmlContent(request);
        } else {
            return null;
        }
    }

    private String getFormattedXmlContent(byte[] contentAsByteArray) {
        String content = new String(contentAsByteArray, Charset.forName(UTF_8));
        try {
            return XmlFormatter.format(content);
        } catch (DocumentException | IOException e) {
            return content;
        }
    }

    public String getResponseAsFormattedXML(){
        if (this.response != null){
            return getFormattedXmlContent(response);
        } else {
            return "No message has been recorded";
        }
    }

    public String getReasonForFailure() {
        return reasonForFailure;
    }

    public void setReasonForFailure(String reasonForFailure) {
        this.reasonForFailure = reasonForFailure;
    }
}
