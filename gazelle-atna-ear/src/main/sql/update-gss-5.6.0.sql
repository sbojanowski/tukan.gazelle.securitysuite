UPDATE app_configuration SET value='https://gazelle.ihe.net/XSD/IHE/ATNA/dicom_ihe_current.xsd' WHERE variable='dicom_xsd';
UPDATE app_configuration SET value='https://gazelle.ihe.net/XSD/IHE/ATNA/RFC3881.xsd' WHERE variable='rfc3881_xsd';
UPDATE app_configuration SET value='https://gazelle.ihe.net/XSD/OASIS/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-protocol-schema-os.xsd' WHERE variable='ser_req_xsd';
UPDATE app_configuration SET value='https://gazelle.ihe.net/XSD/OASIS/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-assertion-schema-os.xsd' WHERE variable='ser_resp_xsd';
UPDATE app_configuration SET value='https://gazelle.ihe.net/XSD/IHE/XUA/saml-schema-assertion-2.0.xsd' WHERE variable='xua_xsd';
