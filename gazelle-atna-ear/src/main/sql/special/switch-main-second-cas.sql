 -- 
 -- Switch main cas and second cas configuration in GSS
 -- 

INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_cas_keyword'),'tmp_cas_keyword');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_cas_name'),'tmp_cas_name');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_cas_url'),'tmp_cas_url');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_tm_application_url'),'tmp_tm_application_url');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_tm_message_ws'),'tmp_tm_message_ws');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), (SELECT value FROM app_configuration WHERE variable='main_tm_pki_admins'),'tmp_tm_pki_admins');

UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_cas_keyword') WHERE variable = 'main_cas_keyword' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_cas_name') WHERE variable = 'main_cas_name' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_cas_url') WHERE variable = 'main_cas_url' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_tm_application_url') WHERE variable = 'main_tm_application_url' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_tm_message_ws') WHERE variable = 'main_tm_message_ws' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='second_tm_pki_admins') WHERE variable = 'main_tm_pki_admins' ;

UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_cas_keyword') WHERE variable = 'second_cas_keyword' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_cas_name') WHERE variable = 'second_cas_name' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_cas_url') WHERE variable = 'second_cas_url' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_tm_application_url') WHERE variable = 'second_tm_application_url' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_tm_message_ws') WHERE variable = 'second_tm_message_ws' ;
UPDATE app_configuration SET value = (SELECT value FROM app_configuration WHERE variable='tmp_tm_pki_admins') WHERE variable = 'second_tm_pki_admins' ;

DELETE FROM app_configuration WHERE variable='tmp_cas_keyword' ;
DELETE FROM app_configuration WHERE variable='tmp_cas_name' ;
DELETE FROM app_configuration WHERE variable='tmp_cas_url' ;
DELETE FROM app_configuration WHERE variable='tmp_tm_application_url';
DELETE FROM app_configuration WHERE variable='tmp_tm_message_ws';
DELETE FROM app_configuration WHERE variable='tmp_tm_pki_admins';
