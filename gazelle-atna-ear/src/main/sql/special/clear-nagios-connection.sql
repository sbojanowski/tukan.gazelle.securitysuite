-- Delete Nagios monitoring connections.
DELETE FROM tls_connection WHERE id NOT IN (SELECT connection_id FROM tls_connection_tls_connection_data_item) AND remote_host='nagios.ihe-europe.net' ;
