INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'java_cacerts_truststore_pwd', 'changeit');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'tls_automatic_simulator_address_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'tls_simulator_address', '127.0.0.1');
