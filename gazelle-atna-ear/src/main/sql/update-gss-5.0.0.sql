-- ! Important : Execute before deploying ear !
ALTER TABLE tls_certificate RENAME COLUMN internaltest TO is_custom ;
-- Otherwise execute :
-- UPDATE tls_certificate SET is_custom = internaltest ;
-- ALTER TABLE tls_certificate DROP COLUMN internaltest ;

-- Execute before deploying ear :
ALTER TABLE tls_certificate_request ALTER COLUMN dtype TYPE character varying(36) ;
UPDATE tls_certificate_request SET dtype='CertificateRequestWithGeneratedKeys' WHERE dtype='CertificateRequestAutoSign' ;


-- Can be executed at the same time or later
DROP TABLE tls_test_keyword ;
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'pki_automatic_request_signing', 'true');

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'main_cas_keyword', 'EU');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'main_cas_name', 'European authentication');
UPDATE app_configuration SET variable='main_cas_url' WHERE variable='cas_url' ;
UPDATE app_configuration SET variable='main_tm_application_url' WHERE variable='tm_application_url';
UPDATE app_configuration SET variable='main_tm_message_ws' WHERE variable='gazelle_tm_message_ws';
UPDATE app_configuration SET variable='main_tm_pki_admins' WHERE variable='gazelle_pki_admins';
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_cas_enabled', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_cas_keyword', null);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_cas_name', null);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_cas_url', null);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_tm_application_url', null);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_tm_message_ws', null);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'second_tm_pki_admins', null);

CREATE INDEX tls_test_connection_tls_actor_id_idx ON tls_test_connection (tls_actor_id) ;
CREATE INDEX tls_test_actor_is_enabled_idx ON tls_test_actor (is_enabled);
CREATE INDEX tls_certificate_certificateauthority_id_idx ON tls_certificate (certificateauthority_id);

-- ! Must be executed After deploying ! :
UPDATE tls_certificate_request SET requester_cas_key = 'EU' ;
UPDATE atna_questionnaire SET username_cas_key = 'EU' ;
UPDATE atna_questionnaire SET last_modifier_cas_key = 'EU' ;
UPDATE atna_questionnaire SET reviewer_cas_key = 'EU' ;
