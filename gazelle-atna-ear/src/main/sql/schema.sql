--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: am_active_participant_description; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_active_participant_description (
    id integer NOT NULL,
    distinguisher character varying(255),
    max character varying(255),
    min character varying(255),
    name character varying(255),
    alternativeuserid_id integer,
    networkaccesspointid_id integer,
    networkaccesspointtypecode_id integer,
    roleidcode_id integer,
    userid_id integer,
    userisrequestor_id integer,
    username_id integer,
    mediaidentifier_id integer
);


ALTER TABLE public.am_active_participant_description OWNER TO gazelle;

--
-- Name: am_active_participant_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_active_participant_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_active_participant_description_id_seq OWNER TO gazelle;

--
-- Name: am_audit_message_specification; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_audit_message_specification (
    id integer NOT NULL,
    document character varying(255),
    generatorversion character varying(255),
    name character varying(255) NOT NULL,
    schema character varying(255),
    section character varying(255),
    auditsource_id integer,
    event_id integer,
    isdicomcompatible boolean,
    forcedicomcompatibility boolean,
    isdeprecated boolean,
    oid character varying(255),
    affinity_domain character varying(255),
    last_changed timestamp with time zone,
    last_modifier_id character varying(255)
);


ALTER TABLE public.am_audit_message_specification OWNER TO gazelle;

--
-- Name: am_audit_message_specification_active_participants; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_audit_message_specification_active_participants (
    am_audit_message_specification_id integer NOT NULL,
    activeparticipants_id integer NOT NULL
);


ALTER TABLE public.am_audit_message_specification_active_participants OWNER TO gazelle;

--
-- Name: am_audit_message_specification_constraint_specifications; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_audit_message_specification_constraint_specifications (
    am_audit_message_specification_id integer NOT NULL,
    extraconstraintspecifications_id integer NOT NULL
);


ALTER TABLE public.am_audit_message_specification_constraint_specifications OWNER TO gazelle;

--
-- Name: am_audit_message_specification_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_audit_message_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_audit_message_specification_id_seq OWNER TO gazelle;

--
-- Name: am_audit_message_specification_participant_object_ident; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_audit_message_specification_participant_object_ident (
    am_audit_message_specification_id integer NOT NULL,
    participantobjectidentifications_id integer NOT NULL
);


ALTER TABLE public.am_audit_message_specification_participant_object_ident OWNER TO gazelle;

--
-- Name: am_audit_source_description; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_audit_source_description (
    id integer NOT NULL,
    max character varying(255),
    min character varying(255),
    auditenterprisesiteid_id integer,
    auditsourceid_id integer,
    auditsourcetypecode_id integer,
    auditsourcecodevalue_id integer
);


ALTER TABLE public.am_audit_source_description OWNER TO gazelle;

--
-- Name: am_audit_source_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_audit_source_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_audit_source_description_id_seq OWNER TO gazelle;

--
-- Name: am_constraint_specification; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_constraint_specification (
    id integer NOT NULL,
    description character varying(800),
    kind character varying(255),
    xpath character varying(1500)
);


ALTER TABLE public.am_constraint_specification OWNER TO gazelle;

--
-- Name: am_constraint_specification_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_constraint_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_constraint_specification_id_seq OWNER TO gazelle;

--
-- Name: am_element_description; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_element_description (
    id integer NOT NULL,
    optionality character varying(255),
    regex character varying(255),
    value character varying(255)
);


ALTER TABLE public.am_element_description OWNER TO gazelle;

--
-- Name: am_element_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_element_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_element_description_id_seq OWNER TO gazelle;

--
-- Name: am_event_identification_desc; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_event_identification_desc (
    id integer NOT NULL,
    eventactioncode_id integer,
    eventdatetime_id integer,
    eventid_id integer,
    eventoutcomeindicator_id integer,
    eventtypecode_id integer,
    eventoutcomedescription_id integer,
    purposeofuse_id integer
);


ALTER TABLE public.am_event_identification_desc OWNER TO gazelle;

--
-- Name: am_event_identification_desc_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_event_identification_desc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_event_identification_desc_id_seq OWNER TO gazelle;

--
-- Name: am_participant_object_identification_desc; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE am_participant_object_identification_desc (
    id integer NOT NULL,
    distinguisher character varying(255),
    max character varying(255),
    min character varying(255),
    name character varying(255),
    participantobjectdatalifecycle_id integer,
    participantobjectdetail_id integer,
    participantobjectid_id integer,
    participantobjectidtypecode_id integer,
    participantobjectname_id integer,
    participantobjectquery_id integer,
    participantobjectsensitivity_id integer,
    participantobjecttypecode_id integer,
    participantobjecttypecoderole_id integer,
    accession_id integer,
    anonymized_id integer,
    encrypted_id integer,
    mpps_id integer,
    participantobjectcontainsstudy_id integer,
    participantobjectdescription_id integer,
    sopclass_id integer,
    instance_id integer,
    numberofinstances_id integer
);


ALTER TABLE public.am_participant_object_identification_desc OWNER TO gazelle;

--
-- Name: am_participant_object_identification_desc_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE am_participant_object_identification_desc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.am_participant_object_identification_desc_id_seq OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: atna_audited_event; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_audited_event (
    id integer NOT NULL,
    audit_trail_message_url character varying(255),
    audited_transaction character varying(255),
    comment text,
    issuing_actor character varying(255),
    name character varying(255),
    produced_by_system integer,
    validation_status integer,
    validator_oid character varying(255),
    atna_questionnaire_id integer,
    file_location character varying(255),
    validation_date timestamp without time zone,
    validation_log_path character varying(255)
);


ALTER TABLE public.atna_audited_event OWNER TO gazelle;

--
-- Name: atna_audited_event_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_audited_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_audited_event_id_seq OWNER TO gazelle;

--
-- Name: atna_network_communication; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_network_communication (
    id integer NOT NULL,
    actor character varying(255),
    comment text,
    convey_phi integer,
    "inout" integer,
    protected_by_tls integer,
    type integer,
    usage character varying(255),
    atna_questionnaire_id integer
);


ALTER TABLE public.atna_network_communication OWNER TO gazelle;

--
-- Name: atna_network_communication_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_network_communication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_network_communication_id_seq OWNER TO gazelle;

--
-- Name: atna_quest_instruction; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_quest_instruction (
    id integer NOT NULL,
    content text,
    language character varying(255),
    section integer
);


ALTER TABLE public.atna_quest_instruction OWNER TO gazelle;

--
-- Name: atna_quest_instruction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_quest_instruction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_quest_instruction_id_seq OWNER TO gazelle;

--
-- Name: atna_questionnaire; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_questionnaire (
    id integer NOT NULL,
    company character varying(255),
    last_modified_date timestamp with time zone,
    last_modifier character varying(255),
    local_user_authentication_process text,
    non_network_mean_for_accessing_phi text,
    oid character varying(255),
    reviewer character varying(255),
    status integer,
    system character varying(255),
    system_type integer,
    username character varying(255),
    testing_session_id integer,
    last_modifier_cas_key character varying(8),
    reviewer_cas_key character varying(8),
    username_cas_key character varying(8)
);


ALTER TABLE public.atna_questionnaire OWNER TO gazelle;

--
-- Name: atna_questionnaire_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_questionnaire_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_questionnaire_id_seq OWNER TO gazelle;

--
-- Name: atna_testing_session; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_testing_session (
    id integer NOT NULL,
    description character varying(255),
    testing_session_id integer
);


ALTER TABLE public.atna_testing_session OWNER TO gazelle;

--
-- Name: atna_testing_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_testing_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_testing_session_id_seq OWNER TO gazelle;

--
-- Name: atna_tls_test; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_tls_test (
    id integer NOT NULL,
    comment text,
    connection_type integer,
    side integer,
    status integer,
    test_url character varying(255),
    atna_questionnaire_id integer
);


ALTER TABLE public.atna_tls_test OWNER TO gazelle;

--
-- Name: atna_tls_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE atna_tls_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atna_tls_test_id_seq OWNER TO gazelle;

--
-- Name: atna_validator_usage; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE atna_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.atna_validator_usage OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: mbv_assertion; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_assertion (
    id integer NOT NULL,
    assertionid character varying(255),
    idscheme character varying(255),
    constraint_id integer
);


ALTER TABLE public.mbv_assertion OWNER TO gazelle;

--
-- Name: mbv_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_assertion_id_seq OWNER TO gazelle;

--
-- Name: mbv_class_type; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_class_type (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    parent_identifier character varying(255),
    xml_name character varying(255),
    constraintadvanced text,
    path character varying(255),
    templateid character varying(255),
    documentation_spec_id integer,
    package_id integer
);


ALTER TABLE public.mbv_class_type OWNER TO gazelle;

--
-- Name: mbv_class_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_class_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_class_type_id_seq OWNER TO gazelle;

--
-- Name: mbv_constraint; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_constraint (
    id integer NOT NULL,
    author character varying(255),
    datecreation character varying(255),
    description text,
    history text,
    lastchange character varying(255),
    name character varying(255),
    ocl text,
    svsref bytea,
    type character varying(255),
    classtype_id integer,
    kind character varying(255)
);


ALTER TABLE public.mbv_constraint OWNER TO gazelle;

--
-- Name: mbv_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_constraint_id_seq OWNER TO gazelle;

--
-- Name: mbv_documentation_spec; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_documentation_spec (
    id integer NOT NULL,
    description text,
    document character varying(255),
    name character varying(255),
    paragraph character varying(255)
);


ALTER TABLE public.mbv_documentation_spec OWNER TO gazelle;

--
-- Name: mbv_documentation_spec_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_documentation_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_documentation_spec_id_seq OWNER TO gazelle;

--
-- Name: mbv_package; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_package (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    package_name character varying(255),
    namespace character varying(255)
);


ALTER TABLE public.mbv_package OWNER TO gazelle;

--
-- Name: mbv_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_package_id_seq OWNER TO gazelle;

--
-- Name: mbv_standards; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE mbv_standards (
    package_id integer NOT NULL,
    standards character varying(255)
);


ALTER TABLE public.mbv_standards OWNER TO gazelle;

--
-- Name: pki_certificate; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pki_certificate (
    id integer NOT NULL,
    crlnumber integer NOT NULL,
    revoked boolean NOT NULL,
    revokeddate timestamp with time zone,
    revokedreason integer,
    serialnumber_counter integer NOT NULL,
    subject character varying(255),
    certificateauthority_id integer,
    certificatex509_id integer,
    privatekey_id integer,
    publickey_id integer,
    request_id integer,
    is_custom boolean
);


ALTER TABLE public.pki_certificate OWNER TO gazelle;

--
-- Name: pki_certificate_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pki_certificate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pki_certificate_id_seq OWNER TO gazelle;

--
-- Name: pki_certificate_key; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pki_certificate_key (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    algorithm character varying(255),
    enc bytea,
    format character varying(255),
    keytype integer NOT NULL
);


ALTER TABLE public.pki_certificate_key OWNER TO gazelle;

--
-- Name: pki_certificate_key_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pki_certificate_key_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pki_certificate_key_id_seq OWNER TO gazelle;

--
-- Name: pki_certificate_request; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pki_certificate_request (
    dtype character varying(36) NOT NULL,
    id integer NOT NULL,
    certificateextension integer,
    notafter timestamp with time zone,
    notbefore timestamp with time zone,
    requesterusername character varying(255),
    signaturealgorithm integer,
    subject character varying(255),
    certificateversion integer,
    issuer character varying(255),
    certificateauthority_id integer,
    privatekey_id integer,
    publickey_id integer,
    requester_cas_key character varying(8),
    request_date timestamp with time zone
);


ALTER TABLE public.pki_certificate_request OWNER TO gazelle;

--
-- Name: pki_certificate_request_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pki_certificate_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pki_certificate_request_id_seq OWNER TO gazelle;

--
-- Name: pki_certificate_x509; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pki_certificate_x509 (
    id integer NOT NULL,
    enc bytea,
    type character varying(255)
);


ALTER TABLE public.pki_certificate_x509 OWNER TO gazelle;

--
-- Name: pki_certificate_x509_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pki_certificate_x509_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pki_certificate_x509_id_seq OWNER TO gazelle;

--
-- Name: pxy_abstract_message; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pxy_abstract_message (
    pxy_abstract_message_type character varying(31) NOT NULL,
    id integer NOT NULL,
    date_received timestamp with time zone,
    from_ip character varying(255),
    local_port integer,
    messagereceived bytea,
    proxy_port integer,
    proxy_side integer,
    remote_port integer,
    request_channel_id integer,
    response_channel_id integer,
    to_ip character varying(255),
    uuid character varying(255),
    http_headers bytea,
    http_messagetype character varying(255),
    dicom_affectedsopclassuid text,
    dicom_commandfield text,
    dicom_filecommandset bytea,
    dicom_filetransfertsyntax text,
    dicom_requestedsopclassuid text,
    matchingmessage_id integer,
    appname text,
    facility integer,
    hostname text,
    messageid text,
    payload text,
    procid text,
    severity integer,
    tag text,
    "timestamp" text,
    attributesforcommandsetstring text,
    filedump text,
    connection_id integer,
    result_oid bytea,
    index character varying(255),
    index_int integer,
    hl7_messagetype character varying(255),
    hl7_version character varying(255)
);


ALTER TABLE public.pxy_abstract_message OWNER TO gazelle;

--
-- Name: pxy_abstract_message_attributesforcommandset; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pxy_abstract_message_attributesforcommandset (
    pxy_abstract_message_id integer NOT NULL,
    element character varying(255),
    dicom_message_id integer NOT NULL
);


ALTER TABLE public.pxy_abstract_message_attributesforcommandset OWNER TO gazelle;

--
-- Name: pxy_command_field_values; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pxy_command_field_values (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    value_of_key character varying(255)
);


ALTER TABLE public.pxy_command_field_values OWNER TO gazelle;

--
-- Name: pxy_command_field_values_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pxy_command_field_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pxy_command_field_values_id_seq OWNER TO gazelle;

--
-- Name: pxy_connection; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pxy_connection (
    id integer NOT NULL,
    uuid character varying(255)
);


ALTER TABLE public.pxy_connection OWNER TO gazelle;

--
-- Name: pxy_dicom_uid_values; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE pxy_dicom_uid_values (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    value_of_key character varying(255)
);


ALTER TABLE public.pxy_dicom_uid_values OWNER TO gazelle;

--
-- Name: pxy_dicom_uid_values_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pxy_dicom_uid_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pxy_dicom_uid_values_id_seq OWNER TO gazelle;

--
-- Name: pxy_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pxy_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pxy_message_id_seq OWNER TO gazelle;

--
-- Name: syslog_message; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE syslog_message (
    id integer NOT NULL,
    sender_ip character varying(15) NOT NULL,
    collector_ip character varying(15) NOT NULL,
    collector_port integer NOT NULL,
    arrival_time timestamp with time zone NOT NULL,
    error_message text,
    rfc3164_error_message text,
    rfc3164_error_substring character varying(255),
    rfc3164_error_location integer,
    rfc5424_error_message text,
    rfc5424_error_substring character varying(255),
    rfc5424_error_location integer,
    raw_message text,
    message_content text,
    rfc3164_parse_succeed boolean,
    rfc5424_parse_succeed boolean,
    transport_type integer
);


ALTER TABLE public.syslog_message OWNER TO gazelle;

--
-- Name: syslog_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE syslog_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.syslog_message_id_seq OWNER TO gazelle;

--
-- Name: tls_connection; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_connection (
    id integer NOT NULL,
    date timestamp with time zone,
    protocol integer,
    uuid character varying(255),
    proxy_connection_uuid character varying(255),
    cipher_suite integer,
    exception_message text,
    exception_stacktrace text,
    local_host character varying(255),
    local_port integer,
    peer_certificate_chain_pem text,
    remote_host character varying(255),
    remote_port integer,
    renegociation_counter integer,
    is_success boolean,
    simulator_id integer
);


ALTER TABLE public.tls_connection OWNER TO gazelle;

--
-- Name: tls_connection_data_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_connection_data_item (
    id integer NOT NULL,
    type integer,
    date timestamp with time zone,
    tls_connection_data_item_type character varying(31),
    auth_type text,
    pem_chain text
);


ALTER TABLE public.tls_connection_data_item OWNER TO gazelle;

--
-- Name: tls_connection_data_item_aliases_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_connection_data_item_aliases_item (
    id integer NOT NULL,
    value text,
    join_key_type integer,
    join_issuer integer
);


ALTER TABLE public.tls_connection_data_item_aliases_item OWNER TO gazelle;

--
-- Name: tls_connection_data_item_aliases_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_connection_data_item_aliases_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_connection_data_item_aliases_item_id_seq OWNER TO gazelle;

--
-- Name: tls_connection_data_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_connection_data_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_connection_data_item_id_seq OWNER TO gazelle;

--
-- Name: tls_connection_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_connection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_connection_id_seq OWNER TO gazelle;

--
-- Name: tls_connection_tls_connection_data_item; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_connection_tls_connection_data_item (
    connection_id integer NOT NULL,
    data_item_id integer NOT NULL
);


ALTER TABLE public.tls_connection_tls_connection_data_item OWNER TO gazelle;

--
-- Name: tls_simulator; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_simulator (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    keyword character varying(255),
    validator integer,
    certificate_id integer,
    is_check_revocation boolean,
    is_enabled boolean,
    is_use_ca_certs boolean,
    channel_type integer,
    local_port integer,
    is_need_client_auth boolean,
    remote_host character varying(255),
    remote_port integer,
    is_running boolean
);


ALTER TABLE public.tls_simulator OWNER TO gazelle;

--
-- Name: tls_simulator_ciphersuites; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_simulator_ciphersuites (
    simulator_id integer NOT NULL,
    ciphersuite integer NOT NULL
);


ALTER TABLE public.tls_simulator_ciphersuites OWNER TO gazelle;

--
-- Name: tls_simulator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_simulator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_simulator_id_seq OWNER TO gazelle;

--
-- Name: tls_simulator_protocols; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_simulator_protocols (
    simulator_id integer NOT NULL,
    protocol integer NOT NULL
);


ALTER TABLE public.tls_simulator_protocols OWNER TO gazelle;

--
-- Name: tls_simulator_trusted_issuers; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_simulator_trusted_issuers (
    simulator_id integer NOT NULL,
    certificate_id integer NOT NULL
);


ALTER TABLE public.tls_simulator_trusted_issuers OWNER TO gazelle;

--
-- Name: tls_test_case; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_case (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    context integer,
    description character varying(1023),
    expected_alert_level integer,
    expected_handshake_success boolean,
    mandatory_alert_description boolean,
    mandatory_alert_level boolean,
    mandatory_handshake_success boolean,
    name character varying(255) NOT NULL,
    suttype integer,
    simulator_id integer,
    dataset_id integer
);


ALTER TABLE public.tls_test_case OWNER TO gazelle;

--
-- Name: tls_test_case_expectedalertdescriptions; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_case_expectedalertdescriptions (
    tls_test_case_id integer NOT NULL,
    element integer NOT NULL
);


ALTER TABLE public.tls_test_case_expectedalertdescriptions OWNER TO gazelle;

--
-- Name: tls_test_case_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_test_case_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_test_case_id_seq OWNER TO gazelle;

--
-- Name: tls_test_data_set; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_data_set (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    is_check_revocation boolean,
    context integer,
    name character varying(255) NOT NULL,
    is_need_client_auth boolean,
    is_use_sys_ca_certs boolean,
    certificate_id integer
);


ALTER TABLE public.tls_test_data_set OWNER TO gazelle;

--
-- Name: tls_test_data_set_ciphersuites; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_data_set_ciphersuites (
    tls_test_data_set_id integer NOT NULL,
    ciphersuite integer NOT NULL
);


ALTER TABLE public.tls_test_data_set_ciphersuites OWNER TO gazelle;

--
-- Name: tls_test_data_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_test_data_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_test_data_set_id_seq OWNER TO gazelle;

--
-- Name: tls_test_data_set_protocols; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_data_set_protocols (
    tls_test_data_set_id integer NOT NULL,
    protocol integer NOT NULL
);


ALTER TABLE public.tls_test_data_set_protocols OWNER TO gazelle;

--
-- Name: tls_test_data_set_trusted_issuers; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_data_set_trusted_issuers (
    tls_test_data_set_id integer NOT NULL,
    certificate_id integer NOT NULL
);


ALTER TABLE public.tls_test_data_set_trusted_issuers OWNER TO gazelle;

--
-- Name: tls_test_instance; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_instance (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    alert_description_verdict integer,
    alert_level_verdict integer,
    handshake_verdict integer,
    sut_host character varying(255),
    sut_port integer,
    test_verdict integer,
    connection_id integer,
    test_case_id integer
);


ALTER TABLE public.tls_test_instance OWNER TO gazelle;

--
-- Name: tls_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_test_instance_id_seq OWNER TO gazelle;

--
-- Name: tls_test_suite; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_suite (
    id integer NOT NULL,
    context integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.tls_test_suite OWNER TO gazelle;

--
-- Name: tls_test_suite_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_test_suite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_test_suite_id_seq OWNER TO gazelle;

--
-- Name: tls_test_suite_instance; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_suite_instance (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    test_verdict integer,
    sut_host character varying(255),
    sut_port integer,
    test_suite_id integer
);


ALTER TABLE public.tls_test_suite_instance OWNER TO gazelle;

--
-- Name: tls_test_suite_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tls_test_suite_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tls_test_suite_instance_id_seq OWNER TO gazelle;

--
-- Name: tls_test_suite_instance_tls_test_instance; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_suite_instance_tls_test_instance (
    tls_test_suite_instance_id integer NOT NULL,
    testinstances_id integer NOT NULL
);


ALTER TABLE public.tls_test_suite_instance_tls_test_instance OWNER TO gazelle;

--
-- Name: tls_test_suite_tls_test_case; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE tls_test_suite_tls_test_case (
    tls_test_suite_id integer NOT NULL,
    testcases_id integer NOT NULL
);


ALTER TABLE public.tls_test_suite_tls_test_case OWNER TO gazelle;

--
-- Name: xua_ancillary_transaction; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_ancillary_transaction (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    name character varying(255),
    path_to_soap_body character varying(255),
    soap_action character varying(255)
);


ALTER TABLE public.xua_ancillary_transaction OWNER TO gazelle;

--
-- Name: xua_ancillary_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_ancillary_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_ancillary_transaction_id_seq OWNER TO gazelle;

--
-- Name: xua_exception; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_exception (
    id integer NOT NULL,
    exception_type integer,
    new_value character varying(255),
    xpath text,
    part_id integer
);


ALTER TABLE public.xua_exception OWNER TO gazelle;

--
-- Name: xua_exception_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_exception_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_exception_id_seq OWNER TO gazelle;

--
-- Name: xua_message_variable; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_message_variable (
    id integer NOT NULL,
    default_value character varying(255),
    token character varying(255),
    transaction_id integer
);


ALTER TABLE public.xua_message_variable OWNER TO gazelle;

--
-- Name: xua_message_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_message_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_message_variable_id_seq OWNER TO gazelle;

--
-- Name: xua_service_provider_test_case; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_service_provider_test_case (
    id integer NOT NULL,
    description text,
    expected_result_details character varying(255),
    keyword character varying(255),
    last_changed timestamp without time zone,
    last_modifier character varying(255),
    available boolean,
    active boolean,
    expected_result character varying(255)
);


ALTER TABLE public.xua_service_provider_test_case OWNER TO gazelle;

--
-- Name: xua_service_provider_test_case_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_service_provider_test_case_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_service_provider_test_case_id_seq OWNER TO gazelle;

--
-- Name: xua_service_provider_test_instance; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_service_provider_test_instance (
    id integer NOT NULL,
    request bytea,
    response bytea,
    test_status character varying(255),
    tested_endpoint character varying(255),
    "timestamp" timestamp without time zone,
    username character varying(255),
    test_case_id integer,
    transaction_id integer,
    reason_for_failure text
);


ALTER TABLE public.xua_service_provider_test_instance OWNER TO gazelle;

--
-- Name: xua_service_provider_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_service_provider_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_service_provider_test_instance_id_seq OWNER TO gazelle;

--
-- Name: xua_soap_header_part; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE xua_soap_header_part (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    created_offset integer,
    duration integer,
    local_name character varying(255),
    must_understand boolean,
    namespace_uri character varying(255),
    value character varying(255),
    password character varying(255),
    sts_endpoint character varying(255),
    username character varying(255),
    signed_element_namespace_uri character varying(255),
    signed_element_local_name character varying(255),
    test_case_id integer,
    keystore character varying(255),
    alias character varying(255),
    keystore_password character varying(255),
    private_key_password character varying(255)
);


ALTER TABLE public.xua_soap_header_part OWNER TO gazelle;

--
-- Name: xua_soap_header_part_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_soap_header_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_soap_header_part_id_seq OWNER TO gazelle;

--
-- Name: am_active_participant_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT am_active_participant_description_pkey PRIMARY KEY (id);


--
-- Name: am_audit_message_specificatio_extraconstraintspecifications_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_constraint_specifications
    ADD CONSTRAINT am_audit_message_specificatio_extraconstraintspecifications_key UNIQUE (extraconstraintspecifications_id);


--
-- Name: am_audit_message_specificatio_participantobjectidentificati_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_participant_object_ident
    ADD CONSTRAINT am_audit_message_specificatio_participantobjectidentificati_key UNIQUE (participantobjectidentifications_id);


--
-- Name: am_audit_message_specification_active_activeparticipants_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_active_participants
    ADD CONSTRAINT am_audit_message_specification_active_activeparticipants_id_key UNIQUE (activeparticipants_id);


--
-- Name: am_audit_message_specification_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification
    ADD CONSTRAINT am_audit_message_specification_name_key UNIQUE (name);


--
-- Name: am_audit_message_specification_oid_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification
    ADD CONSTRAINT am_audit_message_specification_oid_key UNIQUE (oid);


--
-- Name: am_audit_message_specification_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification
    ADD CONSTRAINT am_audit_message_specification_pkey PRIMARY KEY (id);


--
-- Name: am_audit_source_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_source_description
    ADD CONSTRAINT am_audit_source_description_pkey PRIMARY KEY (id);


--
-- Name: am_constraint_specification_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_constraint_specification
    ADD CONSTRAINT am_constraint_specification_pkey PRIMARY KEY (id);


--
-- Name: am_element_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_element_description
    ADD CONSTRAINT am_element_description_pkey PRIMARY KEY (id);


--
-- Name: am_event_identification_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT am_event_identification_desc_pkey PRIMARY KEY (id);


--
-- Name: am_participant_object_identification_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT am_participant_object_identification_desc_pkey PRIMARY KEY (id);


--
-- Name: app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: atna_audited_event_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_audited_event
    ADD CONSTRAINT atna_audited_event_pkey PRIMARY KEY (id);


--
-- Name: atna_network_communication_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_network_communication
    ADD CONSTRAINT atna_network_communication_pkey PRIMARY KEY (id);


--
-- Name: atna_quest_instruction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_quest_instruction
    ADD CONSTRAINT atna_quest_instruction_pkey PRIMARY KEY (id);


--
-- Name: atna_quest_instruction_section_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_quest_instruction
    ADD CONSTRAINT atna_quest_instruction_section_language_key UNIQUE (section, language);


--
-- Name: atna_questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_questionnaire
    ADD CONSTRAINT atna_questionnaire_pkey PRIMARY KEY (id);


--
-- Name: atna_testing_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_testing_session
    ADD CONSTRAINT atna_testing_session_pkey PRIMARY KEY (id);


--
-- Name: atna_tls_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_tls_test
    ADD CONSTRAINT atna_tls_test_pkey PRIMARY KEY (id);


--
-- Name: atna_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_validator_usage
    ADD CONSTRAINT atna_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: mbv_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT mbv_assertion_pkey PRIMARY KEY (id);


--
-- Name: mbv_class_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT mbv_class_type_pkey PRIMARY KEY (id);


--
-- Name: mbv_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT mbv_constraint_pkey PRIMARY KEY (id);


--
-- Name: mbv_documentation_spec_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY mbv_documentation_spec
    ADD CONSTRAINT mbv_documentation_spec_pkey PRIMARY KEY (id);


--
-- Name: mbv_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY mbv_package
    ADD CONSTRAINT mbv_package_pkey PRIMARY KEY (id);


--
-- Name: pki_certificate_key_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pki_certificate_key
    ADD CONSTRAINT pki_certificate_key_pkey PRIMARY KEY (id);


--
-- Name: pki_certificate_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT pki_certificate_pkey PRIMARY KEY (id);


--
-- Name: pki_certificate_request_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pki_certificate_request
    ADD CONSTRAINT pki_certificate_request_pkey PRIMARY KEY (id);


--
-- Name: pki_certificate_x509_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pki_certificate_x509
    ADD CONSTRAINT pki_certificate_x509_pkey PRIMARY KEY (id);


--
-- Name: pxy_abstract_message_attributesforcommandset_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pxy_abstract_message_attributesforcommandset
    ADD CONSTRAINT pxy_abstract_message_attributesforcommandset_pkey PRIMARY KEY (pxy_abstract_message_id, dicom_message_id);


--
-- Name: pxy_abstract_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pxy_abstract_message
    ADD CONSTRAINT pxy_abstract_message_pkey PRIMARY KEY (id);


--
-- Name: pxy_command_field_values_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pxy_command_field_values
    ADD CONSTRAINT pxy_command_field_values_pkey PRIMARY KEY (id);


--
-- Name: pxy_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pxy_connection
    ADD CONSTRAINT pxy_connection_pkey PRIMARY KEY (id);


--
-- Name: pxy_dicom_uid_values_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY pxy_dicom_uid_values
    ADD CONSTRAINT pxy_dicom_uid_values_pkey PRIMARY KEY (id);


--
-- Name: syslog_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY syslog_message
    ADD CONSTRAINT syslog_message_pkey PRIMARY KEY (id);


--
-- Name: tls_connection_data_item_aliases_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_connection_data_item_aliases_item
    ADD CONSTRAINT tls_connection_data_item_aliases_item_pkey PRIMARY KEY (id);


--
-- Name: tls_connection_data_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_connection_data_item
    ADD CONSTRAINT tls_connection_data_item_pkey PRIMARY KEY (id);


--
-- Name: tls_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_connection
    ADD CONSTRAINT tls_connection_pkey PRIMARY KEY (id);


--
-- Name: tls_connection_tls_connection_data_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_connection_tls_connection_data_item
    ADD CONSTRAINT tls_connection_tls_connection_data_item_pkey PRIMARY KEY (connection_id, data_item_id);


--
-- Name: tls_simulator_ciphersuites_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_simulator_ciphersuites
    ADD CONSTRAINT tls_simulator_ciphersuites_pkey PRIMARY KEY (simulator_id, ciphersuite);


--
-- Name: tls_simulator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_simulator
    ADD CONSTRAINT tls_simulator_pkey PRIMARY KEY (id);


--
-- Name: tls_simulator_protocols_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_simulator_protocols
    ADD CONSTRAINT tls_simulator_protocols_pkey PRIMARY KEY (simulator_id, protocol);


--
-- Name: tls_simulator_trusted_issuers_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_simulator_trusted_issuers
    ADD CONSTRAINT tls_simulator_trusted_issuers_pkey PRIMARY KEY (simulator_id, certificate_id);


--
-- Name: tls_test_case_expectedalertdescriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_case_expectedalertdescriptions
    ADD CONSTRAINT tls_test_case_expectedalertdescriptions_pkey PRIMARY KEY (tls_test_case_id, element);


--
-- Name: tls_test_case_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_case
    ADD CONSTRAINT tls_test_case_name_key UNIQUE (name);


--
-- Name: tls_test_case_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_case
    ADD CONSTRAINT tls_test_case_pkey PRIMARY KEY (id);


--
-- Name: tls_test_data_set_ciphersuites_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_data_set_ciphersuites
    ADD CONSTRAINT tls_test_data_set_ciphersuites_pkey PRIMARY KEY (tls_test_data_set_id, ciphersuite);


--
-- Name: tls_test_data_set_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_data_set
    ADD CONSTRAINT tls_test_data_set_name_key UNIQUE (name);


--
-- Name: tls_test_data_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_data_set
    ADD CONSTRAINT tls_test_data_set_pkey PRIMARY KEY (id);


--
-- Name: tls_test_data_set_protocols_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_data_set_protocols
    ADD CONSTRAINT tls_test_data_set_protocols_pkey PRIMARY KEY (tls_test_data_set_id, protocol);


--
-- Name: tls_test_data_set_trusted_issuers_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_data_set_trusted_issuers
    ADD CONSTRAINT tls_test_data_set_trusted_issuers_pkey PRIMARY KEY (tls_test_data_set_id, certificate_id);


--
-- Name: tls_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_instance
    ADD CONSTRAINT tls_test_instance_pkey PRIMARY KEY (id);


--
-- Name: tls_test_suite_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_suite_instance
    ADD CONSTRAINT tls_test_suite_instance_pkey PRIMARY KEY (id);


--
-- Name: tls_test_suite_instance_tls_test_instance_testinstances_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_suite_instance_tls_test_instance
    ADD CONSTRAINT tls_test_suite_instance_tls_test_instance_testinstances_id_key UNIQUE (testinstances_id);


--
-- Name: tls_test_suite_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_suite
    ADD CONSTRAINT tls_test_suite_name_key UNIQUE (name);


--
-- Name: tls_test_suite_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_suite
    ADD CONSTRAINT tls_test_suite_pkey PRIMARY KEY (id);


--
-- Name: uk_419qp4bppvxha50r3ry3lx4wp; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_active_participants
    ADD CONSTRAINT uk_419qp4bppvxha50r3ry3lx4wp UNIQUE (activeparticipants_id);


--
-- Name: uk_4v3op9k3wk35xofa51gdjslno; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY tls_test_suite_instance_tls_test_instance
    ADD CONSTRAINT uk_4v3op9k3wk35xofa51gdjslno UNIQUE (testinstances_id);


--
-- Name: uk_d6ojt538hbi914w6tj4panibr; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_constraint_specifications
    ADD CONSTRAINT uk_d6ojt538hbi914w6tj4panibr UNIQUE (extraconstraintspecifications_id);


--
-- Name: uk_l7mwnnxh9y2u4nky1kf1da5p6; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY am_audit_message_specification_participant_object_ident
    ADD CONSTRAINT uk_l7mwnnxh9y2u4nky1kf1da5p6 UNIQUE (participantobjectidentifications_id);


--
-- Name: uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: uk_nwlr4p68kn6gynd9g29pxp2lr; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY atna_quest_instruction
    ADD CONSTRAINT uk_nwlr4p68kn6gynd9g29pxp2lr UNIQUE (section, language);


--
-- Name: variable_unique; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT variable_unique UNIQUE (variable);


--
-- Name: xua_ancillary_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_ancillary_transaction
    ADD CONSTRAINT xua_ancillary_transaction_pkey PRIMARY KEY (id);


--
-- Name: xua_exception_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_exception
    ADD CONSTRAINT xua_exception_pkey PRIMARY KEY (id);


--
-- Name: xua_message_variable_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_message_variable
    ADD CONSTRAINT xua_message_variable_pkey PRIMARY KEY (id);


--
-- Name: xua_service_provider_test_case_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_service_provider_test_case
    ADD CONSTRAINT xua_service_provider_test_case_pkey PRIMARY KEY (id);


--
-- Name: xua_service_provider_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT xua_service_provider_test_instance_pkey PRIMARY KEY (id);


--
-- Name: xua_soap_header_part_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY xua_soap_header_part
    ADD CONSTRAINT xua_soap_header_part_pkey PRIMARY KEY (id);


--
-- Name: pki_certificate_certificateauthority_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_certificateauthority_idx ON pki_certificate USING btree (certificateauthority_id);


--
-- Name: pki_certificate_certificatex509_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_certificatex509_idx ON pki_certificate USING btree (certificatex509_id);


--
-- Name: pki_certificate_privatekey_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_privatekey_idx ON pki_certificate USING btree (privatekey_id);


--
-- Name: pki_certificate_publickey_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_publickey_idx ON pki_certificate USING btree (publickey_id);


--
-- Name: pki_certificate_request_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_request_idx ON pki_certificate USING btree (request_id);


--
-- Name: pki_certificate_subject_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_certificate_subject_idx ON pki_certificate USING btree (subject);


--
-- Name: pki_request_certificateauthority_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_request_certificateauthority_idx ON pki_certificate_request USING btree (certificateauthority_id);


--
-- Name: pki_request_privatekey_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_request_privatekey_idx ON pki_certificate_request USING btree (privatekey_id);


--
-- Name: pki_request_publickey_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_request_publickey_idx ON pki_certificate_request USING btree (publickey_id);


--
-- Name: pki_request_requesterusername_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_request_requesterusername_idx ON pki_certificate_request USING btree (requesterusername);


--
-- Name: pki_request_subject_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX pki_request_subject_idx ON pki_certificate_request USING btree (subject);


--
-- Name: syslog_message_sender_ip_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX syslog_message_sender_ip_idx ON syslog_message USING btree (sender_ip);


--
-- Name: syslog_message_transport_type_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX syslog_message_transport_type_idx ON syslog_message USING btree (transport_type);


--
-- Name: tls_connection_data_item_aliases_item_issuer_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_connection_data_item_aliases_item_issuer_idx ON tls_connection_data_item_aliases_item USING btree (join_issuer);


--
-- Name: tls_connection_data_item_aliases_item_key_type_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_connection_data_item_aliases_item_key_type_idx ON tls_connection_data_item_aliases_item USING btree (join_key_type);


--
-- Name: tls_connection_is_success_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_connection_is_success_idx ON tls_connection USING btree (is_success);


--
-- Name: tls_connection_remote_host_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_connection_remote_host_idx ON tls_connection USING btree (remote_host);


--
-- Name: tls_connection_simulator_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_connection_simulator_idx ON tls_connection USING btree (simulator_id);


--
-- Name: tls_simulator_certificate_id_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_simulator_certificate_id_idx ON tls_simulator USING btree (certificate_id);


--
-- Name: tls_simulator_channel_type_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_simulator_channel_type_idx ON tls_simulator USING btree (channel_type);


--
-- Name: tls_simulator_is_enabled_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_simulator_is_enabled_idx ON tls_simulator USING btree (is_enabled);


--
-- Name: tls_test_case_dataset_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_case_dataset_idx ON tls_test_case USING btree (dataset_id);


--
-- Name: tls_test_case_simulator_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_case_simulator_idx ON tls_test_case USING btree (simulator_id);


--
-- Name: tls_test_data_set_certificate_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_data_set_certificate_idx ON tls_test_data_set USING btree (certificate_id);


--
-- Name: tls_test_instance_connection_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_instance_connection_idx ON tls_test_instance USING btree (connection_id);


--
-- Name: tls_test_instance_last_modifier_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_instance_last_modifier_idx ON tls_test_instance USING btree (last_modifier_id);


--
-- Name: tls_test_instance_sut_host_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_instance_sut_host_idx ON tls_test_instance USING btree (sut_host);


--
-- Name: tls_test_instance_test_case_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_instance_test_case_idx ON tls_test_instance USING btree (test_case_id);


--
-- Name: tls_test_instance_verdict_idx; Type: INDEX; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE INDEX tls_test_instance_verdict_idx ON tls_test_instance USING btree (test_verdict);


--
-- Name: fk16ce89c47778d25e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT fk16ce89c47778d25e FOREIGN KEY (constraint_id) REFERENCES mbv_constraint(id);


--
-- Name: fk1ef6b70955bc92ac; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_suite_tls_test_case
    ADD CONSTRAINT fk1ef6b70955bc92ac FOREIGN KEY (tls_test_suite_id) REFERENCES tls_test_suite(id);


--
-- Name: fk1ef6b709df082d10; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_suite_tls_test_case
    ADD CONSTRAINT fk1ef6b709df082d10 FOREIGN KEY (testcases_id) REFERENCES tls_test_case(id);


--
-- Name: fk2134ab55a341f988; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_case_expectedalertdescriptions
    ADD CONSTRAINT fk2134ab55a341f988 FOREIGN KEY (tls_test_case_id) REFERENCES tls_test_case(id);


--
-- Name: fk25b5fc05705bd2f0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_suite_instance
    ADD CONSTRAINT fk25b5fc05705bd2f0 FOREIGN KEY (test_suite_id) REFERENCES tls_test_suite(id);


--
-- Name: fk27d4d5cc34c881ce; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_simulator
    ADD CONSTRAINT fk27d4d5cc34c881ce FOREIGN KEY (certificate_id) REFERENCES pki_certificate(id);


--
-- Name: fk350aaf1f8e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f8e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: fk350aaf1f9ba1deeb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f9ba1deeb FOREIGN KEY (documentation_spec_id) REFERENCES mbv_documentation_spec(id);


--
-- Name: fk385b49f18997ce01; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_active_participants
    ADD CONSTRAINT fk385b49f18997ce01 FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id);


--
-- Name: fk385b49f1f0d7ed5f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_active_participants
    ADD CONSTRAINT fk385b49f1f0d7ed5f FOREIGN KEY (activeparticipants_id) REFERENCES am_active_participant_description(id);


--
-- Name: fk3afefb5bc668fe56; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT fk3afefb5bc668fe56 FOREIGN KEY (classtype_id) REFERENCES mbv_class_type(id);


--
-- Name: fk3c1f796f22dc2dd8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_source_description
    ADD CONSTRAINT fk3c1f796f22dc2dd8 FOREIGN KEY (auditenterprisesiteid_id) REFERENCES am_element_description(id);


--
-- Name: fk3c1f796f2cde76b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_source_description
    ADD CONSTRAINT fk3c1f796f2cde76b9 FOREIGN KEY (auditsourcetypecode_id) REFERENCES am_element_description(id);


--
-- Name: fk3c1f796f3f3cbe48; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_source_description
    ADD CONSTRAINT fk3c1f796f3f3cbe48 FOREIGN KEY (auditsourcecodevalue_id) REFERENCES am_element_description(id);


--
-- Name: fk3c1f796fa35f14c5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_source_description
    ADD CONSTRAINT fk3c1f796fa35f14c5 FOREIGN KEY (auditsourceid_id) REFERENCES am_element_description(id);


--
-- Name: fk41dd2c188e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_standards
    ADD CONSTRAINT fk41dd2c188e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: fk439433f8caf208ed; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_abstract_message
    ADD CONSTRAINT fk439433f8caf208ed FOREIGN KEY (connection_id) REFERENCES pxy_connection(id);


--
-- Name: fk439433f8de980cfc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_abstract_message
    ADD CONSTRAINT fk439433f8de980cfc FOREIGN KEY (matchingmessage_id) REFERENCES pxy_abstract_message(id);


--
-- Name: fk447356a314bff85; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate_request
    ADD CONSTRAINT fk447356a314bff85 FOREIGN KEY (privatekey_id) REFERENCES pki_certificate_key(id);


--
-- Name: fk447356a32bc6010f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate_request
    ADD CONSTRAINT fk447356a32bc6010f FOREIGN KEY (publickey_id) REFERENCES pki_certificate_key(id);


--
-- Name: fk447356a3e45e63b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate_request
    ADD CONSTRAINT fk447356a3e45e63b9 FOREIGN KEY (certificateauthority_id) REFERENCES pki_certificate(id);


--
-- Name: fk4528e0c1162f3501; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1162f3501 FOREIGN KEY (instance_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c11f8ccd72; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c11f8ccd72 FOREIGN KEY (encrypted_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c130b0b312; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c130b0b312 FOREIGN KEY (accession_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c13fb498ec; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c13fb498ec FOREIGN KEY (participantobjectdescription_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1510f229e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1510f229e FOREIGN KEY (participantobjectcontainsstudy_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c16943bad2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c16943bad2 FOREIGN KEY (sopclass_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c173c8d5c9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c173c8d5c9 FOREIGN KEY (participantobjectid_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c17abd0158; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c17abd0158 FOREIGN KEY (numberofinstances_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c18655bd42; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c18655bd42 FOREIGN KEY (participantobjectidtypecode_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c19fc00746; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c19fc00746 FOREIGN KEY (anonymized_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1a2e5a6d9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1a2e5a6d9 FOREIGN KEY (participantobjectname_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1b0cc64bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1b0cc64bd FOREIGN KEY (participantobjecttypecode_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1bacd3fd3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1bacd3fd3 FOREIGN KEY (participantobjectdetail_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1be5a2e20; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1be5a2e20 FOREIGN KEY (participantobjectquery_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1c87bad07; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1c87bad07 FOREIGN KEY (participantobjecttypecoderole_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1deff1d10; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1deff1d10 FOREIGN KEY (mpps_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1e2cfb2e8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1e2cfb2e8 FOREIGN KEY (participantobjectdatalifecycle_id) REFERENCES am_element_description(id);


--
-- Name: fk4528e0c1fed1d769; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_participant_object_identification_desc
    ADD CONSTRAINT fk4528e0c1fed1d769 FOREIGN KEY (participantobjectsensitivity_id) REFERENCES am_element_description(id);


--
-- Name: fk4e62e8d82240c33c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_connection_tls_connection_data_item
    ADD CONSTRAINT fk4e62e8d82240c33c FOREIGN KEY (connection_id) REFERENCES tls_connection(id);


--
-- Name: fk4e62e8d8e763b740; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_connection_tls_connection_data_item
    ADD CONSTRAINT fk4e62e8d8e763b740 FOREIGN KEY (data_item_id) REFERENCES tls_connection_data_item(id);


--
-- Name: fk545ba3aed4deaa18; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_simulator_protocols
    ADD CONSTRAINT fk545ba3aed4deaa18 FOREIGN KEY (simulator_id) REFERENCES tls_simulator(id);


--
-- Name: fk5e6a71634c881ce; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_data_set
    ADD CONSTRAINT fk5e6a71634c881ce FOREIGN KEY (certificate_id) REFERENCES pki_certificate(id);


--
-- Name: fk6297d12a69ffe2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_data_set_trusted_issuers
    ADD CONSTRAINT fk6297d12a69ffe2 FOREIGN KEY (certificate_id) REFERENCES pki_certificate(id);


--
-- Name: fk6297d12ac617f425; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_data_set_trusted_issuers
    ADD CONSTRAINT fk6297d12ac617f425 FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id);


--
-- Name: fk6c894af7d4deaa18; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_simulator_ciphersuites
    ADD CONSTRAINT fk6c894af7d4deaa18 FOREIGN KEY (simulator_id) REFERENCES tls_simulator(id);


--
-- Name: fk6d7b13a4a1744d6b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_suite_instance_tls_test_instance
    ADD CONSTRAINT fk6d7b13a4a1744d6b FOREIGN KEY (tls_test_suite_instance_id) REFERENCES tls_test_suite_instance(id);


--
-- Name: fk6d7b13a4e57cd8da; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_suite_instance_tls_test_instance
    ADD CONSTRAINT fk6d7b13a4e57cd8da FOREIGN KEY (testinstances_id) REFERENCES tls_test_instance(id);


--
-- Name: fk72243e4a12b70155; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4a12b70155 FOREIGN KEY (eventtypecode_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4a69961684; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4a69961684 FOREIGN KEY (purposeofuse_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4a6a50cd59; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4a6a50cd59 FOREIGN KEY (eventactioncode_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4a9ae799b2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4a9ae799b2 FOREIGN KEY (eventoutcomedescription_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4aa826a581; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4aa826a581 FOREIGN KEY (eventdatetime_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4aceaac461; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4aceaac461 FOREIGN KEY (eventid_id) REFERENCES am_element_description(id);


--
-- Name: fk72243e4ae6dc0bf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_event_identification_desc
    ADD CONSTRAINT fk72243e4ae6dc0bf FOREIGN KEY (eventoutcomeindicator_id) REFERENCES am_element_description(id);


--
-- Name: fk7524c05314bff85; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT fk7524c05314bff85 FOREIGN KEY (privatekey_id) REFERENCES pki_certificate_key(id);


--
-- Name: fk7524c0532bc6010f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT fk7524c0532bc6010f FOREIGN KEY (publickey_id) REFERENCES pki_certificate_key(id);


--
-- Name: fk7524c0535694cbcf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT fk7524c0535694cbcf FOREIGN KEY (request_id) REFERENCES pki_certificate_request(id);


--
-- Name: fk7524c053851e162e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT fk7524c053851e162e FOREIGN KEY (certificatex509_id) REFERENCES pki_certificate_x509(id);


--
-- Name: fk7524c053e45e63b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pki_certificate
    ADD CONSTRAINT fk7524c053e45e63b9 FOREIGN KEY (certificateauthority_id) REFERENCES pki_certificate(id);


--
-- Name: fk8e53f07c4331afb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY atna_audited_event
    ADD CONSTRAINT fk8e53f07c4331afb6 FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id);


--
-- Name: fk954fad2a13d699b1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2a13d699b1 FOREIGN KEY (networkaccesspointtypecode_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2a84e54d20; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2a84e54d20 FOREIGN KEY (username_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2aa40ad1bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2aa40ad1bd FOREIGN KEY (networkaccesspointid_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2aa83b61d0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2aa83b61d0 FOREIGN KEY (userid_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2ac0186a79; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2ac0186a79 FOREIGN KEY (userisrequestor_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2ac793b709; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2ac793b709 FOREIGN KEY (mediaidentifier_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2aed782f78; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2aed782f78 FOREIGN KEY (roleidcode_id) REFERENCES am_element_description(id);


--
-- Name: fk954fad2af7738cc3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_active_participant_description
    ADD CONSTRAINT fk954fad2af7738cc3 FOREIGN KEY (alternativeuserid_id) REFERENCES am_element_description(id);


--
-- Name: fk95bebe4a73b1fcca; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY atna_questionnaire
    ADD CONSTRAINT fk95bebe4a73b1fcca FOREIGN KEY (testing_session_id) REFERENCES atna_testing_session(id);


--
-- Name: fk95eef879399ff94f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_case
    ADD CONSTRAINT fk95eef879399ff94f FOREIGN KEY (simulator_id) REFERENCES tls_simulator(id);


--
-- Name: fk95eef87993ae5383; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_case
    ADD CONSTRAINT fk95eef87993ae5383 FOREIGN KEY (dataset_id) REFERENCES tls_test_data_set(id);


--
-- Name: fk981a216748fd5413; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_connection
    ADD CONSTRAINT fk981a216748fd5413 FOREIGN KEY (simulator_id) REFERENCES tls_simulator(id);


--
-- Name: fk9879e44f4331afb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY atna_tls_test
    ADD CONSTRAINT fk9879e44f4331afb6 FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id);


--
-- Name: fk_2p4ij7mxf9e0lr0tfbsbp20vw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT fk_2p4ij7mxf9e0lr0tfbsbp20vw FOREIGN KEY (transaction_id) REFERENCES xua_ancillary_transaction(id);


--
-- Name: fk_a358pmfdujmer38h7k1xpn96f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_soap_header_part
    ADD CONSTRAINT fk_a358pmfdujmer38h7k1xpn96f FOREIGN KEY (test_case_id) REFERENCES xua_service_provider_test_case(id);


--
-- Name: fk_bfkcbkxlqwxyh6pmqkbt8nvxk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT fk_bfkcbkxlqwxyh6pmqkbt8nvxk FOREIGN KEY (test_case_id) REFERENCES xua_service_provider_test_case(id);


--
-- Name: fk_hrl8prtbpjgm7us84svlisc6v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_message_variable
    ADD CONSTRAINT fk_hrl8prtbpjgm7us84svlisc6v FOREIGN KEY (transaction_id) REFERENCES xua_ancillary_transaction(id);


--
-- Name: fk_t3b4kiyy8v7e7ymgm48vi0i5m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_exception
    ADD CONSTRAINT fk_t3b4kiyy8v7e7ymgm48vi0i5m FOREIGN KEY (part_id) REFERENCES xua_soap_header_part(id);


--
-- Name: fka58bbfd46c80db26; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification
    ADD CONSTRAINT fka58bbfd46c80db26 FOREIGN KEY (auditsource_id) REFERENCES am_audit_source_description(id);


--
-- Name: fka58bbfd47538881d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification
    ADD CONSTRAINT fka58bbfd47538881d FOREIGN KEY (event_id) REFERENCES am_event_identification_desc(id);


--
-- Name: fkbc55455e8b57a0c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_instance
    ADD CONSTRAINT fkbc55455e8b57a0c4 FOREIGN KEY (test_case_id) REFERENCES tls_test_case(id);


--
-- Name: fkbc55455ebad35e65; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_instance
    ADD CONSTRAINT fkbc55455ebad35e65 FOREIGN KEY (connection_id) REFERENCES tls_connection(id);


--
-- Name: fkbd3b7678c617f425; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_data_set_protocols
    ADD CONSTRAINT fkbd3b7678c617f425 FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id);


--
-- Name: fkc31c0bedc617f425; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_test_data_set_ciphersuites
    ADD CONSTRAINT fkc31c0bedc617f425 FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id);


--
-- Name: fkc72dd3e032c873ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_simulator_trusted_issuers
    ADD CONSTRAINT fkc72dd3e032c873ab FOREIGN KEY (certificate_id) REFERENCES pki_certificate(id);


--
-- Name: fkc72dd3e0d4deaa18; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_simulator_trusted_issuers
    ADD CONSTRAINT fkc72dd3e0d4deaa18 FOREIGN KEY (simulator_id) REFERENCES tls_simulator(id);


--
-- Name: fkd3475e372619767; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_connection_data_item_aliases_item
    ADD CONSTRAINT fkd3475e372619767 FOREIGN KEY (join_key_type) REFERENCES tls_connection_data_item(id);


--
-- Name: fkd3475e38dfe9646; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tls_connection_data_item_aliases_item
    ADD CONSTRAINT fkd3475e38dfe9646 FOREIGN KEY (join_issuer) REFERENCES tls_connection_data_item(id);


--
-- Name: fkd6d3ad2c4331afb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY atna_network_communication
    ADD CONSTRAINT fkd6d3ad2c4331afb6 FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id);


--
-- Name: fke275d9c78997ce01; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_constraint_specifications
    ADD CONSTRAINT fke275d9c78997ce01 FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id);


--
-- Name: fke275d9c792c0e8a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_constraint_specifications
    ADD CONSTRAINT fke275d9c792c0e8a7 FOREIGN KEY (extraconstraintspecifications_id) REFERENCES am_constraint_specification(id);


--
-- Name: fkf75b62202ed380; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_abstract_message_attributesforcommandset
    ADD CONSTRAINT fkf75b62202ed380 FOREIGN KEY (pxy_abstract_message_id) REFERENCES pxy_abstract_message(id);


--
-- Name: fkfda18c876da6787c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_participant_object_ident
    ADD CONSTRAINT fkfda18c876da6787c FOREIGN KEY (participantobjectidentifications_id) REFERENCES am_participant_object_identification_desc(id);


--
-- Name: fkfda18c878997ce01; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY am_audit_message_specification_participant_object_ident
    ADD CONSTRAINT fkfda18c878997ce01 FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

