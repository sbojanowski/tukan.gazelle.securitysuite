package net.ihe.gazelle.pki.validator.epsos.v1;

public class EpsosVPNServerValidator extends EpsosServerValidator {

    @Override
    protected String getCertificateType() {
        return "VPN server";
    }

}
