package net.ihe.gazelle.pki.model;

import java.security.PublicKey;
import java.security.cert.CertificateException;

import javax.persistence.Entity;

@Entity
public class CertificatePublicKey extends CertificateKey<PublicKey> {

    public CertificatePublicKey() {
        super();
    }

    public CertificatePublicKey(PublicKey key) throws CertificateException {
        super(key);
    }

}
