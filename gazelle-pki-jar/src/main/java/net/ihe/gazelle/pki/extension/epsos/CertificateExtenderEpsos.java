package net.ihe.gazelle.pki.extension.epsos;

import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import net.ihe.gazelle.pki.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.extension.CertificateExtenderCRL;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

public class CertificateExtenderEpsos extends CertificateExtenderCRL {

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);
        try {
            if (parameters.getCertificateRequest().getCertificateAuthority() != null) {
                certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(
                        parameters.getCertificateRequest().getCertificateAuthority().getPublicKey().getKey()));
            }
            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(
                    parameters.getPublicKey()));
        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        } catch (CertificateParsingException e) {
            throw new CertificateException(e);
        }

        certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));

        // FIXME
        // certGen.addExtension(X509Extensions.CertificatePolicies.getId(),
        // false, policies);

    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // nothing
    }

}
