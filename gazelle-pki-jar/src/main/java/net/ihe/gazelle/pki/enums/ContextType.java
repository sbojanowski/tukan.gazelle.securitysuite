package net.ihe.gazelle.pki.enums;

import javax.faces.model.SelectItem;
import java.io.Serializable;

public enum ContextType implements Serializable {

    OTHER(0),
    EPSOS(1),
    IHE(2);

    private int value;

    private ContextType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    /**
     * Get the enumeration values formated as Items for jsf or rich components
     * @return the enumeration values formated as Items for jsf or rich components
     */
    public static SelectItem[] getSelectItems() {
        Object[] items = ContextType.values();
        SelectItem[] result = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            result[i] = new SelectItem(items[i], items[i].toString());
        }
        return result;
    }

}
