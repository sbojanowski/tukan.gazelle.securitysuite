package net.ihe.gazelle.pki;

import java.math.BigInteger;
import java.security.PublicKey;

import net.ihe.gazelle.pki.model.CertificateRequest;

import org.bouncycastle.jce.X509Principal;

public class X509CertificateParametersContainer {

    private CertificateRequest certificateRequest;
    private X509Principal issuerPrincipal;
    private PublicKey publicKey;
    private BigInteger serialNumber;
    private X509Principal subjectPrincipal;
    private int certificateId;

    public X509CertificateParametersContainer(CertificateRequest certificateRequest, X509Principal issuerPrincipal,
                                              PublicKey publicKey, BigInteger serialNumber, X509Principal subjectPrincipal, int certificateId) {
        super();
        this.certificateRequest = certificateRequest;
        this.issuerPrincipal = issuerPrincipal;
        this.publicKey = publicKey;
        this.serialNumber = serialNumber;
        this.subjectPrincipal = subjectPrincipal;
        this.certificateId = certificateId;
    }

    public CertificateRequest getCertificateRequest() {
        return certificateRequest;
    }

    public X509Principal getIssuerPrincipal() {
        return issuerPrincipal;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    public X509Principal getSubjectPrincipal() {
        return subjectPrincipal;
    }

    public int getCertificateId() {
        return certificateId;
    }

}
