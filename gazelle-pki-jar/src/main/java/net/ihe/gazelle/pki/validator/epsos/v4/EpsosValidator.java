package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.validator.AbstractCertificateValidator;
import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x509.qualified.QCStatement;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.*;
import java.util.*;

public abstract class EpsosValidator extends AbstractCertificateValidator {

    private static final KeyPurposeId[] KEY_PURPOSE_IDS = new KeyPurposeId[]{KeyPurposeId.anyExtendedKeyUsage,
            KeyPurposeId.id_kp_capwapAC, KeyPurposeId.id_kp_clientAuth, KeyPurposeId.id_kp_capwapWTP,
            KeyPurposeId.id_kp_codeSigning, KeyPurposeId.id_kp_dvcs, KeyPurposeId.id_kp_eapOverLAN,
            KeyPurposeId.id_kp_eapOverPPP, KeyPurposeId.id_kp_emailProtection, KeyPurposeId.id_kp_ipsecEndSystem,
            KeyPurposeId.id_kp_ipsecIKE, KeyPurposeId.id_kp_ipsecTunnel, KeyPurposeId.id_kp_ipsecUser,
            KeyPurposeId.id_kp_OCSPSigning, KeyPurposeId.id_kp_sbgpCertAAServerAuth, KeyPurposeId.id_kp_scvp_responder,
            KeyPurposeId.id_kp_scvpClient, KeyPurposeId.id_kp_scvpServer, KeyPurposeId.id_kp_serverAuth,
            KeyPurposeId.id_kp_smartcardlogon, KeyPurposeId.id_kp_timeStamping};

    private static final String[] KEY_PURPOSE_STRINGS = new String[]{"anyExtendedKeyUsage", "id_kp_capwapAC",
            "id_kp_clientAuth", "id_kp_capwapWTP", "id_kp_codeSigning", "id_kp_dvcs", "id_kp_eapOverLAN",
            "id_kp_eapOverPPP", "id_kp_emailProtection", "id_kp_ipsecEndSystem", "id_kp_ipsecIKE", "id_kp_ipsecTunnel",
            "id_kp_ipsecUser", "id_kp_OCSPSigning", "id_kp_sbgpCertAAServerAuth", "id_kp_scvp_responder",
            "id_kp_scvpClient", "id_kp_scvpServer", "id_kp_serverAuth", "id_kp_smartcardlogon", "id_kp_timeStamping"};

    public static final String CA_S_CRLS_VALIDATION = "CA's CRLs validation";
    public static final int MAX_BYTES_SIZE = 20;
    public static final String EHEALTH_X509_3_1 = "eHealth DSI - X.509 Certificates Profile - Chapter 3.1";
    public static final String EHEALTH_X509_3_2 = "eHealth DSI - X.509 Certificates Profile - Chapter 3.2";
    public static final int SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE = 64;

    private List<String> countries = Arrays.asList(Locale.getISOCountries());

    @SuppressWarnings("unchecked")
    protected final Vector<String> getSubjectVector(X509Name name, DERObjectIdentifier identifier) {
        return name.getValues(identifier);
    }

    protected String showFund(List<String> extendedKeyUsage) {
        StringBuilder sb = new StringBuilder(" Fund : {");
        for (int i = 0; i < extendedKeyUsage.size(); i++) {
            if (i != 0) {
                sb.append(", ");
            }
            String value = extendedKeyUsage.get(i);
            for (int j = 0; j < KEY_PURPOSE_IDS.length; j++) {
                KeyPurposeId keyPurpose = KEY_PURPOSE_IDS[j];
                if (keyPurpose.getId().equals(value)) {
                    value = KEY_PURPOSE_STRINGS[j];
                }
            }
            sb.append(value);
        }
        sb.append("}");
        return sb.toString();
    }

    private void validateCA(List<X509Certificate> list, List<CertificateException> exceptions,
                            List<CertificateException> warnings, boolean revocation) {

        if (revocation) {
            List<String> crlDistributionPointsCA = new ArrayList<String>();

            try {
                getCRLDistributionPoints(list.get(0), crlDistributionPointsCA);
                for (String location : crlDistributionPointsCA) {
                    try {
                        X509CRL crl = getCRL(location);
                        validateCRL(crl, list, exceptions, warnings);
                    } catch (CertificateException e) {
                        warnings.add(new CertificateNormException("Failed to check CRL of CA - " + location,
                                CA_S_CRLS_VALIDATION, e));
                    }
                }
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to check CRL of CA", CA_S_CRLS_VALIDATION, e));
            }
        }
    }

    private void validateCRL(X509CRL crl, List<X509Certificate> list, List<CertificateException> exceptions,
                             List<CertificateException> warnings) {

        if (crl.getVersion() != 2) {
            exceptions.add(new CertificateNormException(
                    "CRLs to be deployed MUST be v2", EHEALTH_X509_3_2));
        }

        String crlSubject = null;
        if (crl.getIssuerX500Principal() != null) {
            crlSubject = crl.getIssuerX500Principal().toString();
        }

        boolean checked = false;
        for (X509Certificate caCertificate : list) {
            String caSubject = null;
            if (caCertificate.getIssuerX500Principal() != null) {
                caSubject = caCertificate.getIssuerX500Principal().toString();
            }
            if (StringUtils.equals(crlSubject, caSubject)) {
                checked = true;
            }
        }
        if (!checked) {
            exceptions.add(new CertificateNormException(
                    "The DName MUST be identical to the subject DName of the issuer certificate.",
                    EHEALTH_X509_3_2));
        }

        if (crl.getThisUpdate() == null || crl.getNextUpdate() == null) {
            exceptions.add(new CertificateNormException("The CRL’s period of validity (from/to) MUST be stated.",
                    EHEALTH_X509_3_2));
        }

        byte[] declaredCaKeyIdentifier = null;
        byte[] authorityKeyIdentifierBytes = crl.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
        if (authorityKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        authorityKeyIdentifierBytes)).readObject());
                AuthorityKeyIdentifier authorityKeyIdentifier = new AuthorityKeyIdentifier(
                        (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject());

                declaredCaKeyIdentifier = authorityKeyIdentifier.getKeyIdentifier();
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                        CA_S_CRLS_VALIDATION, e));
            }
        }

        if (declaredCaKeyIdentifier == null) {
            exceptions
                    .add(new CertificateNormException("The KeyIdentifier MUST be indicated.", EHEALTH_X509_3_2));
        } else {
            checked = false;

            for (X509Certificate caCertificate : list) {
                byte[] caKeyIdentifier = null;

                byte[] subjectKeyIdentifierBytes = caCertificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier
                        .getId());
                if (subjectKeyIdentifierBytes != null) {
                    try {
                        DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                                subjectKeyIdentifierBytes)).readObject());
                        SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                                new ByteArrayInputStream(oct.getOctets())).readObject());
                        caKeyIdentifier = keyId.getKeyIdentifier();
                    } catch (Exception e) {
                        exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                                CA_S_CRLS_VALIDATION, e));
                    }
                }

                if (caKeyIdentifier == null) {
                    exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                            CA_S_CRLS_VALIDATION));
                } else {
                    if (Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                        checked = true;
                    }
                }
            }

            if (!checked) {
                exceptions.add(new CertificateNormException(
                        "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                        CA_S_CRLS_VALIDATION));
            }
        }

        byte[] crlNumberBytes = crl.getExtensionValue(X509Extensions.CRLNumber.getId());
        if (crlNumberBytes == null) {
            exceptions.add(new CertificateNormException("The consecutive number for the CRL issued MUST be indicated.",
                    EHEALTH_X509_3_2));
        } else {

            try {

                ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(crlNumberBytes));
                ASN1OctetString octs = (ASN1OctetString) aIn.readObject();
                aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
                DERObject obj = aIn.readObject();

                DERInteger crlnum = CRLNumber.getInstance(obj);
                BigInteger crlNumberBigInt = crlnum.getPositiveValue();

                byte[] byteArray = crlNumberBigInt.toByteArray();
                if (byteArray.length > MAX_BYTES_SIZE) {
                    exceptions.add(new CertificateNormException(
                            "It MUST have a unique positive integer value with a maximum length of 20 bytes.",
                            EHEALTH_X509_3_2));
                }

            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to load CRLNumber", CA_S_CRLS_VALIDATION, e));
            }
        }


        try {
            DERObject issuerAltNameObject = getExtensionValue(crl, X509Extensions.IssuerAlternativeName.getId());
            if (issuerAltNameObject != null) {
                //TODO
            } else {
                warnings.add(new CertificateNormException("\"IssuerAltNames\" MAY be an extension in the CRL.",
                        EHEALTH_X509_3_1));
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    private X509CRL getCRL(String location) throws CertificateException {
        X509CRL result = null;
        try {
            URL url = new URL(location);

            if (url.getProtocol().equals("http") || url.getProtocol().equals("https")) {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
                    result = (X509CRL) cf.generateCRL(conn.getInputStream());
                } else {
                    throw new Exception(conn.getResponseMessage());
                }
            }
        } catch (Exception e) {
            throw new CertificateNormException("Failed to retrieve CRL from " + location, "Validation of the CA CRLs", e);
        }
        return result;
    }

    @Override
    public void validate(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                         List<CertificateException> warnings, boolean revocation) {
        if (chain != null && chain.size() >= 2) {
            for (int i = 0; i < chain.size(); i++) {
                X509Certificate x509Certificate = chain.get(i);
                if (i == 0) {
                    boolean issuingCaSupportOcsp = false;
                    List<AccessDescription[]> accessDescriptionList = getAccessDescriptions(chain.get(1));
                    for (AccessDescription[] accessDescriptions : accessDescriptionList) {
                        if (accessDescriptions != null) {
                            for (AccessDescription accessDescription : accessDescriptions) {
                                if (accessDescription.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                    issuingCaSupportOcsp = true;
                                }
                            }
                        }
                    }
                    validate_2_2_2(x509Certificate, pkiValidation, warnings, issuingCaSupportOcsp);
                } else {
                    List<X509Certificate> subList = chain.subList(i, chain.size());
                    validateCA(subList, pkiValidation, warnings, revocation);
                }
            }
            validateIssuers(chain, pkiValidation, warnings);
        } else {
            // FIXME
            if (chain != null && chain.size() == 1) {
                validate_2_2_2(chain.get(0), pkiValidation, warnings, false);
                validateCA(chain, pkiValidation, warnings, revocation);
            }
            pkiValidation.add(new CertificateException("Invalid chain of certificates (at last 2 certificates)"));
        }
    }

    /**
     * Extract the value of the given extension, if it exists.
     *
     * @param ext The extension object.
     * @param oid The object identifier to obtain.
     * @return the extension as {@link DERObject}
     * @throws CertificateException if the extension cannot be read.
     */
    protected static DERObject getExtensionValue(java.security.cert.X509Extension ext, String oid)
            throws CertificateException {
        byte[] bytes = ext.getExtensionValue(oid);
        if (bytes == null) {
            return null;
        }

        return getObject(oid, bytes);
    }

    //X.509 Certificate Profiles v2.2.2 (eHealth DSI provider)
    private void validate_2_2_2(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                List<CertificateException> warnings, boolean issuingCaSupportOcsp) {

        // Signature alorithm
        // 5.1
        //FIXME missing signature algorithm validation

        // Serial number
        validateSerialNumber(x509Certificate, exceptions);

        // Validity from/to
        validateDate(x509Certificate, exceptions, warnings);

        // IssuerUniqueID
        boolean[] issuerUniqueID = x509Certificate.getIssuerUniqueID();
        if (issuerUniqueID != null && issuerUniqueID.length > 0) {
            // Not sure about max length...
            exceptions.add(new CertificateNormException("The field \"IssuerUniqueID\" MUST NOT be used.",
                    EHEALTH_X509_3_1));
        }

        // SubjectUniqueID
        boolean[] subjectUniqueID = x509Certificate.getSubjectUniqueID();
        if (subjectUniqueID != null && subjectUniqueID.length > 0) {
            // Not sure about max length...
            exceptions.add(new CertificateNormException("The field \"SubjectUniqueID\" MUST NOT be used.",
                    EHEALTH_X509_3_1));
        }

        // Subject
        X500Principal subject = x509Certificate.getSubjectX500Principal();

        validateSubject(subject.getName(), exceptions, warnings);


        Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
        Set<String> nonCriticalExtensionOIDs = x509Certificate.getNonCriticalExtensionOIDs();

        validateNonCritical("AuthorityKeyIdentifier", X509Extensions.AuthorityKeyIdentifier, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // checked in validateIssuers

        validateSubjectKeyIdentifier(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions,
                warnings);
        validateKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

        validateNonCritical("IssuerAlternativeName", X509Extensions.IssuerAlternativeName, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("SubjectAlternativeName", X509Extensions.SubjectAlternativeName, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        try {
            Collection<List<?>> subjectAltNames = x509Certificate.getSubjectAlternativeNames();
            boolean flagURI = false;
            boolean flagrfc822Name = false; //email
            if (subjectAltNames != null) {
                for (List subjectAltName : subjectAltNames) {
                    if ((Integer) subjectAltName.get(0) == GeneralName.rfc822Name) {
                        flagrfc822Name = true;
                    } else if ((Integer) subjectAltName.get(0) == GeneralName.uniformResourceIdentifier) {
                        if (subjectAltName.get(1) instanceof String) {
                            String uri = ((String) subjectAltName.get(1));
                            if (uri.startsWith("http://") || uri.startsWith("ldap://") || uri.startsWith("ftp://")) {
                                flagURI = true;
                            }
                        }
                    }
                }
                if (!flagURI) {
                    warnings.add(new CertificateNormException("If [SubjectAltNames] is used, a corresponding LDAP-, HTTP- or FTP-URL SHOULD be " +
                            "provided.",
                            EHEALTH_X509_3_1));
                }
                if (!flagrfc822Name) {
                    warnings.add(new CertificateNormException("If [SubjectAltNames] is used, E-Mail addresses (RFC822-name) [RFC 822] MAY also be " +
                            "made available.",
                            EHEALTH_X509_3_1));
                }
            }
        } catch (CertificateParsingException e) {
            e.printStackTrace();
        }

        validateCritical("BasicConstraints", X509Extensions.BasicConstraints, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);

        // checked in validateIssuers

        validateExtendedKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

        validateNonCritical("CRLDistributionPoints", X509Extensions.CRLDistributionPoints, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("CertificatePolicies", X509Extensions.CertificatePolicies, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        try {
            DERObject certificatePoliciesObject = getExtensionValue(x509Certificate, X509Extensions.CertificatePolicies.getId());
            if (certificatePoliciesObject != null) {
                if (certificatePoliciesObject instanceof DERObject) {
                    DERSequence seq = (DERSequence) certificatePoliciesObject;
                    boolean aresPresent = false;
                    for (int pos = 0; pos < seq.size(); pos++) {
                        PolicyInformation policyInformation = new PolicyInformation((ASN1Sequence) seq.getObjectAt(pos));
                        String policyIdentifier = policyInformation.getPolicyIdentifier().getId();
                        if (policyIdentifier.startsWith("1.3.130.0.2017.")) {
                            aresPresent = true;
                        }
                    }
                    if (!aresPresent) {
                        warnings.add(new CertificateNormException("CertificatePolicies SHOULD be included as an extension in the certificate to " +
                                "include the eHealth DSI certificate policy identifier: 1.3.130.0.2017.ARES_number_of_the_present_document",
                                EHEALTH_X509_3_1));
                    }
                }
            }
        } catch (CertificateException e) {
            exceptions.add(e);
        }

        validateNonCritical("AuthorityInfoAccess", X509Extensions.AuthorityInfoAccess, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        // checked in validateIssuers
        /*
        try {
            DERObject authorityInfoAccesObject = getExtensionValue(x509Certificate, X509Extensions.AuthorityInfoAccess.getId());
        } catch (CertificateException e) {
            exceptions.add(e);
        }*/
        byte[] crlDistributionPoints = x509Certificate.getExtensionValue(X509Extensions.CRLDistributionPoints.getId());

        byte[] authorityInfoAccess = x509Certificate.getExtensionValue(X509Extensions.AuthorityInfoAccess.getId());
        boolean crlDistributionPointsNull = false;
        if (crlDistributionPoints == null || crlDistributionPoints.length == 0) {
            crlDistributionPointsNull = true;
        }


        try {
            if (!crlDistributionPointsNull) {
                List<String> crlDistributionPointList = new ArrayList<String>();
                getCRLDistributionPoints(x509Certificate, crlDistributionPointList);
                boolean crlUriPresent = false;
                if (crlDistributionPointList != null) {
                    for (String crlDistributionPoint : crlDistributionPointList) {
                        if (crlDistributionPoint.startsWith("http://") || crlDistributionPoint.startsWith("ldap://")) {
                            crlUriPresent = true;
                        }
                    }
                }
                if (!crlUriPresent) {
                    exceptions.add(new CertificateNormException(
                            "In the CRL Distribution Points, at least one of the present references MUST use either http (http://) [RFC 7230-7235] " +
                                    "or ldap (ldap://) [RFC 4516] scheme.", EHEALTH_X509_3_1));
                }
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        boolean ocspPresent = false;
        boolean accessLocationForOcspPresent = false;
        List<AccessDescription[]> accessDescriptionList = getAccessDescriptions(x509Certificate);
        for (AccessDescription[] accessDescriptions : accessDescriptionList) {
            for (AccessDescription accessDescription : accessDescriptions) {
                if (accessDescription.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                    ocspPresent = true;
                }
                if (issuingCaSupportOcsp && ocspPresent && accessDescription.getAccessLocation() != null) {
                    accessLocationForOcspPresent = true;
                }
            }
        }
        if (!accessLocationForOcspPresent) {
            exceptions.add(new CertificateNormException(
                    "When OCSP is supported by the issuing CA, the Authority Information Access extension MUST include an accessMethod OID, " +
                            "id-ad-ocsp, with an accessLocation value specifying at least one access location of an OCSP [RFC 6960] responder " +
                            "authoritative to provide certificate status information for the present certificate.", EHEALTH_X509_3_1));
        }

        if (crlDistributionPointsNull && !ocspPresent) {
            exceptions.add(new CertificateNormException(
                    "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the " +
                            "certificate MUST include a CRL distribution point extension.", EHEALTH_X509_3_1));
            exceptions.add(new CertificateNormException(
                    "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                            "extension.", EHEALTH_X509_3_1));
        }

        validateQCStatementUsages(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

    }

    private List<AccessDescription[]> getAccessDescriptions(X509Certificate x509Certificate) {
        List<AccessDescription[]> accessDescriptionsList = new ArrayList<>();
        try {
            DERObject authorityInfoAccessObject = getExtensionValue(x509Certificate, X509Extensions.AuthorityInfoAccess.getId());
            if (authorityInfoAccessObject != null) {
                if (authorityInfoAccessObject instanceof DERSequence) {
                    DERSequence seq = (DERSequence) authorityInfoAccessObject;
                    for (int pos = 0; pos < seq.size(); pos++) {
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess((ASN1Sequence) seq.getObjectAt(pos));
                        accessDescriptionsList.add(authorityInformationAccess.getAccessDescriptions());
                    }
                }
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return accessDescriptionsList;
    }

    private void validateCritical(String string, DERObjectIdentifier objectIdentifier,
                                  Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs, boolean should,
                                  List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs == null || !criticalExtensionOIDs.contains(objectIdentifier.getId())) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD be included as a critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            }
        }

        if (nonCriticalExtensionOIDs != null && nonCriticalExtensionOIDs.contains(objectIdentifier.getId())) {
            exceptions.add(new CertificateNormException(string
                    + " MUST always be designated as critical in the certificate (fund it as non critical).",
                    EHEALTH_X509_3_1));
        }
    }

    private void validateNonCritical(String string, DERObjectIdentifier objectIdentifier,
                                     Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs, boolean should,
                                     List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs != null && criticalExtensionOIDs.contains(objectIdentifier.getId())) {
            exceptions.add(new CertificateNormException(string
                    + " MUST always be designated as non-critical in the certificate (fund it as critical).",
                    EHEALTH_X509_3_1));
        }
        if (nonCriticalExtensionOIDs == null || !nonCriticalExtensionOIDs.contains(objectIdentifier.getId())) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a non-critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD be included as a non-critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            }
        }
    }

    private void validateDate(X509Certificate x509Certificate, List<CertificateException> exceptions,
                              List<CertificateException> warnings) {
        Date notAfter = x509Certificate.getNotAfter();
        Date notBefore = x509Certificate.getNotBefore();
        Calendar cl = Calendar.getInstance();
        cl.setTime(notBefore);
        cl.add(Calendar.YEAR, 2);
        if (notAfter.after(cl.getTime())) {
            warnings.add(new CertificateNormException(
                    "Certificates used by epSOS services SHOULD be valid for a maximum of 2 year.",
                    EHEALTH_X509_3_1));
        }
    }

    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        validateNonCritical("ExtendedKeyUsage", X509Extensions.ExtendedKeyUsage, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        List<String> extendedKeyUsages;
        try {
            extendedKeyUsages = x509Certificate.getExtendedKeyUsage();
        } catch (CertificateParsingException e) {
            exceptions.add(e);
            return;
        }
        if (extendedKeyUsages != null) {
            validateExtendedKeyUsages(extendedKeyUsages, exceptions);
        }
    }

    protected void validateExtendedKeyUsages(List<String> extendedKeyUsages, List<CertificateException> exceptions) {
        // KeyPurposeId.
    }

    protected void validateQCStatementUsages(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                             Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                             List<CertificateException> warnings) {
        try {
            DERObject qcStatementObject = getExtensionValue(x509Certificate, X509Extensions.QCStatements.getId());
            if (qcStatementObject != null) {
                if (qcStatementObject instanceof ASN1Sequence) {
                    ASN1Sequence seq = (ASN1Sequence) qcStatementObject;
                    QCStatement qcStatement = new QCStatement(seq);
                    validateQCStatementUsages(qcStatement, exceptions);
                }
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    protected void validateQCStatementUsages(QCStatement qcStatement, List<CertificateException> exceptions) {
    }

    protected final void validateExtendedKeyUsagesOneValue(List<String> extendedKeyUsage, String string,
                                                           KeyPurposeId keyPurposeId, List<CertificateException> exceptions, String chapter) {
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            if (extendedKeyUsage.size() > 1) {
                exceptions.add(new CertificateNormException(
                        "If ExtendedKeyUsages extension is included in the certificate, "
                                + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                        "epSOS WP 3.4.2 - Chapter " + chapter));
            } else {
                if (!extendedKeyUsage.get(0).equals(keyPurposeId.getId())) {
                    exceptions.add(new CertificateNormException(
                            "If ExtendedKeyUsages extension is included in the certificate, "
                                    + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                            "epSOS WP 3.4.2 - Chapter " + chapter));
                }
            }
        }
    }

    private void validateIssuers(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                                 List<CertificateException> warnings) {
        X509Certificate endCert = chain.get(0);
        X509Certificate caCert = chain.get(1);

        // 5.4.2 AuthorityKeyIdentifier

        byte[] caKeyIdentifier = null;

        byte[] subjectKeyIdentifierBytes = caCert.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        if (subjectKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        subjectKeyIdentifierBytes)).readObject());
                SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                        new ByteArrayInputStream(oct.getOctets())).readObject());

                caKeyIdentifier = keyId.getKeyIdentifier();
            } catch (Exception e) {
                pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                        EHEALTH_X509_3_1, e));
            }
        }

        if (caKeyIdentifier == null) {
            pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                    EHEALTH_X509_3_1));
        } else {
            byte[] declaredCaKeyIdentifier = null;

            byte[] authorityKeyIdentifierBytes = endCert.getExtensionValue(X509Extensions.AuthorityKeyIdentifier
                    .getId());
            if (authorityKeyIdentifierBytes != null) {
                try {
                    DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                            authorityKeyIdentifierBytes)).readObject());
                    AuthorityKeyIdentifier authorityKeyIdentifier = new AuthorityKeyIdentifier(
                            (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject());

                    declaredCaKeyIdentifier = authorityKeyIdentifier.getKeyIdentifier();

                    if (authorityKeyIdentifier.getAuthorityCertIssuer() != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertIssuer SHOULD NOT be used in AuthorityKeyIdentifier.",
                                EHEALTH_X509_3_1));
                    }
                    if (authorityKeyIdentifier.getAuthorityCertSerialNumber() != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertSerialNumber SHOULD NOT be used in AuthorityKeyIdentifier.",
                                EHEALTH_X509_3_1));
                    }

                } catch (Exception e) {
                    pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                            EHEALTH_X509_3_1, e));
                }
            }

            if (declaredCaKeyIdentifier == null) {
                pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier of end cert",
                        EHEALTH_X509_3_1));
            } else {
                if (!Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                    pkiValidation.add(new CertificateNormException(
                            "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                            EHEALTH_X509_3_1));
                }
            }
        }

        List<String> crlDistributionPointsEndCert = new ArrayList<String>();
        List<String> crlDistributionPointsCA = new ArrayList<String>();

        try {
            getCRLDistributionPoints(endCert, crlDistributionPointsEndCert);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of end certificate",
                    EHEALTH_X509_3_1, e));
        }
        try {
            getCRLDistributionPoints(caCert, crlDistributionPointsCA);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of CA certificate",
                    EHEALTH_X509_3_1, e));
        }
        if (crlDistributionPointsEndCert.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for end certificate",
                    EHEALTH_X509_3_1));
        }
        if (crlDistributionPointsCA.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for CA certificate",
                    EHEALTH_X509_3_1));
        }
        if (crlDistributionPointsEndCert.size() == 1 &&
                crlDistributionPointsCA.size() == 1 &&
                !StringUtils.equals(crlDistributionPointsEndCert.get(0), crlDistributionPointsCA.get(0))) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD include the HTTP address from "
                    + "which the certificate-issuing authority’s complete revocation list can be retrieved.",
                    EHEALTH_X509_3_1));
        }

        String ocspEndCert0 = null;
        try {
            ocspEndCert0 = getOCSPFromAuthorityInfoAccess(endCert);
            if (ocspEndCert0 != null) {
                log.info(ocspEndCert0);
                URL url = new URL(ocspEndCert0);
                String protocol = url.getProtocol().toLowerCase();
                if (!StringUtils.equals(protocol, "http") && !StringUtils.equals(protocol, "https")) {
                    log.info(ocspEndCert0);
                    pkiValidation.add(new CertificateNormException("When the issuing CA offers an OCSP service, "
                            + "its HTTP URI MUST be included in the AuthorityInfoAccess extension. " + ocspEndCert0,
                            EHEALTH_X509_3_1));
                }
            }
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check AuthorityInfoAccess of end certificate",
                    EHEALTH_X509_3_1, e));
        }

    }

    private String getOCSPFromAuthorityInfoAccess(X509Certificate cert) throws CertificateException {
        DERObject authInfoAcc = getExtensionValue(cert, X509Extensions.AuthorityInfoAccess.getId());
        if (authInfoAcc != null) {
            if (authInfoAcc instanceof ASN1Sequence) {
                ASN1Sequence seq = (ASN1Sequence) authInfoAcc;
                Enumeration e = seq.getObjects();
                while (e.hasMoreElements()) {
                    Object nextElement = e.nextElement();
                    try {
                        AuthorityInformationAccess authInfoAccess = AuthorityInformationAccess.getInstance(nextElement);
                        if (authInfoAccess != null) {
                            AccessDescription[] ads = authInfoAccess.getAccessDescriptions();
                            for (int i = 0; i < ads.length; i++) {
                                if (ads[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                    GeneralName name = ads[i].getAccessLocation();
                                    if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                        return ((DERIA5String) name.getName()).getString();
                                    }
                                }
                            }
                        }
                    } catch (IllegalArgumentException iae) {
                        AccessDescription ad = AccessDescription.getInstance(nextElement);
                        if (ad != null && ad.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                            GeneralName name = ad.getAccessLocation();
                            if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                return ((DERIA5String) name.getName()).getString();
                            }
                        }
                    }
                }
            } else {
                throw new CertificateNormException("Failed to load CertificatePolicies",
                        EHEALTH_X509_3_1);
            }
        }
        return null;
    }

    private void getCRLDistributionPoints(X509Certificate endCert, List<String> crlDistributionPointsEndCert)
            throws CertificateException {
        DERObject crlDPDERObject = getExtensionValue(endCert, X509Extensions.CRLDistributionPoints.getId());
        if (crlDPDERObject != null) {
            CRLDistPoint crlDistPoints = CRLDistPoint.getInstance(crlDPDERObject);
            if (crlDistPoints != null) {
                DistributionPoint[] distPoints = crlDistPoints.getDistributionPoints();
                for (int i = 0; i < distPoints.length; i++) {
                    DistributionPointName distributionPointName = distPoints[i].getDistributionPoint();
                    if (distributionPointName.getType() == DistributionPointName.FULL_NAME) {
                        GeneralName[] generalNames = GeneralNames.getInstance(distributionPointName.getName()).getNames();
                        for (int j = 0; j < generalNames.length; j++) {
                            if (generalNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                                String url = ((DERIA5String) generalNames[j].getName()).getString();
                                crlDistributionPointsEndCert.add(url);
                            }
                        }
                    }
                }
            }
        }
    }

    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
    }

    private void validateKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                  Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                  List<CertificateException> warnings) {
        validateCritical("KeyUsage", X509Extensions.KeyUsage, criticalExtensionOIDs, nonCriticalExtensionOIDs, false,
                exceptions, warnings);
        byte[] keyUsageBytes = x509Certificate.getExtensionValue(X509Extensions.KeyUsage.getId());
        if (keyUsageBytes == null) {
            exceptions.add(new CertificateNormException("KeyUsage is null", EHEALTH_X509_3_1));
        } else {
            ASN1Object obj;
            try {
                obj = X509ExtensionUtil.fromExtensionValue(keyUsageBytes);
            } catch (IOException e) {
                exceptions.add(new CertificateNormException(e, EHEALTH_X509_3_1));
                return;
            }
            DERBitString keyUsage = KeyUsage.getInstance(obj);
            int bits = keyUsage.getBytes()[0] & 0xff;
            validateKeyUsage(bits, exceptions);
        }
    }

    protected final void validateKeyUsageValue(int bits, int keyUsageBits, String error,
                                               List<CertificateException> exceptions, String chapter) {
        if (bits != keyUsageBits) {
            exceptions.add(new CertificateNormException(error + keyUsageToString(bits), chapter));
        }
    }

    protected final void validateKeyUsageValue(int bits, int[] keyUsageBitsTab, String error,
                                               List<CertificateException> exceptions, String chapter) {
        boolean flag = false;
        for (int keyUsageBits : keyUsageBitsTab) {
            if (bits == keyUsageBits) {
                flag = true;
                break;
            }
        }

        if (flag == false) {
            exceptions.add(new CertificateNormException(error + keyUsageToString(bits), chapter));
        }
    }

    private String keyUsageToString(int bits) {
        List<String> usages = new ArrayList<String>();
        if ((bits & KeyUsage.digitalSignature) == KeyUsage.digitalSignature) {
            usages.add("digitalSignature");
        }
        if ((bits & KeyUsage.nonRepudiation) == KeyUsage.nonRepudiation) {
            usages.add("nonRepudiation");
        }
        if ((bits & KeyUsage.keyEncipherment) == KeyUsage.keyEncipherment) {
            usages.add("keyEncipherment");
        }
        if ((bits & KeyUsage.dataEncipherment) == KeyUsage.dataEncipherment) {
            usages.add("dataEncipherment");
        }
        if ((bits & KeyUsage.keyAgreement) == KeyUsage.keyAgreement) {
            usages.add("keyAgreement");
        }
        if ((bits & KeyUsage.keyCertSign) == KeyUsage.keyCertSign) {
            usages.add("keyCertSign");
        }
        if ((bits & KeyUsage.cRLSign) == KeyUsage.cRLSign) {
            usages.add("cRLSign");
        }
        if ((bits & KeyUsage.encipherOnly) == KeyUsage.encipherOnly) {
            usages.add("encipherOnly");
        }
        if ((bits & KeyUsage.decipherOnly) == KeyUsage.decipherOnly) {
            usages.add("decipherOnly");
        }
        return showFund(usages);
    }

    private void validateSerialNumber(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        byte[] byteArray = x509Certificate.getSerialNumber().toByteArray();
        if (byteArray.length > MAX_BYTES_SIZE) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous integer value with a maximum of 20 bytes.",
                    EHEALTH_X509_3_1));
        }
        int compareTo = x509Certificate.getSerialNumber().compareTo(BigInteger.valueOf(0));
        if (compareTo < 0) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous positive integer value.",
                    EHEALTH_X509_3_1));
        }
    }

    public void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings) {
        X509Name x509name;
        try {
            x509name = new X509Name(name);
        } catch (IllegalArgumentException e) {
            exceptions.add(new CertificateNormException(e, EHEALTH_X509_3_1));
            return;
        }

        validateSubjectCountry(x509name, exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.O, SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, false, "O", exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.CN, SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, false, "CN", exceptions, warnings);

        validateSubjectAttribute(x509name, X509Name.OU, SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, true, "OU", exceptions, warnings);

        Vector<DERObjectIdentifier> oids = new Vector<DERObjectIdentifier>();
        oids.add(X509Name.DN_QUALIFIER);
        oids.add(X509Name.C);
        oids.add(X509Name.O);
        oids.add(X509Name.CN);
        oids.add(X509Name.OU);
        Vector<DERObjectIdentifier> oiDs = x509name.getOIDs();

        for (DERObjectIdentifier oid : oiDs) {
            if (!oids.contains(oid)) {
                String oidName = (String) X509Name.DefaultSymbols.get(oid);
                if (oidName == null) {
                    oidName = oid.toString();
                }
                warnings.add(new CertificateNormException(oidName + " SHOULD NOT be provided",
                        EHEALTH_X509_3_1));
            }
        }
        if (!oiDs.contains(X509Name.OU)) {
            warnings.add(new CertificateNormException("The DName MAY have OU.", EHEALTH_X509_3_1));
        }
    }

    private String validateSubjectAttribute(X509Name name, DERObjectIdentifier identifier, int length,
                                            boolean maxBytes, boolean should, String identifierName, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        Vector<String> vector = getSubjectVector(name, identifier);

        if (should) {
            if (vector == null || vector.size() == 0) {
                warnings.add(new CertificateNormException("The DName SHOULD have " + identifierName + ".",
                        EHEALTH_X509_3_1));
                return null;
            }
        } else {
            if (vector == null || vector.size() == 0) {
                exceptions.add(new CertificateNormException("The DName MUST have " + identifierName + ".",
                        EHEALTH_X509_3_1));
                return null;
            }
        }

        if (vector.size() != 1) {
            exceptions.add(new CertificateNormException("The DName MUST have only one " + identifierName + ".",
                    EHEALTH_X509_3_1));
        }
        String value = vector.get(0);

        if (maxBytes) {
            if (value == null || value.length() > length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", EHEALTH_X509_3_1));
            }
        } else {
            if (value == null || value.length() != length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", EHEALTH_X509_3_1));
            }
        }
        return value;
    }

    private void validateSubjectCountry(X509Name name, List<CertificateException> exceptions,
                                        List<CertificateException> warnings) {
        String country = validateSubjectAttribute(name, X509Name.C, 2, false, false, "C", exceptions, warnings);
        int indexOfCountry = countries.indexOf(country);
        if (indexOfCountry == -1) {
            exceptions.add(new CertificateNormException("C MUST be a ISO 3166 code", EHEALTH_X509_3_1));
        }
    }

    private void validateSubjectKeyIdentifier(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                              Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                              List<CertificateException> warnings) {
        byte[] value = x509Certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        if (value == null || value.length == 0) {
            exceptions.add(new CertificateNormException(
                    "SubjectKeyIdentifier MUST be included as an extension in the certificate.",
                    EHEALTH_X509_3_1));
        }
        validateNonCritical("SubjectKeyIdentifier", X509Extensions.SubjectKeyIdentifier, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.
        try {
            SubjectKeyIdentifierStructure subjectKeyIdentifierStructure = new SubjectKeyIdentifierStructure(
                    x509Certificate.getPublicKey());
            byte[] goodKeyIdentifier = subjectKeyIdentifierStructure.getKeyIdentifier();

            byte[] subjectKeyIdentifierBytes = x509Certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier
                    .getId());
            byte[] providedKeyIdentifier = null;
            if (subjectKeyIdentifierBytes != null) {
                try {
                    DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                            subjectKeyIdentifierBytes)).readObject());
                    SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                            new ByteArrayInputStream(oct.getOctets())).readObject());

                    providedKeyIdentifier = keyId.getKeyIdentifier();
                } catch (Exception e) {
                    exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                            EHEALTH_X509_3_1, e));
                }
            }
            if (providedKeyIdentifier != null && !Arrays.equals(providedKeyIdentifier, goodKeyIdentifier)) {
                exceptions.add(new CertificateNormException(
                        "One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.",
                        EHEALTH_X509_3_1));
            }
        } catch (CertificateParsingException e) {
            exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                    EHEALTH_X509_3_1));
        }
    }
}
