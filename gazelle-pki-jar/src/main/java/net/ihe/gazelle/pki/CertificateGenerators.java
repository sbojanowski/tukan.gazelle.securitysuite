package net.ihe.gazelle.pki;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.TimeZone;

import javax.persistence.EntityManager;

import net.ihe.gazelle.pki.model.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.RevokedReason;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;

/**
 * Used to quickly generate testing certificates (valid, expired, revoked, unknown, etc...)
 *
 * @author cel
 */
public abstract class CertificateGenerators {

    private static final KeyAlgorithm KEY_ALGORITHM = KeyAlgorithm.RSA;
    private static final int KEY_LENGTH = 1024;

    public interface CertificateGenerator {

        String getSubject();

        void generate(String commonName, String organization, String country, String requester, String casKey,
                      Certificate cACertificate, EntityManager entityManager) throws CertificateException;
    }

    public static class ValidCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "valid";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {
            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);
            entityManager.flush();
        }

    }

    public static class ExpiredCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "expired";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);
            // change period validity
            Calendar cl = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cl.add(Calendar.YEAR, -2);
            request.setNotAfter(cl.getTime());
            cl.add(Calendar.YEAR, 1);
            request.setNotBefore(cl.getTime());
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);
            entityManager.flush();
        }

    }

    public static class RevokedCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "revoked";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);
            certificate.revoke(RevokedReason.UNSPECIFIED);
            entityManager.merge(certificate);
            entityManager.flush();
        }

    }

    /**
     * @author cel Generate a certificate chained to an unknown CA.
     */
    public static class UnknownCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "unknown";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            // Create unknown CA
            CertificateRequestAuthority cARequest = generateSelfSignedCACertificateRequest(commonName + " " + getSubject(),
                    organization, country, requester, casKey);
            entityManager.persist(cARequest);
            cACertificate = CertificateManager.createCertificateAuthority(cARequest, entityManager);
            cACertificate.setCustom(true);

            // create certificate chained to the unknown CA.
            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);
            entityManager.flush();

        }

    }

    public static class CorruptedCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "corrupted";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);

            // get the newly created certificate
            CertificateX509 certX509 = certificate.getCertificateX509();

            // retrieve certificate in hexadecimal
            byte[] enc = certX509.getEnc();

            // delete the right certificate
            certX509.setX509Certificate(null);

            // corrupt certificate (in hexadecimal)
            enc[enc.length - 7] = (byte) (enc[enc.length - 7] + 1);

            // replace by the corrupted (the certificate in X509 format will be generated at next call of getX509Certificate())
            certX509.setEnc(enc);

            // save
            entityManager.merge(certificate);
            entityManager.flush();

        }

    }

    public static class WrongKeyCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "wrongKey";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, cACertificate);

            entityManager.persist(request);

            Certificate certificate = CertificateManager.createCertificate(request, entityManager);
            certificate.setCustom(true);
            certificate = mixKeysOfCertificate(certificate, KEY_ALGORITHM, KEY_LENGTH);

            entityManager.merge(certificate);
            entityManager.flush();

        }

    }

    public static class SelfSignedCertificateGenerator implements CertificateGenerator {

        @Override
        public String getSubject() {
            return "self-signed";
        }

        @Override
        public void generate(String commonName, String organization, String country, String requester, String casKey,
                             Certificate cACertificate, EntityManager entityManager) throws CertificateException {

            CertificateRequest request = generateCertificateRequest(commonName + " " + getSubject(), organization,
                    country, requester, casKey, null);
            entityManager.persist(request);
            Certificate certificate = CertificateManager.createSelfSignedCertificate(request, entityManager);
            certificate.setCustom(true);
            entityManager.merge(certificate);
            entityManager.flush();

        }

    }

    private static CertificateRequest generateCertificateRequest(String commonName, String organization,
                                                                 String country, String requester, String casKey,
                                                                 Certificate cACertificate)
            throws CertificateException {

        CertificateRequest request = new CertificateRequestWithGeneratedKeys(KEY_ALGORITHM, KEY_LENGTH);

        request.setSubjectUsingAttributes(country, organization, commonName, null, null, null, null, null);
        request.setCertificateAuthority(cACertificate);
        request.setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        request.setNotAfter(CertificateUtil.getNotAfter());
        request.setNotBefore(CertificateUtil.getNotBefore());
        request.setRequester(requester, casKey);

        return request;

    }

    private static CertificateRequestAuthority generateSelfSignedCACertificateRequest(String commonName,
                                                                                      String organization,
                                                                                      String country, String requester,
                                                                                      String casKey)
            throws CertificateException {

        CertificateRequestAuthority request = new CertificateRequestAuthority(KEY_ALGORITHM, KEY_LENGTH);

        request.setSubjectUsingAttributes(country, organization, commonName + " CA", null, null, null, null, null);
        request.setIssuer(request.getSubject());
        request.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        request.setCertificateVersion(CertificateVersion.V3);
        request.setNotAfter(CertificateUtil.getNotAfter());
        request.setNotBefore(CertificateUtil.getNotBefore());
        request.setRequester(requester, casKey);

        return request;

    }

    private static Certificate mixKeysOfCertificate(Certificate certificate, KeyAlgorithm keyAlgorithm, int keySize)
            throws CertificateException {
        KeyPair keyPair;
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance(keyAlgorithm.getKeySpec(),
                    BouncyCastleProvider.PROVIDER_NAME);
            keygen.initialize(keySize);
            keyPair = keygen.generateKeyPair();

            certificate.setPrivateKey(new CertificatePrivateKey(keyPair.getPrivate()));

            return certificate;

        } catch (NoSuchProviderException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        }
    }

}
