package net.ihe.gazelle.pki.validator.epsos.v1;

public class EpsosServiceProviderValidator extends EpsosServerValidator {

    @Override
    protected String getCertificateType() {
        return "SSL server";
    }

}
