package net.ihe.gazelle.pki;

import net.ihe.gazelle.preferences.PreferenceService;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.security.cert.CertificateEncodingException;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public final class CertificateUtil {

    private static Logger log = LoggerFactory.getLogger(CertificateUtil.class);

    private static final int VALIDITY_MINUTES_OFFSET = -10;
    private static final int VALIDITY_YEARS_OFFSET = 10;
    private static final int WRAPPER_SCREEN_WIDTH = 80;

    private static final int CA_KEY_EXTENDER = 0x6;
    private static final int CLIENT_KEY_EXTENDER = 0xe0;
    private static final int HTTP_SERVER_KEY_EXTENDER = 0xf0;

    private CertificateUtil() {
        super();
    }

    public static Session getSession(EntityManager entityManager) {
        if (entityManager != null) {
            Object delegate = entityManager.getDelegate();
            if (delegate instanceof Session) {
                return (Session) delegate;
            }
        }
        return null;
    }

    public static Date getNotBefore() {
        Calendar cl = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cl.add(Calendar.MINUTE, VALIDITY_MINUTES_OFFSET);
        return cl.getTime();
    }

    public static Date getNotAfter() {
        Calendar cl = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cl.add(Calendar.YEAR, VALIDITY_YEARS_OFFSET);
        cl.add(Calendar.MINUTE, VALIDITY_MINUTES_OFFSET);
        return cl.getTime();
    }

    public static List<X509Certificate> getJavaCAs() throws CertificateException {
        List<X509Certificate> trusted = new ArrayList<X509Certificate>();

        KeyStore caCerts = getLoadedCacertsTrustStore();
        try {
            for (Enumeration<String> aliases = caCerts.aliases(); aliases.hasMoreElements(); ) {
                String alias = aliases.nextElement();
                Certificate certificate = caCerts.getCertificate(alias);
                if (certificate instanceof X509Certificate) {
                    X509Certificate x509Certificate = (X509Certificate) certificate;
                    trusted.add(x509Certificate);
                }
            }
        } catch (KeyStoreException e) {
            logErrorAndThrowCertificateException("Java cacerts truststore is not loaded.", e);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
        return trusted;
    }

    private static KeyStore getLoadedCacertsTrustStore() throws CertificateException {
        KeyStore caCerts = getNewTrustore();
        String path = System.getProperty("java.home") + "/lib/security/cacerts";
        FileInputStream fis = null;
        try {
            try {
                fis = new FileInputStream(path);
            } catch (FileNotFoundException e) {
                logErrorAndThrowCertificateException("Unable to find java cacerts truststore at " + path, e);
            } catch (SecurityException e) {
                logErrorAndThrowCertificateException("Missing rights to access java cacerts truststore at " + path, e);
            }
            caCerts.load(fis, PreferenceService.getString("java_cacerts_truststore_pwd").toCharArray());
        } catch (IOException e) {
            if (((Exception) e.getCause()).getClass().equals(UnrecoverableKeyException.class)) {
                logErrorAndThrowCertificateException("Wrong password provided for java cacerts truststore",
                        (Exception) e.getCause());
            } else {
                logErrorAndThrowCertificateException("Error on java cacerts truststore format", e);
            }
        } catch (NoSuchAlgorithmException e) {
            logErrorAndThrowCertificateException(
                    "The algorithm used to check the integrity of the keystore cannot be found", e);
        } catch (CertificateException e) {
            logErrorAndThrowCertificateException("None of the certificates in the keystore can be loaded", e);
        } finally {
            closeQuietly(fis);
        }
        return caCerts;
    }

    private static KeyStore getNewTrustore() throws CertificateException {
        String msg;
        KeyStore truststore = null;
        try {
            truststore = KeyStore.getInstance("JKS");
        } catch (KeyStoreException e) {
            msg = "no Provider supports a KeyStoreSpi implementation for the 'JKS' type.";
            log.error(msg);
            throw new CertificateException(msg, e);
        }
        return truststore;
    }

    private static void logErrorAndThrowCertificateException(String msg, Exception error) throws CertificateException {
        log.error(msg);
        throw new CertificateException(msg, error);
    }


    public static String getChain(List<X509Certificate> chain) {
        StringBuilder sb = new StringBuilder();
        sb.append("Chain length   : ").append(chain.size()).append(CertificateConstants.NEW_LINE);
        int i = 0;
        for (X509Certificate certificate : chain) {
            if (certificate == null) {
                sb.append("Chain[").append(i).append("] : null");
            } else {
                sb.append("Chain[").append(i).append("] : ").append(CertificateConstants.NEW_LINE);
                sb.append("************").append(CertificateConstants.NEW_LINE);
                sb.append(certificate.toString());
                sb.append("************").append(CertificateConstants.NEW_LINE);
            }
            i++;
        }
        return sb.toString();
    }

    public static List<X509Certificate> loadCertificates(String certificatesInPEMFormat) throws CertificateException {
        List<X509Certificate> certificates = new ArrayList<X509Certificate>();

        if (certificatesInPEMFormat != null) {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(
                    certificatesInPEMFormat.getBytes(CertificateConstants.UTF_8));
            Reader reader = new InputStreamReader(inputStream, CertificateConstants.UTF_8);
            PEMReader pemReader = new PEMReader(reader);

            try {
                Object readObject = pemReader.readObject();
                while (readObject != null) {
                    if (readObject instanceof X509Certificate) {
                        certificates.add((X509Certificate) readObject);
                    }
                    readObject = pemReader.readObject();
                }
            } catch (Exception e) {
                throw new CertificateException(e);
            } finally {
                try {
                    pemReader.close();
                } catch (IOException e) {
                    throw new CertificateException(e);
                }
            }
        }

        if (certificates.size() == 0) {
            throw new CertificateException("No certificate found");
        }
        return certificates;
    }

    /**
     * Checks whether given X.509 certificate is self-signed.
     *
     * @param cert {@link X509Certificate} to verify
     * @return true if the certificate is self-signed, false otherwise
     */
    public static boolean isSelfSigned(X509Certificate cert) {
        try {
            // Try to verify certificate signature with its own public key
            PublicKey key = cert.getPublicKey();
            cert.verify(key);
            return true;
        } catch (SignatureException sigEx) {
            // Invalid signature --> not self-signed
            return false;
        } catch (InvalidKeyException keyEx) {
            // Invalid key --> not self-signed
            return false;
        } catch (CertificateException e) {
            return false;
        } catch (NoSuchAlgorithmException e) {
            return false;
        } catch (NoSuchProviderException e) {
            return false;
        }
    }

    public static void main(String[] args) {

        printKeyUsage("CertificateExtenderCA", CA_KEY_EXTENDER);
        printKeyUsage("CertificateExtenderClient", CLIENT_KEY_EXTENDER);
        printKeyUsage("CertificateExtenderHttpServer", HTTP_SERVER_KEY_EXTENDER);

    }

    public static void printKeyUsage(String clas, int keyUsage) {
        log.info("*** " + clas);
        testUsage(keyUsage, KeyUsage.cRLSign, "CRLSign");
        testUsage(keyUsage, KeyUsage.dataEncipherment, "dataEncipherment");
        testUsage(keyUsage, KeyUsage.decipherOnly, "decipherOnly");
        testUsage(keyUsage, KeyUsage.digitalSignature, "digitalSignature");
        testUsage(keyUsage, KeyUsage.encipherOnly, "encipherOnly");
        testUsage(keyUsage, KeyUsage.keyAgreement, "keyAgreement");
        testUsage(keyUsage, KeyUsage.keyCertSign, "keyCertSign");
        testUsage(keyUsage, KeyUsage.keyEncipherment, "keyEncipherment");
        testUsage(keyUsage, KeyUsage.nonRepudiation, "nonRepudiation");
    }

    private static void testUsage(int keyUsage, int usage, String strUsage) {
        int test = keyUsage & usage;
        if (test == usage) {
            log.info(strUsage);
        }
    }

    public static java.security.cert.X509Certificate convert(javax.security.cert.X509Certificate cert)
            throws CertificateException, CertificateEncodingException {
        byte[] encoded = cert.getEncoded();
        ByteArrayInputStream bis = new ByteArrayInputStream(encoded);
        java.security.cert.CertificateFactory cf = java.security.cert.CertificateFactory.getInstance("X.509");
        return (java.security.cert.X509Certificate) cf.generateCertificate(bis);
    }

    public static String getPEM(java.security.cert.X509Certificate[] certificates) throws CertificateException {
        try {
            ByteArrayOutputStream sb = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(sb, CertificateConstants.UTF_8);
            PEMWriter pemWriter = new PEMWriter(writer);

            for (java.security.cert.X509Certificate x509Certificate : certificates) {
                pemWriter.writeObject(x509Certificate);
            }

            pemWriter.close();
            writer.close();
            sb.close();

            return new String(sb.toByteArray(), CertificateConstants.UTF_8);
        } catch (Exception e) {
            log.error("Failed to convert certificates", e);
            throw new CertificateException(e);
        }
    }

    public static String getPEM(javax.security.cert.X509Certificate[] certificates) throws CertificateException {
        try {
            ByteArrayOutputStream sb = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(sb, CertificateConstants.UTF_8);
            PEMWriter pemWriter = new PEMWriter(writer);

            for (javax.security.cert.X509Certificate x509Certificate : certificates) {
                pemWriter.writeObject(convert(x509Certificate));
            }

            pemWriter.close();
            writer.close();
            sb.close();

            return new String(sb.toByteArray(), CertificateConstants.UTF_8);
        } catch (Exception e) {
            log.error("Failed to convert certificates", e);
            throw new CertificateException(e);
        }
    }

    public static String rnTobr(String result) {
        String splitted = StringWrapper.wrap(result, WRAPPER_SCREEN_WIDTH);
        return splitted.replaceAll(" ", "&nbsp;").replaceAll(CertificateConstants.NEW_LINE, "<br />")
                .replaceAll("\r", "<br />").replaceAll("\n", "<br />");
    }


    public static SelectItem[] getCountries(Locale displayLocale) {
        List<String> list = Arrays.asList(Locale.getISOCountries());
        List<SelectItem> result = new ArrayList<SelectItem>();

        for (String country : list) {
            Locale locale = new Locale("", country);
            result.add(new SelectItem(country, locale.getDisplayCountry(displayLocale) + " (" + country
                    + ")"));
        }
        result.add(new SelectItem("TS", "Tiani Spirit (TS)"));

        Collections.sort(result, new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem o1, SelectItem o2) {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });

        return result.toArray(new SelectItem[result.size()]);
    }

    /**
     * Close a possibly null stream and ignoring IOExceptions.<br/><b>Only use this method on streams where IOExceptions
     * really are irrelevant.</b> As example, do not use this method on an OutputStream there is a "once in a blue moon"
     * chance that an exception will be thrown due to disc errors or file system full. In this case, you could flush the
     * stream before to trigger any possible error.
     *
     * @param closeable
     */
    private static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException ex) {
                //ignore
            }
        }
    }

}
