package net.ihe.gazelle.pki.extension;

import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import net.ihe.gazelle.pki.X509CertificateParametersContainer;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

public class CertificateExtenderBasic extends CertificateExtenderCRL {

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);
        certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));
        try {
            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(
                    parameters.getPublicKey()));

            if (parameters.getCertificateRequest().getCertificateAuthority() != null) {
                certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(
                        parameters.getCertificateRequest().getCertificateAuthority().getPublicKey().getKey()));
            }

        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        } catch (CertificateParsingException e) {
            throw new CertificateException(e);
        }

    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        // bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
        // new DERBMPString(parameters
        // .getCertificateRequest().getName() + " Certificate"));
    }

}
