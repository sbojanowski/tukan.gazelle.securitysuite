package net.ihe.gazelle.pki.validator.epsos.v1;

import java.security.cert.CertificateException;
import java.util.List;

import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;

public abstract class EpsosClientValidator extends EpsosValidator {

    protected abstract String getCertificateType();

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        validateExtendedKeyUsagesOneValue(extendedKeyUsage, "ClientAuth (OID 1.3.6.1.5.5.7.3.2)",
                KeyPurposeId.id_kp_clientAuth, exceptions, "epSOS WP 3.4.2 - Chapter 5.4.3/5.4.5 - "
                        + getCertificateType());
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        validateKeyUsageValue(bits, KeyUsage.digitalSignature, "For " + getCertificateType()
                        + " certificates, only the digitalSignature usage type MUST be specified.", exceptions,
                "epSOS WP 3.4.2 - Chapter 5.4.3/5.4.5 - " + getCertificateType());
    }

}
