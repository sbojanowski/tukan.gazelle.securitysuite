package net.ihe.gazelle.pki.validator.epsos.v2;

import java.security.cert.CertificateException;
import java.util.List;

import net.ihe.gazelle.pki.validator.CertificateNormException;

import org.bouncycastle.asn1.x509.KeyUsage;

public class EpsosPPTObjectSigningValidator extends EpsosPPTValidator {

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.nonRepudiation | KeyUsage.keyEncipherment;
        validateKeyUsageValue(bits, keyUsageBits,
                "Only the nonRepudiation AND keyEncipherment usage type MUST be specified.", exceptions,
                "epSOS NCP Certificate Procurement - Chapter 2.1 - Object Signing");
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            exceptions.add(new CertificateNormException("ExtendedKeyUsages extension is not allowed.",
                    "epSOS NCP Certificate Procurement - Chapter 2.1 - Object Signing"));
        }
    }

}
