package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;

import javax.persistence.Entity;
import java.security.cert.CertificateException;

@Entity
public class CertificateRequestAuthority extends CertificateRequestWithGeneratedKeys {

    private String issuer;

    private CertificateVersion certificateVersion;

    /**
     * Instantiates a new certificate request authority. Only for Hibernate.
     */
    protected CertificateRequestAuthority() {
        super();
    }

    public CertificateRequestAuthority(KeyAlgorithm keyAlgorithm, int keySize) throws CertificateException {
        super(keyAlgorithm, keySize);
    }

    public CertificateVersion getCertificateVersion() {
        return certificateVersion;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setCertificateVersion(CertificateVersion certificateVersion) {
        this.certificateVersion = certificateVersion;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public void setIssuerUsingAttributes(String country, String organization, String commonName, String title,
                                         String givenName, String surname, String organizationalUnit, String eMail) {
        this.issuer = computeSubjectUsingAttributes(country, organization, commonName, title, givenName, surname,
                organizationalUnit, eMail);
    }

}
