package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.extension.*;
import net.ihe.gazelle.pki.extension.epsos.CertificateExtenderEpsosClient;
import net.ihe.gazelle.pki.extension.epsos.CertificateExtenderEpsosNcp;
import net.ihe.gazelle.pki.extension.epsos.CertificateExtenderEpsosOcsp;
import net.ihe.gazelle.pki.extension.epsos.CertificateExtenderEpsosServer;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public enum CertificateType implements CertificateExtender {

    CA_KEY_USAGE_ALL("CA", false, CertificateValidatorType.CA, CertificateExtenderCA.class),

    HTTPS_SERVER("TLS server", true, CertificateValidatorType.TLS_SERVER, CertificateExtenderHttpServer.class),

    HTTPS_CLIENT_AUTH("TLS client", true, CertificateValidatorType.TLS_CLIENT, CertificateExtenderClient.class),

    EPSOS_VPN_CLIENT("epSOS VPN client", true, CertificateValidatorType.EPSOS_VPN_CLIENT,
            CertificateExtenderEpsosClient.class),

    EPSOS_VPN_SERVER("epSOS VPN server", true, CertificateValidatorType.EPSOS_VPN_SERVER,
            CertificateExtenderEpsosServer.class),

    EPSOS_SERVICE_CONSUMER("epSOS service consumer", true, CertificateValidatorType.EPSOS_SERVICE_CONSUMER,
            CertificateExtenderEpsosClient.class),

    EPSOS_SERVICE_PROVIDER("epSOS service provider", true, CertificateValidatorType.EPSOS_SERVICE_PROVIDER,
            CertificateExtenderEpsosServer.class),

    EPSOS_NCP_SIGNATURE("epSOS NCP signature", true, CertificateValidatorType.EPSOS_NCP_SIGNATURE,
            CertificateExtenderEpsosNcp.class),

    EPSOS_OCSP_RESPONDER("epSOS OCSP responder", true, CertificateValidatorType.EPSOS_OCSP_RESPONDER,
            CertificateExtenderEpsosOcsp.class),

    CLIENT_AND_SERVER("Client and Server", true, CertificateValidatorType.TLS_CLIENT,
            CertificateExtenderClientServer.class),

    EPSOS_SEAL("Epsos Seal", true, CertificateValidatorType.EPSOS_SEAL_V4, CertificateExtenderRaw.class),

    EPSOS_TLS("Epsos TLS", true, CertificateValidatorType.EPSOS_TLS_V4, CertificateExtenderRaw.class),

    BASIC("Basic", true, CertificateValidatorType.TLS_CLIENT, CertificateExtenderRaw.class);

    private CertificateExtender certificateExtender;
    private CertificateValidatorType validator;
    private String friendlyName;
    private boolean alwaysVisible;

    private CertificateType(String friendlyName, boolean alwaysVisible, CertificateValidatorType validator,
                            Class<? extends CertificateExtender> certificateExtenderClass) {
        this.alwaysVisible = alwaysVisible;
        this.friendlyName = friendlyName;
        this.validator = validator;
        try {
            this.certificateExtender = certificateExtenderClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        certificateExtender.addExtension(certGen, parameters);
    }

    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        certificateExtender.modifyCertificate(cert, parameters);
    }

    @FilterLabel
    public String getFriendlyName() {
        return friendlyName;
    }

    public CertificateValidatorType getValidator() {
        return validator;
    }

    public boolean isAlwaysVisible() {
        return alwaysVisible;
    }

}
