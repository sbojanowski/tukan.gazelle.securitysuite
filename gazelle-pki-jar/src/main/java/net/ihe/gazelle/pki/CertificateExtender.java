package net.ihe.gazelle.pki;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.bouncycastle.x509.X509V3CertificateGenerator;

public interface CertificateExtender {

    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException;

    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException;

}
