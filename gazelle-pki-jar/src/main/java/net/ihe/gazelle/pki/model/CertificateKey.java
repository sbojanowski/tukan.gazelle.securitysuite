package net.ihe.gazelle.pki.model;

import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

@Entity
@Table(name = "pki_certificate_key", schema = "public")
@Inheritance
@SequenceGenerator(name = "pki_certificate_key_id", sequenceName = "pki_certificate_key_id_seq", allocationSize = 1)
public abstract class CertificateKey<K extends Key> {

    static final int KEY_PRIVATE = 0;
    static final int KEY_PUBLIC = 1;
    static final int KEY_SECRET = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pki_certificate_key_id")
    private Integer id;

    protected int keyType;
    protected String format;
    protected String algorithm;
    protected byte[] enc = null;

    protected transient Key key = null;

    public CertificateKey() {
        super();
        this.key = null;
        this.enc = null;
        this.format = null;
    }

    public CertificateKey(K key) throws CertificateException {
        super();
        setKey(key);
    }

    protected void loadKey() throws CertificateException {
        if (enc == null || enc.length == 0) {
            key = null;
        } else {
            KeySpec spec;

            if (format.equals("PKCS#8") || format.equals("PKCS8")) {
                spec = new PKCS8EncodedKeySpec(enc);
            } else if (format.equals("X.509") || format.equals("X509")) {
                spec = new X509EncodedKeySpec(enc);
            } else if (format.equals("RAW")) {
                key = new SecretKeySpec(enc, algorithm);
                return;
            } else {
                throw new CertificateException("Key format " + format + " not recognised!");
            }

            try {
                switch (keyType) {
                    case KEY_PRIVATE:
                        key = KeyFactory.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME).generatePrivate(spec);
                        break;
                    case KEY_PUBLIC:
                        key = KeyFactory.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME).generatePublic(spec);
                        break;
                    case KEY_SECRET:
                        key = SecretKeyFactory.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME).generateSecret(
                                spec);
                        break;
                    default:
                        throw new IOException("Key type " + keyType + " not recognised!");
                }
            } catch (Exception e) {
                throw new CertificateException("Exception creating key: " + e.toString(), e);
            }
        }
    }

    protected void saveKey() throws CertificateException {
        if (key != null) {
            if (key instanceof PrivateKey) {
                keyType = KEY_PRIVATE;
            } else if (key instanceof PublicKey) {
                keyType = KEY_PUBLIC;
            } else {
                keyType = KEY_SECRET;
            }

            format = key.getFormat();
            algorithm = key.getAlgorithm();

            enc = key.getEncoded();
        } else {
            enc = null;
        }
    }

    @SuppressWarnings("unchecked")
    public K getKey() throws CertificateException {
        if (key == null) {
            loadKey();
        }
        return (K) key;
    }

    public void setKey(K key) throws CertificateException {
        this.key = key;
        saveKey();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        try {
            if (getKey() != null) {
                return keyType + " / " + getKey().toString();
            } else {
                return "No key specified";
            }
        } catch (CertificateException e) {
            return "Failed to load key";
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((algorithm == null) ? 0 : algorithm.hashCode());
        result = prime * result + Arrays.hashCode(enc);
        result = prime * result + ((format == null) ? 0 : format.hashCode());
        result = prime * result + keyType;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CertificateKey other = (CertificateKey) obj;
        if (algorithm == null) {
            if (other.algorithm != null)
                return false;
        } else if (!algorithm.equals(other.algorithm))
            return false;
        if (!Arrays.equals(enc, other.enc))
            return false;
        if (format == null) {
            if (other.format != null)
                return false;
        } else if (!format.equals(other.format))
            return false;
        if (keyType != other.keyType)
            return false;
        return true;
    }

}
