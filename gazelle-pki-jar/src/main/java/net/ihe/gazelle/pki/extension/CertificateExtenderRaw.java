package net.ihe.gazelle.pki.extension;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.bouncycastle.x509.X509V3CertificateGenerator;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.X509CertificateParametersContainer;

public class CertificateExtenderRaw implements CertificateExtender {

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

}
