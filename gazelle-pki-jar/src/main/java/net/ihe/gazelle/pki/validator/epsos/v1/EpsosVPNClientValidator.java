package net.ihe.gazelle.pki.validator.epsos.v1;

public class EpsosVPNClientValidator extends EpsosClientValidator {

    @Override
    protected String getCertificateType() {
        return "IPSec client";
    }

}
