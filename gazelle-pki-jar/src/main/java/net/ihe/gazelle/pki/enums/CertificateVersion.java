package net.ihe.gazelle.pki.enums;

public enum CertificateVersion {
    V1, V3;
}
