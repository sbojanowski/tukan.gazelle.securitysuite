package net.ihe.gazelle.pki.validator.epsos.v3;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.pki.validator.CertificateNormException;

import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;

public class EpsosNCP extends EpsosValidator {

    @Override
    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        if (criticalExtensionOIDs.contains(X509Extensions.ExtendedKeyUsage.getId())
                || nonCriticalExtensionOIDs.contains(X509Extensions.ExtendedKeyUsage.getId())) {
            exceptions.add(new CertificateNormException(
                    "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.",
                    "epSOS WP 3.4.2 - Chapter 5.4.7"));
        }
        super.validateExtendedKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions,
                warnings);
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.nonRepudiation | KeyUsage.digitalSignature;
        validateKeyUsageValue(bits, keyUsageBits,
                "Only the nonRepudiation AND digitalSignature usage type MUST be specified.", exceptions,
                "epSOS NCP Certificate Procurement - Chapter 2.1 - Object Signing");
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            exceptions.add(new CertificateNormException("ExtendedKeyUsages extension is not allowed.",
                    "epSOS NCP Certificate Procurement - Chapter 2.1 - Object Signing"));
        }
    }

}
