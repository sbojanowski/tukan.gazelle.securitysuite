package net.ihe.gazelle.pki.model;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Entity
@Table(name = "pki_certificate_x509", schema = "public")
@SequenceGenerator(name = "pki_certificate_x509_id", sequenceName = "pki_certificate_x509_id_seq", allocationSize = 1)
public class CertificateX509 {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pki_certificate_x509_id")
    private Integer id;

    protected transient X509Certificate x509Certificate = null;

    private byte[] enc;

    private String type;

    public CertificateX509() {
        super();
        enc = null;
    }

    public CertificateX509(X509Certificate x509Certificate) throws CertificateException {
        super();
        setX509Certificate(x509Certificate);
    }

    public X509Certificate getX509Certificate() throws CertificateException {
        if (x509Certificate == null) {
            loadCertificate();
        }
        return x509Certificate;
    }

    public void setX509Certificate(X509Certificate x509Certificate) throws CertificateException {
        this.x509Certificate = x509Certificate;
        saveCertificate();
    }

    /**
     * <b>Do not call this method !</b> It is exclusively used to corrupt the certificate via the
     * CorruptedCertificateGenerator
     *
     * @return Certificate in hexadecimal
     */
    public byte[] getEnc() {
        return enc.clone();
    }

    /**
     * <b>Do not call this method !</b> Do not directly set enc value !  It is exclusively used to corrupt the
     * certificate via the CorruptedCertificateGenerator.
     *
     * @param enc Certificate in hexadecimal
     */
    public void setEnc(byte[] enc) {
        this.enc = enc.clone();
    }

    private void saveCertificate() throws CertificateException {
        if (x509Certificate == null) {
            enc = null;
        } else {
            enc = x509Certificate.getEncoded();
            type = x509Certificate.getType();
        }
    }

    private void loadCertificate() throws CertificateException {
        if (enc == null || enc.length == 0) {
            x509Certificate = null;
        } else {
            CertificateFactory cFact;
            try {
                cFact = CertificateFactory.getInstance(type, BouncyCastleProvider.PROVIDER_NAME);
            } catch (NoSuchProviderException e) {
                throw new CertificateException(e);
            }
            ByteArrayInputStream bIn = new ByteArrayInputStream(enc);
            x509Certificate = (X509Certificate) cFact.generateCertificate(bIn);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
