package net.ihe.gazelle.pki.validator.epsos.v1;

import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.pki.validator.CertificateNormException;

import org.bouncycastle.asn1.x509.KeyUsage;

public class EpsosNCPValidator extends EpsosValidator {

    @Override
    protected void validateEndCert(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                   List<CertificateException> warnings) {
        super.validateEndCert(x509Certificate, exceptions, warnings);
        List<String> extendedKeyUsage;
        try {
            extendedKeyUsage = x509Certificate.getExtendedKeyUsage();
        } catch (CertificateParsingException e) {
            exceptions.add(e);
            return;
        }
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            exceptions.add(new CertificateNormException(
                    "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.",
                    "epSOS WP 3.4.2 - Chapter 5.4.7"));
        }
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.digitalSignature | KeyUsage.nonRepudiation;
        validateKeyUsageValue(bits, keyUsageBits,
                "Only the digitalSignature AND nonRepudiation usage type MUST be specified.", exceptions,
                "epSOS WP 3.4.2 - Chapter 5.4.7");
    }
    
    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
            List<CertificateException> warnings) {
    	// added to override the parent method, which is not applicable for epSOS NCP signature validation
    	// do not remove this method !
    }

}
