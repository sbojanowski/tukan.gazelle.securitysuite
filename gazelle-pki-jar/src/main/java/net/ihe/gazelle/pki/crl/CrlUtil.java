package net.ihe.gazelle.pki.crl;

import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;

import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.preferences.PreferenceService;

import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CrlUtil {

    private static final CrlDefaultProvider CRL_DEFAULT_PROVIDER = new CrlDefaultProvider();

    private static Map<Certificate, CrlProvider> testCertificates = Collections
            .synchronizedMap(new HashMap<Certificate, CrlProvider>());

    private CrlUtil() {
        super();
    }

    public static void addTestCertificate(Certificate testCertificate) {
        testCertificates.put(testCertificate, null);
    }

    public static void addTestCertificate(Certificate testCertificate, CrlProvider crlProvider) {
        testCertificates.put(testCertificate, crlProvider);
    }

    public static void addCRL(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters,
                              boolean netscape) {
        Integer caId = 0;
        if (parameters != null) {
            if ((parameters.getCertificateRequest() != null) &&
                    (parameters.getCertificateRequest().getCertificateAuthority() != null)) {
                caId = parameters.getCertificateRequest().getCertificateAuthority().getId();
            } else {
                caId = parameters.getCertificateId();
            }
        }

        String crlUrl = generateCrlUrl(caId);

        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(crlUrl));
        GeneralNames gns = new GeneralNames(new DERSequence(gn));
        DistributionPointName dpn = new DistributionPointName(0, gns);
        DistributionPoint distp = new DistributionPoint(dpn, null, null);
        certGen.addExtension(X509Extensions.CRLDistributionPoints, false, new DERSequence(distp));

        certGen.addExtension(MiscObjectIdentifiers.netscapeCARevocationURL, false, new DERIA5String(crlUrl));
        certGen.addExtension(MiscObjectIdentifiers.netscapeRevocationURL, false, new NetscapeRevocationURL(
                new DERIA5String(crlUrl)));
    }

    public static byte[] generateCRL(int id, EntityManager entityManager) throws CertificateException {
        Certificate certificateAuthority = null;
        CrlProvider crlProvider = CRL_DEFAULT_PROVIDER;
        if (entityManager != null) {
            certificateAuthority = CertificateDAO.getByID(id, entityManager);
        } else {
            Map<Certificate, CrlProvider> certificates = new HashMap<Certificate, CrlProvider>(testCertificates);
            Set<Entry<Certificate, CrlProvider>> entrySet = certificates.entrySet();
            for (Entry<Certificate, CrlProvider> entry : entrySet) {
                if (entry.getKey().getId() == id) {
                    certificateAuthority = entry.getKey();
                    if (entry.getValue() != null) {
                        crlProvider = entry.getValue();
                    }
                }
            }
        }
        if (certificateAuthority != null) {
            return crlProvider.generateCRL(certificateAuthority);
        } else {
            return null;
        }
    }

    private static String generateCrlUrl(Integer caId) {
        String crlBase = PreferenceService.getString("crl_url");
        if (!crlBase.endsWith("/")) {
            crlBase = crlBase + "/";
        }
        String url = crlBase + "crl/" + Integer.toString(caId) + "/cacrl.crl";
        return url;
    }

}
