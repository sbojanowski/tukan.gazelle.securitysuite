package net.ihe.gazelle.pki.validator.epsos.v1;

import java.security.cert.CertificateException;
import java.util.List;

import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;

public class EpsosOCSPResponderValidator extends EpsosValidator {

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        validateExtendedKeyUsagesOneValue(extendedKeyUsage, "id-kp-OCSPSigning (OID 1.3.6.1.5.5.7.3.9)",
                KeyPurposeId.id_kp_OCSPSigning, exceptions, "epSOS WP 3.4.2 - Chapter 5.4.8");
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.nonRepudiation;
        validateKeyUsageValue(bits, keyUsageBits, "Only the nonRepudiation usage type MUST be specified.", exceptions,
                "epSOS WP 3.4.2 - Chapter 5.4.8");
    }

}
