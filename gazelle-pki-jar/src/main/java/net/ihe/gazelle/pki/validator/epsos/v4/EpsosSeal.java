package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.qualified.QCStatement;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

public class EpsosSeal extends EpsosValidator {

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.digitalSignature | KeyUsage.keyEncipherment;
        validateKeyUsageValue(bits, keyUsageBits,
                "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other KeyUSage bits that MUST be " +
                        "set to false.", exceptions,
                EHEALTH_X509_3_1);
    }

    @Override
    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        if (x509Certificate.getExtensionValue(X509Extensions.ExtendedKeyUsage.getId()) != null) {
            exceptions.add(new CertificateNormException(
                    "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.", EHEALTH_X509_3_1));
        }
    }

    @Override
    protected void validateQCStatementUsages(QCStatement qcStatement, List<CertificateException> exceptions) {
        if (qcStatement.getStatementId() != new DERObjectIdentifier(QCStatement.id_etsi_qcs + ".6.2")) {
            exceptions.add(new CertificateNormException(
                    "The esi4-qcStatement-6 id-etsi-qcs-QcType 2 SHOULD be included on its own as specified in [ETSI EN 319 412-5] to indicate that" +
                            " it is used for the purposes of electronic seal.", EHEALTH_X509_3_1));
        }
    }
}
