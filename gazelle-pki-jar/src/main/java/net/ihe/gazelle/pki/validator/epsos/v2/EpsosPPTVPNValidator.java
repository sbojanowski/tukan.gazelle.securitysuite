package net.ihe.gazelle.pki.validator.epsos.v2;

import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;

import net.ihe.gazelle.pki.validator.CertificateNormException;

import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;

public class EpsosPPTVPNValidator extends EpsosPPTValidator {

    private static final List<String> extendedUsages = Arrays.asList(new String[]{
            KeyPurposeId.id_kp_clientAuth.getId(), KeyPurposeId.id_kp_serverAuth.getId()});

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        super.validateExtendedKeyUsages(extendedKeyUsage, exceptions);
        if (extendedKeyUsage != null) {
            if (extendedKeyUsage.size() != 0 && extendedKeyUsage.size() != 2) {
                exceptions.add(new CertificateNormException(
                        "If ExtendedKeyUsages extension is included in the certificate, "
                                + "it MUST only accept the value ClientAuth AND ServerAuth."
                                + showFund(extendedKeyUsage),
                        "epSOS NCP Certificate Procurement - Chapter 2.1 - VPN Client, Server"));
            }
            if (extendedKeyUsage.size() == 2) {
                if (!extendedKeyUsage.containsAll(extendedUsages)) {
                    exceptions.add(new CertificateNormException(
                            "If ExtendedKeyUsages extension is included in the certificate, "
                                    + "it MUST only accept the value ClientAuth AND ServerAuth."
                                    + showFund(extendedKeyUsage),
                            "epSOS NCP Certificate Procurement - Chapter 2.1 - VPN Client, Server"));
                }
            }
        }
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = KeyUsage.digitalSignature | KeyUsage.keyEncipherment;
        validateKeyUsageValue(bits, keyUsageBits,
                "Only the digitalSignature AND keyEncipherment usage type MUST be specified.", exceptions,
                "epSOS NCP Certificate Procurement - Chapter 2.1 - VPN Client, Server");
    }

}
