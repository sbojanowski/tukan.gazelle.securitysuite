package net.ihe.gazelle.pki.extension;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.crl.CrlUtil;

import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CertificateExtenderCRL implements CertificateExtender {

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        CrlUtil.addCRL(certGen, parameters, true);
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

}
