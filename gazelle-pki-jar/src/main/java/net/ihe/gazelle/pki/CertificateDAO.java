package net.ihe.gazelle.pki;

import java.security.cert.CertificateException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.TransactionRequiredException;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateQuery;
import net.ihe.gazelle.pki.model.CertificateRequest;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.*;

@SuppressWarnings("unchecked")
public class CertificateDAO {

    private CertificateDAO() {
        super();
    }

    public static Certificate getUniqueBySubjectNew(String subject) {
        List<Certificate> certList = null;
        Certificate certResult = null;
        CertificateQuery certQuery = new CertificateQuery();

        certQuery.subject().eq(subject);
        certList = certQuery.getListDistinct();
        if (certList != null && certList.size() >= 1) {
            certResult = certList.get(0);
        }
        return certResult;
    }

    public static Certificate getUniqueBySubject(String subject, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(Certificate.class);
        criteria.add(Restrictions.eq("subject", subject));
        List<Certificate> list = criteria.list();
        if (list != null && list.size() >= 1) {
            return list.get(0);
        }
        return null;
    }

    public static Certificate getByID(int certificateId, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(Certificate.class);
        criteria.add(Restrictions.idEq(Integer.valueOf(certificateId)));
        criteria.setFetchMode("privateKey", FetchMode.JOIN);
        criteria.setFetchMode("publicKey", FetchMode.JOIN);
        criteria.setFetchMode("certificateX509", FetchMode.JOIN);
        List<Certificate> list = criteria.list();
        if (list != null && list.size() > 0) {
            Certificate certificate = list.get(0);
            try {
                certificate.getChainAsArray();
            } catch (CertificateException e) {
                // FIXME log
            }
            return certificate;
        }
        return null;
    }

    public static List<CertificateRequest> getRequests(EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(CertificateRequest.class);
        criteria.addOrder(Order.desc("id"));
        criteria.setFirstResult(0);
        criteria.setMaxResults(10);
        List<CertificateRequest> list = criteria.list();
        return list;
    }

    public static List<Certificate> getCertificates(EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(Certificate.class);
        criteriaNotForCustom(criteria);
        criteria.addOrder(Order.desc("id"));
        criteria.setFirstResult(0);
        criteria.setMaxResults(10);
        List<Certificate> list = criteria.list();
        return list;
    }

    public static CertificateRequest getRequestByID(int requestId, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(CertificateRequest.class);
        criteria.add(Restrictions.idEq(Integer.valueOf(requestId)));
        List<CertificateRequest> list = criteria.list();
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public static List<Certificate> getCAs(boolean withPrivateKey) {
        CertificateQuery q = new CertificateQuery();
        if (withPrivateKey) {
            q.privateKey().format().isNotNull();
            q.privateKey().algorithm().isNotNull();
        }
        HQLRestriction or = HQLRestrictions.or(q.certificates().isNotEmptyRestriction(), q.request().certificateExtension().eqRestriction(CertificateType.CA_KEY_USAGE_ALL));
        q.addRestriction(or);

        return q.getListDistinct();
    }

    public static List<Certificate> getCertificateAuthorities(EntityManager entityManager, boolean withPrivateKey) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = getCertificateAuthoritiesCriteria(session, withPrivateKey);

        List<Certificate> list = criteria.list();
        return list;
    }

    private static Criteria getCertificateAuthoritiesCriteria(Session session, boolean withPrivateKey) {
        Criteria criteria = session.createCriteria(Certificate.class);
        if (withPrivateKey) {
            criteria.createAlias("privateKey", "pk");
        }
        criteria.createAlias("request", "req", Criteria.LEFT_JOIN);

        LogicalExpression notNullPK = null;
        if (withPrivateKey) {
            notNullPK = Restrictions.and(Restrictions.ne("pk.format", ""), Restrictions.ne("pk.algorithm", ""));
        }

        Criterion hasCertificates = Restrictions.sizeGt("certificates", 0);
        Criterion isCARequest = Restrictions.eq("req.certificateExtension", CertificateType.CA_KEY_USAGE_ALL);

        LogicalExpression isCA = Restrictions.or(hasCertificates, isCARequest);

        if (withPrivateKey) {
            criteria.add(Restrictions.and(notNullPK, isCA));
        } else {
            criteria.add(isCA);
        }
        return criteria;
    }

    public static void delete(Integer certificateId, EntityManager entityManager)
            throws IllegalArgumentException, TransactionRequiredException {

        Certificate certificate = getByID(certificateId, entityManager);
        entityManager.remove(certificate);
    }

    public static void criteriaNotForCustom(Criteria criteria) {
        criteria.add(Restrictions.or(Restrictions.eq("custom", Boolean.FALSE),
                Restrictions.isNull("custom")));
    }

    public static void criteriaForCustom(Criteria criteria) {
        criteria.add(Restrictions.eq("custom", Boolean.TRUE));
    }

    public static List<Certificate> getCertificatesForSigningBySource(EntityManager entityManager, String username,
                                                                      boolean fromPKI) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = getCertificatesForSigningCriteria(session);
        if (fromPKI) {
            criteriaNotForCustom(criteria);
        } else {
            criteriaForCustom(criteria);
        }
        List<Certificate> list = criteria.list();
        return list;
    }

    public static List<Certificate> getCertificatesForSigning(EntityManager entityManager, String username) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = getCertificatesForSigningCriteria(session);
        List<Certificate> list = criteria.list();
        return list;
    }

    private static Criteria getCertificatesForSigningCriteria(Session session) {
        Criteria criteria = session.createCriteria(Certificate.class);
        criteria.createAlias("privateKey", "pk");

        criteria.setFetchMode("privateKey", FetchMode.JOIN);
        criteria.setFetchMode("publicKey", FetchMode.JOIN);
        criteria.setFetchMode("certificateX509", FetchMode.JOIN);

        LogicalExpression notNullPK = Restrictions.and(Restrictions.ne("pk.format", ""),
                Restrictions.ne("pk.algorithm", ""));

        criteria.add(notNullPK);
        return criteria;
    }

    public static List<Certificate> getCertificateAuthoritiesBySource(EntityManager entityManager, boolean fromPKI) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = getCertificateAuthoritiesCriteria(session, true);
        if (fromPKI) {
            criteriaNotForCustom(criteria);
        } else {
            criteriaForCustom(criteria);
        }
        List<Certificate> list = criteria.list();
        return list;
    }

    public static void criteriaNotForCustomsQueryBuilder(HQLQueryBuilder<Certificate> queryBuilder) {
        queryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("custom", Boolean.FALSE),
                HQLRestrictions.eq("custom", null)));
        // String certificateAuthorityId =
        // Preferences.getProperty("certificateAuthorityId");
        // if (StringUtils.isNumeric(certificateAuthorityId)) {
        // queryBuilder.addRestriction(HQLRestrictions.eq("certificateAuthority.id",
        // Integer.parseInt(certificateAuthorityId)));
        // }
    }

    public static void criteriaForCustomsQueryBuilder(HQLQueryBuilder<Certificate> queryBuilder) {
        queryBuilder.addEq("custom", Boolean.TRUE);
    }

}
