package net.ihe.gazelle.pki.validator.epsos.v2;

import net.ihe.gazelle.pki.validator.AbstractCertificateValidator;
import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.*;
import java.util.*;

public class EpsosPPTValidator extends AbstractCertificateValidator {

    private static final KeyPurposeId[] KeyPurposeIds = new KeyPurposeId[]{KeyPurposeId.anyExtendedKeyUsage,
            KeyPurposeId.id_kp_capwapAC, KeyPurposeId.id_kp_clientAuth, KeyPurposeId.id_kp_capwapWTP,
            KeyPurposeId.id_kp_codeSigning, KeyPurposeId.id_kp_dvcs, KeyPurposeId.id_kp_eapOverLAN,
            KeyPurposeId.id_kp_eapOverPPP, KeyPurposeId.id_kp_emailProtection, KeyPurposeId.id_kp_ipsecEndSystem,
            KeyPurposeId.id_kp_ipsecIKE, KeyPurposeId.id_kp_ipsecTunnel, KeyPurposeId.id_kp_ipsecUser,
            KeyPurposeId.id_kp_OCSPSigning, KeyPurposeId.id_kp_sbgpCertAAServerAuth, KeyPurposeId.id_kp_scvp_responder,
            KeyPurposeId.id_kp_scvpClient, KeyPurposeId.id_kp_scvpServer, KeyPurposeId.id_kp_serverAuth,
            KeyPurposeId.id_kp_smartcardlogon, KeyPurposeId.id_kp_timeStamping};

    private static final String[] KeyPurposeStrings = new String[]{"anyExtendedKeyUsage", "id_kp_capwapAC",
            "id_kp_clientAuth", "id_kp_capwapWTP", "id_kp_codeSigning", "id_kp_dvcs", "id_kp_eapOverLAN",
            "id_kp_eapOverPPP", "id_kp_emailProtection", "id_kp_ipsecEndSystem", "id_kp_ipsecIKE", "id_kp_ipsecTunnel",
            "id_kp_ipsecUser", "id_kp_OCSPSigning", "id_kp_sbgpCertAAServerAuth", "id_kp_scvp_responder",
            "id_kp_scvpClient", "id_kp_scvpServer", "id_kp_serverAuth", "id_kp_smartcardlogon", "id_kp_timeStamping"};

    private List<String> countries = Arrays.asList(Locale.getISOCountries());

    @SuppressWarnings("unchecked")
    protected final Vector<String> getSubjectVector(X509Name name, DERObjectIdentifier identifier) {
        Vector<String> vector = name.getValues(identifier);
        return vector;
    }

    protected String showFund(List<String> extendedKeyUsage) {
        StringBuilder resultSB = new StringBuilder(" Fund : {");
        for (int i = 0; i < extendedKeyUsage.size(); i++) {
            if (i != 0) {
                resultSB.append(", ");
            }
            String value = extendedKeyUsage.get(i);
            for (int j = 0; j < KeyPurposeIds.length; j++) {
                KeyPurposeId keyPurpose = KeyPurposeIds[j];
                if (keyPurpose.getId().equals(value)) {
                    value = KeyPurposeStrings[j];
                }
            }
            resultSB.append(value);
        }
        resultSB.append("}");
        return resultSB.toString();
    }

    private void validateCA(List<X509Certificate> list, List<CertificateException> exceptions,
                            List<CertificateException> warnings, boolean revocation) {

        if (revocation) {
            List<String> crlDistributionPointsCA = new ArrayList<String>();

            try {
                getCRLDistributionPoints(list.get(0), crlDistributionPointsCA);
                for (String location : crlDistributionPointsCA) {
                    try {
                        X509CRL crl = getCRL(location);
                        validateCRL(crl, list, exceptions, warnings);
                    } catch (CertificateException e) {
                        warnings.add(new CertificateNormException("Failed to check CRL of CA - " + location,
                                "CA's CRLs validation", e));
                    }
                }
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to check CRL of CA", "CA's CRLs validation", e));
            }
        }
    }

    private void validateCRL(X509CRL crl, List<X509Certificate> list, List<CertificateException> exceptions,
                             List<CertificateException> warnings) {

        String crlSubject = null;
        if (crl.getIssuerX500Principal() != null) {
            crlSubject = crl.getIssuerX500Principal().toString();
        }

        boolean checked = false;
        for (X509Certificate caCertificate : list) {
            String caSubject = null;
            if (caCertificate.getIssuerX500Principal() != null) {
                caSubject = caCertificate.getIssuerX500Principal().toString();
            }
            if (StringUtils.equals(crlSubject, caSubject)) {
                checked = true;
            }
        }
        if (!checked) {
            exceptions.add(new CertificateNormException(
                    "The DName MUST be identical to the subject DName of the issuer certificate.",
                    "CA's CRLs validation"));
        }

        if (crl.getThisUpdate() == null || crl.getNextUpdate() == null) {
            exceptions.add(new CertificateNormException("The CRL’s period of validity (from/to) MUST be stated.",
                    "CA's CRLs validation"));
        }

        byte[] declaredCaKeyIdentifier = null;
        byte[] authorityKeyIdentifierBytes = crl.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
        if (authorityKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        authorityKeyIdentifierBytes)).readObject());
                AuthorityKeyIdentifier authorityKeyIdentifier = new AuthorityKeyIdentifier(
                        (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject());

                declaredCaKeyIdentifier = authorityKeyIdentifier.getKeyIdentifier();
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                        "CA's CRLs validation", e));
            }
        }

        if (declaredCaKeyIdentifier == null) {
            exceptions
                    .add(new CertificateNormException("The KeyIdentifier MUST be indicated.", "CA's CRLs validation"));
        } else {
            checked = false;

            for (X509Certificate caCertificate : list) {
                byte[] caKeyIdentifier = null;

                byte[] subjectKeyIdentifierBytes = caCertificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier
                        .getId());
                if (subjectKeyIdentifierBytes != null) {
                    try {
                        DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                                subjectKeyIdentifierBytes)).readObject());
                        SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                                new ByteArrayInputStream(oct.getOctets())).readObject());
                        caKeyIdentifier = keyId.getKeyIdentifier();
                    } catch (Exception e) {
                        exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                                "CA's CRLs validation", e));
                    }
                }

                if (caKeyIdentifier == null) {
                    exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                            "CA's CRLs validation"));
                } else {
                    if (Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                        checked = true;
                    }
                }
            }

            if (!checked) {
                exceptions.add(new CertificateNormException(
                        "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                        "CA's CRLs validation"));
            }
        }

        byte[] crlNumberBytes = crl.getExtensionValue(X509Extensions.CRLNumber.getId());
        if (crlNumberBytes == null) {
            exceptions.add(new CertificateNormException("The consecutive number for the CRL issued MUST be indicated.",
                    "CA's CRLs validation"));
        } else {

            try {

                ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(crlNumberBytes));
                ASN1OctetString octs = (ASN1OctetString) aIn.readObject();
                aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
                DERObject obj = aIn.readObject();

                DERInteger crlnum = CRLNumber.getInstance(obj);
                BigInteger crlNumberBigInt = crlnum.getPositiveValue();

                byte[] byteArray = crlNumberBigInt.toByteArray();
                if (byteArray.length > 20) {
                    exceptions.add(new CertificateNormException(
                            "It MUST have a unique positive integer value with a maximum length of 20 bytes.",
                            "CA's CRLs validation - CRL number"));
                }

            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to load CRLNumber", "CA's CRLs validation", e));
            }
        }
    }

    private X509CRL getCRL(String location) throws CertificateException {
        X509CRL result = null;
        try {
            URL url = new URL(location);

            if (url.getProtocol().equals("http") || url.getProtocol().equals("https")) {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setUseCaches(false);
                // conn.setConnectTimeout(2000);
                conn.setDoInput(true);
                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
                    result = (X509CRL) cf.generateCRL(conn.getInputStream());
                } else {
                    throw new Exception(conn.getResponseMessage());
                }
            }
        } catch (Exception e) {
            throw new CertificateNormException("Failed to retrieve CRL from " + location, "Validation of the CA CRLs");
        }
        return result;
    }

    protected void validateEndCert(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                   List<CertificateException> warnings) {
        // 5.4.1
        validate_5_4_1(x509Certificate, exceptions, warnings);

        // 5.4.2
        validate_5_4_2(x509Certificate, exceptions, warnings);
    }

    @Override
    public void validate(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                         List<CertificateException> warnings, boolean revocation) {
        if (chain != null && chain.size() >= 2) {
            for (int i = 0; i < chain.size(); i++) {
                X509Certificate x509Certificate = chain.get(i);
                if (i == 0) {
                    validateEndCert(x509Certificate, pkiValidation, warnings);
                } else {
                    List<X509Certificate> subList = chain.subList(i, chain.size());
                    validateCA(subList, pkiValidation, warnings, revocation);
                }
            }
            validateIssuers(chain, pkiValidation, warnings);
        } else {
            // FIXME
            if (chain != null && chain.size() == 1) {
                validateEndCert(chain.get(0), pkiValidation, warnings);
                validateCA(chain, pkiValidation, warnings, revocation);
            }
            pkiValidation.add(new CertificateException("Invalid chain of certificates (at last 2 certificates)"));
        }
    }

    private void validate_5_4_1(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                List<CertificateException> warnings) {
        // Version
        validateVersion(x509Certificate, exceptions);

        // Signature alorithm
        // 5.1
        validateSignatureAlgorithm(x509Certificate, exceptions);

        // Serial number
        validateSerialNumber(x509Certificate, exceptions);

        // Validity from/to
        validateDate(x509Certificate, exceptions, warnings);

        // IssuerUniqueID
        boolean[] issuerUniqueID = x509Certificate.getIssuerUniqueID();
        if (issuerUniqueID != null) {
            if (issuerUniqueID.length > 0) { // Not sure...
                exceptions.add(new CertificateNormException("The field \"IssuerUniqueID\" MUST NOT be used.",
                        "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
        }

        // SubjectUniqueID
        boolean[] subjectUniqueID = x509Certificate.getSubjectUniqueID();
        if (subjectUniqueID != null) {
            if (subjectUniqueID.length > 0) { // Not sure...
                exceptions.add(new CertificateNormException("The field \"SubjectUniqueID\" MUST NOT be used.",
                        "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
        }

        // Subject
        X500Principal subject = x509Certificate.getSubjectX500Principal();

        validateSubject(subject.getName(), exceptions, warnings);
    }

    /**
     * Extract the value of the given extension, if it exists.
     *
     * @param ext The extension object.
     * @param oid The object identifier to obtain.
     * @return the extension as {@link DERObject}
     * @throws CertificateException if the extension cannot be read.
     */
    protected static DERObject getExtensionValue(java.security.cert.X509Extension ext, String oid)
            throws CertificateException {
        byte[] bytes = ext.getExtensionValue(oid);
        if (bytes == null) {
            return null;
        }

        return getObject(oid, bytes);
    }

    private void validate_5_4_2(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                List<CertificateException> warnings) {
        Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
        Set<String> nonCriticalExtensionOIDs = x509Certificate.getNonCriticalExtensionOIDs();

        validateNonCritical("AuthorityKeyIdentifier", X509Extensions.AuthorityKeyIdentifier, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // checked in validateIssuers

        validateSubjectKeyIdentifier(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions,
                warnings);
        validateKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

        validateNonCritical("IssuerAlternativeName", X509Extensions.IssuerAlternativeName, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("SubjectAlternativeName", X509Extensions.SubjectAlternativeName, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        validateCritical("BasicConstraints", X509Extensions.BasicConstraints, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);

        byte[] basicConstraintsBytes = x509Certificate.getExtensionValue(X509Extensions.BasicConstraints.getId());
        if (basicConstraintsBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        basicConstraintsBytes)).readObject());
                BasicConstraints basicConstraints = BasicConstraints.getInstance(new ASN1InputStream(
                        new ByteArrayInputStream(oct.getOctets())).readObject());

                if (basicConstraints.isCA()) {
                    exceptions.add(new CertificateNormException(
                            "The BasicConstraints extension MUST assume the value FALSE for ca.",
                            "epSOS WP 3.4.2 - Chapter 5.4.2"));
                }
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to validate BasicConstraints",
                        "epSOS WP 3.4.2 - Chapter 5.4.2", e));
            }
        }

        // checked in validateIssuers

        validateExtendedKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions,
                warnings);

        validateNonCritical("CRLDistributionPoints", X509Extensions.CRLDistributionPoints, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("CertificatePolicies", X509Extensions.CertificatePolicies, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        try {
            DERObject certificatePoliciesObject = getExtensionValue(x509Certificate,
                    X509Extensions.CertificatePolicies.getId());
            if (certificatePoliciesObject != null) {
                if (certificatePoliciesObject instanceof ASN1Sequence) {
                    ASN1Sequence seq = (ASN1Sequence) certificatePoliciesObject;
                    Enumeration e = seq.getObjects();
                    while (e.hasMoreElements()) {
                        ASN1Sequence s = ASN1Sequence.getInstance(e.nextElement());
                        if (s.size() > 1) {
                            warnings.add(new CertificateNormException(
                                    "CertificatePolicies Policyinformation SHOULD only include an OID, no PolicyQualifier.",
                                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
                        }
                    }
                } else {
                    exceptions.add(new CertificateNormException("Failed to load CertificatePolicies",
                            "epSOS WP 3.4.2 - Chapter 5.4.2"));
                }
                /*
                ASN1Sequence policyQualifiers = policyInformation.getPolicyQualifiers();
				if (policyQualifiers != null) {
					exceptions.add(new CertificateException(
							"CertificatePolicies Policyinformation SHOULD only include an OID, no PolicyQualifier."));
				}
				*/
            }
        } catch (CertificateException e) {
            exceptions.add(e);
        }

        validateNonCritical("AuthorityInfoAccess", X509Extensions.AuthorityInfoAccess, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        // checked in validateIssuers

        byte[] crlDistributionPoints = x509Certificate.getExtensionValue(X509Extensions.CRLDistributionPoints.getId());

        byte[] authorityInfoAccess = x509Certificate.getExtensionValue(X509Extensions.AuthorityInfoAccess.getId());
        boolean crlDistributionPointsNull = false;
        if (crlDistributionPoints == null || crlDistributionPoints.length == 0) {
            crlDistributionPointsNull = true;
        }
        boolean authorityInfoAccessNull = false;
        if (authorityInfoAccess == null || authorityInfoAccess.length == 0) {
            authorityInfoAccessNull = true;
        }
        if (crlDistributionPointsNull && authorityInfoAccessNull) {
            exceptions.add(new CertificateNormException(
                    "Even though AuthorityInfoAccess and CRLDistributionPoints are "
                            + "specified as non-mandatory extensions, "
                            + "one of them MUST be included in the certificate.", "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }

    }

    private void validateCritical(String string, DERObjectIdentifier objectIdentifier,
                                  Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs,
                                  boolean should,
                                  List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs == null || !criticalExtensionOIDs.contains(objectIdentifier.getId())) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a critical extension in the certificate.",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD be included as a critical extension in the certificate.",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            }
        }

        if (nonCriticalExtensionOIDs != null && nonCriticalExtensionOIDs.contains(objectIdentifier.getId())) {
            exceptions.add(new CertificateNormException(string
                    + " MUST always be designated as critical in the certificate (fund it as non critical).",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
    }

    private void validateNonCritical(String string, DERObjectIdentifier objectIdentifier,
                                     Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs,
                                     boolean should,
                                     List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs != null && criticalExtensionOIDs.contains(objectIdentifier.getId())) {
            exceptions.add(new CertificateNormException(string
                    + " MUST always be designated as non-critical in the certificate (fund it as critical).",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
        if (nonCriticalExtensionOIDs == null || !nonCriticalExtensionOIDs.contains(objectIdentifier.getId())) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a non-critical extension in the certificate.",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD/MAY be included as a non-critical extension in the certificate.",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            }
        }
    }

    private void validateDate(X509Certificate x509Certificate, List<CertificateException> exceptions,
                              List<CertificateException> warnings) {
        Date notAfter = x509Certificate.getNotAfter();
        Date notBefore = x509Certificate.getNotBefore();
        Calendar cl = Calendar.getInstance();
        cl.setTime(notBefore);
        cl.add(Calendar.YEAR, 1);
        if (notAfter.after(cl.getTime())) {
            warnings.add(new CertificateNormException(
                    "Certificates used by epSOS services SHOULD be valid for a maximum of 1 year.",
                    "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
    }

    private void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                          Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                          List<CertificateException> warnings) {
        validateNonCritical("ExtendedKeyUsage", X509Extensions.ExtendedKeyUsage, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        List<String> extendedKeyUsages;
        try {
            extendedKeyUsages = x509Certificate.getExtendedKeyUsage();
        } catch (CertificateParsingException e) {
            exceptions.add(e);
            return;
        }
        if (extendedKeyUsages != null) {
            validateExtendedKeyUsages(extendedKeyUsages, exceptions);
        }
    }

    protected void validateExtendedKeyUsages(List<String> extendedKeyUsage, List<CertificateException> exceptions) {
        // KeyPurposeId.
    }

    protected final void validateExtendedKeyUsagesOneValue(List<String> extendedKeyUsage, String string,
                                                           KeyPurposeId keyPurposeId,
                                                           List<CertificateException> exceptions, String chapter) {
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            if (extendedKeyUsage.size() > 1) {
                exceptions.add(new CertificateNormException(
                        "If ExtendedKeyUsages extension is included in the certificate, "
                                + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                        "epSOS WP 3.4.2 - Chapter " + chapter));
            } else {
                if (!extendedKeyUsage.get(0).equals(keyPurposeId.getId())) {
                    exceptions.add(new CertificateNormException(
                            "If ExtendedKeyUsages extension is included in the certificate, "
                                    + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                            "epSOS WP 3.4.2 - Chapter " + chapter));
                }
            }
        }
    }

    private void validateIssuers(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                                 List<CertificateException> warnings) {
        X509Certificate endCert = chain.get(0);
        X509Certificate caCert = chain.get(1);

        // 5.4.2 AuthorityKeyIdentifier

        byte[] caKeyIdentifier = null;

        byte[] subjectKeyIdentifierBytes = caCert.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        if (subjectKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        subjectKeyIdentifierBytes)).readObject());
                SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                        new ByteArrayInputStream(oct.getOctets())).readObject());

                caKeyIdentifier = keyId.getKeyIdentifier();
            } catch (Exception e) {
                pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                        "epSOS WP 3.4.2 - Chapter 5.4.2", e));
            }
        }

        if (caKeyIdentifier == null) {
            pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        } else {
            byte[] declaredCaKeyIdentifier = null;

            byte[] authorityKeyIdentifierBytes = endCert.getExtensionValue(X509Extensions.AuthorityKeyIdentifier
                    .getId());
            if (authorityKeyIdentifierBytes != null) {
                try {
                    DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                            authorityKeyIdentifierBytes)).readObject());
                    AuthorityKeyIdentifier authorityKeyIdentifier = new AuthorityKeyIdentifier(
                            (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject());

                    declaredCaKeyIdentifier = authorityKeyIdentifier.getKeyIdentifier();

                    if (authorityKeyIdentifier.getAuthorityCertIssuer() != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertIssuer SHOULD NOT be used in AuthorityKeyIdentifier.",
                                "epSOS WP 3.4.2 - Chapter 5.4.2"));
                    }
                    if (authorityKeyIdentifier.getAuthorityCertSerialNumber() != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertSerialNumber SHOULD NOT be used in AuthorityKeyIdentifier.",
                                "epSOS WP 3.4.2 - Chapter 5.4.2"));
                    }

                } catch (Exception e) {
                    pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                            "epSOS WP 3.4.2 - Chapter 5.4.2", e));
                }
            }

            if (declaredCaKeyIdentifier == null) {
                pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier of end cert",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            } else {
                if (!Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                    pkiValidation.add(new CertificateNormException(
                            "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                            "epSOS WP 3.4.2 - Chapter 5.4.2"));
                }
            }
        }

        List<String> crlDistributionPointsEndCert = new ArrayList<String>();
        List<String> crlDistributionPointsCA = new ArrayList<String>();

        try {
            getCRLDistributionPoints(endCert, crlDistributionPointsEndCert);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of end certificate",
                    "epSOS WP 3.4.2 - Chapter 5.4.2", e));
        }
        try {
            getCRLDistributionPoints(caCert, crlDistributionPointsCA);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of CA certificate",
                    "epSOS WP 3.4.2 - Chapter 5.4.2", e));
        }
        if (crlDistributionPointsEndCert.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for end certificate",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
        if (crlDistributionPointsCA.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for CA certificate",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
        if (crlDistributionPointsEndCert.size() == 1 && crlDistributionPointsCA.size() == 1) {
            if (!StringUtils.equals(crlDistributionPointsEndCert.get(0), crlDistributionPointsCA.get(0))) {
                warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD include the HTTP address from "
                        + "which the certificate-issuing authority’s complete revocation list can be retrieved.",
                        "epSOS WP 3.4.2 - Chapter 5.4.2"));
            }
        }

        String ocspEndCert0 = null;
        try {
            ocspEndCert0 = getOCSPFromAuthorityInfoAccess(endCert);
            if (ocspEndCert0 != null) {
                System.out.println(ocspEndCert0);
                URL url = new URL(ocspEndCert0);
                String protocol = url.getProtocol().toLowerCase();
                if (!StringUtils.equals(protocol, "http") && !StringUtils.equals(protocol, "https")) {
                    System.out.println(ocspEndCert0);
                    pkiValidation.add(new CertificateNormException("When the issuing CA offers an OCSP service, "
                            + "its HTTP URI MUST be included in the AuthorityInfoAccess extension. " + ocspEndCert0,
                            "epSOS WP 3.4.2 - Chapter 5.4.2"));
                }
            }
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check AuthorityInfoAccess of end certificate",
                    "epSOS WP 3.4.2 - Chapter 5.4.2", e));
        }

        // byte[] extensionValue =
        // x509Certificate.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
        // byte[] extensionValue =
        // x509Certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        // 5.4.2 BasicConstraints (CA)
        // 5.4.2 Authority Info Access (CA)
        // When the issuing CA offers an OCSP service, its HTTP URI MUST be
        // included in the extension.

    }

    private String getOCSPFromAuthorityInfoAccess(X509Certificate cert) throws CertificateException {
        DERObject auth_info_acc = getExtensionValue(cert, X509Extensions.AuthorityInfoAccess.getId());
        if (auth_info_acc != null) {
            if (auth_info_acc instanceof ASN1Sequence) {
                ASN1Sequence seq = (ASN1Sequence) auth_info_acc;
                Enumeration e = seq.getObjects();
                while (e.hasMoreElements()) {
                    Object nextElement = e.nextElement();
                    try {
                        AuthorityInformationAccess authInfoAccess = AuthorityInformationAccess.getInstance(nextElement);
                        if (authInfoAccess != null) {
                            AccessDescription[] ads = authInfoAccess.getAccessDescriptions();
                            for (int i = 0; i < ads.length; i++) {
                                if (ads[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                    GeneralName name = ads[i].getAccessLocation();
                                    if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                        String url = ((DERIA5String) name.getName()).getString();
                                        return url;
                                    }
                                }
                            }
                        }
                    } catch (IllegalArgumentException iae) {
                        AccessDescription ad = AccessDescription.getInstance(nextElement);
                        if (ad != null) {
                            if (ad.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                GeneralName name = ad.getAccessLocation();
                                if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                    String url = ((DERIA5String) name.getName()).getString();
                                    return url;
                                }
                            }
                        }
                    }
                }
            } else {
                throw new CertificateNormException("Failed to load CertificatePolicies",
                        "epSOS WP 3.4.2 - Chapter 5.4.2");
            }
        }
        return null;
    }

    private void getCRLDistributionPoints(X509Certificate endCert, List<String> crlDistributionPointsEndCert)
            throws CertificateException {
        DERObject crl_dp = getExtensionValue(endCert, X509Extensions.CRLDistributionPoints.getId());
        if (crl_dp != null) {
            CRLDistPoint crlDistPoints = CRLDistPoint.getInstance(crl_dp);
            if (crlDistPoints != null) {
                DistributionPoint[] distPoints = crlDistPoints.getDistributionPoints();
                for (int i = 0; i < distPoints.length; i++) {
                    DistributionPointName dp_name = distPoints[i].getDistributionPoint();
                    if (dp_name.getType() == DistributionPointName.FULL_NAME) {
                        GeneralName[] generalNames = GeneralNames.getInstance(dp_name.getName()).getNames();
                        for (int j = 0; j < generalNames.length; j++) {
                            if (generalNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                                String url = ((DERIA5String) generalNames[j].getName()).getString();
                                crlDistributionPointsEndCert.add(url);
                            }
                        }
                    }
                }
            }
        }
    }

    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        // if ((bits & keyUsageBits) != keyUsageBits) {
        // this.failWithError(AL_fatal, AP_certificate_unknown);
        // }
    }

    private void validateKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                  Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                  List<CertificateException> warnings) {
        validateCritical("KeyUsage", X509Extensions.KeyUsage, criticalExtensionOIDs, nonCriticalExtensionOIDs, false,
                exceptions, warnings);
        byte[] keyUsageBytes = x509Certificate.getExtensionValue(X509Extensions.KeyUsage.getId());
        if (keyUsageBytes == null) {
            exceptions.add(new CertificateNormException("KeyUsage is null", "epSOS WP 3.4.2 - Chapter 5.4.2"));
        } else {
            ASN1Object obj;
            try {
                obj = X509ExtensionUtil.fromExtensionValue(keyUsageBytes);
            } catch (IOException e) {
                exceptions.add(new CertificateNormException(e, "epSOS WP 3.4.2 - Chapter 5.4.2"));
                return;
            }
            DERBitString keyUsage = KeyUsage.getInstance(obj);
            int bits = keyUsage.getBytes()[0] & 0xff;
            validateKeyUsage(bits, exceptions);
        }
    }

    protected final void validateKeyUsageValue(int bits, int keyUsageBits, String error,
                                               List<CertificateException> exceptions, String chapter) {
        // if ((bits & keyUsageBits) != keyUsageBits) {
        if (bits != keyUsageBits) {
            exceptions.add(new CertificateNormException(error + keyUsageToString(bits), "epSOS WP 3.4.2 - Chapter "
                    + chapter));
        }
    }

    private String keyUsageToString(int bits) {
        List<String> usages = new ArrayList<String>();
        if ((bits & KeyUsage.digitalSignature) == KeyUsage.digitalSignature) {
            usages.add("digitalSignature");
        }
        if ((bits & KeyUsage.nonRepudiation) == KeyUsage.nonRepudiation) {
            usages.add("nonRepudiation");
        }
        if ((bits & KeyUsage.keyEncipherment) == KeyUsage.keyEncipherment) {
            usages.add("keyEncipherment");
        }
        if ((bits & KeyUsage.dataEncipherment) == KeyUsage.dataEncipherment) {
            usages.add("dataEncipherment");
        }
        if ((bits & KeyUsage.keyAgreement) == KeyUsage.keyAgreement) {
            usages.add("keyAgreement");
        }
        if ((bits & KeyUsage.keyCertSign) == KeyUsage.keyCertSign) {
            usages.add("keyCertSign");
        }
        if ((bits & KeyUsage.cRLSign) == KeyUsage.cRLSign) {
            usages.add("cRLSign");
        }
        if ((bits & KeyUsage.encipherOnly) == KeyUsage.encipherOnly) {
            usages.add("encipherOnly");
        }
        if ((bits & KeyUsage.decipherOnly) == KeyUsage.decipherOnly) {
            usages.add("decipherOnly");
        }
        return showFund(usages);
    }

    private void validateSerialNumber(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        byte[] byteArray = x509Certificate.getSerialNumber().toByteArray();
        if (byteArray.length > 20) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous integer value with a maximum of 20 bytes.",
                    "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
        int compareTo = x509Certificate.getSerialNumber().compareTo(BigInteger.valueOf(0));
        if (compareTo < 0) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous positive integer value.",
                    "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
    }

    private void validateSignatureAlgorithm(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        // x509Certificate.getPublicKey(). FIXME
    }

    public void validateSubject(String name, List<CertificateException> exceptions,
                                List<CertificateException> warnings) {
        X509Name x509name;
        try {
            x509name = new X509Name(name);
        } catch (IllegalArgumentException e) {
            exceptions.add(new CertificateNormException(e, "epSOS WP 3.4.2 - Chapter 5.4.1"));
            return;
        }

        validateSubjectCountry(x509name, exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.O, 64, true, false, "O", exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.CN, 64, true, false, "CN", exceptions, warnings);

        validateSubjectAttribute(x509name, X509Name.T, 64, true, true, "T", exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.GIVENNAME, 64, true, true, "G", exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.SURNAME, 64, true, true, "SURNAME", exceptions, warnings);
        validateSubjectAttribute(x509name, X509Name.OU, 64, true, true, "OU", exceptions, warnings);

        Vector<DERObjectIdentifier> oids = new Vector<DERObjectIdentifier>();
        oids.add(X509Name.DN_QUALIFIER);
        oids.add(X509Name.C);
        oids.add(X509Name.O);
        oids.add(X509Name.CN);
        oids.add(X509Name.T);
        oids.add(X509Name.GIVENNAME);
        oids.add(X509Name.SURNAME);
        oids.add(X509Name.OU);
        Vector<DERObjectIdentifier> oiDs = x509name.getOIDs();

        for (DERObjectIdentifier oid : oiDs) {
            if (!oids.contains(oid)) {
                String oidName = (String) X509Name.DefaultSymbols.get(oid);
                if (oidName == null) {
                    oidName = oid.toString();
                }
                warnings.add(new CertificateNormException(oidName + " SHOULD NOT be provided",
                        "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
            if (oid.equals(X509Name.OU)) {
                warnings.add(new CertificateNormException("The DName MAY have OU.", "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
        }
    }

    private String validateSubjectAttribute(X509Name name, DERObjectIdentifier identifier, int length,
                                            boolean maxBytes, boolean should, String identifierName,
                                            List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        Vector<String> vector = getSubjectVector(name, identifier);

        if (should) {
            if (vector == null || vector.size() == 0) {
                warnings.add(new CertificateNormException("The DName SHOULD have " + identifierName + ".",
                        "epSOS WP 3.4.2 - Chapter 5.4.1"));
                return null;
            }
        } else {
            if (vector == null || vector.size() == 0) {
                exceptions.add(new CertificateNormException("The DName MUST have " + identifierName + ".",
                        "epSOS WP 3.4.2 - Chapter 5.4.1"));
                return null;
            }
        }

        if (vector.size() != 1) {
            exceptions.add(new CertificateNormException("The DName MUST have only one " + identifierName + ".",
                    "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
        String value = vector.get(0);

        if (maxBytes) {
            if (value == null || value.length() > length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
        } else {
            if (value == null || value.length() != length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", "epSOS WP 3.4.2 - Chapter 5.4.1"));
            }
        }
        return value;
    }

    private void validateSubjectCountry(X509Name name, List<CertificateException> exceptions,
                                        List<CertificateException> warnings) {
        String country = validateSubjectAttribute(name, X509Name.C, 2, false, false, "C", exceptions, warnings);
        int indexOfCountry = countries.indexOf(country);
        if (indexOfCountry == -1) {
            exceptions.add(new CertificateNormException("C MUST be a ISO 3166 code", "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
    }

    private void validateSubjectKeyIdentifier(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                              Set<String> nonCriticalExtensionOIDs,
                                              List<CertificateException> exceptions,
                                              List<CertificateException> warnings) {
        byte[] value = x509Certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier.getId());
        if (value == null || value.length == 0) {
            exceptions.add(new CertificateNormException(
                    "SubjectKeyIdentifier MUST be included as an extension in the certificate.",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
        validateNonCritical("SubjectKeyIdentifier", X509Extensions.SubjectKeyIdentifier, criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.
        try {
            SubjectKeyIdentifierStructure subjectKeyIdentifierStructure = new SubjectKeyIdentifierStructure(
                    x509Certificate.getPublicKey());
            byte[] goodKeyIdentifier = subjectKeyIdentifierStructure.getKeyIdentifier();

            byte[] subjectKeyIdentifierBytes = x509Certificate.getExtensionValue(X509Extensions.SubjectKeyIdentifier
                    .getId());
            byte[] providedKeyIdentifier = null;
            if (subjectKeyIdentifierBytes != null) {
                try {
                    DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                            subjectKeyIdentifierBytes)).readObject());
                    SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                            new ByteArrayInputStream(oct.getOctets())).readObject());

                    providedKeyIdentifier = keyId.getKeyIdentifier();
                } catch (Exception e) {
                    exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                            "epSOS WP 3.4.2 - Chapter 5.4.2", e));
                }
            }
            if (providedKeyIdentifier != null) {
                if (!Arrays.equals(providedKeyIdentifier, goodKeyIdentifier)) {
                    exceptions.add(new CertificateNormException(
                            "One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.",
                            "epSOS WP 3.4.2 - Chapter 5.4.2"));
                }
            }
        } catch (CertificateParsingException e) {
            exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                    "epSOS WP 3.4.2 - Chapter 5.4.2"));
        }
    }

    private void validateVersion(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        if (x509Certificate.getVersion() != 3) {
            exceptions.add(new CertificateNormException("Certificates to be deployed MUST be v3.",
                    "epSOS WP 3.4.2 - Chapter 5.4.1"));
        }
    }

}
