package net.ihe.gazelle.pki.crl;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;

import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;

public class CrlDefaultProvider implements CrlProvider {

    public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
        X509V2CRLGenerator generator = new X509V2CRLGenerator();

        X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
        generator.setIssuerDN(caCertificate.getSubjectX500Principal());
        generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
        generator.setThisUpdate(new Date());
        generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24));

        addCertificateAuthority(certificateAuthority, generator);
        AuthorityKeyIdentifierStructure authorityKeyIdentifier;
        try {
            authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(certificateAuthority.getPublicKey().getKey());
        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        }
        generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

        CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
        generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);
        certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

        return generateCRLWithOneException(certificateAuthority, generator);
    }

    private void addCertificateAuthority(Certificate certificateAuthority, X509V2CRLGenerator generator)
            throws CertificateException {
        List<Certificate> certificates = certificateAuthority.getCertificates();
        for (Certificate certificate : certificates) {
            if (certificate.isRevoked()) {
                BigInteger serialNumber = certificate.getCertificateX509().getX509Certificate().getSerialNumber();
                generator.addCRLEntry(serialNumber, certificate.getRevokedDate(), certificate.getRevokedReason()
                        .getCode());
            }
            addCertificateAuthority(certificate, generator);
        }
    }

    public static byte[] generateCRLWithOneException(Certificate certificateAuthority, X509V2CRLGenerator generator)
            throws CertificateException {
        byte[] result;
        try {
            X509CRL generated = generator.generate(certificateAuthority.getPrivateKey().getKey());
            result = generated.getEncoded();
        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        } catch (CRLException e) {
            throw new CertificateException(e);
        } catch (IllegalStateException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        } catch (SignatureException e) {
            throw new CertificateException(e);
        }
        return result;
    }
}
