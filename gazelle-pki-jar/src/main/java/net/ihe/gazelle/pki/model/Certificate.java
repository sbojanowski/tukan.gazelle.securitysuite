package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.dates.TimestampNano;
import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.enums.RevokedReason;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;

import javax.persistence.*;
import java.io.*;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

@Entity
@Table(name = "pki_certificate", schema = "public")
@SequenceGenerator(name = "pki_certificate_id", sequenceName = "pki_certificate_id_seq", allocationSize = 1)
public class Certificate implements Serializable {

    private static final long serialVersionUID = 2588242106637524422L;

    private enum KeystoreType {
        JKS, PKCS12
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pki_certificate_id")
    private Integer id;

    @Column(name = "subject")
    private String subject;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private CertificateRequest request;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CertificatePublicKey publicKey;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CertificatePrivateKey privateKey;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CertificateX509 certificateX509;

    private boolean revoked;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp with time zone")
    private Date revokedDate;

    private RevokedReason revokedReason;

    @ManyToOne(fetch = FetchType.EAGER)
    private Certificate certificateAuthority;

    @Column(name = "serialnumber_counter")
    private int serialNumberCounter;

    private int crlNumber;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "certificateAuthority", fetch = FetchType.LAZY)
    private List<Certificate> certificates;

    @Column(name = "is_custom")
    private boolean custom = false;

    public Certificate() {
        super();
        publicKey = new CertificatePublicKey();
        privateKey = new CertificatePrivateKey();
        certificateX509 = new CertificateX509();
        serialNumberCounter = 1;
        crlNumber = 1;
        revoked = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public CertificatePublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(CertificatePublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public CertificateRequest getRequest() {
        return request;
    }

    public void setRequest(CertificateRequest request) {
        this.request = request;
    }

    public CertificatePrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(CertificatePrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public CertificateX509 getCertificateX509() {
        return certificateX509;
    }

    public void setCertificateX509(CertificateX509 certificateX509) {
        this.certificateX509 = certificateX509;
    }

    public List<Certificate> getRealChain() throws CertificateException {
        List<Certificate> list = new ArrayList<Certificate>();
        addRealCertificate(list);
        return Collections.unmodifiableList(list);
    }

    public List<X509Certificate> getChain() throws CertificateException {
        List<X509Certificate> list = new ArrayList<X509Certificate>();
        addCertificate(list);
        return Collections.unmodifiableList(list);
    }

    protected void addCertificate(List<X509Certificate> list) throws CertificateException {
        list.add(getCertificateX509().getX509Certificate());
        if (getCertificateAuthority() != null) {
            getCertificateAuthority().addCertificate(list);
        }
    }

    protected void addRealCertificate(List<Certificate> list) throws CertificateException {
        list.add(this);
        if (getCertificateAuthority() != null) {
            getCertificateAuthority().addRealCertificate(list);
        }
    }

    public X509Certificate[] getChainAsArray() throws CertificateException {
        List<X509Certificate> chain = getChain();
        X509Certificate[] result = new X509Certificate[chain.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = chain.get(i);
        }
        return result;
    }

    public boolean isRevoked() {
        return revoked;
    }

    public Date getRevokedDate() {
        return revokedDate;
    }

    public RevokedReason getRevokedReason() {
        return revokedReason;
    }

    public Certificate getCertificateAuthority() {
        return certificateAuthority;
    }

    public void setCertificateAuthority(Certificate certificateAuthority) {
        this.certificateAuthority = certificateAuthority;
    }

    public int getNextSerialNumber() {
        this.serialNumberCounter += 1;
        return serialNumberCounter;
    }

    public int getSerialNumberCounter() {
        return this.serialNumberCounter;
    }

    public String getSerialNumber() {
        BigInteger serialNumber;
        try {
            serialNumber = this.getCertificateX509().getX509Certificate().getSerialNumber();
        } catch (Exception e) {
            return "Unable to read X.509 Serial Number";
        }
        return serialNumber.toString();
    }

    public int getCrlNumber() {
        return crlNumber;
    }

    public void setCrlNumber(int crlNumber) {
        this.crlNumber = crlNumber;
    }

    public List<Certificate> getCertificates() {
        if (certificates == null) {
            certificates = new ArrayList<Certificate>();
        }
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public byte[] getJKS() throws CertificateException {
        return getJKS("password", "tomcat");
    }

    public byte[] getP12() throws CertificateException {
        return getP12("password");
    }

    public byte[] getJKS(String keystorePassword, String alias) throws CertificateException {
        return getKeystore(keystorePassword, alias, KeystoreType.JKS, "SUN");
    }

    public byte[] getP12(String keystorePassword) throws CertificateException {

        String alias = "imported";

        X509Principal principal = new X509Principal(getSubject());
        Vector names = principal.getValues(X509Principal.CN);
        for (Object object : names) {
            if (object != null) {
                alias = object.toString();
            }
        }

        return getKeystore(keystorePassword, alias, KeystoreType.PKCS12, BouncyCastleProvider.PROVIDER_NAME);
    }

    private byte[] getKeystore(String keystorePassword, String alias, KeystoreType type, String provider)
            throws CertificateException {
        try {
            KeyStore store = KeyStore.getInstance(type.name(), provider);
            store.load(null, null);
            java.security.cert.Certificate[] chainArray = getChainAsArray();
            if (type == KeystoreType.JKS) {
                store.setCertificateEntry("rootCA", getRootCertificateAuthority());
            }

            char[] effectivePassword = CertificateConstants.passwd;
            if (keystorePassword != null && !"".equals(keystorePassword)) {
                effectivePassword = keystorePassword.toCharArray();
            }
            store.setKeyEntry(alias, privateKey.getKey(), effectivePassword, chainArray);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            store.store(bos, effectivePassword);
            bos.close();
            return bos.toByteArray();
        } catch (NoSuchProviderException e) {
            throw new CertificateException(e);
        } catch (KeyStoreException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        } catch (java.security.cert.CertificateException e) {
            throw new CertificateException(e);
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

    private X509Certificate getRootCertificateAuthority() throws CertificateException {
        if (getCertificateAuthority() != null) {
            return getCertificateAuthority().getRootCertificateAuthority();
        }
        return this.getCertificateX509().getX509Certificate();
    }

    public String getPEM() throws CertificateException {
        try {
            ByteArrayOutputStream sb = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(sb);
            PEMWriter pemWriter = new PEMWriter(writer);

            pemWriter.writeObject(certificateX509.getX509Certificate());

            pemWriter.close();
            writer.close();
            sb.close();

            return new String(sb.toByteArray(), CertificateConstants.UTF_8);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public String getKEY() throws CertificateException {
        try {
            ByteArrayOutputStream sb = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(sb);
            PEMWriter pemWriter = new PEMWriter(writer);

            pemWriter.writeObject(getPrivateKey().getKey());

            pemWriter.close();
            writer.close();
            sb.close();

            return new String(sb.toByteArray(), CertificateConstants.UTF_8);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public String getChainAsPEM() throws CertificateException {
        try {
            X509Certificate[] chainAsArray = getChainAsArray();
            ByteArrayOutputStream sb = new ByteArrayOutputStream();

            Writer writer = new OutputStreamWriter(sb);
            PEMWriter pemWriter = new PEMWriter(writer);
            for (X509Certificate x509Certificate : chainAsArray) {
                pemWriter.writeObject(x509Certificate);
            }
            pemWriter.close();
            writer.close();
            sb.close();
            return new String(sb.toByteArray(), CertificateConstants.UTF_8);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString()).append(CertificateConstants.NEW_LINE);
        sb.append("ID             : ").append(id).append(CertificateConstants.NEW_LINE);
        sb.append("Subject        : ").append(subject).append(CertificateConstants.NEW_LINE);
        sb.append("Public key     : ").append(publicKey.toString()).append(CertificateConstants.NEW_LINE);
        // sb.append("Private key    : ").append(privateKey.toString()).append(CertificateConstants.NEW_LINE);
        sb.append("Serial number  : ").append(serialNumberCounter).append(CertificateConstants.NEW_LINE);
        sb.append("Crl number     : ").append(crlNumber).append(CertificateConstants.NEW_LINE);
        if (certificates != null) {
            sb.append("Issued certificates : ").append(certificates.size()).append(CertificateConstants.NEW_LINE);
        }
        if (certificateAuthority == null) {
            sb.append("CA             : ").append("None").append(CertificateConstants.NEW_LINE);
        } else {
            sb.append("CA             : ").append(certificateAuthority.getSubject())
                    .append(CertificateConstants.NEW_LINE);
        }
        sb.append("Revoked        : ").append(revoked).append(CertificateConstants.NEW_LINE);
        sb.append("Revoked date   : ").append(revokedDate).append(CertificateConstants.NEW_LINE);
        sb.append("Revoked reason : ").append(revokedReason).append(CertificateConstants.NEW_LINE);
        List<X509Certificate> chain;
        try {
            chain = getChain();
            sb.append("Chain length   : ").append(chain.size()).append(CertificateConstants.NEW_LINE);
            int i = 0;
            for (X509Certificate certificate : chain) {
                if (certificate == null) {
                    sb.append("Chain[").append(i).append("] : null");
                } else {
                    sb.append("Chain[").append(i).append("] : ").append(CertificateConstants.NEW_LINE);
                    sb.append("************").append(CertificateConstants.NEW_LINE);
                    sb.append(certificate.toString());
                    sb.append("************").append(CertificateConstants.NEW_LINE);
                }
                i++;
            }
        } catch (CertificateException e) {
            sb.append("Failed to load chain").append(CertificateConstants.NEW_LINE);
        }

        return sb.toString();
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Certificate other = (Certificate) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public void revoke(RevokedReason reason) {
        revokedReason = reason;
        revokedDate = new TimestampNano();
        revoked = true;
    }

}
