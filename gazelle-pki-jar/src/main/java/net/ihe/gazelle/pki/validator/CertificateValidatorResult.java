package net.ihe.gazelle.pki.validator;

import java.util.ArrayList;
import java.util.List;

public class CertificateValidatorResult {

    private CertificateValidatorResultEnum result;

    private String validatedCertificates;

    private List<CertificateValidatorErrorTrace> errors = new ArrayList<CertificateValidatorErrorTrace>();

    private List<CertificateValidatorErrorTrace> warnings = new ArrayList<CertificateValidatorErrorTrace>();

    public CertificateValidatorResult() {
        super();
    }

    public CertificateValidatorResultEnum getResult() {
        return result;
    }

    public void setResult(CertificateValidatorResultEnum result) {
        this.result = result;
    }

    public List<CertificateValidatorErrorTrace> getErrors() {
        return errors;
    }

    public void setErrors(List<CertificateValidatorErrorTrace> errors) {
        this.errors = errors;
    }

    public List<CertificateValidatorErrorTrace> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<CertificateValidatorErrorTrace> warnings) {
        this.warnings = warnings;
    }

    public void setValidatedCertificates(String validatedCertificates) {
        this.validatedCertificates = validatedCertificates;
    }

    public String getValidatedCertificates() {
        return validatedCertificates;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("result=");
        sb.append(result.toString());
        sb.append("\r\n");
        if (errors.size() > 0) {
            sb.append(" errors list :");
            sb.append("\r\n");
            for (CertificateValidatorErrorTrace error : errors) {
                sb.append(error.getCleanMessage());
                sb.append("\r\n");
            }
        }
        if (warnings.size() > 0) {
            sb.append(" warning list :");
            sb.append("\r\n");
            for (CertificateValidatorErrorTrace warning : warnings) {
                sb.append(warning.getCleanMessage());
                sb.append("\r\n");
            }
        }
        return sb.toString();
    }

    public boolean containsError(String errorString) {
        for (CertificateValidatorErrorTrace error : errors) {
            if (error.getExceptionMessage().contains(errorString)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsWarning(String errorString) {
        for (CertificateValidatorErrorTrace error : warnings) {
            if (error.getExceptionMessage().contains(errorString)) {
                return true;
            }
        }
        return false;
    }

    public String toLongString() {
        StringBuilder sb = new StringBuilder();
        sb.append("result=");
        sb.append(result.toString());
        sb.append("\r\n");
        if (errors.size() > 0) {
            sb.append(" errors list :");
            sb.append("\r\n");
            for (CertificateValidatorErrorTrace error : errors) {
                sb.append(error.toLongString());
                sb.append("\r\n");
            }
        }
        if (warnings.size() > 0) {
            sb.append(" warning list :");
            sb.append("\r\n");
            for (CertificateValidatorErrorTrace warning : warnings) {
                sb.append(warning.toLongString());
                sb.append("\r\n");
            }
        }
        return sb.toString();
    }
}
