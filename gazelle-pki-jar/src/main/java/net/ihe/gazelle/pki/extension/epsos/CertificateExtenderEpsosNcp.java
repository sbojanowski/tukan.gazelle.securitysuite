package net.ihe.gazelle.pki.extension.epsos;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import net.ihe.gazelle.pki.X509CertificateParametersContainer;

import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CertificateExtenderEpsosNcp extends CertificateExtenderEpsos {

    @Override
    public void addExtension(X509V3CertificateGenerator certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);
        int keyUsage = KeyUsage.digitalSignature | KeyUsage.nonRepudiation;
        certGen.addExtension(X509Extensions.KeyUsage, true, new X509KeyUsage(keyUsage));
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // nothing
    }

}
