package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.X509Principal;

import javax.persistence.*;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

@Entity
@Table(name = "pki_certificate_request", schema = "public")
@Inheritance
@DiscriminatorColumn(length = RequestConstants.REQUEST_CLASS_NAME_MAX_LENGTH)
@SequenceGenerator(name = "pki_certificate_request_id", sequenceName = "pki_certificate_request_id_seq", allocationSize = 1)
public abstract class CertificateRequest implements Serializable {

    // http://www.bouncycastle.org/jira/browse/BJA-309
    public static Hashtable oidSymbols;
    static {
        oidSymbols = new Hashtable(X509Name.DefaultSymbols);

    }
    private static final long serialVersionUID = 6123699149030712356L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pki_certificate_request_id")
    private Integer id;

    private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.SHA512WITHRSAENCRYPTION;

    private String subject;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp with time zone")
    private Date notBefore = CertificateUtil.getNotBefore();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp with time zone")
    private Date notAfter = CertificateUtil.getNotAfter();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CertificatePublicKey publicKey = new CertificatePublicKey();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CertificatePrivateKey privateKey = new CertificatePrivateKey();

    @OneToOne(fetch = FetchType.EAGER)
    private Certificate certificateAuthority;

    private CertificateType certificateExtension;

    @OneToOne(mappedBy = "request", fetch = FetchType.LAZY)
    private Certificate certificate;

    private String requesterUsername;

    @Column(name = "requester_cas_key", length = 8)
    private String requesterCasKey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "request_date", columnDefinition = "timestamp with time zone")
    private Date requestDate;

    public CertificateRequest() {
        super();
    }

    public Certificate getCertificateAuthority() {
        return certificateAuthority;
    }

    public CertificateType getCertificateExtension() {
        return certificateExtension;
    }

    public CertificateExtender getCertificateExtender() {
        return certificateExtension;
    }

    public Integer getId() {
        return id;
    }

    public Date getNotAfter() {
        return (Date) notAfter.clone();
    }

    public Date getNotBefore() {
        return (Date) notBefore.clone();
    }

    public PublicKey getPublicKey() throws CertificateException {
        return publicKey.getKey();
    }

    public CertificatePublicKey getCertificatePublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() throws CertificateException {
        return privateKey.getKey();
    }

    public CertificatePrivateKey getCertificatePrivateKey() {
        return privateKey;
    }

    public String getRequesterUsername() {
        return requesterUsername;
    }

    public String getRequesterCasKey() {
        return requesterCasKey;
    }

    public String getRequesterUsernameWithKey() {
        if (requesterCasKey != null && !requesterCasKey.isEmpty()) {
            return requesterUsername + " (" + requesterCasKey + ")";
        } else {
            return requesterUsername;
        }
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public String getSubject() {
        return subject;
    }

    public Date getRequestDate() {
        return (Date) requestDate.clone() ;
    }

    public void setCertificateAuthority(Certificate certificateAuthority) {
        this.certificateAuthority = certificateAuthority;
    }

    public void setCertificateExtension(CertificateType certificateExtension) {
        this.certificateExtension = certificateExtension;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = (Date) notAfter.clone();
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = (Date) notBefore.clone();
    }

    public void setPrivateKey(PrivateKey privateKey) throws CertificateException {
        this.privateKey.setKey(privateKey);
    }

    public void setPublicKey(PublicKey publicKey) throws CertificateException {
        this.publicKey.setKey(publicKey);
    }

    public void setRequester(String username, String casKey) {
        this.requesterUsername = username;
        this.requesterCasKey = casKey;
    }

    public void setSignatureAlgorithm(SignatureAlgorithm signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public void setSubjectUsingAttributes(String country, String organization, String commonName, String title,
                                          String givenName, String surname, String organizationalUnit, String eMail) {
        subject = computeSubjectUsingAttributes(country, organization, commonName, title, givenName, surname,
                organizationalUnit, eMail);
    }

    @PrePersist
    public void initRequestDate() {
        this.requestDate = new Date();
    }

    protected String computeSubjectUsingAttributes(String country, String organization, String commonName, String title,
                                                   String givenName, String surname, String organizationalUnit, String eMail) {
        Hashtable<DERObjectIdentifier, String> attrs = new Hashtable<DERObjectIdentifier, String>();
        Vector<DERObjectIdentifier> order = new Vector<DERObjectIdentifier>();

        attrs.put(X509Principal.C, country);
        order.addElement(X509Principal.C);

        attrs.put(X509Principal.O, organization);
        order.addElement(X509Principal.O);

        attrs.put(X509Principal.CN, commonName);
        order.addElement(X509Principal.CN);

        if (StringUtils.trimToNull(title) != null) {
            attrs.put(X509Principal.T, title);
            order.addElement(X509Principal.T);
        }
        if (StringUtils.trimToNull(givenName) != null) {
            attrs.put(X509Principal.GIVENNAME, givenName);
            order.addElement(X509Principal.GIVENNAME);
        }
        if (StringUtils.trimToNull(surname) != null) {
            attrs.put(X509Principal.SURNAME, surname);
            order.addElement(X509Principal.SURNAME);
        }
        if (StringUtils.trimToNull(organizationalUnit) != null) {
            attrs.put(X509Principal.OU, organizationalUnit);
            order.addElement(X509Principal.OU);
        }
        if (StringUtils.trimToNull(eMail) != null) {
            attrs.put(X509Principal.E, eMail);
            order.addElement(X509Principal.E);
        }

        X509Principal principal = new X509Principal(order, attrs);
        return principal.toString(X509Name.DefaultReverse, oidSymbols);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((privateKey == null) ? 0 : privateKey.hashCode());
        result = prime * result + ((publicKey == null) ? 0 : publicKey.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CertificateRequest other = (CertificateRequest) obj;
        if (privateKey == null) {
            if (other.privateKey != null)
                return false;
        } else if (!privateKey.equals(other.privateKey))
            return false;
        if (publicKey == null) {
            if (other.publicKey != null)
                return false;
        } else if (!publicKey.equals(other.publicKey))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        return true;
    }

}
