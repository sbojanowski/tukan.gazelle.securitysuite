package net.ihe.gazelle.pki.validator;

public enum CertificateValidatorResultEnum {

    FAILED, PASSED;

}
