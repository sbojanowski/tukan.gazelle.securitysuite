package net.ihe.gazelle.pki.enums;

public enum KeyAlgorithm {
    RSA("RSA"),

    ECDSA("ECDSA"),

    DSA("DSA");

    private String keySpec;

    private KeyAlgorithm(String keySpec) {
        this.keySpec = keySpec;
    }

    public String getKeySpec() {
        return keySpec;
    }

}
