package net.ihe.gazelle.pki;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.persistence.EntityManager;

import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;

import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CertificateManager {

    protected static int testCertificateId = 0;

    private CertificateManager() {
        super();
    }

    public static Certificate createCertificate(CertificateRequest certificateRequest, EntityManager entityManager)
            throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = null ;

        if(entityManager != null) {
            certificateAuthority = entityManager.find(
                    Certificate.class, certificateRequest.getCertificateAuthority().getId());
        } else {
            certificateAuthority = certificateRequest.getCertificateAuthority() ;
        }

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate() == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(testCertificateId++);
        }

        int serialNumber = certificateAuthority.getNextSerialNumber();
        X509Certificate cert = createX509Certificate(certificateRequest, publicKey, certificateAuthority,
                BigInteger.valueOf(serialNumber), certificate);
        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            certificate = entityManager.merge(certificate);
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

    public static Certificate createSelfSignedCertificate(CertificateRequest certificateRequest,
                                                          EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();

        X509Principal issuerPrincipal = new X509Principal(certificateRequest.getSubject());
        X509Principal subjectPrincipal = new X509Principal(certificateRequest.getSubject());

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificate.setSubject(certificateRequest.getSubject());
        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(testCertificateId++);
        }

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerPrincipal, publicKey, BigInteger.valueOf(1), subjectPrincipal, certificate.getId());

        X509Certificate cert = null;

        X509V3CertificateGenerator certificateGenerator = getCertificateGenerator(parameters);
        cert = generateCertificate(privateKey, certificateGenerator, parameters);

        certificate.getCertificateX509().setX509Certificate(cert);

        if (entityManager != null) {
            entityManager.merge(certificate);
        }

        return certificate;
    }

    public static void updateCertificate(CertificateRequest certificateRequest, EntityManager entityManager)
            throws CertificateException {
        Certificate certificate = certificateRequest.getCertificate();
        if (certificate != null) {
            PublicKey publicKey = certificateRequest.getPublicKey();

            Certificate certificateAuthority = certificateRequest.getCertificateAuthority();
            if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                    || certificateAuthority.getCertificateX509().getX509Certificate() == null) {
                throw new IllegalArgumentException("Invalid certificateAuthority");
            }
            int serialNumber = certificate.getCertificateX509().getX509Certificate().getSerialNumber().intValue();
            X509Certificate cert = createX509Certificate(certificateRequest, publicKey, certificateAuthority,
                    BigInteger.valueOf(serialNumber), certificate);
            certificate.getCertificateX509().setX509Certificate(cert);
            if (entityManager != null) {
                entityManager.merge(certificate);
                entityManager.merge(certificateRequest);
            }
        }
    }

    static X509Certificate createX509Certificate(CertificateRequest certificateRequest, PublicKey publicKey,
                                                 Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate)
            throws CertificateException {
        X509Certificate caCert = certificateAuthority.getCertificateX509().getX509Certificate();

        X509Principal issuerPrincipal;
        try {
            issuerPrincipal = PrincipalUtil.getSubjectX509Principal(caCert);
        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        }
        X509Principal subjectPrincipal = new X509Principal(certificateRequest.getSubject());

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerPrincipal, publicKey, serialNumber, subjectPrincipal, certificate.getId());
        X509V3CertificateGenerator certificateGenerator = getCertificateGenerator(parameters);

        X509Certificate cert = generateCertificate(certificateAuthority.getPrivateKey().getKey(), certificateGenerator,
                parameters);
        return cert;
    }

    public static Certificate createCertificateAuthority(CertificateRequestAuthority certificateRequest,
                                                         EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();

        X509Principal issuerPrincipal = new X509Principal(certificateRequest.getIssuer());
        X509Principal subjectPrincipal = new X509Principal(certificateRequest.getSubject());

        Certificate ca = new Certificate();
        ca.setRequest(certificateRequest);
        ca.setSubject(certificateRequest.getSubject());
        ca.getPublicKey().setKey(publicKey);
        ca.getPrivateKey().setKey(privateKey);
        if (entityManager != null) {
            entityManager.persist(ca);
        } else {
            ca.setId(testCertificateId++);
        }

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerPrincipal, publicKey, BigInteger.valueOf(1), subjectPrincipal, ca.getId());

        X509Certificate cert = null;
        switch (certificateRequest.getCertificateVersion()) {
            case V1:
                X509V1CertificateGenerator certificateGeneratorv1 = getCertificateGeneratorV1(parameters);
                cert = generateCertificateV1(privateKey, certificateGeneratorv1, parameters);
                break;
            case V3:
            default:
                X509V3CertificateGenerator certificateGenerator = getCertificateGenerator(parameters);
                cert = generateCertificate(privateKey, certificateGenerator, parameters);
                break;
        }
        ca.getCertificateX509().setX509Certificate(cert);

        if (entityManager != null) {
            entityManager.merge(ca);
        }

        return ca;
    }

    public static void updateCertificateAuthority(Certificate certificate, EntityManager entityManager)
            throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);

        X509Principal issuerPrincipal = new X509Principal(certificate.getSubject());
        X509Principal subjectPrincipal = new X509Principal(certificate.getSubject());
        PublicKey publicKey = certificate.getPublicKey().getKey();
        PrivateKey privateKey = certificate.getPrivateKey().getKey();

        cra.setSubject(certificate.getSubject());
        cra.setIssuer(certificate.getSubject());

        cra.setPublicKey(publicKey);
        cra.setPrivateKey(privateKey);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);

        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(cra, issuerPrincipal, publicKey,
                BigInteger.valueOf(1), subjectPrincipal, certificate.getId());

        X509V3CertificateGenerator certificateGenerator = getCertificateGenerator(parameters);
        X509Certificate newCert = generateCertificate(privateKey, certificateGenerator, parameters);

        certificate.setRequest(cra);
        certificate.getCertificateX509().setX509Certificate(newCert);

        entityManager.merge(certificate);
    }

    static X509V3CertificateGenerator getCertificateGenerator(X509CertificateParametersContainer parameters)
            throws CertificateException {
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        certGen.setSerialNumber(parameters.getSerialNumber());
        certGen.setIssuerDN(parameters.getIssuerPrincipal());
        certGen.setNotBefore(parameters.getCertificateRequest().getNotBefore());
        certGen.setNotAfter(parameters.getCertificateRequest().getNotAfter());
        certGen.setSubjectDN(parameters.getSubjectPrincipal());
        certGen.setPublicKey(parameters.getPublicKey());
        certGen.setSignatureAlgorithm(parameters.getCertificateRequest().getSignatureAlgorithm().getAlgorithmName());
        CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
        if (certificateExtension != null) {
            certificateExtension.addExtension(certGen, parameters);
        }
        return certGen;
    }

    static X509V1CertificateGenerator getCertificateGeneratorV1(X509CertificateParametersContainer parameters) {
        X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
        certGen.setSerialNumber(parameters.getSerialNumber());
        certGen.setIssuerDN(parameters.getIssuerPrincipal());
        certGen.setNotBefore(parameters.getCertificateRequest().getNotBefore());
        certGen.setNotAfter(parameters.getCertificateRequest().getNotAfter());
        certGen.setSubjectDN(parameters.getSubjectPrincipal());
        certGen.setPublicKey(parameters.getPublicKey());
        certGen.setSignatureAlgorithm(parameters.getCertificateRequest().getSignatureAlgorithm().getAlgorithmName());
        return certGen;
    }

    static X509Certificate generateCertificate(PrivateKey privateKey, X509V3CertificateGenerator certGen,
                                               X509CertificateParametersContainer parameters) throws CertificateException {
        X509Certificate cert;
        try {
            cert = certGen.generate(privateKey, BouncyCastleProvider.PROVIDER_NAME);

            CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
            if (certificateExtension != null) {
                certificateExtension.modifyCertificate(cert, parameters);
            }

        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        } catch (IllegalStateException e) {
            throw new CertificateException(e);
        } catch (NoSuchProviderException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        } catch (SignatureException e) {
            throw new CertificateException(e);
        }
        return cert;
    }

    static X509Certificate generateCertificateV1(PrivateKey private1, X509V1CertificateGenerator certificateGenerator,
                                                 X509CertificateParametersContainer parameters) throws CertificateException {
        X509Certificate cert;
        try {
            cert = certificateGenerator.generate(private1, BouncyCastleProvider.PROVIDER_NAME);

            CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
            if (certificateExtension != null) {
                certificateExtension.modifyCertificate(cert, parameters);
            }

        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        } catch (InvalidKeyException e) {
            throw new CertificateException(e);
        } catch (IllegalStateException e) {
            throw new CertificateException(e);
        } catch (NoSuchProviderException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        } catch (SignatureException e) {
            throw new CertificateException(e);
        }
        return cert;
    }

    public static Certificate getCertificateFromX509(EntityManager entityManager, X509Certificate x509Certificate,
                                                     KeyPair keys) throws CertificateException {

        Certificate certificate = new Certificate();

        Certificate certificateAuthority = null;
        Principal issuerDN = x509Certificate.getIssuerDN();
        Principal subjectDN = x509Certificate.getSubjectDN();

        PrivateKey privateKey = null;
        PublicKey publicKey = x509Certificate.getPublicKey();

        if (keys != null) {
            if (!keys.getPublic().equals(publicKey)) {
                throw new CertificateException("Public key from keypair not matching certificate public key");
            }
            privateKey = keys.getPrivate();
        }

        if (issuerDN != null) {
            String issuerDNName = issuerDN.getName();
            boolean shouldSetCA = true;
            if (subjectDN != null && subjectDN.getName() != null && subjectDN.getName().equals(issuerDNName)) {
                shouldSetCA = false;
            }
            if (shouldSetCA && issuerDNName != null && !"".equals(issuerDNName)) {
                certificateAuthority = CertificateDAO.getUniqueBySubject(issuerDNName, entityManager);
                if (certificateAuthority == null) {
                    throw new CertificateException(issuerDNName + " not found");
                }
                certificate.setCertificateAuthority(certificateAuthority);
            }
        }

        certificate.setCertificateAuthority(certificateAuthority);
        certificate.getCertificateX509().setX509Certificate(x509Certificate);
        certificate.setSubject(x509Certificate.getSubjectDN().getName());
        certificate.getPrivateKey().setKey(privateKey);
        certificate.getPublicKey().setKey(publicKey);
        certificate.setRequest(null);
        return certificate;
    }

}
