package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.apache.commons.lang.RandomStringUtils;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.junit.Test;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Locale;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.*;

public abstract class EpsosCertificateTest_EPSOS_ABSTRACT_V4 extends PKITestEnvironment {

    abstract CertificateType getCertificateType();

    @Test
    public void testCreateValidateUpdate() throws CertificateException {
        //testCreateValidateUpdateCertificate(getCertificateType());
    }

    @Test
    public void testEHEALTH_X509_3_1_Signature() throws CertificateException {
        // FIXME
    }

    @Test
    public void testEHEALTH_X509_3_1_SerialNumber() throws CertificateException {

        // Impossible to set with BC
        // testEHEALTH_X509_3_1_SerialNumber(BigInteger.valueOf(-1));

        byte[] bytes = new byte[24];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = 13;
        }

        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);
        Certificate certificate = createCertificateSerialNumber(certificateRequestWithGeneratedKeys, null, new BigInteger(bytes));
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "The serial number MUST be an unambiguous integer value with a maximum of 20 bytes.");
    }

    @Test
    public void testEHEALTH_X509_3_1_Validity() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);

        Calendar notAfter = Calendar.getInstance();
        notAfter.setTime(certificateRequestWithGeneratedKeys.getNotBefore());
        notAfter.add(Calendar.YEAR, 3);
        certificateRequestWithGeneratedKeys.setNotAfter(notAfter.getTime());
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "Certificates used by epSOS services SHOULD be valid for a maximum of 2 year.");
    }

    @Test
    public void testEHEALTH_X509_3_1_IssuerUniqueID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        boolean[] uniqueID = new boolean[1];
                        uniqueID[0] = true;
                        certGen.setIssuerUniqueID(uniqueID);
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The field \"IssuerUniqueID\" MUST NOT be used.");
    }

    @Test
    public void testEHEALTH_X509_3_1_SubjectUniqueID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        boolean[] uniqueID = new boolean[1];
                        uniqueID[0] = true;
                        certGen.setSubjectUniqueID(uniqueID);
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The field \"SubjectUniqueID\" MUST NOT be used.");
    }

    private void testEHEALTH_X509_3_1_Subject(String subject, boolean error, String errorString) throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);
        certificateRequestWithGeneratedKeys.setSubject(subject);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, error, errorString);
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_C() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", true, "The DName MUST have C.");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=ABC", true, "C MUST be limited by");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=A", true, "C MUST be limited by");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=EU", true, "C MUST be a ISO 3166 code");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_O() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", true, "The DName MUST have O.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,O=" + longValue, true, "O MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_CN() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("C=FR", true, "The DName MUST have CN.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,CN=" + longValue, true, "CN MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_OU() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", false, "The DName MAY have OU.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,OU=" + longValue, true, "OU MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_E() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais,E=test@test.com", false, "E SHOULD NOT be provided");
    }

    @Test
    public void testEHEALTH_X509_3_1_Issuer() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);

        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais2", null,
                null, null, "IRISA", null);
        Certificate certificate = createCertificateBadIssuer(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        // System.out.println(result);
        assertContainsError(result, true, "Issuer of certificate is not valid.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, true,
                                    new AuthorityKeyIdentifierStructure(parameters.getCertificateRequest()
                                            .getCertificateAuthority().getPublicKey().getKey()));
                        } catch (InvalidKeyException e) {
                            throw new CertificateException(e);
                        }
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST always be designated as non-critical in the certificate");
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_KeyUsage_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, false, new X509KeyUsage(KeyUsage.keyEncipherment));
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "KeyUsage MUST always be designated as critical in the certificate");
        assertContainsError(result, true, "KeyUsage MUST be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_KeyUsage_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "KeyUsage MUST be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_CASubjectKeyIdentifier() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(
                                    KeyAlgorithm.RSA, 1024);
                            certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, true,
                                    new AuthorityKeyIdentifierStructure(certificateRequestWithGeneratedKeys.getPublicKey()));
                        } catch (InvalidKeyException e) {
                            throw new CertificateException(e);
                        }
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_AuthorityKeyIdentifierParameters() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        X509Certificate x509Certificate = parameters.getCertificateRequest().getCertificateAuthority()
                                .getCertificateX509().getX509Certificate();
                        AuthorityKeyIdentifierStructure value = new AuthorityKeyIdentifierStructure(x509Certificate);
                        certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, value);
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false, "AuthorityCertIssuer SHOULD NOT be used in AuthorityKeyIdentifier.");
        assertContainsError(result, false, "AuthorityCertSerialNumber SHOULD NOT be used in AuthorityKeyIdentifier.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.SubjectKeyIdentifier, true,
                                new SubjectKeyIdentifierStructure(parameters.getPublicKey()));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST always be designated as non-critical in the certificate");
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_RFC5280_4_2_1_2() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(
                                KeyAlgorithm.RSA, 1024);
                        certGen.addExtension(X509Extensions.SubjectKeyIdentifier, true,
                                new SubjectKeyIdentifierStructure(certificateRequestWithGeneratedKeys.getPublicKey()));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.");
    }

    @Test
    public void testEHEALTH_X509_3_2_IssuerAltNames_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        GeneralName genName = new GeneralName(GeneralName.directoryName, "CN=toto");
                        GeneralNames generalNames = new GeneralNames(genName);
                        certGen.addExtension(X509Extensions.IssuerAlternativeName, true, generalNames);
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "IssuerAlternativeName MUST always be designated as non-critical in the certificate");

    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectAltNames_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        GeneralName genName = new GeneralName(GeneralName.directoryName, "CN=toto");
                        GeneralNames generalNames = new GeneralNames(genName);
                        certGen.addExtension(X509Extensions.SubjectAlternativeName, true, generalNames);
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectAlternativeName MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectAltNames_uri() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        GeneralName genName = new GeneralName(GeneralName.directoryName, "CN=toto");
                        GeneralNames generalNames = new GeneralNames(genName);
                        certGen.addExtension(X509Extensions.SubjectAlternativeName, true, generalNames);
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "If [SubjectAltNames] is used, a corresponding LDAP-, HTTP- or FTP-URL SHOULD be provided.");
        assertContainsError(result, false,
                "If [SubjectAltNames] is used, E-Mail addresses (RFC822-name) [RFC 822] MAY also be made available.");
    }

    @Test
    public void testEHEALTH_X509_3_2_BasicConstraints_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "BasicConstraints MUST be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_BasicConstraints_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.BasicConstraints, false, new BasicConstraints(false));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "BasicConstraints MUST be included as a critical extension in the certificate.");
        assertContainsError(result, true, "BasicConstraints MUST always be designated as critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CRLDistributionPoints SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String crlUrl = "http://server/crl/1/cacrl.crl";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier,
                                new DERIA5String(crlUrl));
                        GeneralNames gns = new GeneralNames(new DERSequence(gn));
                        DistributionPointName dpn = new DistributionPointName(0, gns);
                        DistributionPoint distp = new DistributionPoint(dpn, null, null);
                        certGen.addExtension(X509Extensions.CRLDistributionPoints, true, new DERSequence(distp));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CRLDistributionPoints SHOULD be included as a non-critical extension in the certificate.");
        assertContainsError(result, true,
                "CRLDistributionPoints MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_noHTTP() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String crlUrl = "http://toto/crl/1/cacrl.crl";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier,
                                new DERIA5String(crlUrl));
                        GeneralNames gns = new GeneralNames(new DERSequence(gn));
                        DistributionPointName dpn = new DistributionPointName(0, gns);
                        DistributionPoint distp = new DistributionPoint(dpn, null, null);
                        certGen.addExtension(X509Extensions.CRLDistributionPoints, false, new DERSequence(distp));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false, "CRLDistributionPoints SHOULD include the HTTP address from "
                + "which the certificate-issuing authority’s complete revocation list can be retrieved.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        PolicyQualifierInfo policyQualifierInfo = new PolicyQualifierInfo("www.test.com/policy.dpc");
                        PolicyInformation pi = new PolicyInformation(new DERObjectIdentifier("2.22.22.2.2.2"),
                                new DERSequence(policyQualifierInfo));
                        certGen.addExtension(X509Extensions.CertificatePolicies, true, new DERSequence(pi));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as a non-critical extension in the certificate.");
        assertContainsError(result, true,
                "CertificatePolicies MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_Policyinformation_OID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        PolicyInformation pi = new PolicyInformation(new DERObjectIdentifier("2.22.22.2.2.2"));
                        certGen.addExtension(X509Extensions.CertificatePolicies, true, new DERSequence(pi));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as an extension in the certificate to include the eHealth DSI certificate policy " +
                        "identifier: 1.3.130.0.2017.ARES_number_of_the_present_document");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "AuthorityInfoAccess SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String authorityInfoAccess = "http://server/pki/1/authorityInfoAccess";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.ocspAccessMethod, gn);
                        certGen.addExtension(X509Extensions.AuthorityInfoAccess, true, new DERSequence(
                                authorityInformationAccess));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, false,
                "AuthorityInfoAccess SHOULD be included as a non-critical extension in the certificate.");
        assertContainsError(result, true,
                "AuthorityInfoAccess MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OCSP_CA_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String authorityInfoAccess = "http://server/pki/1/authorityInfoAccess";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.ocspAccessMethod, gn);
                        certGen.addExtension(X509Extensions.AuthorityInfoAccess, false, new DERSequence(
                                authorityInformationAccess));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertNotContainsError(result, true,
                "When the issuing CA offers an OCSP service, its HTTP URI MUST be included in the AuthorityInfoAccess extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OCSP_CA_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String authorityInfoAccess = "ftp://server/pki/2/authorityInfoAccess";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.ocspAccessMethod, gn);
                        certGen.addExtension(X509Extensions.AuthorityInfoAccess, false, new DERSequence(
                                authorityInformationAccess));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, true,
                "When the issuing CA offers an OCSP service, its HTTP URI MUST be included in the AuthorityInfoAccess extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_OK_AuthorityInfoAccess()
            throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String authorityInfoAccess = "http://server/pki/1/authorityInfoAccess";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.id_ad_ocsp, gn);
                        certGen.addExtension(X509Extensions.AuthorityInfoAccess, false, new DERSequence(
                                authorityInformationAccess));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertNotContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertNotContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_OK_CRLDistributionPoints()
            throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        String crlUrl = "http://server/crl/1/cacrl.crl";

                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier,
                                new DERIA5String(crlUrl));
                        GeneralNames gns = new GeneralNames(new DERSequence(gn));
                        DistributionPointName dpn = new DistributionPointName(0, gns);
                        DistributionPoint distp = new DistributionPoint(dpn, null, null);
                        certGen.addExtension(X509Extensions.CRLDistributionPoints, false, new DERSequence(distp));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertNotContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOCSP("http://server/pki/1/authorityInfoAccess");
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

}
