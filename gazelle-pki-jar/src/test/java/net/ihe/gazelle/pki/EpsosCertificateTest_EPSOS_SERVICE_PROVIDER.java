package net.ihe.gazelle.pki;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.*;

import java.security.cert.CertificateException;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;

import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.junit.Test;

public class EpsosCertificateTest_EPSOS_SERVICE_PROVIDER extends EpsosCertificateTest_EPSOS_ABSTRACT {

    private static final CertificateType CERTIFICATE_TYPE = CertificateType.EPSOS_SERVICE_PROVIDER;

    @Override
    CertificateType getCertificateType() {
        return CERTIFICATE_TYPE;
    }

    @Test
    public void testWP342_5_4_6_Subject() throws CertificateException {
        // we do not check DNS
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(CERTIFICATE_TYPE,
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = CERTIFICATE_TYPE.getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false, "The CN (Common Name) MUST include the DNS server name; if it doesn’t, "
                + "the client’s default setting identifies the server certificate as untrustworthy.");
    }

    @Test
    public void testWP342_5_4_6_KeyUsage() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(CERTIFICATE_TYPE,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, true, new X509KeyUsage(KeyUsage.decipherOnly));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = CERTIFICATE_TYPE.getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "For SSL server certificates, only the keyEncipherment usage type MUST be specified.");
    }

    @Test
    public void testWP342_5_4_6_ExtendedKeyUsage_value() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(CERTIFICATE_TYPE,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
                                KeyPurposeId.id_kp_codeSigning));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = CERTIFICATE_TYPE.getValidator().validate(certificate.getChain(), true);
        assertContainsError(
                result,
                true,
                "If ExtendedKeyUsages extension is included in the certificate, it MUST only accept the value ServerAuth (OID 1.3.6.1.5.5.7.3.1).");
    }

}
