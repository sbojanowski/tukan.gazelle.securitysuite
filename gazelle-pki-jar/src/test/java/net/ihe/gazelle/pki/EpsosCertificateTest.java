package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import org.junit.Test;

import java.security.cert.CertificateException;

public class EpsosCertificateTest extends PKITestEnvironment {

    @Test
    public void testVPNClient() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_VPN_CLIENT);
    }

    @Test
    public void testVPNServer() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_VPN_SERVER);
    }

    @Test
    public void testServiceConsumer() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_SERVICE_CONSUMER);
    }

    @Test
    public void testServiceProvider() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_SERVICE_PROVIDER);
    }

    @Test
    public void testNCPSignature() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_NCP_SIGNATURE);
    }

    @Test
    public void testOCSPResponder() throws CertificateException {
        EpsosCertificateTestTools.testEpsos(CertificateType.EPSOS_OCSP_RESPONDER);
    }

}
