package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.testCreateValidateUpdateCertificate;
import static org.junit.Assert.assertTrue;

public class CertificateManagerTest extends PKITestEnvironment {

    @Test
    public void testWolfiPEM() throws CertificateException {
        validatePEM("wolfi.pem", CertificateType.EPSOS_NCP_SIGNATURE);
    }

    @Test
    public void testCreateValidateUpdateCertificateCA() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.CA_KEY_USAGE_ALL);
    }

    @Test
    public void testCreateValidateUpdateCertificateClientServer() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.CLIENT_AND_SERVER);
    }

    @Test
    public void testCreateValidateUpdateCertificateClient() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.HTTPS_CLIENT_AUTH);
    }

    @Test
    public void testCreateValidateUpdateCertificateServer() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.HTTPS_SERVER);
    }

    @Test
    public void testMassiCAdisig() throws CertificateException {
        validatePEM("ca_disig.pem", CertificateType.EPSOS_SERVICE_PROVIDER);
    }

    @Test
    public void testMassiSPNodeAuthenticity() throws CertificateException {
        validatePEM("SP_NodeAuthenticity.pem", CertificateType.EPSOS_SERVICE_PROVIDER);
    }

    private void validatePEM(String fileName, CertificateType type) throws CertificateException {
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream("/" + fileName);
            String pem = IOUtils.toString(resourceAsStream, null);
            List<X509Certificate> certificates = CertificateUtil.loadCertificates(pem);

            CertificateValidatorResult result = type.getValidator().validate(certificates, true);
            assertTrue(result.getResult().equals(CertificateValidatorResultEnum.FAILED));
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

}
