package net.ihe.gazelle.pki;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public abstract class CertificateExtenderAbstract implements CertificateExtender {

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        //
    }

}
