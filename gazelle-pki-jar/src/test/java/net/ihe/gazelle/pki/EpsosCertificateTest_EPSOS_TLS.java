package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.junit.Test;

import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.*;

public class EpsosCertificateTest_EPSOS_TLS extends EpsosCertificateTest_EPSOS_ABSTRACT_V4 {

    private static final CertificateType CERTIFICATE_TYPE = CertificateType.EPSOS_TLS;

    @Override
    CertificateType getCertificateType() {
        return CERTIFICATE_TYPE;
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_only_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, false, new X509KeyUsage(KeyUsage.keyEncipherment + KeyUsage.digitalSignature));
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_and_dataEncipherment_only_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, false, new X509KeyUsage(KeyUsage.keyEncipherment + KeyUsage.digitalSignature
                                + KeyUsage.dataEncipherment));
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_only_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, false, new X509KeyUsage(KeyUsage.keyEncipherment));
                    }

                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsage_id_kp_clientAuth_and_id_kp_serverAuth_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        List<KeyPurposeId> extendedKeyUsageList = new ArrayList<>();
                        extendedKeyUsageList.add(KeyPurposeId.id_kp_clientAuth);
                        extendedKeyUsageList.add(KeyPurposeId.id_kp_serverAuth);
                        certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(new Vector(extendedKeyUsageList)));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true,
                "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.");
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsage_id_kp_clientAuth_and_id_kp_serverAuth_KO() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(getCertificateType(),
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(KeyPurposeId.id_kp_serverAuth));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.");
    }
}
