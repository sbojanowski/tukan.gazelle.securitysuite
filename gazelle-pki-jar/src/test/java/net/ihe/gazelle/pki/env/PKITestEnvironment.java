package net.ihe.gazelle.pki.env;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.preferences.PreferenceService;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.security.Security;

/**
 * Created by cel on 22/07/15.
 */
@PowerMockIgnore({"javax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class })
@RunWith(PowerMockRunner.class)
public abstract class PKITestEnvironment extends AbstractTestQueryJunit4 {

    private static CrlServer crlServer;
    private static String BCproviderName;

    @Before
    public void mockPreferences(){
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getString("crl_url")).thenReturn(TestParam.CRL_URL);
        Mockito.when(PreferenceService.getString("java_cacerts_truststore_pwd")).thenReturn(TestParam.CACERTS_PASSWORD);
    }

    /*******
     * SETUP / TEARDOWN
     ******/

    @BeforeClass
    public static void classSetUp() throws Exception {
        System.out.println("setUp PKI Test dependencies");
        setUpBouncyCastle();
        setUpCrlServer();
    }

    @Before
    public void setUp() throws Exception {
        super.setUp(); //JPA setup
        System.out.println("Begin transaction");
        EntityManagerService.provideEntityManager().getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Commit transaction");
        EntityManagerService.provideEntityManager().getTransaction().commit();
        super.tearDown(); //JPA teardown
    }

    @AfterClass
    public static void classTearDown() throws Exception {
        System.out.println("tearDown PKI Test dependencies");
        tearDownCrlServer();
        tearDownBouncyCastle();
    }

    /******
     * GAZELLE JUNIT SETUP
     ******/

    @Override
    protected String getDb() {
        return "tls-unit-test";
    }

    @Override
    protected String getHbm2ddl() {
        return "update";
    }

    protected boolean getShowSql() {
        return true;
    }

    /******
     * BOUNCYCASTLE
     ******/

    protected static void setUpBouncyCastle() {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        BCproviderName = provider.getName();
        Security.addProvider(provider);
    }

    protected static void tearDownBouncyCastle() {
        Security.removeProvider(BCproviderName);
    }

    /******
     * CRL SERVER
     *****/

    protected static void setUpCrlServer() {
        crlServer = new CrlServer(TestParam.CRL_PORT, false);
        crlServer.start();
    }

    protected static void tearDownCrlServer() {
        crlServer.stop();
    }

}
