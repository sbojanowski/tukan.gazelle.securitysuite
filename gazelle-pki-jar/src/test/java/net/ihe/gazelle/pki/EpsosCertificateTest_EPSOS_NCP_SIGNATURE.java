package net.ihe.gazelle.pki;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.*;

import java.security.cert.CertificateException;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;

import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.junit.Test;

public class EpsosCertificateTest_EPSOS_NCP_SIGNATURE extends EpsosCertificateTest_EPSOS_ABSTRACT {

    private static final CertificateType CERTIFICATE_TYPE = CertificateType.EPSOS_NCP_SIGNATURE;

    @Override
    CertificateType getCertificateType() {
        return CERTIFICATE_TYPE;
    }

    @Test
    public void testWP342_5_4_7_KeyUsage() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(CERTIFICATE_TYPE,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.KeyUsage, true, new X509KeyUsage(KeyUsage.decipherOnly));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = CERTIFICATE_TYPE.getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "Only the digitalSignature AND nonRepudiation usage type MUST be specified.");
    }

    @Test
    public void testWP342_5_4_7_ExtendedKeyUsage_value() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignHack(CERTIFICATE_TYPE,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
                                KeyPurposeId.id_kp_codeSigning));
                    }
                });
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = CERTIFICATE_TYPE.getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.");
    }

    @Test
    @Override
    public void testWP342_5_4_2_ExtendedKeyUsage_critical() {
        // Override this method to skip the test
        // This is not applicable for NCP Signature
    }

    @Test
    @Override
    public void testWP342_5_4_2_ExtendedKeyUsage_none() {
        // Override this method to skip the test
        // This is not applicable for NCP Signature
    }
}
