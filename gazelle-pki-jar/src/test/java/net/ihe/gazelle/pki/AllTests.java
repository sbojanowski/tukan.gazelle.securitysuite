package net.ihe.gazelle.pki;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({

        CertificateManagerTest.class,

        EpsosCertificateTest.class,

        EpsosCertificateTest_EPSOS_VPN_CLIENT.class,

        EpsosCertificateTest_EPSOS_VPN_SERVER.class,

        EpsosCertificateTest_EPSOS_SERVICE_CONSUMER.class,

        EpsosCertificateTest_EPSOS_SERVICE_PROVIDER.class,

        EpsosCertificateTest_EPSOS_NCP_SIGNATURE.class,

        EpsosCertificateTest_EPSOS_OCSP_RESPONDER.class,

        EpsosCertificateTest_5_4_9.class

})
public class AllTests {



}
