package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.extension.CertificateExtenderCA;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EpsosCertificateTestTools {

    static CertificateRequest getCertificateRequest(CertificateType type, Certificate certificateAuthority)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(type);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);

        return certificateRequestWithGeneratedKeys;
    }

    static CertificateRequestAuthority getCertificateRequestAuthority() throws CertificateException {

        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setIssuerUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);

        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());
        return cra;
    }

    static void testCreateValidateUpdateCertificate(CertificateType certificateType) throws CertificateException {
        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority);

        CertificateValidatorResult result = CertificateType.CA_KEY_USAGE_ALL.getValidator().validate(
                certificateAuthority.getChain(), true);

        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));

        CertificateRequest certificateRequest = getCertificateRequest(certificateType, certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        result = certificateType.getValidator().validate(certificate.getChain(), true);
        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));
        CertificateManager.updateCertificate(certificateRequest, null);
        result = certificateType.getValidator().validate(certificate.getChain(), true);
        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));
    }

    static void testEpsos(CertificateType certificateType) throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        Certificate certificate = getCertificateOK(certificateType, certificateAuthority);
        CertificateValidatorResult result = certificateType.getValidator().validate(certificate.getChain(), true);
        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));
    }

    static Certificate getCertificateOK(CertificateType certificateType, Certificate certificateAuthority)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(certificateType,
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        return certificate;
    }

    static CertificateRequestWithGeneratedKeys getCertificateRequestAutoSignOK(CertificateType certificateType,
                                                                               Certificate certificateAuthority) throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(certificateType);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);
        return certificateRequestWithGeneratedKeys;
    }

    static CertificateRequestWithGeneratedKeys getCertificateRequestAutoSignHack(CertificateType certificateType,
                                                                                 Certificate certificateAuthority, final CertificateExtender hackedCertificateExtender)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024) {
            @Override
            public CertificateExtender getCertificateExtender() {
                return hackedCertificateExtender;
            }
        };
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(certificateType);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);
        return certificateRequestWithGeneratedKeys;
    }

    static Certificate getCertificateAuthorityOK() throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setIssuerUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(cra, null);
        CrlUtil.addTestCertificate(certificateAuthority);
        return certificateAuthority;
    }

    static Certificate getCertificateAuthorityOCSP(final String authorityInfoAccess) throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024) {
            @Override
            public CertificateExtender getCertificateExtender() {
                CertificateExtender certificateExtender = new CertificateExtenderCA() {
                    @Override
                    public void addExtension(X509V3CertificateGenerator certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        super.addExtension(certGen, parameters);
                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.ocspAccessMethod, gn);
                        certGen.addExtension(X509Extensions.AuthorityInfoAccess, false, new DERSequence(
                                authorityInformationAccess));
                    }
                };
                return certificateExtender;
            }
        };
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setIssuerUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(cra, null);
        CrlUtil.addTestCertificate(certificateAuthority);
        return certificateAuthority;
    }

    public static Certificate createCertificateBadIssuer(CertificateRequest certificateRequest,
                                                         EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = certificateRequest.getCertificateAuthority();

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate() == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(CertificateManager.testCertificateId++);
        }

        int serialNumber = certificateAuthority.getNextSerialNumber();
        X509Certificate cert = createX509CertificateBadIssuer(certificateRequest, publicKey, certificateAuthority,
                BigInteger.valueOf(serialNumber), certificate);
        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            entityManager.merge(certificate);
            // cascade all !
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

    static X509Certificate createX509CertificateBadIssuer(CertificateRequest certificateRequest, PublicKey publicKey,
                                                          Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate)
            throws CertificateException {
        X509Certificate caCert = certificateAuthority.getCertificateX509().getX509Certificate();

        X509Principal issuerPrincipal;
        try {
            issuerPrincipal = PrincipalUtil.getSubjectX509Principal(caCert);
        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        }
        X509Principal subjectPrincipal = new X509Principal(certificateRequest.getSubject());

        // hacking the issuer principal
        String ipS = issuerPrincipal.toString();
        issuerPrincipal = new X509Principal(ipS + "a");

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerPrincipal, publicKey, serialNumber, subjectPrincipal, certificate.getId());
        X509V3CertificateGenerator certificateGenerator = CertificateManager.getCertificateGenerator(parameters);

        X509Certificate cert = CertificateManager.generateCertificate(certificateAuthority.getPrivateKey().getKey(),
                certificateGenerator, parameters);
        return cert;
    }

    static Certificate createCertificateV1(CertificateRequest certificateRequest, EntityManager entityManager)
            throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = certificateRequest.getCertificateAuthority();

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate() == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(CertificateManager.testCertificateId++);
        }

        int serialNumber = certificateAuthority.getNextSerialNumber();

        X509Principal issuerPrincipal = new X509Principal(certificateRequest.getSubject());
        X509Principal subjectPrincipal = new X509Principal(certificateRequest.getSubject());
        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerPrincipal, publicKey, BigInteger.valueOf(serialNumber), subjectPrincipal, certificate.getId());

        X509V1CertificateGenerator certificateGeneratorv1 = CertificateManager.getCertificateGeneratorV1(parameters);
        X509Certificate cert = CertificateManager.generateCertificateV1(privateKey, certificateGeneratorv1, parameters);

        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            entityManager.merge(certificate);
            // cascade all !
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

    static void assertContainsError(CertificateValidatorResult result, boolean error, String errorString) {
        if (error) {
            assertTrue(errorString, result.containsError(errorString));
        } else {
            assertTrue(errorString, result.containsWarning(errorString));
        }
    }

    static void assertNotContainsError(CertificateValidatorResult result, boolean error, String errorString) {
        if (error) {
            assertFalse(errorString, result.containsError(errorString));
        } else {
            assertFalse(errorString, result.containsWarning(errorString));
        }
    }

    static Certificate createCertificateSerialNumber(CertificateRequest certificateRequest,
                                                     EntityManager entityManager, BigInteger serialNumber) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = certificateRequest.getCertificateAuthority();

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate() == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(CertificateManager.testCertificateId++);
        }

        //the given serial number is set to the X509 cert, but it cannot be taken into account in the CA serialNumber counter.
        X509Certificate cert = CertificateManager.createX509Certificate(certificateRequest, publicKey,
                certificateAuthority, serialNumber, certificate);
        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            entityManager.merge(certificate);
            // cascade all !
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

}
