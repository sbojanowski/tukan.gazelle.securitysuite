package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlDefaultProvider;
import net.ihe.gazelle.pki.crl.CrlProvider;
import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import static net.ihe.gazelle.pki.EpsosCertificateTestTools.*;

public class EpsosCertificateTest_5_4_9 extends PKITestEnvironment {

    @Test
    public void testWP342_5_4_9_1_Version() throws CertificateException {
        // FIXME Impossible to check using BC
    }

    @Test
    public void testWP342_5_4_9_1_SignatureAlgorithm() throws CertificateException {
        // TODO
    }

    @Test
    public void testWP342_5_4_9_1_Issuer() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                generator.setIssuerDN(new X500Principal("CN=toto"));
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());
                generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60));

                AuthorityKeyIdentifierStructure authorityKeyIdentifier;
                try {
                    authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(certificateAuthority.getPublicKey()
                            .getKey());
                } catch (InvalidKeyException e) {
                    throw new CertificateException(e);
                }
                generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

                CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
                generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);
                certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true, "The DName MUST be identical to the subject DName of the issuer certificate.");
    }

    private CertificateType getCertificateType() {
        return CertificateType.EPSOS_SERVICE_CONSUMER;
    }

    @Test
    public void testWP342_5_4_9_1_Validity() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
                generator.setIssuerDN(caCertificate.getSubjectX500Principal());
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());

                AuthorityKeyIdentifierStructure authorityKeyIdentifier;
                try {
                    authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(certificateAuthority.getPublicKey()
                            .getKey());
                } catch (InvalidKeyException e) {
                    throw new CertificateException(e);
                }
                generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

                CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
                generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);
                certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true, "The CRL’s period of validity (from/to) MUST be stated.");
    }

    @Test
    public void testWP342_5_4_9_2_AuthorityKeyIdentifier_none() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
                generator.setIssuerDN(caCertificate.getSubjectX500Principal());
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());
                generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60));

                CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
                generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);
                certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true, "The KeyIdentifier MUST be indicated.");
    }

    @Test
    public void testWP342_5_4_9_2_AuthorityKeyIdentifier_wrong() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
                generator.setIssuerDN(caCertificate.getSubjectX500Principal());
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());
                generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60));

                CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
                generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);
                certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

                CertificateRequestWithGeneratedKeys cras = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024);
                AuthorityKeyIdentifierStructure authorityKeyIdentifier;
                try {
                    authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(cras.getPublicKey());
                } catch (InvalidKeyException e) {
                    throw new CertificateException(e);
                }
                generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.");
    }

    @Test
    public void testWP342_5_4_9_2_CRLNumber_none() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
                generator.setIssuerDN(caCertificate.getSubjectX500Principal());
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());
                generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60));

                AuthorityKeyIdentifierStructure authorityKeyIdentifier;
                try {
                    authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(certificateAuthority.getPublicKey()
                            .getKey());
                } catch (InvalidKeyException e) {
                    throw new CertificateException(e);
                }
                generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true, "The consecutive number for the CRL issued MUST be indicated.");
    }

    @Test
    public void testWP342_5_4_9_2_CRLNumber_big() throws CertificateException {
        CrlProvider crlProvider = new CrlProvider() {

            @Override
            public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
                X509V2CRLGenerator generator = new X509V2CRLGenerator();

                X509Certificate caCertificate = certificateAuthority.getCertificateX509().getX509Certificate();
                generator.setIssuerDN(caCertificate.getSubjectX500Principal());
                generator.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName());
                generator.setThisUpdate(new Date());
                generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60));

                AuthorityKeyIdentifierStructure authorityKeyIdentifier;
                try {
                    authorityKeyIdentifier = new AuthorityKeyIdentifierStructure(certificateAuthority.getPublicKey()
                            .getKey());
                } catch (InvalidKeyException e) {
                    throw new CertificateException(e);
                }
                generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, authorityKeyIdentifier);

                byte[] bytes = new byte[24];
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = 13;
                }
                BigInteger bigInt = new BigInteger(bytes);
                CRLNumber crlNumber = new CRLNumber(bigInt);
                generator.addExtension(X509Extensions.CRLNumber, false, crlNumber);

                return CrlDefaultProvider.generateCRLWithOneException(certificateAuthority, generator);
            }
        };

        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority, crlProvider);

        CertificateRequest certificateRequest = getCertificateRequest(getCertificateType(), certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);

        assertContainsError(result, true,
                "It MUST have a unique positive integer value with a maximum length of 20 bytes.");
    }

    @Test
    public void testWP342_5_4_9_2_IssuerAltNames() throws CertificateException {
        // TODO
    }
}
